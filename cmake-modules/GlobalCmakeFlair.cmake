if(DEFINED GLOBALCMAKEFLAIR_INCLUDED)
    return()
endif()

SET(GLOBALCMAKEFLAIR_INCLUDED true)

if(NOT DEFINED ENV{FLAIR_ROOT})
	message(FATAL_ERROR  "variable FLAIR_ROOT not defined")
endif()

include($ENV{FLAIR_ROOT}/flair-src/cmake-modules/ColoredMessage.cmake)

#check if we have a flair-hds directory
if (IS_DIRECTORY $ENV{FLAIR_ROOT}/flair-hds )
	info("found flair-hds directory")
	SET(FOUND_HDS_DIR TRUE)	
endif()

include($ENV{FLAIR_ROOT}/flair-src/cmake-modules/ArchDir.cmake)

list(APPEND CMAKE_MODULE_PATH $ENV{FLAIR_ROOT}/flair-src/cmake-modules/)

#default executable ouput paths
if(NOT DEFINED TARGET_EXECUTABLE_OUTPUT_PATH)
	SET(TARGET_EXECUTABLE_OUTPUT_PATH bin)
endif()

#add definitions for archs
if("${CMAKE_SYSTEM_PROCESSOR_DEFAULTTUNE}" MATCHES "armv7a-neon")	
    ADD_DEFINITIONS(-DARMV7A)
endif()

if("${CMAKE_SYSTEM_PROCESSOR_DEFAULTTUNE}" MATCHES "armv5te")	
    ADD_DEFINITIONS(-DARMV5TE)
endif()

if("${CMAKE_SYSTEM_PROCESSOR_DEFAULTTUNE}" MATCHES "core2-64")	
    ADD_DEFINITIONS(-DCORE2_64)
endif()


#function FLAIR_LIB: compile a flair lib, and:
#- add a message to display architecture
#- add compile info in a .h (used by lib constructor)
#- install lib and .h if INCLUDES_DEST_DIR is set
function(FLAIR_LIB PROJECT_NAME SRC_FILES)
    set(options "")#not used
    set(oneValueArgs INCLUDES_DEST_DIR)
    set(multiValueArgs "")#not used
    cmake_parse_arguments(PARSE_ARGV 2 LIB "${options}" "${oneValueArgs}" "${multiValueArgs}")

    if(DEFINED LIB_UNPARSED_ARGUMENTS)
        warn("FLAIR_LIB function: unparsed arguments (${LIB_UNPARSED_ARGUMENTS})")
    endif()

    execute_process(COMMAND ${CMAKE_C_COMPILER} -dumpversion
                OUTPUT_VARIABLE GCC_VERSION)

    if (GCC_VERSION VERSION_GREATER 4.3 OR GCC_VERSION VERSION_EQUAL 4.3)
	    if (GCC_VERSION VERSION_GREATER 4.7 OR GCC_VERSION VERSION_EQUAL 4.7)
		    ADD_DEFINITIONS("-std=c++11")
	    else()
		    ADD_DEFINITIONS("-std=c++0x")
	    endif()
    else()
	    message(STATUS "GCC version < 4.3, c+11 is not supported!")
    endif()

    if("${CMAKE_SYSTEM_PROCESSOR_DEFAULTTUNE}" MATCHES "core2-64")	
        ADD_DEFINITIONS("-fPIE")
    endif()

    ADD_LIBRARY(${PROJECT_NAME}
	    ${SRC_FILES}
    )
    
    add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD
            COMMENT "${PROJECT_NAME} built for ${CMAKE_SYSTEM_PROCESSOR_DEFAULTTUNE} architecture")

    add_custom_target(
	    ${PROJECT_NAME}_compile_info
    	COMMAND $ENV{FLAIR_ROOT}/flair-src/scripts/compile_info.sh ${CMAKE_C_COMPILER} ${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_BINARY_DIR}/generated/${PROJECT_NAME}/compile_info.h
    )
    add_dependencies(${PROJECT_NAME} ${PROJECT_NAME}_compile_info)
    
    INCLUDE_DIRECTORIES(
        ${CMAKE_BINARY_DIR}/generated/${PROJECT_NAME}
    )

    INSTALL(TARGETS ${PROJECT_NAME}
    	LIBRARY DESTINATION $ENV{FLAIR_ROOT}/flair-install/lib/${ARCH_DIR}
    	ARCHIVE DESTINATION $ENV{FLAIR_ROOT}/flair-install/lib/${ARCH_DIR}
    )

    #install .h if needed
    if(DEFINED LIB_INCLUDES_DEST_DIR)
        FILE(GLOB files "${CMAKE_CURRENT_SOURCE_DIR}/src/*.h")
        INSTALL(FILES ${files} DESTINATION $ENV{FLAIR_ROOT}/flair-install/include/${LIB_INCLUDES_DEST_DIR})
    endif()

endfunction()


function(FLAIR_DEMO PROJECT_NAME SRC_FILES)
    set(options COPY_RESOURCES)
    set(oneValueArgs DEST_DIR)
    set(multiValueArgs "")#not used
    cmake_parse_arguments(PARSE_ARGV 2 DEMO "${options}" "${oneValueArgs}" "${multiValueArgs}")

    if(DEFINED DEMO_UNPARSED_ARGUMENTS)
        warn("FLAIR_DEMO function: unparsed arguments (${DEMO_UNPARSED_ARGUMENTS})")
    endif()

    #get ${FLAIR_INCLUDE_DIR}, ${FLAIR_LIBRARIES_RT} and ${FLAIR_LIBRARIES_NRT}
    include($ENV{FLAIR_ROOT}/flair-src/cmake-modules/FlairUseFile.cmake)

    #add flair include dirs
    INCLUDE_DIRECTORIES(
        ${FLAIR_INCLUDE_DIR}
    )

    #real time executable
    ADD_EXECUTABLE(${PROJECT_NAME}_rt
	    ${SRC_FILES}
    )
    TARGET_LINK_LIBRARIES(${PROJECT_NAME}_rt ${FLAIR_LIBRARIES_RT})

    #non real time executable
    ADD_EXECUTABLE(${PROJECT_NAME}_nrt
	    ${SRC_FILES}
    )
    TARGET_LINK_LIBRARIES(${PROJECT_NAME}_nrt ${FLAIR_LIBRARIES_NRT})

    #install demo if needed
    if(DEFINED DEMO_DEST_DIR)
        INSTALL(
        	TARGETS ${PROJECT_NAME}_rt ${PROJECT_NAME}_nrt
        	RUNTIME DESTINATION $ENV{FLAIR_ROOT}/flair-install/bin/demos/${ARCH_DIR}/${DEMO_DEST_DIR}
        )
    endif()

    #copy resources if needed
    if(${DEMO_COPY_RESOURCES})
        FILE(GLOB RESOURCE_FILES "${CMAKE_CURRENT_SOURCE_DIR}/resources/${ARCH_DIR}/*")
        foreach(item IN LISTS RESOURCE_FILES)
            get_filename_component(filename ${item} NAME)
            #do not overwrite: user can change xml or scripts 
            INSTALL(CODE "
                if (NOT EXISTS \"$ENV{FLAIR_ROOT}/flair-install/bin/demos/${ARCH_DIR}/${DEMO_DEST_DIR}/${filename}\")
                    file(INSTALL \"${item}\" DESTINATION \"$ENV{FLAIR_ROOT}/flair-install/bin/demos/${ARCH_DIR}/${DEMO_DEST_DIR}\" USE_SOURCE_PERMISSIONS)
                else()
                    message(\"-- Not installing \" $ENV{FLAIR_ROOT} \"/flair-install/bin/demos/\" ${ARCH_DIR} \"/\" ${DEMO_DEST_DIR} \"/\" ${filename} \" (file already exists)\")
                endif()
            ")
        endforeach()
        add_custom_target(
	        ${PROJECT_NAME}_overwrite_all_resources_in_install_dir
	        COMMAND cp -a ${CMAKE_CURRENT_SOURCE_DIR}/resources/${ARCH_DIR}/* $ENV{FLAIR_ROOT}/flair-install/bin/demos/${ARCH_DIR}/${DEMO_DEST_DIR}/
	    )
        add_custom_target(
	        ${PROJECT_NAME}_overwrite_sh_resources_in_install_dir
	        COMMAND cp -a ${CMAKE_CURRENT_SOURCE_DIR}/resources/${ARCH_DIR}/*.sh $ENV{FLAIR_ROOT}/flair-install/bin/demos/${ARCH_DIR}/${DEMO_DEST_DIR}/
	    )
        add_custom_target(
	        ${PROJECT_NAME}_overwrite_xml_resources_in_install_dir
	        COMMAND cp -a ${CMAKE_CURRENT_SOURCE_DIR}/resources/${ARCH_DIR}/*.xml $ENV{FLAIR_ROOT}/flair-install/bin/demos/${ARCH_DIR}/${DEMO_DEST_DIR}/
	    )
    endif()

endfunction()

#create a demo using host cxx and libs
#usefull for simulator app when there are graphical lib incompatibilities (eg log2@glibc_2.29)
function(FLAIR_DEMO_HOST_CXX PROJECT_NAME SRC_FILES)
    if("${CMAKE_SYSTEM_PROCESSOR}" MATCHES "i686" OR "${CMAKE_SYSTEM_PROCESSOR}" MATCHES "i586" OR "${CMAKE_SYSTEM_PROCESSOR}" MATCHES "x86_64")

        set(CMAKE_CXX_FLAGS "" PARENT_SCOPE)
        set(CMAKE_CXX_COMPILER "g++" PARENT_SCOPE)
        set(CMAKE_CXX_LINK_FLAGS "-Wl,-unresolved-symbols=ignore-in-shared-libs -Wl,-rpath=$ENV{FLAIR_ROOT}/flair-install/lib/${ARCH_DIR}/ -Wl,--disable-new-dtags" PARENT_SCOPE)
        ADD_DEFINITIONS("-D_GLIBCXX_USE_CXX11_ABI=0")

        set(LIB_LIST libudt libvrpn libFileLib libIrrlicht libquat)

        foreach(LIB_NAME IN LISTS LIB_LIST) #all libs
            FILE(GLOB LIB_FILES "${CMAKE_SYSROOT}/usr/lib/${LIB_NAME}*")
            foreach(item IN LISTS LIB_FILES) #all files and symlinks of a lib
                get_filename_component(filename ${item} NAME)              
                INSTALL(CODE "
                        if (NOT EXISTS \"$ENV{FLAIR_ROOT}/flair-install/lib/${ARCH_DIR}/${filename}\")
                            file(INSTALL \"${item}\" DESTINATION \"$ENV{FLAIR_ROOT}/flair-install/lib/${ARCH_DIR}\" USE_SOURCE_PERMISSIONS)
                        else()
                            message(\"-- Not installing \" $ENV{FLAIR_ROOT} \"/flair-install/lib/\" ${ARCH_DIR} \"/\" ${filename} \" (file already exists)\")
                        endif()
                    ")
                  
            endforeach()
        endforeach()
#reconstruct arguments because src_files is a list
#todo, use multiValueArgs for src_file; but it will brake all demos cmakelists...
        FLAIR_DEMO(${ARGV0} "${ARGV1}" ${ARGV2} ${ARGV3} ${ARGV4})
    else()
        warn("${PROJECT_NAME} will not be built for ${CMAKE_SYSTEM_PROCESSOR} architecture")
    endif()

endfunction()

function(FLAIR_NRT_TOOL PROJECT_NAME SRC_FILES)
    set(options COPY_RESOURCES)
    set(oneValueArgs "")#not used
    set(multiValueArgs "")#not used
    cmake_parse_arguments(PARSE_ARGV 2 TOOL "${options}" "${oneValueArgs}" "${multiValueArgs}")

    if(DEFINED TOOL_UNPARSED_ARGUMENTS)
        warn("FLAIR_NRT_TOOL function: unparsed arguments (${TOOL_UNPARSED_ARGUMENTS})")
    endif()

    #get ${FLAIR_INCLUDE_DIR}, ${FLAIR_LIBRARIES_RT} and ${FLAIR_LIBRARIES_NRT}
    include($ENV{FLAIR_ROOT}/flair-src/cmake-modules/FlairUseFile.cmake)

    #add flair include dirs
    INCLUDE_DIRECTORIES(
        ${FLAIR_INCLUDE_DIR}
    )

    #real time executable
    ADD_EXECUTABLE(${PROJECT_NAME}
	    ${SRC_FILES}
    )
    TARGET_LINK_LIBRARIES(${PROJECT_NAME} ${FLAIR_LIBRARIES_NRT})
   
    INSTALL(
    	TARGETS ${PROJECT_NAME}
    	RUNTIME DESTINATION $ENV{FLAIR_ROOT}/flair-install/bin/tools/${ARCH_DIR}/
    )

    #copy resources if needed
    if(${TOOL_COPY_RESOURCES})
        FILE(GLOB RESOURCE_FILES "${CMAKE_CURRENT_SOURCE_DIR}/resources/${ARCH_DIR}/*")
        foreach(item IN LISTS RESOURCE_FILES)
            get_filename_component(filename ${item} NAME)
            #do not overwrite: user can change xml or scripts 
            INSTALL(CODE "
                if (NOT EXISTS \"$ENV{FLAIR_ROOT}/flair-install/bin/tools/${ARCH_DIR}/${filename}\")
                    file(INSTALL \"${item}\" DESTINATION \"$ENV{FLAIR_ROOT}/flair-install/bin/tools/${ARCH_DIR}\" USE_SOURCE_PERMISSIONS)
                else()
                    message(\"-- Not installing \" $ENV{FLAIR_ROOT} \"/flair-install/bin/tools/\" ${ARCH_DIR} \"/\" ${filename} \" (file already exists)\")
                endif()
            ")
        endforeach()
    endif()

endfunction()

#reimplement add executable:
#- add a custom target for delivery (only on ARM), delivery are read from ssh config file
#- add a message to display architecture
function(ADD_EXECUTABLE)
	if("${CMAKE_SYSTEM_PROCESSOR}" MATCHES "arm" AND EXISTS "$ENV{HOME}/.ssh/config")
		file(STRINGS $ENV{HOME}/.ssh/config TEST)
		foreach(f ${TEST})
			string(FIND ${f} "Host " POS)#cherche ligne host
			if(${POS} GREATER -1)
				string(REPLACE Host "" TARGET_NAME ${f})#enleve Host
				string(STRIP ${TARGET_NAME} TARGET_NAME)#enleve les espaces
			endif()
			string(FIND ${f} HostName POS)#cherche hostname
			if(${POS} GREATER -1)
				string(FIND ${f} "192.168." POS)#cherche addresse
				if(${POS} GREATER 0)#garde que les adresses en 192.168.x.x
					string(REPLACE HostName "" ADDRESS ${f})#enleve Hostname
					string(STRIP ${ADDRESS} ADDRESS)#enleve les espaces
					#message("adding delivery target for " ${ARGV0} " (" ${ADDRESS} ")")
					string(REPLACE "/" "_" TARGET_PATH ${TARGET_EXECUTABLE_OUTPUT_PATH})#les / ne sont pas acceptés
					add_custom_target(
					    delivery_root_${ADDRESS}_${TARGET_PATH}_${ARGV0}
					    COMMAND make
					    COMMAND scp ${EXECUTABLE_OUTPUT_PATH}/${ARGV0} root@${ADDRESS}:${TARGET_EXECUTABLE_OUTPUT_PATH}
					)
				endif()
			endif()
		endforeach(f) 
	endif()

    #call original function
	_ADD_EXECUTABLE(${ARGV})

    #check if it is qt4 internal target
    string(SUBSTRING ${ARGV0} 0 5 TESTNAME)
    if(NOT ${TESTNAME} MATCHES "Qt4::")
        #add a message to display architecture
        add_custom_command(TARGET ${ARGV0} POST_BUILD
            COMMENT "${ARGV0} built for ${CMAKE_SYSTEM_PROCESSOR_DEFAULTTUNE} architecture")
    endif()

    #add a dependance to FlairLibs and FlairHdsLibs if we use the top level CMakeLists.txt and we need flairlibs
    if(TARGET FlairLibs AND DEFINED FLAIRUSEFILE_INCLUDED)
        add_dependencies(${ARGV0} FlairLibs)
    endif()
    if(TARGET FlairHdsLibs AND DEFINED FLAIRUSEFILE_INCLUDED)
        add_dependencies(${ARGV0} FlairHdsLibs)
    endif()

endfunction()
