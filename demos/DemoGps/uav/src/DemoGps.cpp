//  created:    2016/07/01
//  filename:   DemoGps.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    demo GPS
//
//
/*********************************************************************/

#include "DemoGps.h"
#include <TargetController.h>
#include <Uav.h>
#include <GridLayout.h>
#include <PushButton.h>
#include <DoubleSpinBox.h>
#include <DataPlot1D.h>
#include <DataPlot2D.h>
#include <FrameworkManager.h>
#include <TrajectoryGenerator2DCircle.h>
#include <cmath>
#include <Tab.h>
#include <TabWidget.h>
#include <Pid.h>
#include <Ahrs.h>
#include <AhrsData.h>
#include <GpsData.h>
#include <Matrix.h>
#include <NmeaGps.h>

using namespace std;
using namespace flair::core;
using namespace flair::gui;
using namespace flair::sensor;
using namespace flair::filter;
using namespace flair::meta;

//in this demo earth frame is NED, origin of frame is taken when clicking on set ref on GPS tab
//origin will be the current UAV position

DemoGps::DemoGps(TargetController* controller)
    : UavStateMachine(controller)
    , behaviourMode(BehaviourMode_t::Default) {
  Uav* uav = GetUav();
  startCircle = new PushButton(GetButtonsLayout()->NewRow(), "start circle");
  stopCircle = new PushButton(GetButtonsLayout()->LastRowLastCol(), "stop circle");
  positionHold = new PushButton(GetButtonsLayout()->NewRow(), "position hold");
  gotoPosition = new PushButton(GetButtonsLayout()->NewRow(), "goto position");
  xGoto= new DoubleSpinBox(GetButtonsLayout()->LastRowLastCol(), "x", " m", -10, 10, 1, 1,0);
  yGoto= new DoubleSpinBox(GetButtonsLayout()->LastRowLastCol(), "y", " m", -10, 10, 1, 1,0);

  circle = new TrajectoryGenerator2DCircle(uav->GetGps()->GetLayout()->NewRow(), "circle");
 
  uX = new Pid(setupLawTab->At(1, 0), "u_x");
  uX->UseDefaultPlot(graphLawTab->NewRow());
  uY = new Pid(setupLawTab->At(1, 1), "u_y");
  uY->UseDefaultPlot(graphLawTab->LastRowLastCol());
  
  //matrix holding earth frame position and reference
  MatrixDescriptor *desc = new MatrixDescriptor(6, 2);
  desc->SetElementName(0, 0, "x");
  desc->SetElementName(0, 1, "ref x");
  desc->SetElementName(1, 0, "y");
  desc->SetElementName(1, 1, "ref y");
  desc->SetElementName(2, 0, "z");
  desc->SetElementName(2, 1, "ref z");
  desc->SetElementName(3, 0, "vx");
  desc->SetElementName(3, 1, "ref vx");
  desc->SetElementName(4, 0, "vy");
  desc->SetElementName(4, 1, "ref vy");
  desc->SetElementName(5, 0, "vz");
  desc->SetElementName(5, 1, "ref vz");
  NEDMatrix = new Matrix(this, desc, floatType,"NED");
  
  Tab* NEDTab = new Tab(getFrameworkManager()->GetTabWidget(), "NED");
  TabWidget* NEDTabWidget = new TabWidget(NEDTab->NewRow(), "NED");
  Tab* plot1DTab = new Tab(NEDTabWidget, "1D");
  DataPlot1D* xPlot = new DataPlot1D(plot1DTab->NewRow(), "x", -10, 10);
  xPlot->AddCurve(NEDMatrix->Element(0,0));
  xPlot->AddCurve(NEDMatrix->Element(0,1),DataPlot::Blue);
  DataPlot1D* yPlot = new DataPlot1D(plot1DTab->LastRowLastCol(), "y", -10, 10);
  yPlot->AddCurve(NEDMatrix->Element(1,0));
  yPlot->AddCurve(NEDMatrix->Element(1,1),DataPlot::Blue);
  DataPlot1D* zPlot = new DataPlot1D(plot1DTab->LastRowLastCol(), "z", -10, 0);
  zPlot->AddCurve(NEDMatrix->Element(2,0));
  zPlot->AddCurve(NEDMatrix->Element(2,1),DataPlot::Blue);
  DataPlot1D* vxPlot = new DataPlot1D(plot1DTab->NewRow(), "vx", -10, 10);
  vxPlot->AddCurve(NEDMatrix->Element(3,0));
  vxPlot->AddCurve(NEDMatrix->Element(3,1),DataPlot::Blue);
  DataPlot1D* vyPlot = new DataPlot1D(plot1DTab->LastRowLastCol(), "vy", -10, 10);
  vyPlot->AddCurve(NEDMatrix->Element(4,0));
  vyPlot->AddCurve(NEDMatrix->Element(4,1),DataPlot::Blue);
  DataPlot1D* vzPlot = new DataPlot1D(plot1DTab->LastRowLastCol(), "vz", -10, 0);
  vzPlot->AddCurve(NEDMatrix->Element(5,0));
  vzPlot->AddCurve(NEDMatrix->Element(5,1),DataPlot::Blue);
  Tab* plot2DTab = new Tab(NEDTabWidget, "2D");
  DataPlot2D* xy_plot = new DataPlot2D(plot2DTab->NewRow(), "xy", "y", -5, 5, "x", -5, 5);
  xy_plot->AddCurve(NEDMatrix->Element(1, 0), NEDMatrix->Element(0, 0));
  xy_plot->AddCurve(NEDMatrix->Element(1, 1), NEDMatrix->Element(0, 1),DataPlot::Blue);

  customReferenceOrientation = new AhrsData(this, "reference");
  uav->GetAhrs()->AddPlot(customReferenceOrientation, DataPlot::Yellow);
  AddDataToControlLawLog(customReferenceOrientation);

  customOrientation = new AhrsData(this, "orientation");
}

DemoGps::~DemoGps() {
}

AhrsData* DemoGps::GetReferenceOrientation(void) {
  Vector2Df pos_err, vel_err; // in Uav coordinate system
  float yaw_ref;
  Euler refAngles;

  PositionValues(pos_err, vel_err, yaw_ref);

  refAngles.yaw = yaw_ref;

  uX->SetValues(pos_err.x, vel_err.x);
  uX->Update(GetTime());
  refAngles.pitch = uX->Output();

  uY->SetValues(pos_err.y, vel_err.y);
  uY->Update(GetTime());
  refAngles.roll = -uY->Output();

  customReferenceOrientation->SetQuaternionAndAngularRates(refAngles.ToQuaternion(), Vector3Df(0, 0, 0));

  return customReferenceOrientation;
}

void DemoGps::PositionValues(Vector2Df& pos_error, Vector2Df& vel_error, float& yaw_ref) {
  Vector2Df uav_2Dpos,uav_2Dvel;
 
  GetNED(uav_2Dpos,uav_2Dvel);

  if(behaviourMode == BehaviourMode_t::PositionHold || behaviourMode == BehaviourMode_t::GotoPosition) {
    pos_error = uav_2Dpos - posHold;
    vel_error = uav_2Dvel;
    yaw_ref = yawHold;
  } else if(behaviourMode == BehaviourMode_t::Circle){
    Vector2Df circle_pos, circle_vel;
    Vector2Df target_2Dpos;

    circle->GetCenter(target_2Dpos);

    // circle reference
    circle->Update(GetTime());
    circle->GetPosition(circle_pos);
    circle->GetSpeed(circle_vel);

    // error in earth frame
    pos_error = uav_2Dpos - circle_pos;
    vel_error = uav_2Dvel - circle_vel;
    yaw_ref = atan2(target_2Dpos.y - uav_2Dpos.y, target_2Dpos.x - uav_2Dpos.x);
  } else {
    Thread::Warn("unhandled mode!\n");
    EnterFailSafeMode();
  }

  // error in uav frame
  float currentYaw=GetCurrentQuaternion().ToEuler().yaw;
  pos_error.Rotate(-currentYaw);
  vel_error.Rotate(-currentYaw);
}

void DemoGps::SignalEvent(Event_t event) {
  UavStateMachine::SignalEvent(event);
  switch(event) {
  case Event_t::TakingOff:
    behaviourMode = BehaviourMode_t::Default;
    break;
  case Event_t::EnteringControlLoop:
  {
    Vector2Df position,velocity,circle_pos,circle_vel;
    GetNED(position,velocity);
    circle->GetPosition(circle_pos);
    circle->GetSpeed(circle_vel);
    NEDMatrix->GetMutex();
    NEDMatrix->SetValueNoMutex(0,0,position.x);
    NEDMatrix->SetValueNoMutex(1,0,position.y);
    NEDMatrix->SetValueNoMutex(2,0,0);
    NEDMatrix->SetValueNoMutex(0,1,circle_pos.x);
    NEDMatrix->SetValueNoMutex(1,1,circle_pos.y);
    NEDMatrix->SetValueNoMutex(2,1,0);
    NEDMatrix->SetValueNoMutex(3,0,velocity.x);
    NEDMatrix->SetValueNoMutex(4,0,velocity.y);
    NEDMatrix->SetValueNoMutex(5,0,0);
    NEDMatrix->SetValueNoMutex(3,1,circle_vel.x);
    NEDMatrix->SetValueNoMutex(4,1,circle_vel.y);
    NEDMatrix->SetValueNoMutex(5,1,0);
    NEDMatrix->ReleaseMutex();
    NEDMatrix->SetDataTime(GetTime());
    
    if((behaviourMode == BehaviourMode_t::Circle) && (!circle->IsRunning())) {
      PositionHold();
    }
    break;
  }
  case Event_t::EnteringFailSafeMode:
    behaviourMode = BehaviourMode_t::Default;
    break;
  }
}

void DemoGps::ExtraSecurityCheck(void) {
}

void DemoGps::ExtraCheckPushButton(void) {
  if(startCircle->Clicked()) {
    StartCircle();
  }
  if(stopCircle->Clicked()) {
    StopCircle();
  }
  if(positionHold->Clicked()) {
    PositionHold();
  }
  if(gotoPosition->Clicked()) {
    GotoPosition();
  }
}

void DemoGps::ExtraCheckJoystick(void) {
  // R1 and Circle
  if(GetTargetController()->ButtonClicked(4) && GetTargetController()->IsButtonPressed(9)) {
    StartCircle();
  }

  // R1 and Cross
  if(GetTargetController()->ButtonClicked(5) && GetTargetController()->IsButtonPressed(9)) {
    StopCircle();
  }
  
  //R1 and Square
  if(GetTargetController()->ButtonClicked(2) && GetTargetController()->IsButtonPressed(9)) {
    PositionHold();
  }
}

void DemoGps::StartCircle(void) {
  if(!GetUav()->GetGps()->IsReferenceTaken()) {
    Thread::Warn("DemoGps: you need to take reference first, to set frame origin\n");
    EnterFailSafeMode();
    return;
  }
  if( behaviourMode==BehaviourMode_t::Circle) {
    Thread::Warn("DemoGps: already in circle mode\n");
    return;
  }
  if(SetOrientationMode(OrientationMode_t::Custom)) {
    Thread::Info("DemoGps: start circle\n");
  } else {
    Thread::Warn("DemoGps: could not start circle, OrientationMode_t::Custom failed\n");
    return;
  }
  Vector2Df uav_2Dpos, uav_2Dvel,center_offset;

  GetNED(uav_2Dpos,uav_2Dvel);
  //set circle center in front of UAV
  center_offset.x=circle->GetRadius();//center in UAV frame
  center_offset.y=0;
  center_offset.Rotate(GetDefaultOrientation()->GetQuaternion().ToEuler().yaw);//center in earth frame
  circle->SetCenter(uav_2Dpos+center_offset);
  circle->StartTraj(uav_2Dpos);

  uX->Reset();
  uY->Reset();
  behaviourMode = BehaviourMode_t::Circle;
}

void DemoGps::StopCircle(void) {
  if( behaviourMode!=BehaviourMode_t::Circle) {
    Thread::Warn("DemoGps: not in circle mode\n");
    return;
  }
  circle->FinishTraj();
  // GetJoystick()->Rumble(0x70);
  Thread::Info("DemoGps: finishing circle\n");
}

void DemoGps::GotoPosition(void) {
  if(!GetUav()->GetGps()->IsReferenceTaken()) {
    Thread::Warn("DemoGps: you need to take reference first, to set frame origin\n");
    EnterFailSafeMode();
    return;
  }
  if(SetOrientationMode(OrientationMode_t::Custom)) {
    Thread::Info("DemoGps: goto position\n");
  } else {
    Thread::Warn("DemoGps: could not start GotoPosition, OrientationMode_t::Custom failed\n");
    return;
  }
  
  yawHold=GetDefaultOrientation()->GetQuaternion().ToEuler().yaw;
  posHold.x=xGoto->Value();
  posHold.y=yGoto->Value();
  
  uX->Reset();
  uY->Reset();
  behaviourMode = BehaviourMode_t::GotoPosition;
}

void DemoGps::PositionHold(void) {
  if(!GetUav()->GetGps()->IsReferenceTaken()) {
    Thread::Warn("DemoGps: you need to take reference first, to set frame origin\n");
    EnterFailSafeMode();
    return;
  }
  if(SetOrientationMode(OrientationMode_t::Custom)) {
    Thread::Info("DemoGps: holding position\n");
  } else {
    Thread::Warn("DemoGps: could not start PositionHold, OrientationMode_t::Custom failed\n");
    return;
  }
  
  Vector2Df uav_2Dvel;//not used
  GetNED(posHold,uav_2Dvel);
  yawHold=GetDefaultOrientation()->GetQuaternion().ToEuler().yaw;
  
  uX->Reset();
  uY->Reset();
  behaviourMode = BehaviourMode_t::PositionHold;
}

void DemoGps::GetNED(Vector2Df &position,Vector2Df &velocity) const{
  float east,north,up;//up not used
  float eastVelocity,northVelocity;
  
  GetUav()->GetGps()->GetDatas()->GetEnu(east,north,up);
  GetUav()->GetGps()->GetDatas()->GetVelocity(eastVelocity,northVelocity);
  //change enu to ned
  position.x=north;
  position.y=east;
  velocity.x=northVelocity;
  velocity.y=eastVelocity;
}