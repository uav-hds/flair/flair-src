//  created:    2016/07/01
//  filename:   DemoGps.h
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    demo GPS
//
//
/*********************************************************************/

#ifndef DEMOGPS_H
#define DEMOGPS_H

#include <UavStateMachine.h>

namespace flair {
    namespace gui {
        class PushButton;
        class DoubleSpinBox;
    }
    namespace filter {
        class TrajectoryGenerator2DCircle;
    }
    namespace sensor {
        class TargetController;
    }
    namespace core {
        class Matrix;
    }
}

class DemoGps : public flair::meta::UavStateMachine {
    public:
        DemoGps(flair::sensor::TargetController *controller);
        ~DemoGps();

    private:
        enum class BehaviourMode_t {
            Default,
            PositionHold,
            GotoPosition,
            Circle
        };

        BehaviourMode_t behaviourMode;

        void PositionHold(void);
        void GotoPosition(void);
        void StartCircle(void);
        void StopCircle(void);
        void ExtraSecurityCheck(void) override;
        void ExtraCheckPushButton(void) override;
        void ExtraCheckJoystick(void) override;
        void PositionValues(flair::core::Vector2Df &pos_error,flair::core::Vector2Df &vel_error,float &yaw_ref);
        flair::core::AhrsData *GetReferenceOrientation(void) override;
        void SignalEvent(Event_t event) override;
        void GetNED(flair::core::Vector2Df &position,flair::core::Vector2Df &velocity) const;
          
        flair::filter::Pid *uX, *uY;

        flair::core::Vector2Df posHold;
        flair::core::Matrix *NEDMatrix;
        float yawHold;

        flair::gui::PushButton *startCircle,*stopCircle,*positionHold,*gotoPosition;
        flair::gui::DoubleSpinBox *xGoto,*yGoto;
        flair::filter::TrajectoryGenerator2DCircle *circle;
        flair::core::AhrsData *customReferenceOrientation,*customOrientation;
};

#endif // DEMOGPS_H
