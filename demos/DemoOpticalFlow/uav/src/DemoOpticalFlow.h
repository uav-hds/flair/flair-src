//  created:    2011/05/01
//  filename:   DemoOpticalFlow.h
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    demo optical flow
//
//
/*********************************************************************/
//------------

#ifndef DEMOOPTICALFLOW_H
#define DEMOOPTICALFLOW_H

#include <UavStateMachine.h>

namespace flair {
    namespace core {
        class Matrix;
    }
    namespace gui {
        class GroupBox;
        class DoubleSpinBox;
        class PushButton;
    }
    namespace filter {
        class OpticalFlow;
        class OpticalFlowCompensated;
        class OpticalFlowSpeed;
        class LowPassFilter;
        class EulerDerivative;
        class CvtColor;
    }
    namespace sensor {
        class TargetController;
    }
}

class DemoOpticalFlow : public flair::meta::UavStateMachine {

    public:
        DemoOpticalFlow(flair::sensor::TargetController *controller);
        ~DemoOpticalFlow();

    protected:
        void SignalEvent(Event_t event) override;
        void ExtraCheckJoystick(void) override;
        void ExtraCheckPushButton(void) override;
        void ExtraSecurityCheck(void) override;
        const flair::core::AhrsData *GetReferenceOrientation(void) override;

        flair::gui::GroupBox* opticalFlowGroupBox;
        flair::gui::DoubleSpinBox *maxXSpeed,*maxYSpeed;
        flair::core::Matrix *opticalFlowReference;
        flair::filter::Pid *u_x, *u_y;
        flair::filter::CvtColor* greyCameraImage;
        flair::core::AhrsData *customReferenceOrientation;
        flair::core::Matrix *opticalFlowRealSpeed,*opticalFlowRealAcceleration;

    private:
        flair::filter::OpticalFlow *opticalFlow;
        flair::filter::OpticalFlowCompensated *opticalFlowCompensated;
        flair::filter::OpticalFlowSpeed *opticalFlowSpeedRaw;
        flair::filter::EulerDerivative *opticalFlowAccelerationRaw;
        flair::gui::PushButton *startOpticalflow,*stopOpticalflow;
        void StartOpticalFlow(void);
        flair::filter::LowPassFilter* opticalFlowSpeed;
        flair::filter::LowPassFilter* opticalFlowAcceleration;
        bool flagCameraLost;
};

#endif // DEMOOPTICALFLOW_H
