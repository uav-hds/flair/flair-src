//  created:    2012/04/18
//  filename:   main.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 6599
//
//  version:    $Id: $
//
//  purpose:    main simulateur
//
//
/*********************************************************************/
#ifdef GL

#include <tclap/CmdLine.h>
#include <Simulator.h>
#include <X4.h>
#include <SimuImu.h>
#include <Parser.h>
#include <SimuUsGL.h>
#include <SimuCameraGL.h>

#define UAV_REAL_MODEL_ID 0

using namespace TCLAP;
using namespace std;
using namespace flair::simulator;
using namespace flair::sensor;

int port;
int opti_time;
string xml_file;
string media_path;
string scene_file;
string address;

void parseOptions(int argc, char** argv)
{
  try {
    CmdLine cmd("Command description message", ' ', "0.1");

    ValueArg<string> xmlArg("x", "xml", "xml file", true, "./reglages.xml", "string");
    cmd.add(xmlArg);

    ValueArg<int> portArg("p", "port", "ground station port", true, 9002, "int");
    cmd.add(portArg);

    ValueArg<string> addressArg("a", "address", "ground station address", true, "127.0.0.1", "string");
    cmd.add(addressArg);
    
    ValueArg<int> optiArg("o", "opti", "optitrack time ms", false, 0, "int");
    cmd.add(optiArg);
    
    ValueArg<string> mediaArg("m", "media", "path to media files", true, "./", "string");
    cmd.add(mediaArg);

    ValueArg<string> sceneArg("s", "scene", "path to scene file", true, "./voliere.xml", "string");
    cmd.add(sceneArg);

    cmd.parse(argc, argv);

    // Get the value parsed by each arg.
    port = portArg.getValue();
    xml_file = xmlArg.getValue();
    opti_time = optiArg.getValue();
    address = addressArg.getValue();
    media_path = mediaArg.getValue();
    scene_file = sceneArg.getValue();
  } catch(ArgException& e) {
    cerr << "error: " << e.error() << " for arg " << e.argId() << endl;
    exit(EXIT_FAILURE);
  }
}

int main(int argc, char* argv[]) {
  parseOptions(argc, argv);

  Simulator* simu = new Simulator("simulator_real", opti_time, 90);
  simu->SetupConnection(address, port);
  simu->SetupUserInterface(xml_file);
  Parser* gui = new Parser(960, 480, 640, 480, media_path, scene_file);
 
  X4* uav_real = new X4("Drone_0", UAV_REAL_MODEL_ID);
  SimuImu* imu = new SimuImu(uav_real, "imu", UAV_REAL_MODEL_ID,0);
  SimuUsGL* us_gl = new SimuUsGL(uav_real, "us", UAV_REAL_MODEL_ID,0);
  SimuCameraGL* cam_bas = new SimuCameraGL(uav_real, "bottom camera", 320, 240, 640, 0, UAV_REAL_MODEL_ID,0);
  gui->setVisualizationCamera(uav_real);
  simu->RunSimu();

  delete simu;
  return 0;
}

#endif