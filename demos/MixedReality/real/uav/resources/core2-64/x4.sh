#! /bin/bash

if [ -f /proc/xenomai/version ];then
	EXEC=./MixedReality_uav_real_rt
else
	EXEC=./MixedReality_uav_real_nrt
fi

$EXEC -a 127.0.0.1 -p 9000 -l /tmp -x setup_x4.xml -t x4_simu0 -v 127.0.0.1:3884 -d 20010
