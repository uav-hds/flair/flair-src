#! /bin/bash
if [ -f /proc/xenomai/version ];then
	EXEC=./MixedReality_simulator_virtual_rt
else
	EXEC=./MixedReality_simulator_virtual_nrt
fi

$EXEC -p 9000 -a 127.0.0.1 -x simulator_x4.xml -o 10 -m $FLAIR_ROOT/flair-src/models -s $FLAIR_ROOT/flair-src/models/indoor_flight_arena.xml -v 127.0.0.1:3883
