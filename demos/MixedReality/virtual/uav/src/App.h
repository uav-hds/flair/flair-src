//  created:    2019/01/09
//  filename:   App.h
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    mixed reality demo, virtual uav side
//
//
/*********************************************************************/

#ifndef APP_H
#define APP_H

#include <UavStateMachine.h>

namespace flair {
    namespace gui {
        class PushButton;
    }
    namespace meta {
        class MetaVrpnObject;
    }
    namespace sensor {
        class TargetController;
    }
}

class App : public flair::meta::UavStateMachine {
    public:
        App(flair::sensor::TargetController *controller);
        ~App();

    private:

	enum class BehaviourMode_t {
            Default,
            PositionHold,
        };

        BehaviourMode_t behaviourMode;
        bool vrpnLost;

        void VrpnPositionHold(void);
        void ExtraSecurityCheck(void) override;
        void ExtraCheckPushButton(void) override;
        void ExtraCheckJoystick(void) override;
        const flair::core::AhrsData *GetOrientation(void) const override;
        void AltitudeValues(float &z,float &dz) const override;
        void PositionValues(flair::core::Vector2Df &pos_error,flair::core::Vector2Df &vel_error,float &yaw_ref);
        flair::core::AhrsData *GetReferenceOrientation(void) override;
        void SignalEvent(Event_t event) override;

        flair::filter::Pid *uX, *uY;

        flair::core::Vector2Df posHold;
        float yawHold;

        flair::gui::PushButton *positionHold;
        flair::meta::MetaVrpnObject *uavVrpn;
        flair::core::AhrsData *customReferenceOrientation,*customOrientation;
};

#endif // APP_H
