#! /bin/bash

if [ -f /proc/xenomai/version ];then
	EXEC=./PidStandalone_simulator_rt
else
	EXEC=./PidStandalone_simulator_nrt
fi

$EXEC  -n Drone_0 -t x8 -a 127.0.0.1 -p 9000 -x simulator_x8.xml -o 10 -m $FLAIR_ROOT/flair-src/models -s $FLAIR_ROOT/flair-src/models/indoor_flight_arena.xml
