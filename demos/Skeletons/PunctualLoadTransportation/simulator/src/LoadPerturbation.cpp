// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2023/09/20
//  filename:   LoadPerturbation.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    class simulating the load perturbation, only used to store the perturbation value
//
/*********************************************************************/

#include "LoadPerturbation.h"

using namespace flair::core;

namespace flair {
namespace simulator {


LoadPerturbation::LoadPerturbation(std::string name) : Perturbation(name) {
}

LoadPerturbation::~LoadPerturbation() {}

Vector3D<double> LoadPerturbation::GetValue(void) const{
  return value;
}

void LoadPerturbation::SetValue(Vector3D<double> value) {
  this->value=value;
}
    



} // end namespace simulator
} // end namespace flair
