// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2023/09/20
//  filename:   LoadPerturbation.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    class simulating the load perturbation, only used to store the perturbation value
//
/*********************************************************************/

#ifndef LOADPERTURBATION_H
#define LOADPERTURBATION_H

#include <Perturbation.h>
#include <Vector3D.h>

namespace flair {
namespace simulator {

class LoadPerturbation : public Perturbation {
public:
  LoadPerturbation(std::string name);

  ~LoadPerturbation();

  core::Vector3D<double> GetValue(void) const;
  
  void SetValue(core::Vector3D<double> value);
    

private:
  core::Vector3D<double> value;
  
};
} // end namespace simulator
} // end namespace flair
#endif // LOADPERTURBATION_H