// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2023/09/20
//  filename:   PunctualLoad.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    class simulating a punctual load attached with cables to uavs
//
/*********************************************************************/


#include "PunctualLoad.h"
#include "LoadPerturbation.h"
#include <Gui.h>
#include <Euler.h>
#include <RotationMatrix.h>
#include <ISceneManager.h>
#include <Tab.h>
#include <DoubleSpinBox.h>
#include <MeshSceneNode.h>

#define G (float)9.81 // gravity ( N/(m/s²) )

using namespace irr;
using namespace irr::core;
using namespace irr::scene;
using namespace irr::video;
using namespace flair::core;
using namespace flair::gui;

namespace flair {
namespace simulator {

PunctualLoad::PunctualLoad(std::string name,uint32_t modelId) : Model(name,modelId) {
  Tab *setup_tab = GetParamsTab();
  radius = new DoubleSpinBox(setup_tab->NewRow(), "radius :", "m",0, 1, 0.1,2,0.05);
  cableLength = new DoubleSpinBox(setup_tab->NewRow(), "cable length :", "m",0, 5, 0.1,2,1);
                      
  SetIsReady(true);
}

PunctualLoad::~PunctualLoad() {}

void PunctualLoad::AddEndPoint(Model* model) {
  if(IsCableStretched(model)) {
    //distance between uav and load should be less than cable length at initialization
    Warn("model %s is too far from load, not adding it to the endpoints, please change init positions or cable length\n",model->ObjectName().c_str());
  } else {
    endPoint_t endPoint;
    endPoint.model=model;
    endPoint.perturbation=new LoadPerturbation(model->ObjectName()+"_pert");
    endPoints.push_back(endPoint);
    model->AddPerturbation(endPoint.perturbation);
  }
}

bool PunctualLoad::IsCableStretched(Model* model) {
  Vector3D<double> vect = model->GetPosition()-GetPosition();
  
  if(vect.GetNorm()<cableLength->Value()) {
    return false;
  } else {
    return true;
  }
}

void PunctualLoad::Draw(void) {
  node = getGui()->getSceneManager()->addSphereSceneNode(100,16,getSceneNode());
  
  ITexture *texture = getGui()->getTexture("ball.jpg");
  node->setMaterialTexture(0, texture);
  node->setMaterialFlag(video::EMF_LIGHTING, false);

  setTriangleSelector(getGui()->getSceneManager()->createTriangleSelector(node->getMesh(),node));
  
  node->setScale(vector3df(radius->Value(), radius->Value(), radius->Value()));
 
  //lines
  const IGeometryCreator *geo;
  geo = getGui()->getSceneManager()->getGeometryCreator();
  irr::scene::IMesh *colored_line = geo->createCylinderMesh(0.3, 100, 16, SColor(0, 0, 0, 0));
  for(ssize_t i=0;i<endPoints.size();i++) {
    endPoints.at(i).line=new MeshSceneNode(this, colored_line, vector3df(0, 0, 0),vector3df(0, 0, 0));
  }
}

void PunctualLoad::AnimateModel(void){ 
  // adapt  size
  if (radius->ValueChanged() == true) {
    node->setScale(vector3df(radius->Value(), radius->Value(), radius->Value()));
  }
  if (cableLength->ValueChanged() == true) {
    for(ssize_t i=0;i<endPoints.size();i++) {
      endPoints.at(i).line->setScale(vector3df(cableLength->Value(), cableLength->Value(), cableLength->Value()));
    }
  }
  
  for(ssize_t i=0;i<endPoints.size();i++) {
    if(IsCableStretched(endPoints.at(i).model)) {
      Vector3D<double> vect = endPoints.at(i).model->GetPosition()-GetPosition();
      endPoints.at(i).line->setVisible(true);
      float angleZ=atan2(vect.y,vect.x);
      float angleX=atan2(-vect.z,sqrt(vect.x*vect.x+vect.y*vect.y));
      endPoints.at(i).line->setRotation(vector3df(Euler::ToDegree(angleX),0,Euler::ToDegree(angleZ)-90));
    } else {
      endPoints.at(i).line->setVisible(false);
    }
  }
}

void PunctualLoad::CalcModel(void) {
  float tensions[endPoints.size()];
  //calculate each tension here
  for(ssize_t i=0;i<endPoints.size();i++) {
    tensions[endPoints.size()]=0;
  }
  
  //apply tensions
  for(ssize_t i=0;i<endPoints.size();i++) {
    if(IsCableStretched(endPoints.at(i).model)) {
      Vector3D<double> qi=GetPosition()-endPoints.at(i).model->GetPosition();
      qi.Normalize();
      endPoints.at(i).perturbation->SetValue(tensions[i]*qi);
    } else {
      endPoints.at(i).perturbation->SetValue(Vector3D<double>(0,0,0));
    }
  }
  
  state[0].Pos = 0;//put position equation for load
  state[0].Vel = (state[0].Pos - state[-1].Pos) / dT();
}

size_t PunctualLoad::dbtSize(void) const { return 6 * sizeof(float); }

void PunctualLoad::WritedbtBuf(char *dbtbuf) {
}

void PunctualLoad::ReaddbtBuf(char *dbtbuf) { 
}

} // end namespace simulator
} // end namespace flair
