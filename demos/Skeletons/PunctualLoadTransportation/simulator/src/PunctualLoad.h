// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2023/09/20
//  filename:   Load.h
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    class simulating a punctual load attached with cables to uavs
//
/*********************************************************************/

#ifndef PUNCTUALLOAD_H
#define PUNCTUALLOAD_H

#include <Model.h>

namespace irr {
  namespace scene {
    class IMeshSceneNode;
  }
}

namespace flair {
  namespace gui {
    class DoubleSpinBox;
  }
  namespace simulator {
    class MeshSceneNode;
    class LoadPerturbation;
  }
}

namespace flair {
namespace simulator {

class PunctualLoad : public Model {
public:
  PunctualLoad(std::string name,uint32_t modelId);
  ~PunctualLoad();
  void AddEndPoint(Model* model);// add model endpoints, do it before starting simulation
  void Draw();

private:
  size_t dbtSize(void) const;
  void WritedbtBuf(char *dbtbuf);
  void ReaddbtBuf(char *dbtbuf);
  void CalcModel(void);
  void AnimateModel(void);
  bool IsCableStretched(Model* model);
  
  irr::scene::IMeshSceneNode *node;
  gui::DoubleSpinBox *radius,*cableLength;
  typedef struct endPoint {
    Model* model;
    MeshSceneNode* line;
    LoadPerturbation* perturbation;
  } endPoint_t;
  std::vector<endPoint_t> endPoints;
};
} // end namespace simulator
} // end namespace flair
#endif // PUNCTUALLOAD_H
