//  created:    2023/09/20
//  filename:   main.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 6599
//
//  version:    $Id: $
//
//  purpose:    main simulator
//
//
/*********************************************************************/

#include <tclap/CmdLine.h>
#include <Simulator.h>
#include <X4.h>
#include <SimuImu.h>
#include <SimuUsGL.h>
#include <Parser.h>
#include "PunctualLoad.h"

using namespace TCLAP;
using namespace std;
using namespace flair::simulator;
using namespace flair::sensor;

int port;
int opti_time;
int uavNumber;
string xml_file;
string media_path;
string scene_file;
string name;
string address;
string type;

void parseOptions(int argc, char** argv)
{
	try
	{
        CmdLine cmd("Command description message", ' ', "0.1");

        ValueArg<string> nameArg("n","name","uav name, also used for vrpn",true,"x4","string");
        cmd.add( nameArg );

        ValueArg<string> xmlArg("x","xml","xml file",true,"./reglages.xml","string");
        cmd.add( xmlArg );

        ValueArg<int> portArg("p","port","ground station port",true,9002,"int");
        cmd.add( portArg );

        ValueArg<int> optiArg("o","opti","optitrack time ms",false,0,"int");
        cmd.add( optiArg );

        ValueArg<string> mediaArg("m","media","path to media files",true,"./","string");
        cmd.add( mediaArg );

        ValueArg<string> sceneArg("s","scene","path to scene file",true,"./voliere.xml","string");
        cmd.add( sceneArg );

        ValueArg<string> addressArg("a","address","ground station address",true,"127.0.0.1","string");
        cmd.add( addressArg );
		
        ValueArg<int> uavNumberArg("u","uav","uav number",true,3,"int");
        cmd.add( uavNumberArg );

        cmd.parse( argc, argv );

        // Get the value parsed by each arg.
        port=portArg.getValue();
        address=addressArg.getValue();
        xml_file = xmlArg.getValue();
        opti_time = optiArg.getValue();
        name=nameArg.getValue();
        media_path=mediaArg.getValue();
        scene_file=sceneArg.getValue();
        uavNumber=uavNumberArg.getValue();

	} catch (ArgException &e) {
        cerr << "error: " << e.error() << " for arg " << e.argId() << endl;
        exit(EXIT_FAILURE);
    }
}


int main(int argc, char* argv[]) {
  Simulator *simu;

  parseOptions(argc,argv);

  simu= new Simulator("simulator",opti_time,90);
  simu->SetupConnection(address,port);
  simu->SetupUserInterface(xml_file);
  Parser *gui=new Parser(1024,768,1024,768,media_path,scene_file);

  //load model
  PunctualLoad* load=new PunctualLoad("load",uavNumber);//uav id: [0..uavNumber-1]; load id: uavNumber
  
  for(int i=0; i<uavNumber;i++) {
    stringstream uavName;
    X4 *uav;
    uavName << name.c_str() << "_" << i;
    uav = new X4(uavName.str(),i);
    SimuUsGL *us_gl=new SimuUsGL(uav,"us",i,0);
    SimuImu *imu=new SimuImu(uav,"imu",i,0);
    load->AddEndPoint(uav);//link the cable to the uav
  }
  gui->setVisualizationCamera(load);
  simu->RunSimu();

  delete simu;

	return 0;
}

