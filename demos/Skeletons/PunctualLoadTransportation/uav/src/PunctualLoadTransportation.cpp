//  created:    2023/09/20
//  filename:   PunctualLoadTransportation.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    PunctualLoadTransportation skeleton
//
//
/*********************************************************************/

#include "PunctualLoadTransportation.h"

#include <Uav.h>
#include <FrameworkManager.h>
#include <TargetController.h>
#include <UdpSocket.h>
#include <Vector3D.h>
#include <Vector2D.h>
#include <Euler.h>
#include <Quaternion.h>
#include <Matrix.h>
#include <AhrsData.h>

#include <GridLayout.h>
#include <TabWidget.h>
#include <Tab.h>
#include <PushButton.h>
#include <DataPlot1D.h>
#include <DataPlot2D.h>

#include <Ahrs.h>
#include <MetaUsRangeFinder.h>
#include <MetaDualShock3.h>
#include <VrpnClient.h>
#include <MetaVrpnObject.h>

#include <Pid.h>
#include <PidThrust.h>

#include <string.h>
#include <sstream>

using namespace std;
using namespace flair::core;
using namespace flair::gui;
using namespace flair::sensor;
using namespace flair::filter;
using namespace flair::meta;

PunctualLoadTransportation::PunctualLoadTransportation(string broadcast,TargetController *controller,uint16_t uavNumber): UavStateMachine(controller), behaviourMode(BehaviourMode_t::Default), vrpnLost(false) {
  Uav* uav=GetUav();

  VrpnClient* vrpnclient=new VrpnClient("vrpn", uav->GetDefaultVrpnAddress(),80,uav->GetDefaultVrpnConnectionType());
  if (vrpnclient->ConnectionType()!=VrpnClient::Vrpn) {
    Err("vrpn connection type is not handled in the code\n");
  }
	
	loadVrpn=new MetaVrpnObject("load");
  getFrameworkManager()->AddDeviceToLog(loadVrpn);
	
	for(uint16_t i=0;i<uavNumber;i++) {
    stringstream s;
    s << "Drone_" << i;
		MetaVrpnObject* uavVrpn=new MetaVrpnObject(s.str());
		allUavs.push_back(uavVrpn);
		getFrameworkManager()->AddDeviceToLog(uavVrpn);
    if(s.str()==uav->ObjectName()){ 
       uavIndex=i;
    } 
  }

  uav->GetAhrs()->YawPlot()->AddCurve(allUavs[uavIndex]->State()->Element(2),DataPlot::Green);
    
  if(IsMaster()) {
    positionHold=new PushButton(GetButtonsLayout()->NewRow(),"position hold");
    startTransportation=new PushButton(GetButtonsLayout()->NewRow(),"start transportation");
  }
    
  //set vrpn as failsafe altitude sensor for mamboedu as us in not working well for the moment
  if(uav->GetType()=="mamboedu") {
    SetFailSafeAltitudeSensor(allUavs[uavIndex]->GetAltitudeSensor());
  }
    
  //u_x and u_y only for position hold
  u_x=new Pid(setupLawTab->At(1,0),"u_x");
  u_x->UseDefaultPlot(graphLawTab->NewRow());
  u_y=new Pid(setupLawTab->At(1,1),"u_y");
  u_y->UseDefaultPlot(graphLawTab->LastRowLastCol());

  message=new UdpSocket(uav,"Message",broadcast,true);

  customReferenceOrientation= new AhrsData(this,"reference");
  uav->GetAhrs()->AddPlot(customReferenceOrientation,DataPlot::Yellow);
  customOrientation=new AhrsData(this,"orientation");
   
  allUavs[uavIndex]->XyPlot()->AddCurve(loadVrpn->NEDPosition::Output()->Element(1),loadVrpn->NEDPosition::Output()->Element(0),DataPlot::Yellow,"charge");
  for (size_t i = 0; i < allUavs.size(); i++) {
    if(i==uavIndex) continue;
    //todo: all other uav are in black
    allUavs[uavIndex]->XyPlot()->AddCurve(allUavs.at(i)->NEDPosition::Output()->Element(1),allUavs.at(i)->NEDPosition::Output()->Element(0),DataPlot::Black,"other_uav");
  }
	
  vrpnclient->Start();
}

PunctualLoadTransportation::~PunctualLoadTransportation() {
}

bool PunctualLoadTransportation::IsMaster(void) const {
  if(GetUav()->ObjectName()=="Drone_0") { 
    return true;
  } else {
    return false;
  }
}

const AhrsData *PunctualLoadTransportation::GetOrientation(void) const {
  //get yaw from vrpn
  Quaternion vrpnQuaternion;
  allUavs[uavIndex]->GetQuaternion(vrpnQuaternion);

  //get roll, pitch and w from imu
  Quaternion ahrsQuaternion;
  Vector3Df ahrsAngularSpeed;
  GetDefaultOrientation()->GetQuaternionAndAngularRates(ahrsQuaternion, ahrsAngularSpeed);

  Euler ahrsEuler=ahrsQuaternion.ToEuler();
  ahrsEuler.yaw=vrpnQuaternion.ToEuler().yaw;
  Quaternion mixQuaternion=ahrsEuler.ToQuaternion();

  customOrientation->SetQuaternionAndAngularRates(mixQuaternion,ahrsAngularSpeed);

  return customOrientation;
}

void PunctualLoadTransportation::AltitudeValues(float &z,float &dz) const {
  Vector3Df uav_pos,uav_vel;

  allUavs[uavIndex]->GetPosition(uav_pos);
  allUavs[uavIndex]->GetSpeed(uav_vel);
  //z and dz must be in uav's frame
  z=-uav_pos.z;
  dz=-uav_vel.z;
}

//this method is called by UavStateMachine::Run (main loop) when ThrustMode is Custom
float PunctualLoadTransportation::ComputeCustomThrust(void) {
  float thrust;
  //compute the thrust, with your own control laws
  
  return thrust;
}

//this method is called by UavStateMachine::Run (main loop) when TorqueMode is Custom
void PunctualLoadTransportation::ComputeCustomTorques(Euler &torques) {
  //compute the torques, with your own control laws

  //torques.roll=;
  //torques.pitch=;
  //torques.yaw=;
}


//only in position hold
const AhrsData *PunctualLoadTransportation::GetReferenceOrientation(void) {
  Euler refAngles;
  refAngles.yaw=yawHold;
  
  Vector3Df pos_err; // in uav coordinate system
  Vector2Df vel_err; // in uav coordinate system
  PositionValues(pos_err, vel_err);
  u_x->SetValues(pos_err.x, vel_err.x);
  u_x->Update(GetTime());
  u_y->SetValues(pos_err.y, vel_err.y);
  u_y->Update(GetTime());
  refAngles.pitch=u_x->Output();
  refAngles.roll=-u_y->Output();
  
  customReferenceOrientation->SetQuaternionAndAngularRates(refAngles.ToQuaternion(),Vector3Df(0,0,0));

  return customReferenceOrientation;
}

//only in position hold
void PunctualLoadTransportation::PositionValues(Vector3Df &pos_error,Vector2Df &vel_error) {
  Vector3Df uav_pos,uav_vel; // in VRPN coordinate system
  Vector2Df uav_2Dvel; // in VRPN coordinate system

  allUavs[uavIndex]->GetPosition(uav_pos);
  allUavs[uavIndex]->GetSpeed(uav_vel);
  uav_vel.To2Dxy(uav_2Dvel);

  pos_error=uav_pos-posHold;
  vel_error=uav_2Dvel;

  //error in uav frame
  Quaternion currentQuaternion=GetCurrentQuaternion();
  Euler currentAngles;//in vrpn frame
  currentQuaternion.ToEuler(currentAngles);
  pos_error.RotateZ(-currentAngles.yaw);
  vel_error.Rotate(-currentAngles.yaw);
}

void PunctualLoadTransportation::SignalEvent(Event_t event) {
  UavStateMachine::SignalEvent(event);

  switch(event) {
    case Event_t::Stabilized:
      break;
    case Event_t::EmergencyStop:
      message->SendMessage("EmergencyStop");
      break;
    case Event_t::TakingOff:
      message->SendMessage("TakeOff");
      PositionHold();
      break;
    case Event_t::StartLanding:
      PositionHold();
      message->SendMessage("Landing");
      break;
    case Event_t::EnteringControlLoop:
      CheckMessages();
      break;
    case Event_t::EnteringFailSafeMode:
      behaviourMode=BehaviourMode_t::Default;
      break;
  }
}

void PunctualLoadTransportation::CheckMessages(void) {
  char msg[128];
  char src[64];
  
  size_t src_size=sizeof(src);
  ssize_t rcv_size;
  while(1) {
    rcv_size=message->RecvMessage(msg,sizeof(msg),TIME_NONBLOCK,src,&src_size);
    if(rcv_size<=0) break;
    if(strcmp(src,GetUav()->ObjectName().c_str())!=0) {
      if(strcmp(msg,"TakeOff")==0) {
        Printf("TakeOff fleet\n");
        TakeOff();
      } else if(strcmp(msg,"Landing")==0) {
        Printf("Landing fleet\n");
        Land();
      } else if(strcmp(msg,"StartTransportation")==0) {
        Printf("Start transportation fleet\n");
        StartTransportation();
      } else if(strcmp(msg,"Hold")==0) {
        Printf("hold fleet\n");
        PositionHold();
      } else if(strcmp(msg,"EmergencyStop")==0) {
        Printf("EmergencyStop fleet\n");
        EmergencyStop();
      }
    }
  }
}

void PunctualLoadTransportation::ExtraSecurityCheck(void) {
  if (!vrpnLost && behaviourMode!=BehaviourMode_t::Default) {
    for (size_t i = 0; i < allUavs.size(); i++) {
      if (!allUavs[i]->IsTracked(500)) {
        Thread::Err("Optitrack, uav %s lost\n",allUavs[i]->ObjectName().c_str());
        vrpnLost=true;
        EnterFailSafeMode();
        Land();
      }
    }
    if (!loadVrpn->IsTracked(500)) {
      Thread::Err("Optitrack, charge lost\n");
      vrpnLost=true;
      EnterFailSafeMode();
      Land();
    }
  }
}

void PunctualLoadTransportation::ExtraCheckJoystick(void) {
  //R1 and Cross
  if(GetTargetController()->ButtonClicked(5) && GetTargetController()->IsButtonPressed(9) && (behaviourMode!=BehaviourMode_t::Transportation)) {
    StartTransportation();
    message->SendMessage("StartTransportation");
  }
  
  //R1 and Square
  if(GetTargetController()->ButtonClicked(2) && GetTargetController()->IsButtonPressed(9) && (behaviourMode!=BehaviourMode_t::PositionHold)) {
    PositionHold();
    message->SendMessage("Hold");
  }
}

void PunctualLoadTransportation::ExtraCheckPushButton(void) {
  if(IsMaster()) {
    if(startTransportation->Clicked() && (behaviourMode!=BehaviourMode_t::Transportation)) {
      StartTransportation();
      message->SendMessage("StartTransportation");
    }
    if(positionHold->Clicked() && (behaviourMode!=BehaviourMode_t::PositionHold)) {
      PositionHold();
      message->SendMessage("Hold");
    }
  }
}

//holding position, for security purpose
void PunctualLoadTransportation::PositionHold(void) {
  if (SetOrientationMode(OrientationMode_t::Custom)) {
    Thread::Info("holding position\n");
  } else {
    Thread::Warn("could not hold position, error SetOrientationMode(OrientationMode_t::Custom) failed\n");
    return;
  }  
  behaviourMode=BehaviourMode_t::PositionHold;
  Quaternion vrpnQuaternion;
  allUavs[uavIndex]->GetQuaternion(vrpnQuaternion);
  yawHold=vrpnQuaternion.ToEuler().yaw;

  allUavs[uavIndex]->GetPosition(posHold);
  
  u_x->Reset();
  u_y->Reset();
}

void PunctualLoadTransportation::StartTransportation(void) {
  if (!SetTorqueMode(TorqueMode_t::Custom)) {
    Thread::Warn("could not StartTransportation, error SetTorqueMode(TorqueMode_t::Custom) failed\n");
    return;
  }
  
  if (!SetThrustMode(ThrustMode_t::Custom)) {
    Thread::Warn("could not StartTransportation error SetThrustMode(OrientationMode_t::Custom)\n");
    return;
  }
  Thread::Info("StartTransportation\n");
  behaviourMode=BehaviourMode_t::Transportation;

}
