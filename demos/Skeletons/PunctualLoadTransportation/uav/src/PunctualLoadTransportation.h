//  created:    2023/09/20
//  filename:   ChargeSuspendue.h
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    PunctualLoadTransportation skeleton
//
//
/*********************************************************************/

#ifndef PUNCTUALLOADTRANSPORTATION_H
#define PUNCTUALLOADTRANSPORTATION_H

#include <UavStateMachine.h>

namespace flair {
  namespace core {
    class UdpSocket;
    class AhrsData;
  }
  namespace meta {
    class MetaVrpnObject;
  }
  namespace gui {
    class PushButton;
  }
}


class PunctualLoadTransportation : public flair::meta::UavStateMachine {
  public:
    PunctualLoadTransportation(std::string broadcast,flair::sensor::TargetController *controller,uint16_t uavNumber);
    ~PunctualLoadTransportation();

  private:
    enum class BehaviourMode_t {
      Default,
      Transportation,
      PositionHold,
    };
    
    BehaviourMode_t behaviourMode;
    bool vrpnLost;
    
    void PositionHold(void);
    void StartTransportation(void);
    void ExtraSecurityCheck(void) override;
    void ExtraCheckJoystick(void) override;
    void ExtraCheckPushButton(void) override;
    const flair::core::AhrsData *GetOrientation(void) const override;
    void AltitudeValues(float &z,float &dz) const override;
    void PositionValues(flair::core::Vector3Df &pos_error,flair::core::Vector2Df &vel_error);
    const flair::core::AhrsData *GetReferenceOrientation(void) override;
    void SignalEvent(Event_t event) override;
    void CheckMessages(void);
    bool IsMaster(void) const;
    float ComputeCustomThrust(void) override;
    void ComputeCustomTorques(flair::core::Euler &torques) override;
    
    flair::filter::Pid *u_x, *u_y;
    flair::core::Vector3Df posHold;
    float yawHold;
    flair::core::UdpSocket *message;
    flair::gui::PushButton *startTransportation,*positionHold;
    flair::core::AhrsData *customReferenceOrientation,*customOrientation;
    flair::meta::MetaVrpnObject *loadVrpn;
    std::vector<const flair::meta::MetaVrpnObject *> allUavs;
    uint16_t uavIndex;
};

#endif // PUNCTUALLOADTRANSPORTATION_H
