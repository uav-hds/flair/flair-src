#! /bin/bash

if [ -f /proc/xenomai/version ];then
	EXEC=./TwoWheelRobotSimpleFleet_ugv_rt
else
	EXEC=./TwoWheelRobotSimpleFleet_ugv_nrt
fi

$EXEC -n ugv_0 -t ugv_simu0 -a 127.0.0.1 -p 9000 -l /tmp -x setup_simu0.xml -b 127.255.255.255:20010
