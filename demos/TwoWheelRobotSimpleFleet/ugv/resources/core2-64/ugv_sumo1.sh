#! /bin/bash

if [ -f /proc/xenomai/version ];then
	EXEC=./TwoWheelRobotSimpleFleet_ugv_rt
else
	EXEC=./TwoWheelRobotSimpleFleet_ugv_nrt
fi

$EXEC -n ugv_1 -t sumo -a 127.0.0.1 -p 9000 -l /tmp -x setup_sumo1.xml -b 172.26.223.255:20010
