//  created:    2020/12/21
//  filename:   SimpleFleet.h
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    demo simple fleet avec optitrack
//
//
/*********************************************************************/

#ifndef SIMPLEFLEET_H
#define SIMPLEFLEET_H

#include <Thread.h>

namespace flair {
    namespace core {
        class UdpSocket;
    }
    namespace gui {
        class PushButton;
        class DoubleSpinBox;
    }
    namespace filter {
        class TrajectoryGenerator2DCircle;
        class Pid;
    }
    namespace meta {
        class MetaVrpnObject;
    }
    namespace sensor {
        class TargetController;
    }
}

class SimpleFleet : public flair::core::Thread {
    public:
        SimpleFleet(std::string name,std::string broadcast,flair::sensor::TargetController *controller);
        ~SimpleFleet();

    private:

	enum class BehaviourMode_t {
            Manual,
            Circle
        };

        void Run(void);
        void StartCircle(void);
        void StopCircle(void);
        void ComputeManualControls(void);
        void ComputeCircleControls(void);
        void SecurityCheck(void);
        void CheckMessages(void);
        void CheckJoystick(void);
        void CheckPushButton(void);

        flair::filter::Pid *uX, *uY;
        flair::gui::PushButton *startCircle,*stopCircle,*quitProgram,*startLog,*stopLog;
        flair::gui::DoubleSpinBox *l;
        flair::gui::DoubleSpinBox *xCircleCenter,*yCircleCenter;
        flair::meta::MetaVrpnObject *ugvVrpn;
        flair::filter::TrajectoryGenerator2DCircle *circle;
        BehaviourMode_t behaviourMode;
        bool vrpnLost;
        flair::sensor::TargetController *controller;
        flair::core::UdpSocket *message;
};

#endif // SIMPLEFLEET_H
