#include "FindArgument.h"

std::string FindArgument(std::string args,std::string arg,bool recquired) {
	size_t position=args.find (arg);
	
	if(position!=std::string::npos) {
		size_t endPosition=args.find(" ",position);
		size_t len;
		if(endPosition!=std::string::npos) {
			len=endPosition-position-arg.size();
		} else {
			len=std::string::npos;
		}
		return args.substr(position+arg.size(),len);
	} else {
		if(recquired) printf("no %s statement in arguments (%s)\n",arg.c_str(),args.c_str());
		return "";
	}
}
