// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2012/03/20
//  filename:   Image.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    Class defining an image
//
/*********************************************************************/

#include "Image.h"
#include <string.h>

using std::string;

namespace flair {
namespace core {

char* (*Image::allocFunction)(ssize_t)=Image::DefaultAllocFunction;
void (*Image::freeFunction)(char*)=Image::DefaultFreeFunction;
   
Image::Image(const Object *parent, uint16_t width, uint16_t height,
                 Type::Format format, string name, bool allocate_data, int n)
    : io_data(parent, name, n), dataType(width, height, format) {
  this->allocate_data = allocate_data;

  if (allocate_data) {
    switch (format) {
    case Type::Format::YUYV:
    case Type::Format::UYVY:
      buffer=allocFunction(width*height*2);
      break;
    case Type::Format::BGR:
      buffer=allocFunction(width*height*3);
      break;
    case Type::Format::Gray:
      buffer=allocFunction(width*height*1);
      break;
    default:
      Err("format not supported");
      break;
    }
    if (buffer == NULL) {
      Err("malloc image error\n");
    }
  } else {
    if (n > 1)
      Err("number of samples!=1 not possible when not allocating data\n");
    n = 1;
  }

  SetPtrToCircle((void **)&buffer);

  if (n > 1)
    prev = new Image(this, width, height, format, name, n - 1);
}

Image::~Image() {
  if(allocate_data) freeFunction(buffer);
}

void Image::RawRead(char *dst) const {
  GetMutex();
  memcpy(dst, buffer, dataType.GetSize());
  ReleaseMutex();  
}

char* Image::AllocFunction(ssize_t size) {
  return allocFunction(size);
}

void Image::FreeFunction(char* buffer) {
 freeFunction(buffer); 
}

void Image::RegisterAllocFunction(char*(*func)(ssize_t size)) {
  if(allocFunction==DefaultAllocFunction) allocFunction=func;
}

void Image::RegisterFreeFunction(void(*func)(char* buffer)) {
  if(freeFunction==DefaultFreeFunction) freeFunction=func;
}

bool Image::IsUsingDefaultAllocAndFree(void) {
    return (allocFunction==DefaultAllocFunction);
}

char* Image::DefaultAllocFunction(ssize_t size){
  return (char*)malloc(size);
}

void Image::DefaultFreeFunction(char* buffer){
  free(buffer);
}

} // end namespace core
} // end namespace flair
