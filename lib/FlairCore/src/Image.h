// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file Image.h
 * \brief Class defining an image
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2012/03/20
 * \version 4.0
 */

#ifndef IMAGE_H
#define IMAGE_H

#include <io_data.h>

namespace flair {
namespace sensor {
class V4LCamera;
}
}


namespace flair {
namespace core {
/*! \class Image
*
* \brief Class defining an image
*
*
*/
class Image : public io_data {
    public:
  friend class flair::sensor::V4LCamera;
  class Type : public DataType {
  public:
    /*!
    \enum Format_t
    \brief Picture formats
    */
    enum class Format {
      YUYV, /*!< YUYV 16 bits */
      UYVY, /*!< UYVY 16 bits */
      BGR,  /*!< BGR 24 bits */
      Gray, /*!< gray 8 bits */
    };
    Type(uint16_t _width, uint16_t _height, Format _format)
        : width(_width), height(_height), format(_format) {}

    size_t GetSize() const override {
      return GetPixelSize() * width * height;
    }
    size_t GetPixelSize() const {
      size_t pixelSize;
      switch (format) {
      case Format::Gray:
        pixelSize = 1;
        break;
      case Format::YUYV:
      case Format::UYVY:
        pixelSize = 2;
        break;
      case Format::BGR:
        pixelSize = 3;
        break;
      default:
        pixelSize = 0; // TODO should throw an exception instead
      }
      return pixelSize;
    }
    std::string GetDescription() const override { return "image"; };

    Format GetFormat() const { return format; };
    uint16_t GetWidth() const { return width; };
    uint16_t GetHeight() const { return height; };

  private:
    uint16_t width;
    uint16_t height;
    Format format;
  };

  /*!
  * \brief Constructor
  *
  * Construct an io_data representing an image.
  *
  * \param parent parent
  * \param width image width
  * \param height image height
  * \param name name
  * \param allocate_data if true, image data is allocated; otherwise
  *img->imagedata must be changed
  * \param n number of samples
  */
  Image(const Object *parent, uint16_t width, uint16_t height,
          Type::Format format, std::string name = "", bool allocate_data = true,
          int n = 1);

  /*!
  * \brief Destructor
  *
  */
  ~Image();

  
  char *buffer;

  Type const &GetDataType() const override { return dataType; };
  
  static void RegisterAllocFunction(char*(*func)(ssize_t size));
  static void RegisterFreeFunction(void(*func)(char* buffer));
  static bool IsUsingDefaultAllocAndFree(void);
  static char* AllocFunction(ssize_t size);
  static void FreeFunction(char* buffer);

private:
  /*!
  * \brief Raw read datas
  *
  * Reimplemented from io_data. \n
  * See io_data::RawRead.
  *
  * \param dst destination buffer
  */
  void RawRead(char *dst) const override;

  bool allocate_data;
  Type dataType;
  static char* (*allocFunction)(ssize_t);
  static void (*freeFunction)(char*);
  static char* DefaultAllocFunction(ssize_t size);
  static void DefaultFreeFunction(char* buffer);
  
};

} // end namespace core
} // end namespace flair

#endif // IMAGE_H
