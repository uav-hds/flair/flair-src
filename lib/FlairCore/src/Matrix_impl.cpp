// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2018/01/30
//  filename:   Matrix_impl.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    classe definissant le type de données Matrix
//
/*********************************************************************/

#include "Matrix.h"
#include "Matrix_impl.h"
#include <typeinfo>
#include <sstream>

using std::string;
using namespace flair::core;

Matrix_impl::Matrix_impl(Matrix *self, int rows, int cols,
                             flair::core::ScalarType const &_elementDataType,
                             int n)
    : elementDataType(_elementDataType) {
  descriptor = new MatrixDescriptor(rows, cols);
  Init(self, n);
}

Matrix_impl::Matrix_impl(Matrix *self,
                             const MatrixDescriptor *inDescriptor,
                             flair::core::ScalarType const &_elementDataType,
                             int n)
    : elementDataType(_elementDataType) {
  descriptor = new MatrixDescriptor(inDescriptor->Rows(), inDescriptor->Cols());

  for (uint32_t i = 0; i < descriptor->Rows(); i++) {
    for (uint32_t j = 0; j < descriptor->Cols(); j++) {
      descriptor->SetElementName(i, j, inDescriptor->ElementName(i, j));
    }
  }
  Init(self, n);
}

void Matrix_impl::Init(Matrix *self, int n) {
  this->self = self;

  datas = nullptr;
  try {
    ScalarType const &scalarType =
        dynamic_cast<ScalarType const &>(elementDataType);
    if (typeid(scalarType) == typeid(FloatType)) {
      datas =malloc(descriptor->Rows() * descriptor->Cols() * sizeof(float));
    } else if (typeid(scalarType) == typeid(SignedIntegerType) || typeid(scalarType) == typeid(UnsignedIntegerType) ) {
      switch (elementDataType.GetSize()) {
      case 1:
        datas=malloc(descriptor->Rows() * descriptor->Cols() * sizeof(uint8_t));
        break;
      case 2:
        datas=malloc(descriptor->Rows() * descriptor->Cols() * sizeof(uint16_t));
        break;
      default:
        self->Err("unsupported integer scalar type\n");
      }
    } else {
      self->Err("unsupported scalar type\n");
    }
  } catch (std::bad_cast e) {
    self->Err("type is not a scalar\n");
  }

  if (datas == nullptr)
    self->Err("allocating matrix failed\n");
  if (n > 1)
    self->Warn("n>1 not supported\n");
}

Matrix_impl::~Matrix_impl() {
  free(datas);
  delete descriptor;
}

float Matrix_impl::ValueNoMutex(uint32_t row, uint32_t col) const {
  ScalarType const &scalarType =
        dynamic_cast<ScalarType const &>(elementDataType);
  if (typeid(scalarType) == typeid(FloatType)) {
    return ((float*)datas)[row*descriptor->Cols()+col];
  } else if (typeid(scalarType) == typeid(SignedIntegerType)) {
    switch (elementDataType.GetSize()) {
    case 1:
      return ((int8_t*)datas)[row*descriptor->Cols()+col];
      break;
    case 2:
      return ((int16_t*)datas)[row*descriptor->Cols()+col];
      break;
    default:
      self->Err("unsupported signed integer scalar type\n");
    }
  } else if (typeid(scalarType) == typeid(UnsignedIntegerType)) {
    switch (elementDataType.GetSize()) {
    case 1:
      return ((uint8_t*)datas)[row*descriptor->Cols()+col];
      break;
    case 2:
      return ((uint16_t*)datas)[row*descriptor->Cols()+col];
      break;
    default:
      self->Err("unsupported unsigned integer scalar type\n");
    }
  } else {
    self->Err("unsupported scalar type\n");
  }
  return 0;
}

void Matrix_impl::SetValueNoMutex(uint32_t row, uint32_t col, float value) {
 ScalarType const &scalarType =
        dynamic_cast<ScalarType const &>(elementDataType);
  if (typeid(scalarType) == typeid(FloatType)) {
    ((float*)datas)[row*descriptor->Cols()+col]=value;
  } else if (typeid(scalarType) == typeid(SignedIntegerType)) {
    switch (elementDataType.GetSize()) {
    case 1:
      ((int8_t*)datas)[row*descriptor->Cols()+col]=value;
      break;
    case 2:
      ((int16_t*)datas)[row*descriptor->Cols()+col]=value;
      break;
    default:
      self->Err("unsupported signed integer scalar type\n");
    }
  } else if (typeid(scalarType) == typeid(UnsignedIntegerType)) {
    switch (elementDataType.GetSize()) {
    case 1:
      ((uint8_t*)datas)[row*descriptor->Cols()+col]=value;
      break;
    case 2:
      ((uint16_t*)datas)[row*descriptor->Cols()+col]=value;
      break;
    default:
      self->Err("unsupported unsigned integer scalar type\n");
    }
  } else {
    self->Err("unsupported scalar type\n");
  }
}