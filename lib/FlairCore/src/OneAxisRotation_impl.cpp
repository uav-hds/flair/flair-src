// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2013/04/17
//  filename:   OneAxisRotation_impl.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    objet integrant pour une rotation sur un axe
//
//
/*********************************************************************/

#include "OneAxisRotation_impl.h"
#include "OneAxisRotation.h"
#include "GroupBox.h"
#include "ComboBox.h"
#include "DoubleSpinBox.h"
#include <Euler.h>
#include <Quaternion.h>
#include <math.h>

using std::string;
using namespace flair::core;
using namespace flair::gui;

template void OneAxisRotation_impl::ComputeRotation(Vector3D<float>&) const;
template void OneAxisRotation_impl::ComputeRotation(Vector3D<double>&) const;

OneAxisRotation_impl::OneAxisRotation_impl(GroupBox *box,int rotationType) {
  rot_value =
      new DoubleSpinBox(box->NewRow(), "value", " deg", -180., 180., 10., 1);
  rot_axe = new ComboBox(box->LastRowLastCol(), "axis");
  rot_axe->AddItem("x");
  rot_axe->AddItem("y");
  rot_axe->AddItem("z");
  this->rotationType=rotationType;
}

OneAxisRotation_impl::~OneAxisRotation_impl() {}

void OneAxisRotation_impl::ComputeRotation(Quaternion &quat) const {
  Quaternion rot;
  switch (rot_axe->CurrentIndex()) {
    case 0://x
      rot=Quaternion(cosf(Euler::ToRadian(-rot_value->Value()/2)),sinf(Euler::ToRadian(-rot_value->Value()/2)),0,0);
      break;
    case 1://y
      rot=Quaternion(cosf(Euler::ToRadian(-rot_value->Value()/2)),0,sinf(Euler::ToRadian(-rot_value->Value()/2)),0);
      break;
    case 2://z
      rot=Quaternion(cosf(Euler::ToRadian(-rot_value->Value()/2)),0,0,sinf(Euler::ToRadian(-rot_value->Value()/2)));
      break;
  }
  if(rotationType==OneAxisRotation::RotationType_t::PreRotation) {
    quat=rot*quat;
  } else { //post rotation
    quat=quat*rot;
  }
}

void OneAxisRotation_impl::ComputeRotation(RotationMatrix &matrix) const {
  Printf("not yet implemented\n");
}

// on utilise la rotation d'un vector pour faire une rotation de repere
// d'ou le signe negatif
template <typename T> void OneAxisRotation_impl::ComputeRotation(Vector3D<T> &vector) const {
  switch (rot_axe->CurrentIndex()) {
  case 0:
    vector.RotateXDeg(-rot_value->Value());
    break;
  case 1:
    vector.RotateYDeg(-rot_value->Value());
    break;
  case 2:
    vector.RotateZDeg(-rot_value->Value());
    break;
  }
}

void OneAxisRotation_impl::ComputeRotation(Euler &euler) const {
  Quaternion quat;
  euler.ToQuaternion(quat);
  ComputeRotation(quat);
  quat.ToEuler(euler);
}

int OneAxisRotation_impl::GetAxis() const {
  return rot_axe->CurrentIndex();
}

float OneAxisRotation_impl::GetAngle() const {
  return rot_value->Value();
}

