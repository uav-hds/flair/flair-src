// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2016/11/16
//  filename:   Semaphore.cpp
//
//  author:     Thomas Fuhrmann
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    Class defining a semaphore
//
//
/*********************************************************************/

#include "Semaphore.h"
#include "Semaphore_impl.h"

using std::string;

namespace flair {
namespace core {

Semaphore::Semaphore(const Object *parent, uint32_t initialValue, string name, Type type)
    : Object(parent, name, "semaphore"),type(type) {
  pimpl_ = new Semaphore_impl(this, name, initialValue,type);
}

Semaphore::~Semaphore() { delete pimpl_; }

bool Semaphore::TryGetSemaphore() const { return pimpl_->TryGetSemaphore(); }
bool Semaphore::GetSemaphore(Time timeout) const { return pimpl_->GetSemaphore(timeout); }

bool Semaphore::ReleaseSemaphore(void) const { return pimpl_->ReleaseSemaphore(); }

} // end namespace core
} // end namespace flair
