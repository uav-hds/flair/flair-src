// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file Semaphore.h
 * \brief Class defining a semaphore
 * \author Thomas Fuhrmann, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2016/11/16
 * \version 1.0
 */

#ifndef SEMAPHORE_H
#define SEMAPHORE_H

#include <Object.h>

class Semaphore_impl;
class ConditionVariable_impl;

namespace flair {
namespace core {

/*! \class Semaphore
*
* \brief Class defining a semaphore
*
*/
class Semaphore : public Object {
  friend class ::ConditionVariable_impl;

public:
  enum class Type { anonymous, named };
  /*!
  * \brief Constructor
  *
  * Construct a semaphore.
  *
  * \param parent parent
  * \param name name
  */
  Semaphore(const Object *parent, uint32_t initialValue, std::string name = "", Type type=Type::anonymous);

  /*!
  * \brief Destructor
  *
  */
  ~Semaphore();

  /*!
  * \brief TryGetSemaphore
  *
  * Lock the semaphore. If not possible immediately returns false
  *
  */
  bool TryGetSemaphore() const;

  /*!
  * \brief GetSemaphore
  *
  * Lock the semaphore.
  * 
  * \param timeout The timeout, in nanoseconds.
  */
  bool GetSemaphore(Time timeout = TIME_INFINITE) const;

  /*!
  * \brief ReleaseSemaphore
  *
  * Unlock the semaphore.
  *
  */
  bool ReleaseSemaphore(void) const;

private:
  class Semaphore_impl *pimpl_;
  Type type;
};

} // end namespace core
} // end namespace flair

#endif // SEMAPHORE_H
