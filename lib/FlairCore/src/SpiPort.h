// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file SpiPort.h
 * \brief Base class for spi port
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2021/04/13
 * \version 4.0
 */

#ifndef SPIPORT_H
#define SPIPORT_H

#include <Object.h>

namespace flair {
namespace core {
/*! \class SpiPort
*
* \brief Base class for spi port
*/
class SpiPort : public Object {
public:
  /*!
  * \brief Constructor
  *
  * Construct a spi port.
  *
  * \param parent parent
  * \param name name
  */
  SpiPort(const Object *parent, std::string name) : Object(parent, name) {}

  /*!
  * \brief Destructor
  *
  */
  ~SpiPort(){};


  /*!
  * \brief Write datas
  *
  * \param buf pointer to datas
  * \param nbyte length of datas
  *
  * \return amount of written datas
  */
  virtual ssize_t Write(const void *buf, size_t nbyte) = 0;

  /*!
  * \brief Read datas
  *
  * \param buf pointer to datas
  * \param nbyte length of datas
  *
  * \return amount of read datas
  */
  virtual ssize_t Read(void *buf, size_t nbyte) = 0;

  /*!
  * \brief Write datas
  *
  * \param tx_buf pointer to write datas
  * \param rx_buf pointer to read datas
  * \param nbyte length of datas
  *
  * \return amount of written datas
  */
  virtual ssize_t WriteRead(const void *tx_buf, void *rx_buf,size_t nbyte) = 0;

};
} // end namespace core
} // end namespace flair

#endif // SPIPORT_H
