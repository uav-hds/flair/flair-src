// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2021/04/13
//  filename:   Unix_SpiPort.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    Class for unix spi port
//
//
/*********************************************************************/

#include "Unix_SpiPort.h"
#include <fcntl.h> /* File control definitions */
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>

using std::string;

namespace flair {
namespace core {

Unix_SpiPort::Unix_SpiPort(const Object *parent, string name,
                                 string device,int mode, int bits, int speed) : SpiPort(parent, name) {
  
  fd = open(device.c_str(), O_RDWR);
  if (fd < 0) {
    Err("open_port: Unable to open %s\n", device.c_str());
  }

  if (ioctl(fd, SPI_IOC_WR_MODE, &mode) < 0) {
    Err("can't set bus mode\n");
  }
  
  if (ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &bits) < 0) {
    Err("can't set bits per word\n");
  }

  if (ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed) < 0) {
    Err("can't set max speed [Hz]\n");
  } 
}

Unix_SpiPort::~Unix_SpiPort() { 
  close(fd);
}

ssize_t Unix_SpiPort::Write(const void *buf, size_t nbyte) {
  int retv;
  struct spi_ioc_transfer xfer;
  memset(&xfer, 0, sizeof(xfer));

  xfer.tx_buf = (__u64) buf;
  xfer.rx_buf = (__u64) 0;
  xfer.len    = (__u32) nbyte;

  retv = ioctl(fd, SPI_IOC_MESSAGE(1), &xfer);
  if (retv < 0) {
    Err("ioctl SPI_IOC_MESSAGE\n");
  }

  return retv;
}

ssize_t Unix_SpiPort::Read(void *buf, size_t nbyte) {
  int retv;
  struct spi_ioc_transfer xfer;
  memset(&xfer, 0, sizeof(xfer));

  xfer.tx_buf = (__u64) 0;
  xfer.rx_buf = (__u64) buf;
  xfer.len    = (__u32) nbyte;

  retv = ioctl(fd, SPI_IOC_MESSAGE(1), &xfer);
  if (retv < 0) {
    Err("ioctl SPI_IOC_MESSAGE\n");
  }

  return retv;
}

ssize_t Unix_SpiPort::WriteRead(const void *tx_buf, void *rx_buf,size_t nbyte) {
  int retv;
  struct spi_ioc_transfer xfer;
  memset(&xfer, 0, sizeof(xfer));

  xfer.tx_buf = (__u64) tx_buf;
  xfer.rx_buf = (__u64) rx_buf;
  xfer.len    = (__u32) nbyte;

  retv = ioctl(fd, SPI_IOC_MESSAGE(1), &xfer);
  if (retv < 0){
    Err("ioctl SPI_IOC_MESSAGE\n");
  }

  return retv;
}

} // end namespace core
} // end namespace flair
