// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file Unix_SpiPort.h
 * \brief Class for unix spi port
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2021/04/13
 * \version 4.0
 */

#ifndef UNIX_SPIPORT_H
#define UNIX_SPIPORT_H

#include <SpiPort.h>
#include <linux/spi/spidev.h>

namespace flair {
namespace core {
/*! \class Unix_SpiPort
*
* \brief Class for unix spi port
*
*/
class Unix_SpiPort : public SpiPort {

public:
  /*!
  * \brief Constructor
  *
  * Construct an unix serial spi
  *
  * \param parent parent
  * \param name name
  * \param device serial device (ex /dev/ttyS0)
  * \param mode SPI_* (look "linux/spi/spidev.h")
  * \param bits bits per word (usually 8)
  * \param speed max speed [Hz]
  */
  Unix_SpiPort(const Object *parent, std::string port_name,
                  std::string device,int mode, int bits, int speed);

  /*!
  * \brief Destructor
  *
  */
  ~Unix_SpiPort();

  /*!
  * \brief Write datas
  *
  * \param buf pointer to datas
  * \param nbyte length of datas
  *
  * \return amount of written datas
  */
  ssize_t Write(const void *buf, size_t nbyte) override;

  /*!
  * \brief Read datas
  *
  * \param buf pointer to datas
  * \param nbyte length of datas
  *
  * \return amount of read datas
  */
  ssize_t Read(void *buf, size_t nbyte) override;

  /*!
  * \brief Write datas
  *
  * \param tx_buf pointer to write datas
  * \param rx_buf pointer to read datas
  * \param nbyte length of datas
  *
  * \return amount of written datas
  */
  ssize_t WriteRead(const void *tx_buf, void *rx_buf,size_t nbyte) override;
  

private:
  int fd;
  
};
} // end namespace core
} // end namespace flair

#endif // UNIX_SPIPORT_H
