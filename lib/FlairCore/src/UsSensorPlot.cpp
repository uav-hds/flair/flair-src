// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2018/02/21
//  filename:   UsSensorPlot.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    Class displaying a plot for raw us sensor
//
//
/*********************************************************************/

#include "UsSensorPlot.h"
#include "LayoutPosition.h"
#include "Matrix.h"

using std::string;
using namespace flair::core;

namespace flair {
namespace gui {

UsSensorPlot::UsSensorPlot(const LayoutPosition *position, string name,
                       string y_name,float ymin, float ymax,const Matrix *datas,const Matrix *peakInfos)
    : DataPlot(position, name, "UsSensorPlot") {
  int nbSamples;
  if(datas->Cols()==1) {
    nbSamples=datas->Rows();
  } else if(datas->Rows()==1) {
    nbSamples=datas->Cols();
  } else {
    nbSamples=0;
    Err("matrix must be 1D\n");
  }
  
  if(peakInfos->Cols()!=4 || peakInfos->Rows()!=1) {
    Err("peakInfos matrix must be 1*4 (first start,first end, second start,second end)\n");
  }
  
  if(peakInfos->GetDataType().GetElementDataType().GetDescription()!="uint16_t") {
    Err("peakInfos matrix must be 1of type uint16_t\n");
  }
    
  this->datas = datas;
  this->peakInfos = peakInfos;
  
  SetVolatileXmlProp("ymin", ymin);
  SetVolatileXmlProp("ymax", ymax);
  SetVolatileXmlProp("y_name", y_name);
  SetVolatileXmlProp("samples", nbSamples);
  SetVolatileXmlProp("type", datas->GetDataType().GetElementDataType().GetDescription());
  SendXml();

  SetSendSize(datas->GetDataType().GetSize()+peakInfos->GetDataType().GetSize());
}

UsSensorPlot::~UsSensorPlot() {}

void UsSensorPlot::CopyDatas(char *buf) const {
  datas->RawRead(buf);
  peakInfos->RawRead(buf+datas->GetDataType().GetSize());
}

} // end namespace gui
} // end namespace flair
