// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file UsSensorPlot.h
 * \brief Class displaying a plot for raw us sensor
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2018/02/21
 * \version 4.0
 */

#ifndef USSENSORPLOT_H
#define USSENSORPLOT_H

#include <DataPlot.h>

namespace flair {
namespace core {
class Matrix;
}
}

namespace flair {
namespace gui {

class LayoutPosition;

/*! \class UsSensorPlot
*
* \brief Class displaying a plot for raw us sensor
*
*/
class UsSensorPlot : private DataPlot {
public:
  /*!
  * \brief Constructor
  *
  * Construct a plot for raw us sensor at given place position.
  *
  * \param position position to display the plot
  * \param name name
  * \param y_name name of y axis
  * \param ymin default ymin of the plot
  * \param ymax default ymax of the plot
  * \param datas data matrix, must be 1D
  * \param peakInfos peak infos, must be 1*4 (first start,first end, second start,second end)
  */
  UsSensorPlot(const LayoutPosition *position, std::string name,
                       std::string y_name,float ymin, float ymax,const core::Matrix *datas,const core::Matrix *peakInfos);

  /*!
  * \brief Destructor
  *
  */
  ~UsSensorPlot();
  
private:
  /*!
  * \brief Copy datas to specified buffer
  *
  * Reimplemented from SendData.
  *
  * \param buf output buffer
  */
  void CopyDatas(char *buf) const override;
  
  const core::Matrix *datas,*peakInfos;


};

} // end namespace gui
} // end namespace flair

#endif // USSENSORPLOT_H
