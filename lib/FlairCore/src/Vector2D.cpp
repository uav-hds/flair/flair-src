// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2013/05/02
//  filename:   Vector2D.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    Class defining a 2D vector
//
//
/*********************************************************************/

#include "Vector2D.h"
#include "Euler.h"
#include <math.h>

namespace flair {
namespace core {
  
template class Vector2D<double>;
template Vector2D<double> operator+(const Vector2D<double>&, const Vector2D<double>&);
template Vector2D<double> operator-(const Vector2D<double>&, const Vector2D<double>&);
template Vector2D<double> operator-(const Vector2D<double>&);
template Vector2D<double> operator/(const Vector2D<double>&, float);
template Vector2D<double> operator*(const Vector2D<double>&, float);
template Vector2D<double> operator*(float, const Vector2D<double>&);

template class Vector2D<float>;
template Vector2D<float> operator+(const Vector2D<float>&, const Vector2D<float>&);
template Vector2D<float> operator-(const Vector2D<float>&, const Vector2D<float>&);
template Vector2D<float> operator-(const Vector2D<float>&);
template Vector2D<float> operator/(const Vector2D<float>&, float);
template Vector2D<float> operator*(const Vector2D<float>&, float);
template Vector2D<float> operator*(float, const Vector2D<float>&);

template <typename T> Vector2D<T>::Vector2D(T inX, T inY) : x(inX), y(inY) {}

template <typename T> Vector2D<T>::~Vector2D() {}

template <typename T> Vector2D<T> &Vector2D<T>::operator+=(const Vector2D<T> &vector) {
  x += vector.x;
  y += vector.y;
  return (*this);
}

template <typename T> Vector2D<T> &Vector2D<T>::operator-=(const Vector2D<T> &vector) {
  x -= vector.x;
  y -= vector.y;
  return (*this);
}

template <typename T> Vector2D<T> operator+(const Vector2D<T> &vectorA, const Vector2D<T> &vectorB) {
  return Vector2D<T>(vectorA.x + vectorB.x, vectorA.y + vectorB.y);
}

template <typename T> Vector2D<T> operator-(const Vector2D<T> &vectorA, const Vector2D<T> &vectorB) {
  return Vector2D<T>(vectorA.x - vectorB.x, vectorA.y - vectorB.y);
}

template <typename T> Vector2D<T> operator-(const Vector2D<T> &vectorA) {
 return Vector2D<T>(-vectorA.x, -vectorA.y);
}

template <typename T> Vector2D<T> operator/(const Vector2D<T> &vector, float coeff) {
  if (coeff != 0) {
    return Vector2D<T>(vector.x / coeff, vector.y / coeff);
  } else {
    return Vector2D<T>(0, 0);
  }
}

template <typename T> Vector2D<T> operator*(const Vector2D<T> &vector, float coeff) {
  return Vector2D<T>(vector.x * coeff, vector.y * coeff);
}

template <typename T> Vector2D<T> operator*(float coeff, const Vector2D<T> &vector) {
  return Vector2D<T>(vector.x * coeff, vector.y * coeff);
}

template <typename T> void Vector2D<T>::Rotate(float value) {
  float xTmp;
  xTmp = x * cosf(value) - y * sinf(value);
  y = x * sinf(value) + y * cosf(value);
  x = xTmp;
}

template <typename T> void Vector2D<T>::RotateDeg(float value) { Rotate(Euler::ToRadian(value)); }

template <typename T> float Vector2D<T>::GetNorm(void) const { return sqrtf(x * x + y * y); }

template <typename T> void Vector2D<T>::Normalize(void) {
  float n = GetNorm();
  if (n != 0) {
    x = x / n;
    y = y / n;
  }
}

template <typename T> void Vector2D<T>::Saturate(Vector2D min, Vector2D max) {
  if (x < min.x)
    x = min.x;
  if (x > max.x)
    x = max.x;

  if (y < min.y)
    y = min.y;
  if (y > max.y)
    y = max.y;
}

template <typename T> void Vector2D<T>::Saturate(float min, float max) {
  Saturate(Vector2D(min, min), Vector2D(max, max));
}

template <typename T> void Vector2D<T>::Saturate(const Vector2D &value) {
  float x = fabs(value.x);
  float y = fabs(value.y);
  Saturate(Vector2D(-x, -y), Vector2D(x, y));
}

template <typename T> void Vector2D<T>::Saturate(float value) {
  float sat = fabs(value);
  Saturate(Vector2D(-sat, -sat), Vector2D(sat, sat));
}

} // end namespace core
} // end namespace flair
