// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file Vector2D.h
 * \brief Class defining a 2D vector
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2013/05/02
 * \version 4.0
 */

#ifndef VECTOR2D_H
#define VECTOR2D_H

namespace flair {
namespace core {

/*! \class Vector2D
*
* \brief Class defining a 2D vector
*/
template <typename T>
class Vector2D {
public:
  /*!
  * \brief Constructor
  *
  * Construct a Vector2D using specified values.
  *
  * \param x
  * \param y
  */
  Vector2D(T x = 0, T y = 0);

  /*!
  * \brief Destructor
  *
  */
  ~Vector2D();

  /*!
  * \brief Rotation
  *
  * \param value rotation value in radians
  */
  void Rotate(float value);

  /*!
  * \brief Rotation
  *
  * \param value rotation value in degrees
  */
  void RotateDeg(float value);

  /*!
  * \brief Norm
  *
  * \return value
  */
  float GetNorm(void) const;

  /*!
  * \brief Normalize
  */
  void Normalize(void);

  /*!
  * \brief Saturate
  *
  * Saturate between min and max
  *
  * \param min minimum Vector2D value
  * \param max maximum Vector2D value
  */
  void Saturate(Vector2D<T> min, Vector2D<T> max);

  /*!
  * \brief Saturate
  *
  * Saturate between min and max
  *
  * \param min minimum Vector2D(min,min) value
  * \param max maximum Vector2D(max,max) value
  */
  void Saturate(float min, float max);

  /*!
  * \brief Saturate
  *
  * Saturate between -abs(value) and abs(value)
  *
  * \param value saturation Vector2D value
  */
  void Saturate(const Vector2D<T> &value);

  /*!
  * \brief Saturate
  *
  * Saturate between -abs(Vector2D(value,value)) and abs(Vector2D(value,value))
  *
  * \param value saturation Vector2D(value,value)
  */
  void Saturate(float value);

  /*!
  * \brief x
  */
  T x;

  /*!
  * \brief y
  */
  T y;

  template<typename S> Vector2D<T> &operator=(const Vector2D<S> &vector) {
    x = vector.x;
    y = vector.y;
    return (*this);
  }
  Vector2D<T> &operator+=(const Vector2D<T> &vector);
  Vector2D<T> &operator-=(const Vector2D<T> &vector);
};

typedef Vector2D<float> Vector2Df;

/*! Add
*
* \brief Add
*
* \param vectorA vector
* \param vectorB vector
*/
template<typename T> Vector2D<T> operator+(const Vector2D<T> &vectorA, const Vector2D<T> &vectorB);

/*! Substract
*
* \brief Substract
*
* \param vectorA vector
* \param vectorB vector
*/
template<typename T> Vector2D<T> operator-(const Vector2D<T> &vectorA, const Vector2D<T> &vectorB);

/*! Opposite
*
* \brief Opposite
*
* \param vectorA vector
* 
* \return -vectorA
*/
template<typename T> Vector2D<T> operator-(const Vector2D<T> &vectorA);

/*! Divid
*
* \brief Divid
*
* \param vector vector
* \param coeff coefficent
* \return vector/coefficient
*/
template<typename T> Vector2D<T> operator/(const Vector2D<T> &vector, float coeff);

/*! Multiply
*
* \brief Multiplyf
*
* \param vector vector
* \param coeff coefficent
* \return coefficient*vector
*/
template<typename T> Vector2D<T> operator*(const Vector2D<T> &vector, float coeff);

/*! Multiply
*
* \brief Multiply
*
* \param coeff coefficent
* \param vector vector
* \return coefficient*vector
*/
template<typename T> Vector2D<T> operator*(float coeff, const Vector2D<T> &vector);

} // end namespace core
} // end namespace flair

#endif // VECTOR2D_H
