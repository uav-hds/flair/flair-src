// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2024/04/19
//  filename:   Vector2DSpinBox.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    Class displaying 2 QDoubleSpinBox for x,y on the ground
//  station
//
//
/*********************************************************************/

#include "Vector2DSpinBox.h"

using std::string;

namespace flair {
namespace gui {

Vector2DSpinBox::Vector2DSpinBox(const LayoutPosition *position, string name,
                                 double min, double max, double step,
                                 uint16_t decimals, core::Vector2Df default_value)
    : Box(position, name, "Vector2DSpinBox") {
  // update value from xml file
  default_value.Saturate(min, max);
  box_value = default_value;

  SetVolatileXmlProp("min", min);
  SetVolatileXmlProp("max", max);
  SetVolatileXmlProp("step", step);
  SetVolatileXmlProp("decimals", decimals);
  GetPersistentXmlProp("value_x", box_value.x);
  SetPersistentXmlProp("value_x", box_value.x);
  GetPersistentXmlProp("value_y", box_value.y);
  SetPersistentXmlProp("value_y", box_value.y);
 

  SendXml();
}

Vector2DSpinBox::~Vector2DSpinBox() {}
/*
Vector2DSpinBox::operator core::Vector2D() const {
    return Value();
}
*/
core::Vector2Df Vector2DSpinBox::Value(void) const {
  core::Vector2Df tmp;

  GetMutex();
  tmp = box_value;
  ReleaseMutex();

  return tmp;
}

void Vector2DSpinBox::XmlEvent(void) {
  bool changed = false;
  GetMutex();
  if (GetPersistentXmlProp("value_x", box_value.x))
    changed = true;
  if (GetPersistentXmlProp("value_y", box_value.y))
    changed = true;
  if (changed)
    SetValueChanged();
  ReleaseMutex();
}

} // end namespace gui
} // end namespace flair
