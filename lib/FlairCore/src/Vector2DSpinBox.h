// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file Vector2DSpinBox.h
 * \brief Class displaying 2 QDoubleSpinBox for x,y on the ground station
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2024/04/19
 * \version 4.0
 */

#ifndef VECTOR2DSPINBOX_H
#define VECTOR2DSPINBOX_H

#include <Box.h>
#include <Vector2D.h>

namespace flair {
namespace gui {
class Layout;

/*! \class Vector2DSpinBox
*
* \brief Class displaying 2 QDoubleSpinBox for x,y on the ground station
*
*/
class Vector2DSpinBox : public Box {
public:
  /*!
  * \brief Constructor
  *
  * Construct a Vector2DSpinBox at given position. \n
  * Each DoubleSpinBox is saturated to min and max values.
  *
  * \param position position to display the Vector2DSpinBox
  * \param name name
  * \param min minimum value
  * \param max maximum value
  * \param step step
  * \param decimals number of decimals
  * \param default_value default value if not in the xml config file
  */
  Vector2DSpinBox(const LayoutPosition *position, std::string name, double min,
                  double max, double step, uint16_t decimals = 2,
                  core::Vector2Df default_value = core::Vector2Df(0, 0));

  /*!
  * \brief Destructor
  *
  */
  ~Vector2DSpinBox();

  /*!
  * \brief Value
  *
  * \return value
  */
  core::Vector2Df Value(void) const;
  // operator core::Vector2D() const;
private:
  /*!
  * \brief XmlEvent from ground station
  *
  * Reimplemented from Widget.
  *
  */
  void XmlEvent(void) override;

  core::Vector2D<double> box_value;
};

} // end namespace gui
} // end namespace flair

#endif // VECTOR2DSPINBOX_H
