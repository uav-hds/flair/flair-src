// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}

/*!
 * \file Vector3D.h
 * \brief Class defining a 3D vector
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2013/05/02
 * \version 4.0
 */
#ifndef VECTOR3D_H
#define VECTOR3D_H

#include <stddef.h>
#include <Vector2D.h>

namespace flair {
namespace core {
class RotationMatrix;
class Quaternion;

/*! \class Vector3D
*
* \brief Class defining a 3D vector
*/
template <typename T>
class Vector3D {
public:
  /*!
  * \brief Constructor
  *
  * Construct a Vector3D using specified values.
  *
  * \param x
  * \param y
  * \param z
  */
  Vector3D(T x = 0, T y = 0, T z = 0);

  /*!
  * \brief Destructor
  *
  */
  ~Vector3D();

  /*!
  * \brief x
  */
  T x;

  /*!
  * \brief y
  */
  T y;

  /*!
  * \brief z
  */
  T z;

  /*!
  * \brief x axis rotation
  *
  * \param value rotation value in radians
  */
  void RotateX(float value);

  /*!
  * \brief x axis rotation
  *
  * \param value rotation value in degrees
  */
  void RotateXDeg(float value);

  /*!
  * \brief y axis rotation
  *
  * \param value rotation value in radians
  */
  void RotateY(float value);

  /*!
  * \brief y axis rotation
  *
  * \param value rotation value in degrees
  */
  void RotateYDeg(float value);

  /*!
  * \brief z axis rotation
  *
  * \param value rotation value in radians
  */
  void RotateZ(float value);

  /*!
  * \brief z axis rotation
  *
  * \param value rotation value in degrees
  */
  void RotateZDeg(float value);

  /*!
  * \brief rotation
  *
  * \param matrix rotation matrix
  */
  void Rotate(const RotationMatrix &matrix);

  /*!
  * \brief rotation
  *
  * Compute a rotation from a quaternion. This method uses a rotation matrix
  * internaly.
  *
  * \param quaternion quaternion
  */
  void Rotate(const Quaternion &quaternion);

  /*!
  * \brief Convert to a Vector2D
  *
  * Uses x and y coordinates.
  *
  * \param vector destination
  */
  void To2Dxy(Vector2D<T> &vector) const;

  /*!
  * \brief Convert to a Vector2D
  *
  * Uses x and y coordinates.
  *
  * \return destination
  */
  Vector2D<T> To2Dxy(void) const;

  /*!
  * \brief Norm
  *
  * \return value
  */
  float GetNorm(void) const;

  /*!
  * \brief Normalize
  */
  void Normalize(void);

  /*!
  * \brief Saturate
  *
  * Saturate between min and max
  *
  * \param min minimum value
  * \param max maximum value
  */
  void Saturate(const Vector3D<T> &min, const Vector3D<T> &max);

  /*!
  * \brief Saturate
  *
  * Saturate between min and max
  *
  * \param min minimum Vector3D(min,min,min) value
  * \param max maximum Vector3D(max,max,max) value
  */
  void Saturate(float min, float max);

  /*!
  * \brief Saturate
  *
  * Saturate between -abs(value) and abs(value)
  *
  * \param value saturation Vector3D value
  */
  void Saturate(const Vector3D<T> &value);

  /*!
  * \brief Saturate
  *
  * Saturate between -abs(Vector3D(value,value,value)) and
  *abs(Vector3D(value,value,value))
  *
  * \param value saturation Vector3D(value,value,value)
  */
  void Saturate(float value);

  T &operator[](size_t idx);
  const T &operator[](size_t idx) const;
  //Vector3D<T> &operator=(const Vector3D<T> &vector);
  template<typename S> Vector3D<T> &operator=(const Vector3D<S> &vector) {
    x = vector.x;
    y = vector.y;
    z = vector.z;
    return (*this);
  }
  Vector3D<T> &operator+=(const Vector3D<T> &vector);
  Vector3D<T> &operator-=(const Vector3D<T> &vector);

private:
};

typedef Vector3D<float> Vector3Df;

/*! Add
*
* \brief Add
*
* \param vectorA vector
* \param vectorB vector
*
* \return vectorA+vectorB
*/
template<typename T> Vector3D<T> operator+(const Vector3D<T> &vectorA, const Vector3D<T> &vectorB);

/*! Substract
*
* \brief Substract
*
* \param vectorA vector
* \param vectorB vector
*
* \return vectorA-vectorB
*/
template<typename T> Vector3D<T> operator-(const Vector3D<T> &vectorA, const Vector3D<T> &vectorB);

/*! Minus
*
* \brief Minus
*
* \param vector vector
*
* \return -vector
*/
template<typename T> Vector3D<T> operator-(const Vector3D<T> &vector);

/*! Divid
*
* \brief Divid
*
* \param vector vector
* \param coeff coefficent
*
* \return vector/coefficient
*/
template<typename T> Vector3D<T> operator/(const Vector3D<T> &vector, float coeff);

/*! Hadamard product
*
* \brief Hadamard product
*
* \param vectorA vector
* \param vectorBA vector
*
* \return Hadamard product
*/
template<typename T> Vector3D<T> operator*(const Vector3D<T> &vectorA, const Vector3D<T> &vectorB);

/*! Multiply
*
* \brief Multiply
*
* \param vector vector
* \param coeff coefficent
*
* \return coefficient*vector
*/
template<typename T> Vector3D<T> operator*(const Vector3D<T> &vector, float coeff);

/*! Multiply
*
* \brief Multiply
*
* \param coeff coefficent
* \param vector vector
*
* \return coefficient*vector
*/
template<typename T> Vector3D<T> operator*(float coeff, const Vector3D<T> &vector);

/*! Cross product
*
* \brief Cross product
*
* \param vectorA first vector
* \param vectorB second vector
*
* \return cross product
*/
template<typename T> Vector3D<T> CrossProduct(const Vector3D<T> &vectorA, const Vector3D<T> &vectorB);

/*! Dot product
*
* \brief Dot product
*
* \param vectorA first vector
* \param vectorB second vector
*
* \return dot product
*/
template<typename T> float DotProduct(const Vector3D<T> &vectorA, const Vector3D<T> &vectorB);

} // end namespace core
} // end namespace flair

#endif // VECTOR3D_H
