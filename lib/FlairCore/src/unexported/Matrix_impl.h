// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file Matrix_impl.h
 * \brief Classe définissant une matrice
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2018/01/30
 * \version 4.0
 */

#ifndef MATRIX_IMPL_H
#define MATRIX_IMPL_H

#include <io_data.h>
#include <Matrix.h>


/*! \class Matrix_impl
* \brief Classe définissant une matrice
*
*/
class Matrix_impl {
public:
  Matrix_impl(flair::core::Matrix *self, int rows, int cols,
                flair::core::ScalarType const &elementDataType, int n);
  Matrix_impl(flair::core::Matrix *self,
                const flair::core::MatrixDescriptor *descriptor,
                flair::core::ScalarType const &elementDataType, int n);
  ~Matrix_impl();

  flair::core::ScalarType const &elementDataType;
  flair::core::MatrixDescriptor *descriptor;
  void *datas;
  void SetValueNoMutex(uint32_t row, uint32_t col, float value);
  float ValueNoMutex(uint32_t row, uint32_t col) const;

private:
  flair::core::Matrix *self;
  void Init(flair::core::Matrix *self, int n);
};

#endif // MATRIX_IMPL_H
