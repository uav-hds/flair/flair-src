// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file Semaphore_impl.h
 * \brief Class defining a semaphore implementation
 * \author Thomas Fuhrmann, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2016/11/16
 * \version 1.0
 */

#ifndef SEMAPHORE_IMPL_H
#define SEMAPHORE_IMPL_H

#ifdef __XENO__
#include <native/sem.h>
#else
#include <fcntl.h>
#include <semaphore.h>
#endif

namespace flair {
namespace core {
class Semaphore;
class Object;
}
}

class Semaphore_impl {
public:
  Semaphore_impl(flair::core::Semaphore *self, std::string name, uint32_t initialValue, flair::core::Semaphore::Type &type);
  ~Semaphore_impl();
  bool TryGetSemaphore();
  bool GetSemaphore(flair::core::Time timeout = TIME_INFINITE);
  bool ReleaseSemaphore(void);
#ifdef __XENO__
  RT_SEM semaphore;
#else
  std::string sem_name;
  sem_t *semaphore;
#endif

private:
  flair::core::Semaphore *self;
  flair::core::Semaphore::Type type;
};

#endif // SEMAPHORE_IMPL_H
