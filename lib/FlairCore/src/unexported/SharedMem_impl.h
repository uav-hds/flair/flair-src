// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file SharedMem_impl.h
 * \brief Class defining a shared memory
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2014/02/10
 * \version 4.0
 */

#ifndef SHAREDMEM_IMPL_H
#define SHAREDMEM_IMPL_H

#include <string>

#ifdef __XENO__
#include <native/heap.h>
#endif

#include <Semaphore.h>

#include <SharedMem.h>

/*! \class SharedMem_impl
* \brief Class defining a shared memory
*
*/

class SharedMem_impl {
public:
  SharedMem_impl(const flair::core::SharedMem *self, std::string name,
                 size_t size, flair::core::SharedMem::Type &type);
  ~SharedMem_impl();

  void Write(const char *buf, size_t size);
  bool Read(char *buf, size_t size, flair::core::Time nsTimeout);
  void ReaderReady();

private:
  const flair::core::SharedMem *self;
  flair::core::SharedMem::Type type;
  size_t size;
  char *mem_segment;
  flair::core::Semaphore *sem; //for mutex type shared memory
  flair::core::Semaphore *sem_producer,*sem_consumer; //sem_[produc|consum]er meaning = is [produc|consum]er ready?
#ifdef __XENO__
  RT_HEAP heap;
  bool heap_binded;
#else
  int fd;
  std::string shm_name;
#endif
};

#endif // SHAREDMEM_IMPL_H
