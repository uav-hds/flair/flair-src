// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file UdpSocket_impl.h
 * \brief Classe creant une socket
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2013/11/17
 * \version 4.0
 */

#ifndef UDPSOCKET_IMPL_H
#define UDPSOCKET_IMPL_H

#include <sys/socket.h>
#include <netdb.h>
#include <Object.h>
#ifdef __XENO__
#include <native/pipe.h>
#endif

namespace flair { namespace core {
  class UdpSocket;
} }

class UdpSocket_impl {
public:
  UdpSocket_impl(const flair::core::UdpSocket *self, std::string name,
              std::string address, bool broadcast = false);
  UdpSocket_impl(const flair::core::UdpSocket *self, std::string name, uint16_t port);
  ~UdpSocket_impl();

  void SendMessage(std::string message,int dst_id);
  void SendMessage(const char *msg, size_t msg_len,int dst_id);
  ssize_t RecvMessage(char *msg, size_t msg_len, flair::core::Time timeout,
                      char *src = NULL, size_t *src_len = NULL,int *src_id=NULL);

private:
  int fd;
  uint16_t port;
  std::string address;
  bool broadcast;
  void Init(void);
  const flair::core::UdpSocket *self;
  struct sockaddr_in *sock_in;
  size_t sock_in_size;
#ifdef __XENO__
  bool is_running;
  static void *user(void *arg);
  pthread_t user_thread;
  RT_PIPE pipe;
#endif
};

#endif // UDPSOCKET_IMPL_H
