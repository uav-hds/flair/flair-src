// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
#ifndef COMMUNICATION_H
#define COMMUNICATION_H

// messages data socket
#define XML_HEADER 0x3c
#define ZLIB_HEADER 0x78
#define WATCHDOG_HEADER 0x01
#define DATA_LITTLE_ENDIAN 0x02
#define DATA_BIG_ENDIAN 0x03
#define CLOSING_CONNECTION 0x04
#define MULTIPLE_DATA_LITTLE_ENDIAN 0x05
#define MULTIPLE_DATA_BIG_ENDIAN 0x06

//messages file socket
#define START_SENDING_FILES 0x10
#define FILE_INFO_LITTLE_ENDIAN 0x11
#define FILE_INFO_BIG_ENDIAN 0x12
#define END_SENDING_FILES 0x13

#endif // COMMUNICATION_H
