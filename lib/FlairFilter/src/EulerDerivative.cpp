// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2011/05/01
//  filename:   EulerDerivative.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    Class defining an euler derivative
//
//
/*********************************************************************/

#include "EulerDerivative.h"
#include "EulerDerivative_impl.h"
#include <Matrix.h>
#include <LayoutPosition.h>

using std::string;
using std::vector;
using namespace flair::core;
using namespace flair::gui;

namespace flair {
namespace filter {

EulerDerivative::EulerDerivative(const IODevice *parent,
                                 const LayoutPosition *position, string name,
                                 const Matrix *init_value)
    : IODevice(parent, name) {
  pimpl_ = new EulerDerivative_impl(this, position, name, init_value);
  SetIsReady(true);
}

EulerDerivative::EulerDerivative(const IODevice *parent,
                                 const LayoutPosition *position, string name,
								const std::vector<const Matrix *> *init_values)
				  : IODevice(parent, name) {
  pimpl_ = new EulerDerivative_impl(this, position, name, init_values);
  SetIsReady(true);
}				

EulerDerivative::~EulerDerivative() { delete pimpl_; }

Matrix *EulerDerivative::GetMatrix(int outputNumber) const { return dynamic_cast<Matrix*>(pimpl_->outputs.at(outputNumber)); }

float EulerDerivative::Output(int row, int col) const {
  return dynamic_cast<Matrix*>(pimpl_->outputs.at(0))->Value(row, col);
}

float EulerDerivative::Output(int outputNumber,int row, int col) const {
  return dynamic_cast<Matrix*>(pimpl_->outputs.at(outputNumber))->Value(row, col);
}


void EulerDerivative::UpdateFrom(const io_data *data) {
  pimpl_->UpdateFrom(data);
  ProcessUpdate(pimpl_->outputs.at(0));
}

void EulerDerivative::UpdateFrom(const vector<const io_data*>* datas) {
	pimpl_->UpdateFrom(datas);
  ProcessUpdate(&(pimpl_->outputs));
}


} // end namespace filter
} // end namespace flair
