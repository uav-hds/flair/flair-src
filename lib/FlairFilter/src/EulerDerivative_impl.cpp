// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2011/05/01
//  filename:   EulerDerivative.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    objet permettant le calcul d'une derivee d'Euler
//
//
/*********************************************************************/

#include "EulerDerivative.h"
#include "EulerDerivative_impl.h"
#include <Matrix.h>
#include <Layout.h>
#include <GroupBox.h>
#include <DoubleSpinBox.h>

using std::string;
using std::vector;
using namespace flair::core;
using namespace flair::gui;
using namespace flair::filter;

EulerDerivative_impl::EulerDerivative_impl(EulerDerivative *self,
                                           const LayoutPosition *position,
                                           string name,
                                           const Matrix *init_value) {
  CreateInitMatrix(self,init_value,name);
  ConstructorCommon(self,position,name);
}

EulerDerivative_impl::EulerDerivative_impl(EulerDerivative *self,
                                       const LayoutPosition *position,
                                       string name,
                                       const vector<const Matrix *> *init_values) {
  if (init_values != NULL) {
    for (size_t i = 0; i < init_values->size(); i++) {
			CreateInitMatrix(self,init_values->at(i),name);
		}
  } else {
    CreateInitMatrix(self,NULL,name);
  }
	
	ConstructorCommon(self,position,name);
}

void EulerDerivative_impl::CreateInitMatrix(EulerDerivative *self,const Matrix *init_value,string name) {
	Matrix *output,*prev_input,*prev_output;

	if (init_value != NULL) {
		// init output matrix of same size as init
		MatrixDescriptor *desc =new MatrixDescriptor(init_value->Rows(), init_value->Cols());

		for (int i = 0; i < init_value->Rows(); i++) {
			for (int j = 0; j < init_value->Cols(); j++) {
				desc->SetElementName(i, j, init_value->Name(i, j));
			}
		}
		output = new Matrix(self, desc,init_value->GetDataType().GetElementDataType(), name);
		for (int i = 0; i < init_value->Rows(); i++) {
			for (int j = 0; j < init_value->Cols(); j++) {
				output->SetValue(i, j, init_value->Value(i,j));
			}
		}
		delete desc;
	} else {
    // if NULL, assume dimension 1, and init=0
    MatrixDescriptor *desc = new MatrixDescriptor(1, 1);
    desc->SetElementName(0, 0, "output");
    output = new Matrix(self, desc, floatType, name);
    delete desc;
  }
	outputs.push_back(output);
  
  MatrixDescriptor *desc = new MatrixDescriptor(output->Rows(), output->Cols());
  prev_input = new Matrix(self, desc, output->GetDataType().GetElementDataType(), name);
  prev_output = new Matrix(self, desc, output->GetDataType().GetElementDataType(), name);
  delete desc;
  
  prev_inputs.push_back(prev_input);
  prev_outputs.push_back(prev_output);
}

void EulerDerivative_impl::ConstructorCommon(EulerDerivative *self,const LayoutPosition *position,string name) {
	this->self = self;
  first_update = true;
  
  // init UI
  GroupBox *reglages_groupbox = new GroupBox(position, name);
  T = new DoubleSpinBox(reglages_groupbox->NewRow(), "period, 0 for auto:"," s", 0, 1, 0.01);
  sat = new DoubleSpinBox(reglages_groupbox->NewRow(), "saturation, -1 to disable:",-1,100000,1,1,-1);
  
	for (size_t i = 0; i < outputs.size(); i++) {
		self->AddDataToLog(outputs.at(i));
	}
}

EulerDerivative_impl::~EulerDerivative_impl() {}

void EulerDerivative_impl::UpdateFrom(const vector<const io_data*>* datas) {
  for (size_t i = 0; i < datas->size(); i++) {
    const Matrix *input=dynamic_cast<const Matrix*>(datas->at(i));//output is created in this object, we know it is a matrix
    if (!input) {
      self->Warn("casting %s to Matrix failed\n",datas->at(i)->ObjectName().c_str());
      continue;
    }
		UpdateFrom(input,dynamic_cast<Matrix*>(outputs.at(i)),prev_inputs.at(i),prev_outputs.at(i));
	}
}

void EulerDerivative_impl::UpdateFrom(const io_data *data) {
  const Matrix *input=dynamic_cast<const Matrix*>(data);//output is created in this object, we know it is a matrix
  if (!input) {
      self->Warn("casting %s to Matrix failed\n",data->ObjectName().c_str());
      return;
  }
  UpdateFrom(input,dynamic_cast<Matrix*>(outputs.at(0)),prev_inputs.at(0),prev_outputs.at(0));
}

void EulerDerivative_impl::UpdateFrom(const Matrix *input,Matrix *output,Matrix *prev_input,Matrix *prev_output) {
  float delta_t;

  // on prend une fois pour toute les mutex et on fait des accès directs
  output->GetMutex();
  input->GetMutex();

  if (first_update == true) {
    for (int i = 0; i < input->Rows(); i++) {
      for (int j = 0; j < input->Cols(); j++) {
        prev_input->SetValueNoMutex(i, j, input->ValueNoMutex(i, j));
      }
    }
    first_update = false;
  } else {
    if (T->Value() == 0) {
      delta_t = (float)(input->DataDeltaTime()) / 1000000000.;
    } else {
      delta_t = T->Value();
    }

    if(delta_t!=0) {
      for (int i = 0; i < input->Rows(); i++) {
        for (int j = 0; j < input->Cols(); j++) {
          float result=(input->ValueNoMutex(i, j) - prev_input->ValueNoMutex(i, j)) / delta_t;
          if(sat->Value()!=-1) {
            if(result>sat->Value() && result>0) result=sat->Value();
            if(result<-sat->Value() && result<0) result=-sat->Value();
          }/* filter by acc
          float acc=result-prev_output->ValueNoMutex(i, j)/delta_t;
          if(acc>20 || acc<-20)  {
            printf("%s %f\n",self->ObjectName().c_str(),acc);
          } else {*/
            output->SetValueNoMutex(i, j, result);
          //}
          prev_output->SetValueNoMutex(i, j, result);
          prev_input->SetValueNoMutex(i, j, input->ValueNoMutex(i, j));
          
        }
      }
    }
  }

  input->ReleaseMutex();
  output->ReleaseMutex();

  output->SetDataTime(input->DataTime());
}
