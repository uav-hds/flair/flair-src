// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2011/05/01
//  filename:   LowPassFilter_impl.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    objet permettant le calcul d'un filtre passe bas
//
//
/*********************************************************************/

#include "LowPassFilter_impl.h"
#include "LowPassFilter.h"
#include <Matrix.h>
#include <Layout.h>
#include <GroupBox.h>
#include <SpinBox.h>
#include <DoubleSpinBox.h>
#include <typeinfo>
#define PI ((float)3.14159265358979323846)

using std::string;
using std::vector;
using namespace flair::core;
using namespace flair::gui;
using namespace flair::filter;

LowPassFilter_impl::LowPassFilter_impl(LowPassFilter *self,
                                       const LayoutPosition *position,
                                       string name,
                                       const Matrix *init_value) {
	CreateInitMatrix(self,init_value,name);
  ConstructorCommon(self,position,name);
}

LowPassFilter_impl::LowPassFilter_impl(LowPassFilter *self,
                                       const LayoutPosition *position,
                                       string name,
                                       const vector<const Matrix *> *init_values) {
  if (init_values != NULL) {
    for (size_t i = 0; i < init_values->size(); i++) {
			CreateInitMatrix(self,init_values->at(i),name);
		}
  } else {
    CreateInitMatrix(self,NULL,name);
  }
	
	ConstructorCommon(self,position,name);
}

void LowPassFilter_impl::CreateInitMatrix(LowPassFilter *self,const Matrix *init_value,string name) {
	Matrix *output;

	if (init_value != NULL) {
		// init output matrix of same size as init
		MatrixDescriptor *desc =new MatrixDescriptor(init_value->Rows(), init_value->Cols());

		for (int i = 0; i < init_value->Rows(); i++) {
			for (int j = 0; j < init_value->Cols(); j++) {
				desc->SetElementName(i, j, init_value->Name(i, j));
			}
		}
		output = new Matrix(self, desc,init_value->GetDataType().GetElementDataType(), name);
		for (int i = 0; i < init_value->Rows(); i++) {
			for (int j = 0; j < init_value->Cols(); j++) {
				output->SetValue(i, j, init_value->Value(i,j));
			}
		}
		delete desc;
	} else {
    // if NULL, assume dimension 1, and init=0
    MatrixDescriptor *desc = new MatrixDescriptor(1, 1);
    desc->SetElementName(0, 0, "output");
    output = new Matrix(self, desc, floatType, name);
    delete desc;
  }
	outputs.push_back(output);
}

void LowPassFilter_impl::ConstructorCommon(LowPassFilter *self,const LayoutPosition *position,string name) {
	// init UI
	GroupBox *reglages_groupbox = new GroupBox(position, name);
	T = new DoubleSpinBox(reglages_groupbox->NewRow(), "period, 0 for auto"," s",0, 10, 0.01);
	freq = new DoubleSpinBox(reglages_groupbox->NewRow(), "cutoff frequency"," Hz", 0, 10000, 0.1, 2, 1);
												 
	this->self = self;
	
	for (size_t i = 0; i < outputs.size(); i++) {
		self->AddDataToLog(outputs.at(i));
	}
}
	
LowPassFilter_impl::~LowPassFilter_impl() {}

void LowPassFilter_impl::UpdateFrom(const vector<const io_data*>* datas) {
  for (size_t i = 0; i < datas->size(); i++) {
    const Matrix *input=dynamic_cast<const Matrix*>(datas->at(i));//output is created in this object, we know it is a matrix
    if (!input) {
      self->Warn("casting %s to Matrix failed\n",datas->at(i)->ObjectName().c_str());
      continue;
    }
		UpdateFrom(input,dynamic_cast<Matrix*>(outputs.at(i)));
	}
}

void LowPassFilter_impl::UpdateFrom(const io_data *data) {
  const Matrix *input=dynamic_cast<const Matrix*>(data);//output is created in this object, we know it is a matrix
  if (!input) {
      self->Warn("casting %s to Matrix failed\n",data->ObjectName().c_str());
      return;
  }
  UpdateFrom(input,dynamic_cast<Matrix*>(outputs.at(0)));
}

void LowPassFilter_impl::UpdateFrom(const Matrix *input,Matrix *output) {
  float delta_t;

  // on prend une fois pour toute les mutex et on fait des accès directs
  output->GetMutex();
  input->GetMutex();

  if (T->Value() == 0) {
    delta_t = (float)(input->DataDeltaTime() ) / 1000000000.;
  } else {
    delta_t = T->Value();
  }

  if(delta_t!=0) {
    for (int i = 0; i < input->Rows(); i++) {
      for (int j = 0; j < input->Cols(); j++) {
        float cutoff=freq->Value();
        if (cutoff == 0 || freq->ValueChanged()) {
          output->SetValueNoMutex(i, j, input->ValueNoMutex(i, j));
        } else {
          output->SetValueNoMutex(i, j, (1 - 2 * PI * cutoff * delta_t) *
                                                output->ValueNoMutex(i, j) +
                                            2 * PI * cutoff * delta_t *
                                                input->ValueNoMutex(i, j));
        }
      }
    }
  }

  input->ReleaseMutex();
  output->ReleaseMutex();

  output->SetDataTime(input->DataTime());
}