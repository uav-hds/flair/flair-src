// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2021/04/15
//  filename:   PassthroughAhrs.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    Class for passthrough imu to ahrs. When imu can already delivers ahrs datas
//
//
/*********************************************************************/

#include "PassthroughAhrs.h"
#include <AhrsData.h>


using std::string;
using namespace flair::core;
using namespace flair::sensor;

namespace flair {
namespace filter {

PassthroughAhrs::PassthroughAhrs(Imu *imu,string name)
    : Ahrs(imu, name) {

  SetIsReady(true);
}

PassthroughAhrs::~PassthroughAhrs() {}

// datas outputed imu are AhrsData!
void PassthroughAhrs::UpdateFrom(const io_data *data) {
  AhrsData *input = (AhrsData *)data;
  AhrsData *output;
  GetDatas(&output);

  Quaternion quaternion;
  Vector3Df filteredAngRates;
  input->GetQuaternionAndAngularRates(quaternion, filteredAngRates);
  output->SetQuaternionAndAngularRates(quaternion, filteredAngRates);
  output->SetDataTime(input->DataTime());

  ProcessUpdate(output);
}

} // end namespace filter
} // end namespace flair
