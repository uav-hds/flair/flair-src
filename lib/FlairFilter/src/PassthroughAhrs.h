// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file PassthroughAhrs.h
 * \brief Class for passthrough imu to ahrs. When imu can already delivers ahrs datas
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2021/04/15
 * \version 4.0
 */

#ifndef PASSTHROUGHAHRS_H
#define PASSTHROUGHAHRS_H

#include <Ahrs.h>

namespace flair {
  namespace sensor {
    class Imu;
  }
}

namespace flair {
namespace filter {
/*! \class PassthroughAhrs
*
* \brief Class for passthrough imu to ahrs. When imu can already delivers ahrs datas
*
*/
class PassthroughAhrs : public Ahrs {
public:
  /*!
  * \brief Constructor
  *
  * Construct a PassthroughAhrs
  *
  * \param imu Imu
  * \param name name
  */
  PassthroughAhrs(sensor::Imu* imu,std::string name);

  /*!
  * \brief Destructor
  *
  */
  ~PassthroughAhrs();

private:
  /*!
  * \brief Update using provided datas
  *
  * Reimplemented from IODevice.
  *
  * \param data data from the parent to process
  */
  void UpdateFrom(const core::io_data *data) override;
};
} // end namespace filter
} // end namespace flair
#endif // PASSTHROUGHAHRS_H
