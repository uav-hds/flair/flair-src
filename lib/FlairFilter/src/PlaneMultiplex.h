// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file PlaneMultiplex.h
 * \brief Class defining plane multiplexing
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2022/01/05
 * \version 4.0
 */

#ifndef PLANEMULTIPLEX_H
#define PLANEMULTIPLEX_H

#include <UavMultiplex.h>

namespace flair {

    namespace gui {
        class ComboBox;
        class DataPlot1D;
        class DoubleSpinBox;
    }
    namespace core {
        class Matrix;
    }

}

namespace flair {
namespace filter {
/*! \class PlaneMultiplex
*
* \brief Class defining plane multiplexing
*
* The output order of the motors can be changed through ground station.
*/
class PlaneMultiplex : public UavMultiplex {
 
public:

  /*!
  \enum MotorNames_t
  \brief Motor names
  */
  typedef enum {
    FrontMotor = 0,    
    AileronRight = 1,
    AileronLeft = 2,
  } MotorNames_t;

  /*!
  * \brief Constructor
  *
  * Construct a autogyre multiplexing. It will be child of the FrameworkManager.
  *
  * \param name name
  * \param type type
  */
  PlaneMultiplex(std::string name);

  /*!
  * \brief Destructor
  *
  */
  ~PlaneMultiplex();

  /*!
  * \brief Use default plot
  *
  * Plot the output values.
  *
  */
  void UseDefaultPlot(void);

  /*!
  * \brief Motors count
  *
  * Reimplemented from UavMultiplex.
  *
  * \return motors count
  */
  uint8_t MotorsCount(void) const;
  
  core::IODevice* GetBldcMultiplex(void) const;
  core::IODevice* GetServosMultiplex(void) const;

private:
  /*!
  * \brief Update using provided datas
  *
  * Reimplemented from IODevice.
  *
  * \param data data from the parent to process
  */
  void UpdateFrom(const core::io_data *data) override;
  
  std::string ActuatorName(int index);
 
  core::Matrix *output;
  gui::DataPlot1D *plots[3];
  core::IODevice *bldc,*servos;

};
} // end namespace filter
} // end namespace flair
#endif // PLANEMULTIPLEX_H
