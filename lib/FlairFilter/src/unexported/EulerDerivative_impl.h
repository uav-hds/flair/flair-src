// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file EulerDerivative.h
 * \brief Classe permettant le calcul d'une derivee d'Euler
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2011/05/01
 * \version 4.0
 */

#ifndef EULERDERIVATIVE_IMPL_H
#define EULERDERIVATIVE_IMPL_H

#include <IODevice.h>

namespace flair {
namespace core {
class Matrix;
}
namespace gui {
class LayoutPosition;
class DoubleSpinBox;
}
namespace filter {
class EulerDerivative;
}
}

/*! \class EulerDerivative
* \brief Classe permettant le calcul d'une derivee d'Euler
*/

class EulerDerivative_impl {
public:
  EulerDerivative_impl(flair::filter::EulerDerivative *self,
                       const flair::gui::LayoutPosition *position,
                       std::string name,
                       const flair::core::Matrix *init_value = NULL);
  EulerDerivative_impl(flair::filter::EulerDerivative *self,
                     const flair::gui::LayoutPosition *position,
                     std::string name,
                     const std::vector<const flair::core::Matrix *> *init_values);
  ~EulerDerivative_impl();
  void ConstructorCommon(flair::filter::EulerDerivative *self,
                     const flair::gui::LayoutPosition *position,
                     std::string name);
	void CreateInitMatrix(flair::filter::EulerDerivative *self,const flair::core::Matrix *init_value,std::string name);
  void UpdateFrom(const flair::core::io_data *data);
  void UpdateFrom(const std::vector<const flair::core::io_data*>* datas);
	std::vector<flair::core::io_data*> outputs;

private:
  flair::gui::DoubleSpinBox *T,*sat;
  bool first_update;
  std::vector<flair::core::Matrix*> prev_inputs,prev_outputs;
  flair::filter::EulerDerivative *self;
  void UpdateFrom(const flair::core::Matrix *input,flair::core::Matrix *output,flair::core::Matrix *prev_input,flair::core::Matrix *prev_output);
};

#endif // EULERDERIVATIVE_IMPL_H
