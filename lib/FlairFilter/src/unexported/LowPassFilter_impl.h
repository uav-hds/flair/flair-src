// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file LowPassFilter_impl.h
 * \brief Classe permettant le calcul d'un filtre passe bas
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2011/05/01
 * \version 4.0
 */

#ifndef LOWPASSFILTER_FILTER_IMPL_H
#define LOWPASSFILTER_FILTER_IMPL_H

#include <IODevice.h>

namespace flair {
namespace core {
class Matrix;
}
namespace gui {
class LayoutPosition;
class SpinBox;
class DoubleSpinBox;
}
namespace filter {
class LowPassFilter;
}
}

class LowPassFilter_impl {

public:
  LowPassFilter_impl(flair::filter::LowPassFilter *self,
                     const flair::gui::LayoutPosition *position,
                     std::string name,
                     const flair::core::Matrix *init_value = NULL);
	LowPassFilter_impl(flair::filter::LowPassFilter *self,
                     const flair::gui::LayoutPosition *position,
                     std::string name,
                     const std::vector<const flair::core::Matrix *> *init_values);
  ~LowPassFilter_impl();
	void ConstructorCommon(flair::filter::LowPassFilter *self,
                     const flair::gui::LayoutPosition *position,
                     std::string name);
	void CreateInitMatrix(flair::filter::LowPassFilter *self,const flair::core::Matrix *init_value,std::string name);
  void UpdateFrom(const flair::core::io_data *data);
  void UpdateFrom(const std::vector<const flair::core::io_data*>* datas);
	std::vector<flair::core::io_data*> outputs;
	
private:
  flair::gui::DoubleSpinBox *freq, *T;
  flair::filter::LowPassFilter *self;
  void UpdateFrom(const flair::core::Matrix *input,flair::core::Matrix *output);
	
};

#endif // LOWPASSFILTER_FILTER_IMPL_H
