// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2021/03/03
//  filename:   IpcAhrsSensor.cpp
//
//  author:     Sébastien Ambroziak
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    Class for a simulation ahrs
//
//
/*********************************************************************/

#include "IpcAhrsSensor.h"
#include "ipc_receive.h"
#include <FrameworkManager.h>
#include <AhrsData.h>

using std::string;
using namespace flair::core;
using namespace flair::sensor;

namespace flair {
namespace filter {

IpcAhrsSensor::IpcAhrsSensor(string name, uint8_t priority, const char* ipc_name, int ipc_channel)
    : Ahrs(nullptr, name), Thread(getFrameworkManager(), name, priority) {
  receiver = new IpcReceiver<ipc::type::ahrs>(ipc_name,ipc_channel);
  SetIsReady(true);
}

IpcAhrsSensor::~IpcAhrsSensor() {} 
// datas from SimulatedImu are AhrsData!
void IpcAhrsSensor::Run(void) {
  AhrsData *output;
  GetDatas(&output);

  while (!ToBeStopped()) {

  Quaternion quaternion;
  Vector3Df filteredAngRates;

  receiver->receive();
  quaternion.q0 = receiver->getValue().quaternion.w;
  quaternion.q1 = receiver->getValue().quaternion.x;
  quaternion.q2 = receiver->getValue().quaternion.y;
  quaternion.q3 = receiver->getValue().quaternion.z;
  filteredAngRates.x = receiver->getValue().angular_rate.x;
  filteredAngRates.y = receiver->getValue().angular_rate.y;
  filteredAngRates.z = receiver->getValue().angular_rate.z;

  output->SetQuaternionAndAngularRates(quaternion, filteredAngRates);
  output->SetDataTime(receiver->getTimestamp().toNanosec());

  ProcessUpdate(output);
  }
}

} // end namespace filter
} // end namespace flair
