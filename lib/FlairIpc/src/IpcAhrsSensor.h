// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file IpcAhrsSensor.h
 * \brief Class for an ipc Ahrs
 * \author Sébastien Ambroziak, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2021/03/03
 * \version 4.0
 */

#ifndef IPCAHRSSENSOR_H
#define IPCAHRSSENSOR_H

#include <Ahrs.h>
#include <Thread.h>

template <typename T>
class IpcReceiver;

namespace ipc{
  namespace type{
    struct ahrs;
  }
}

namespace flair {
namespace filter {
/*! \class IpcAhrs
*
* \brief Class for an ipc Ahrs
*
*/
class IpcAhrsSensor : public filter::Ahrs, public core::Thread {
public:
  /*!
  * \brief Constructor
  *
  * Construct an ipc Ahrs. It will be child of the FrameworkManager.
  *
  * \param name name
  * \param priority priority of the SimulatedImu Thread
  * \param ipc_name name of the ipc
  * \param ipc_channel ipc channel number
  */
  IpcAhrsSensor(std::string name, uint8_t priority, const char* ipc_name, int ipc_channel);

  /*!
  * \brief Destructor
  *
  */
  ~IpcAhrsSensor();

private:
  /*!
  * \brief Run function
  *
  * Reimplemented from Thread.
  *
  */
  void Run(void) override;

  /*!
  * \brief Update using provided datas
  *
  * Reimplemented from IODevice.
  *
  * \param data data from the parent to process
  */
  void UpdateFrom(const core::io_data *data) override{};

  IpcReceiver<ipc::type::ahrs>* receiver;
};
} // end namespace filter
} // end namespace flair
#endif // IPCAHRSSENSOR_H
