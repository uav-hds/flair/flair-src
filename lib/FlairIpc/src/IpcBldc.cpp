// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2021/03/03
//  filename:   IpcBldc.cpp
//
//  author:     Sébastien Ambroziak
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    Class for a simulation bldc
//
//
/*********************************************************************/

#include "IpcBldc.h"
#include "shm_send.h"
#include <FrameworkManager.h>
#include <GridLayout.h>
#include <Matrix.h>

using namespace flair::core;
using namespace flair::gui;

namespace flair {
namespace actuator {

IpcBldc::IpcBldc(const IODevice *parent, Layout *layout, string name,
                   uint8_t motors_count, const char* ipc_name, int ipc_channel)
    : Bldc(parent, layout, name, motors_count) {


  //sender = new IpcSenderTab<uint16_t>(motors_count,ipc_name,ipc_channel);
  sender = new ShmSenderTab<float>(motors_count,ipc_name,ipc_channel);
  SetIsReady(true);
}

IpcBldc::~IpcBldc() {
  
}

void IpcBldc::SetMotors(float *value) {

  sender->send(value);
  
  // on prend une fois pour toute le mutex et on fait des accès directs
  output->GetMutex();
  for (int i = 0; i < MotorsCount(); i++) {
    output->SetValueNoMutex(i, 0, value[i]);
  }
  output->ReleaseMutex();
}

} // end namespace sensor
} // end namespace flair