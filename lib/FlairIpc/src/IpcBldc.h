// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file IpcBldc.h
 * \brief Class for a ipc bldc
 * \author Sébastien Ambroziak, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2021/03/03
 * \version 4.0
 */

#ifndef IPCBLDC_H
#define IPCBLDC_H

#include <Bldc.h>

namespace flair {
namespace core {
class IODevice;
}
namespace gui {
class Layout;
}
}

template <typename T>
class ShmSenderTab;

namespace flair {
namespace actuator {
/*! \class IpcBldc
*
* \brief Class for an ipc bldc
*
*/
class IpcBldc : public Bldc {
public:
  /*!
  * \brief Constructor
  *
  * Construct an IpcBldc
  *
  * \param parent parent
  * \param layout layout
  * \param name name
  * \param motors_count number of motors
  * \param ipc_name name of the ipc
  * \param ipc_channel ipc channel number
  */
  IpcBldc(const core::IODevice *parent, gui::Layout *layout, std::string name,
           uint8_t motors_count, const char* ipc_name, int ipc_channel);

  /*!
  * \brief Destructor
  *
  */
  ~IpcBldc();

  /*!
  * \brief Has speed measurement
  *
  * Reimplemented from Bldc. \n
  *
  * \return true if it has speed measurement
  */
  bool HasSpeedMeasurement(void) const  override{ return false; };

  /*!
  * \brief Has current measurement
  *
  * Reimplemented from Bldc. \n
  *
  * \return true if it has current measurement
  */
  bool HasCurrentMeasurement(void) const  override{ return false; };

private:
  /*!
  * \brief Set motors values
  *
  * Reimplemented from Bldc. \n
  * Values size must be the same as MotorsCount()
  *
  * \param values motor values
  */
  void SetMotors(float *value) override;
  
  
  //IpcSenderTab<uint16_t> *sender;
  ShmSenderTab<float> *sender;

};
} // end namespace actuator
} // end namespace flair
#endif // IPCBLDC_H
