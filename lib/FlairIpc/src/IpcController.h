// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2021/03/03
//  filename:   IpcController.h
//
//  author:     Sébastien Ambroziak
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    class that gets remote controls through an ethernet connection.
//              Typical use case: a remote control is plugged in a workstation
//              and sends remote control
//              data to a distant target (this class) through Wifi
//
//
/*********************************************************************/

#ifndef IPCCONTROLLER_H
#define IPCCONTROLLER_H

#include "TargetController.h"

namespace flair {
namespace core {
class FrameworkManager;
class Matrix;
class TcpSocket;
class UdpSocket;
}
namespace gui {
class Tab;
class TabWidget;
class DataPlot1D;
}
}

namespace flair {
namespace sensor {
/*! \class TargetController
*
* \brief Base Class for target side remote controls
*
*/
class IpcController : public TargetController {
public:
  IpcController(std::string name,
                      uint16_t port, uint8_t priority = 0);
  ~IpcController();
  // void DrawUserInterface();
protected:
  bool IsConnected() const override;
  // axis stuff
  std::string GetAxisName(unsigned int axisId) const override;
  // button stuff
  std::string GetButtonName(unsigned int axisId) const override;
  // controller state stuff
  bool ProcessMessage(core::Message *msg) override;
  bool IsControllerActionSupported(ControllerAction action) const override;

  bool IsDataFrameReady() override;
  void AcquireAxisData(core::Matrix &axis) override;     // responsible for getting the
                                                  // axis data from the hardware
  void AcquireButtonData(core::Matrix &button) override; // responsible for getting the
                                                  // button data from the
                                                  // hardware

  bool ControllerInitialization() override;

private:
  uint16_t readBits(uint8_t offsetInBits, uint8_t valueSizeInBits, char *buffer,
                    size_t bufferSize);
  uint8_t getByteOrNull(char *buffer, int byte, size_t bufferSize);
  uint32_t charBufferToUint32(char *buffer, size_t bufferSize);
  core::TcpSocket *listeningSocket;
  int listeningPort;
  core::TcpSocket *controlSocket = nullptr;
  core::UdpSocket *dataSocket;
  std::string *axisName = nullptr;
  std::string *buttonName = nullptr;
  size_t dataFrameSize;
  char *dataFrameBuffer;
  char *receiveFrameBuffer;
  size_t receiveCurrentPosition;
  uint8_t buttonOffset;
};
}
}

#endif // IPCCONTROLLER_H
