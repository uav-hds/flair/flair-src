// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file IpcImu.h
 * \brief Class for an ipc Imu
 * \author Sébastien Ambroziak, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2021/03/03
 * \version 4.0
 */

#ifndef IPCIMU_H
#define IPCIMU_H

#include <Imu.h>
#include <Thread.h>

namespace flair {
  namespace core {
    class AhrsData;
  }
  namespace gui {
    class SpinBox;
  }
}

namespace ipc{
  namespace type{
    struct imu;
    struct ahrs;
    struct vector3D_32;
    struct vector4D_32;
  }
}

template <typename T>
class IpcReceiver;

namespace flair {
namespace sensor {
/*! \class IpcImu
*
* \brief Class for an Ipc Imu
*/
class IpcImu : public Imu, public core::Thread {
public:
  /*!
  * \brief Constructor
  *
  * Construct an IpcImu.
	* It will be child of the FrameworkManager.
  *
  * \param name name
  * \param priority priority of the Thread
  * \param ipc_name name of the ipc
  * \param ipc_channel ipc channel number
  * \param AhrsOnly AhrsOnly
  */
  IpcImu(std::string name, uint8_t priority,
           const char* ipc_name, int ipc_channel, bool AhrsOnly=true);

  /*!
  * \brief Destructor
  *
  */
  ~IpcImu();

private:
  /*!
  * \brief Run function
  *
  * Reimplemented from Thread.
  *
  */
  void Run(void) override;

  /*!
  * \brief Update using provided datas
  *
  * Reimplemented from IODevice.
  *
  * \param data data from the parent to process
  */
  void UpdateFrom(const core::io_data *data) override{};

  typedef struct {
    float q0;
    float q1;
    float q2;
    float q3;
    float wx;
    float wy;
    float wz;
    float ax;
    float ay;
    float az;
    float mx;
    float my;
    float mz;
  } imu_states_t;

  IpcReceiver<ipc::type::imu>* ImuReceiver;
  IpcReceiver<ipc::type::ahrs>* AhrsReceiver;
  IpcReceiver<ipc::type::vector4D_32>* orientation_quat;
  IpcReceiver<ipc::type::vector3D_32>* angular_vel;
  gui::SpinBox *dataRate;
  core::AhrsData *ahrsData;
  bool _AhrsOnly;
};
} // end namespace sensor
} // end namespace flair
#endif // IPCIMU_H
