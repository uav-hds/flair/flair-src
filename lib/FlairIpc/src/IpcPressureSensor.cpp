// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2021/03/03
//  filename:   IpcPressureSensor.cpp
//
//  author:     Sébastien Ambroziak
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    Class for an ipc pressure sensor
//
//
/*********************************************************************/

#include "IpcPressureSensor.h"
#include "shm_receive.h"
#include <FrameworkManager.h>
#include <SpinBox.h>
#include <GroupBox.h>
#include <Matrix.h>


using namespace flair::core;
using namespace flair::gui;

namespace flair {
namespace sensor {

IpcPressureSensor::IpcPressureSensor(string name,
               uint8_t priority, const char* ipc_name, int ipc_channel)
    : Thread(getFrameworkManager(), name, priority), PressureSensor( name) {
  data_rate =
      new SpinBox(GetGroupBox()->NewRow(), "data rate", " Hz", 1, 500, 1, 50);

  receiver = new ShmReceiver<float>(ipc_name,ipc_channel);

  SetIsReady(true);
}


IpcPressureSensor::~IpcPressureSensor() {
  SafeStop();
  Join();
}


void IpcPressureSensor::Run(void) {

  SetPeriodUS((uint32_t)(1000000. / data_rate->Value()));

  while (!ToBeStopped()) {
    WaitPeriod();

    receiver->receive();
    //shmem->Read((char *)&p, sizeof(float));

    if (data_rate->ValueChanged() == true) {
      SetPeriodUS((uint32_t)(1000000. / data_rate->Value()));
    }

    output->SetValue(0, 0, receiver->getValue());
    output->SetDataTime(receiver->getTimestamp().toNanosec());
    ProcessUpdate(output);
  }
}

} // end namespace sensor
} // end namespace flair
