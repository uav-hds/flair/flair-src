// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file IpcPressureSensor.h
 * \brief Class for an ipc PressureSensor
 * \author Sébastien Ambroziak, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2021/03/03
 * \version 4.0
 */

#ifndef IPCPRESSURESENSOR_H
#define IPCPRESSURESENSOR_H

#include <PressureSensor.h>
#include <Thread.h>

namespace flair {
namespace core {
class SharedMem;
}
namespace gui {
class SpinBox;
}
}

template <typename T>
class ShmReceiver;

namespace flair {
namespace sensor {
/*! \class IpcPressureSensor
*
* \brief Class for an ipc PressureSensor
*/
class IpcPressureSensor : public core::Thread, public PressureSensor {
public:
  /*!
  * \brief Constructor
  *
  * Construct an IpcPressureSensor.
	* It will be child of the FrameworkManager.
  *
  * \param name name
  * \param priority priority of the Thread
  * \param ipc_name name of the ipc
  * \param ipc_channel ipc channel number
  */
  IpcPressureSensor(std::string name,
         uint8_t priority, const char* ipc_name, int ipc_channel);

  /*!
  * \brief Destructor
  *
  */
  ~IpcPressureSensor();

protected:
  /*!
  * \brief SharedMem to access datas
  *
  */

private:
  /*!
  * \brief Update using provided datas
  *
  * Reimplemented from IODevice.
  *
  * \param data data from the parent to process
  */
  void UpdateFrom(const core::io_data *data) override{};

  /*!
  * \brief Run function
  *
  * Reimplemented from Thread.
  *
  */
  void Run(void) override;

  gui::SpinBox *data_rate;
  ShmReceiver<float>* receiver;
};
} // end namespace sensor
} // end namespace flair
#endif // IPCPRESSURESENSOR_H
