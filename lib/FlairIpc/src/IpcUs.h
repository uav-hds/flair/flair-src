// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file IpcUs.h
 * \brief Class for an ipc UsRangeFinder
 * \author Sébastien Ambroziak Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2021/03/03
 * \version 4.0
 */

#ifndef IPCUS_H
#define IPCUS_H

#include <UsRangeFinder.h>
#include <Thread.h>

namespace flair {
namespace gui {
class SpinBox;
}
}

template <typename T>
class ShmReceiver;

template <typename T>
class IpcReceiver;

namespace flair {
namespace sensor {
/*! \class IpcUs
*
* \brief Class for a simulation UsRangeFinder
*/
class IpcUs : public core::Thread, public UsRangeFinder {
public:
  /*!
  * \brief Constructor
  *
  * Construct an IpcUs.
	* It will be child of the FrameworkManager.
  *
  * \param name name
  * \param modelId Model id
  * \param ipc_name name of the ipc
  * \param ipc_channel ipc channel number
  
  */
  IpcUs(std::string name, uint8_t priority, const char* ipc_name, int ipc_channel);

  /*!
  * \brief Destructor
  *
  */
  ~IpcUs();


private:
  /*!
  * \brief Update using provided datas
  *
  * Reimplemented from IODevice.
  *
  * \param data data from the parent to process
  */
  void UpdateFrom(const core::io_data *data) override{};

  /*!
  * \brief Run function
  *
  * Reimplemented from Thread.
  *
  */
  void Run(void) override;
  
  gui::SpinBox *data_rate;
  ShmReceiver<float>* receiver;
};
} // end namespace sensor
} // end namespace flair
#endif // IPCUS_H
