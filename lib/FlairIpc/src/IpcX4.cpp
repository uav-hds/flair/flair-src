// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2021/03/03
//  filename:   IpcX4.cpp
//
//  author:     Sébastien Ambroziak
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    Class defining an x4 uav using ipc data
//
//
/*********************************************************************/

#include <IpcX4.h>
#include <IpcBldc.h>
#include <IpcUs.h>
#include <PassthroughAhrs.h>
#include <IpcImu.h>
#include <BatteryMonitor.h>
#include <FrameworkManager.h>
#include <X4X8Multiplex.h>
#include <Tab.h>
//#include <FindArgument.h>


using std::string;
using namespace flair::core;
using namespace flair::gui;
using namespace flair::sensor;
using namespace flair::filter;
using namespace flair::actuator;

namespace flair {
namespace meta {

IpcX4::IpcX4(string name,string options,
               filter::UavMultiplex *multiplex)
    : Uav(name, multiplex) {

  if (multiplex == NULL)
    SetMultiplex(new X4X8Multiplex("motors", X4X8Multiplex::X4));

  SetBldc(new IpcBldc(GetUavMultiplex(), GetUavMultiplex()->GetLayout(),"motors", GetUavMultiplex()->MotorsCount(),"motors",65));
  
  SetUsRangeFinder(new IpcUs("us", 70, "altitude", 65));
 
  SetAhrs(new PassthroughAhrs(new IpcImu("imu",70,"imu",65),"ahrs"));

  Tab *bat_tab = new Tab(getFrameworkManager()->GetTabWidget(), "battery");
  SetBatteryMonitor(new BatteryMonitor(bat_tab->NewRow(), "battery"));
  GetBatteryMonitor()->SetBatteryValue(12);
  
}

IpcX4::~IpcX4() {}

void IpcX4::StartSensors(void) {
  ((IpcImu *)(GetAhrs()->GetImu()))->Start();
  ((IpcUs *)GetUsRangeFinder())->Start();
  
}


} // end namespace meta
} // end namespace flair