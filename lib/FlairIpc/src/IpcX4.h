// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file IpcX4.h
 * \brief Class defining an ipc x4 uav
 * \author Sébastien Ambroziak, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2021/03/03
 * \version 4.0
 */

#ifndef IPCX4_H
#define IPCX4_H

#include "Uav.h"

namespace flair {
namespace meta {
  
/*! \class IpcX4
*
* \brief Class defining an ipc x4 uav
*/
class IpcX4 : public Uav {
  public:
    IpcX4(std::string name,std::string options="",
           filter::UavMultiplex *multiplex = NULL);
    ~IpcX4();
    void StartSensors(void) override;
    virtual std::string GetType(void) const override{return "x4_ipc";}

  private:
  
};
} // end namespace meta
} // end namespace flair
#endif // IPCX4_H
