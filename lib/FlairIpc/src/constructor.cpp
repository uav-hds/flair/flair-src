// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2021/03/03
//  filename:   constructor.cpp
//
//  author:     Sébastien Ambroziak
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    constructor for ipc lib
//
//
/*********************************************************************/

#include <stdio.h>
#include <UavFactory.h>
#include "IpcX4.h"
#include "compile_info.h"

static void constructor() __attribute__((constructor));

using namespace std;
using namespace flair::core;
using namespace flair::filter;
using namespace flair::meta;

Uav *CreateIpcX4(string name, string uav_type,string options,
               UavMultiplex *multiplex) {
  if(uav_type=="x4_ipc") {
    return new IpcX4(name,options,multiplex);
  } else {
    return NULL;
  }
}

void constructor() {
  compile_info("x4_ipc");
  RegisterUavCreator(CreateIpcX4);
}
