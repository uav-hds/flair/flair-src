PROJECT(FlairMeta)
cmake_minimum_required(VERSION 2.8)
include($ENV{FLAIR_ROOT}/flair-src/cmake-modules/GlobalCmakeFlair.cmake)

INCLUDE_DIRECTORIES(
	${CMAKE_CURRENT_SOURCE_DIR}/../FlairCore/src
	${CMAKE_CURRENT_SOURCE_DIR}/../FlairSensorActuator/src
	${CMAKE_CURRENT_SOURCE_DIR}/../FlairFilter/src
	${CMAKE_CURRENT_SOURCE_DIR}/src/unexported
	${CMAKE_CURRENT_SOURCE_DIR}/src/
)

FILE(GLOB SRC_FILES "${CMAKE_CURRENT_SOURCE_DIR}/src/*.cpp")

FLAIR_LIB(${PROJECT_NAME} "${SRC_FILES}"
        INCLUDES_DEST_DIR ${PROJECT_NAME}
)
