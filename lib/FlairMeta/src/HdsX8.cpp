// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2015/02/08
//  filename:   HdsX8.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    Class defining a HDS X8 uav
//
//
/*********************************************************************/
#ifdef ARMV7A

#include "HdsX8.h"
#include <FrameworkManager.h>
#include <RTDM_I2cPort.h>
#include <RTDM_SerialPort.h>
#include <Srf08.h>
#include <PassthroughAhrs.h>
#include <Gx3_25_imu.h>
#include <BlCtrlV2.h>
#include <X4X8Multiplex.h>
#include <Ps3Eye.h>
#include <Mb800.h>
#include <FindArgument.h>

using std::string;
using namespace flair::core;
using namespace flair::sensor;
using namespace flair::filter;
using namespace flair::actuator;

namespace flair {
namespace meta {

HdsX8::HdsX8(string name,string options,
             filter::UavMultiplex *multiplex)
    : Uav(name, multiplex) {
  RTDM_I2cPort *i2cport = new RTDM_I2cPort(getFrameworkManager(), "rtdm_i2c", "rti2c3");
  RTDM_SerialPort *imu_port = new RTDM_SerialPort(getFrameworkManager(), "imu_port", "rtser1");

  if (multiplex == NULL)
    SetMultiplex(new X4X8Multiplex( "motors", X4X8Multiplex::X8));

  SetBldc(new BlCtrlV2(GetUavMultiplex(), GetUavMultiplex()->GetLayout(),
                       "motors", GetUavMultiplex()->MotorsCount(), i2cport));
  SetUsRangeFinder(new Srf08("SRF08", i2cport, 0x70, 60));
 
  SetAhrs(new PassthroughAhrs(new Gx3_25_imu("imu",imu_port,Gx3_25_imu::AccelerationAngularRateAndOrientationMatrix, 70),"ahrs"));
  SetBatteryMonitor(((BlCtrlV2 *)GetBldc())->GetBatteryMonitor());
  
  if(Image::IsUsingDefaultAllocAndFree()) {
    SetVerticalCamera(new Ps3Eye("camv", 0, false,50));//no cmem and no dsp
  } else {
    SetVerticalCamera(new Ps3Eye("camv", 0, true,50));//cmem and dsp
  }
  
  string useGps=FindArgument(options,"use_gps=",false);
  if(useGps=="true") {
    RTDM_SerialPort *gps_port = new RTDM_SerialPort(getFrameworkManager(), "gps_port", "rtser2");
    SetGps(new Mb800("gps",gps_port,(NmeaGps::NMEAFlags_t)(NmeaGps::GGA|NmeaGps::VTG|NmeaGps::GSA),40));
  } 
}

HdsX8::~HdsX8() {}

void HdsX8::StartSensors(void) {
  ((Gx3_25_imu *)(GetAhrs()->GetImu()))->Start();
  ((Srf08 *)GetUsRangeFinder())->Start();
  ((Ps3Eye *)GetVerticalCamera())->Start();
  if(GetGps()) ((Mb800 *)GetGps())->Start();
}

bool HdsX8::isReadyToFly(void) const {
  return GetAhrs()->GetImu()->IsReady();
}

} // end namespace meta
} // end namespace flair

#endif
