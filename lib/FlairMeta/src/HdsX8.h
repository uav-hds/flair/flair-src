// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file HdsX8.h
 * \brief Class defining a HDS X8 uav
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2016/02/08
 * \version 4.0
 */

#ifndef HDSX8_H
#define HDSX8_H

#include "Uav.h"

namespace flair {
namespace meta {
/*! \class HdsX8
*
* \brief Class defining a HDS X8 uav
*/
class HdsX8 : public Uav {
public:
  HdsX8(std::string name,std::string options="",
        filter::UavMultiplex *multiplex = NULL);
  ~HdsX8();
  void StartSensors(void) override;
	std::string GetDefaultVrpnAddress(void) const override{return "172.26.213.1:3883";}
  bool isReadyToFly(void) const override;
  std::string GetType(void) const override{return "hds_x8";}

private:
};
} // end namespace meta
} // end namespace flair
#endif // HDSX8_H
