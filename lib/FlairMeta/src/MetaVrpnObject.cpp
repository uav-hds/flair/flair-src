// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2013/04/08
//  filename:   MetaVrpnObject.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    objet integrant objet vrpn et une dérivée
//              d'euler
//
//
/*********************************************************************/

#include "MetaVrpnObject.h"
#include <EulerDerivative.h>
#include <LowPassFilter.h>
#include <IODevice.h>
#include <GridLayout.h>
#include <DataPlot1D.h>
#include <DataPlot2D.h>
#include <Tab.h>
#include <TabWidget.h>
#include <Matrix.h>
#include <AltitudeSensor.h>

using std::string;
using namespace flair::core;
using namespace flair::gui;
using namespace flair::sensor;
using namespace flair::filter;

namespace flair {
namespace sensor {
class MetaVrpnObjectAltitudeSensor : public AltitudeSensor {
  public:
    MetaVrpnObjectAltitudeSensor(meta::MetaVrpnObject* parent,std::string name): AltitudeSensor(parent,name) {
      this->parent=parent;
    }
    ~MetaVrpnObjectAltitudeSensor() {
    }

    //z and dz must be in uav's frame
    float z(void) const {
      Vector3Df uav_pos;
      parent->GetPosition(uav_pos);
      return -uav_pos.z;
    }
    float Vz(void) const {
      Vector3Df uav_vel;
      parent->GetSpeed(uav_vel);
      return -uav_vel.z;
    }

  private:
     meta::MetaVrpnObject* parent;
      
};
}// end namespace sensor
}// end namespace flair

namespace flair {
namespace meta {

MetaVrpnObject::MetaVrpnObject(string name,VrpnClient *client)
    : VrpnObject( name, client->GetTabWidget(),client) {
  ConstructorCommon(name,client);
}

MetaVrpnObject::MetaVrpnObject(string name,uint8_t id,VrpnClient *client)
    : VrpnObject(name, id, client->GetTabWidget(),client) {
  ConstructorCommon( name,client);
}

void MetaVrpnObject::ConstructorCommon(string name,VrpnClient *client) {
  std::vector<Matrix *> init_values;
  
  MatrixDescriptor *desc = new MatrixDescriptor(3, 1);
  for (int i = 0; i < desc->Rows(); i++) {
    desc->SetElementName(i, 0, NEDPosition::Output()->Name(i, 0));
  }
  Matrix *prev_value_pos = new Matrix(this, desc, floatType, name);
  init_values.push_back(prev_value_pos);
  delete desc;
  
  desc = new MatrixDescriptor(4, 1);
  for (int i = 0; i < desc->Rows(); i++) {
    desc->SetElementName(i, 0, VrpnObject::Output()->Name(i, 0));
  }
  Matrix *prev_value_quat = new Matrix(this, desc, floatType, name);
  init_values.push_back(prev_value_quat);
  delete desc;

  pbas = new LowPassFilter(this, client->GetLayout()->NewRow(),name + " Passe bas", (const std::vector<const Matrix *>*)&init_values);
  delete prev_value_pos;
  delete prev_value_quat;
  init_values.clear();

  desc = new MatrixDescriptor(3, 1);
  for (int i = 0; i < desc->Rows(); i++) {
    desc->SetElementName(i, 0,  "d" +NEDPosition::Output()->Name(i, 0));
  }
  prev_value_pos = new Matrix(this, desc, floatType, name);
  init_values.push_back(prev_value_pos);
  delete desc;
  
  desc = new MatrixDescriptor(4, 1);
  for (int i = 0; i < desc->Rows(); i++) {
    desc->SetElementName(i, 0,  "d" +VrpnObject::Output()->Name(i, 0));
  }
  prev_value_quat = new Matrix(this, desc, floatType, name);
  init_values.push_back(prev_value_quat);
  delete desc;
  
  euler = new EulerDerivative(pbas, client->GetLayout()->NewRow(), name + "_euler", (const std::vector<const Matrix *>*)&init_values);
  delete prev_value_pos;
  delete prev_value_quat;

  vx_opti_plot = new DataPlot1D(GetPlotTab()->NewRow(), "vx", -3, 3);
  vx_opti_plot->AddCurve(euler->GetMatrix(0)->Element(0));
  vy_opti_plot = new DataPlot1D(GetPlotTab()->LastRowLastCol(), "vy", -3, 3);
  vy_opti_plot->AddCurve(euler->GetMatrix(0)->Element(1));
  vz_opti_plot = new DataPlot1D(GetPlotTab()->LastRowLastCol(), "vz", -2, 2);
  vz_opti_plot->AddCurve(euler->GetMatrix(0)->Element(2));

  plot_tab = new Tab(client->GetTabWidget(), "Mesures (xy) " + name);
  xy_plot = new DataPlot2D(plot_tab->NewRow(), "xy", "y", -5, 5, "x", -5, 5);
  xy_plot->AddCurve(NEDPosition::Output()->Element(1), NEDPosition::Output()->Element(0));

  altitudeSensor=new MetaVrpnObjectAltitudeSensor(this,name);
}

sensor::AltitudeSensor *MetaVrpnObject::GetAltitudeSensor(void) const {
  return altitudeSensor;
}

MetaVrpnObject::~MetaVrpnObject() { delete plot_tab; }

DataPlot1D *MetaVrpnObject::VxPlot(void) const { return vx_opti_plot; }

DataPlot1D *MetaVrpnObject::VyPlot(void) const { return vy_opti_plot; }

DataPlot1D *MetaVrpnObject::VzPlot(void) const { return vz_opti_plot; }

DataPlot2D *MetaVrpnObject::XyPlot(void) const { return xy_plot; }

void MetaVrpnObject::GetSpeed(Vector3Df &speed) const {
  speed.x = euler->Output(0,0, 0);
  speed.y = euler->Output(0,1, 0);
  speed.z = euler->Output(0,2, 0);
}

EulerDerivative* MetaVrpnObject::GetEulerDerivative(void) const {
	return euler;
}

} // end namespace meta
} // end namespace flair
