// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file MetaVrpnObject.h
 * \brief Classe haut niveau intégrant un objet VRPN
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2013/04/08
 * \version 3.4
 */

#ifndef METAVRPNOBJECT_H
#define METAVRPNOBJECT_H

#include <VrpnObject.h>
#include <VrpnClient.h>
#include <io_data.h>
#include <Vector3D.h>

namespace flair {
	namespace gui {
		class DataPlot1D;
		class DataPlot2D;
		class Tab;
	}
	namespace filter {
		class EulerDerivative;
		class LowPassFilter;
	}
  namespace sensor {
    class AltitudeSensor;
  }
}

namespace flair {
namespace meta {

/*! \class MetaVrpnObject
*
* \brief Classe haut niveau intégrant un objet VRPN
*
* Contient un objet VRPN et une dérivée, d'euler.
*/
class MetaVrpnObject : public sensor::VrpnObject {
public:
  MetaVrpnObject(std::string name,sensor::VrpnClient *client=sensor::GetVrpnClient());
  MetaVrpnObject(std::string name,uint8_t id,sensor::VrpnClient *client=sensor::GetVrpnClient());
  ~MetaVrpnObject();
  gui::DataPlot1D *VxPlot(void) const; // 1,0
  gui::DataPlot1D *VyPlot(void) const; // 1,1
  gui::DataPlot1D *VzPlot(void) const; // 1,2
  gui::DataPlot2D *XyPlot(void) const;
  void GetSpeed(core::Vector3Df &speed) const;
  filter::EulerDerivative* GetEulerDerivative(void) const;
  //get the associated AltitudeSensor
  //MetaVrpnObject do not derivate from AltitudeSensor because frames are differents
  //and this could be confusing
  //MetaVrpnObject is in vrpn frame (earth)
  //AltitudeSensor is in uav frame
  sensor::AltitudeSensor *GetAltitudeSensor(void) const;

private:
  void ConstructorCommon(std::string name,sensor::VrpnClient *client);
  filter::LowPassFilter *pbas;
  filter::EulerDerivative *euler;
  gui::DataPlot2D *xy_plot;
  gui::DataPlot1D *vx_opti_plot, *vy_opti_plot, *vz_opti_plot;
  gui::Tab *plot_tab;
  sensor::AltitudeSensor *altitudeSensor;
};
} // end namespace meta
} // end namespace flair
#endif // METAVRPNOBJECT_H
