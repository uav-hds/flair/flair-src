// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2022/01/05
//  filename:   Plane.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    Base class to construct sensors/actuators depending on Plane type
//
//
/*********************************************************************/

#include "Plane.h"
#include <FrameworkManager.h>
#include <Tab.h>
#include <GridLayout.h>
#include <DataPlot1D.h>
#include <UavMultiplex.h>
#include <Servos.h>

using std::string;
using namespace flair::core;
using namespace flair::actuator;
using namespace flair::filter;

namespace {
  flair::meta::Plane *uavSingleton = NULL;
}

namespace flair {
namespace meta {

Plane *GetPlane(void) { return uavSingleton; }

Plane::Plane(string name, UavMultiplex *multiplex): Uav(name, multiplex) {
  if (uavSingleton != NULL) {
    Err("Uav must be instanced only one time\n");
    return;
  }

  uavSingleton = this;
  servos=NULL;
}

Plane::~Plane() {}


void Plane::SetServos(const Servos *insevos) { 
  servos = (Servos *)insevos;
}


Servos *Plane::GetServos(void) const { return servos; }

} // end namespace meta
} // end namespace flair
