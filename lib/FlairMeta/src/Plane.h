// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file Plane.h
 * \brief Base class to construct sensors/actuators depending on Plane type
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2022/01/05
 * \version 3.4
 */

#ifndef PLANE_H
#define PLANE_H

#include <Uav.h>

namespace flair {
	namespace filter {
		class UavMultiplex;
	}
	namespace actuator {
		class Servos;
	}
	
}

namespace flair {
namespace meta {


/*! \class Plane
*
* \brief Base class to construct sensors/actuators depending on Plane type.
* The Object is created with 
*  the FrameworkManager as parent. FrameworkManager must be created before.
* Only one instance of this class is allowed by program.
*/
class Plane : public meta::Uav {
public:

  Plane(std::string name,filter::UavMultiplex *multiplex);
  ~Plane();

  actuator::Servos *GetServos(void) const;
  

protected:
  void SetServos(const actuator::Servos *servos);
  

private:
  actuator::Servos *servos;
  
};

/*!
* \brief get Uav
*
* \return the Uav
*/
Plane *GetPlane(void);

} // end namespace meta
} // end namespace flair
#endif // PLANE_H
