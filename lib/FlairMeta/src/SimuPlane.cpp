// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2022/01/05
//  filename:   SimuPlane.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    Class defining a simulation plane
//
//
/*********************************************************************/
#ifdef CORE2_64

#include "SimuPlane.h"
#include <FrameworkManager.h>
#include <SimulatedImu.h>
#include <PassthroughAhrs.h>
#include <SimulatedBldc.h>
#include <SimulatedServos.h>
#include <SimulatedUs.h>
#include <BatteryMonitor.h>
#include "PlaneMultiplex.h"
#include <Tab.h>

using std::string;
using namespace flair::core;
using namespace flair::gui;
using namespace flair::sensor;
using namespace flair::filter;
using namespace flair::actuator;

namespace flair {
namespace meta {

SimuPlane::SimuPlane(string name, uint32_t simu_id,filter::PlaneMultiplex *multiplex)
    : Plane(name, multiplex) {


  SetBldc(new SimulatedBldc(multiplex->GetBldcMultiplex(), multiplex->GetLayout(),"motors",1, simu_id,0));
  SetServos(new SimulatedServos(multiplex->GetServosMultiplex(),multiplex->GetLayout(),"servos",2, simu_id,0));
  SetUsRangeFinder(new SimulatedUs("us", simu_id,0, 60));
  SetAhrs(new PassthroughAhrs(new SimulatedImu("imu", simu_id,0, 70),"ahrs"));
  Tab *bat_tab = new Tab(getFrameworkManager()->GetTabWidget(), "battery");
  SetBatteryMonitor(new BatteryMonitor(bat_tab->NewRow(), "battery"));
  GetBatteryMonitor()->SetBatteryValue(12);

}

SimuPlane::~SimuPlane() {}

void SimuPlane::StartSensors(void) {
  ((SimulatedImu *)(GetAhrs()->GetImu()))->Start();
  ((SimulatedUs *)GetUsRangeFinder())->Start();
}



} // end namespace meta
} // end namespace flair

#endif