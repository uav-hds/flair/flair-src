// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file SimuPlane.h
 * \brief Class defining a simulation plane
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2022/01/05
 * \version 4.0
 */

#ifndef SIMUPLANE_H
#define SIMUPLANE_H

#include "Plane.h"

namespace flair {
    namespace filter {
        class PlaneMultiplex;
    }

}

namespace flair {
namespace meta {
  
/*! \class SimuPlane
*
* \brief Class defining a simulation plane
*/
class SimuPlane : public Plane {
  public:
    // simu_id: 0 if simulating only one UAV
    //>0 otherwise
    SimuPlane(std::string name, uint32_t simu_id,filter::PlaneMultiplex *multiplex);
    ~SimuPlane();
    void StartSensors(void) override;
    virtual std::string GetType(void) const override{return "plane_simu";}

  private:
    
  
};
} // end namespace meta
} // end namespace flair
#endif // SIMUPLANE_H
