// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2020/12/16
//  filename:   SimuUgv.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    Class defining a simulation ugv
//
//
/*********************************************************************/
#ifdef CORE2_64

#include "SimuUgv.h"
#include <SimulatedUgvControls.h>
#include <FrameworkManager.h>

using std::string;
using namespace flair::core;
using namespace flair::actuator;

namespace flair {
namespace meta {

SimuUgv::SimuUgv(string name, uint32_t simu_id)
    : Ugv(name) {


  SetUgvControls(new SimulatedUgvControls("controls", simu_id,0));
  getFrameworkManager()->AddDeviceToLog(GetUgvControls());
  
}

SimuUgv::~SimuUgv() {}

void SimuUgv::StartSensors(void) {
 
}



} // end namespace meta
} // end namespace flair

#endif