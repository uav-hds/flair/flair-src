// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file SimuUgv.h
 * \brief Class defining a simulation ugv
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2020/12/16
 * \version 4.0
 */

#ifndef SIMUUGV_H
#define SIMUUGV_H

#include "Ugv.h"

namespace flair {
namespace meta {
  
/*! \class SimuUgv
*
* \brief Class defining a simulation ugv
*/
class SimuUgv : public Ugv {
  public:
    // simu_id: 0 if simulating only one ugv
    //>0 otherwise
    SimuUgv(std::string name, uint32_t simu_id = 0);
    ~SimuUgv();
    void StartSensors(void) override;
    std::string GetType(void) const override{return "ugv_simu";}

  private:
   
};
} // end namespace meta
} // end namespace flair
#endif // SIMUUGV_H
