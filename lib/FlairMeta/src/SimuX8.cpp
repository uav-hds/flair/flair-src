// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2016/02/08
//  filename:   SimuX8.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    Class defining a simulation x8 uav
//
//
/*********************************************************************/
#ifdef CORE2_64

#include "SimuX8.h"
#include <FrameworkManager.h>
#include <X4X8Multiplex.h>
#include <SimulatedImu.h>
#include <PassthroughAhrs.h>
#include <SimulatedBldc.h>
#include <SimulatedUs.h>
#include <SimulatedCamera.h>
#include <SimulatedPressureSensor.h>
#include <BatteryMonitor.h>
#include <SimulatedGps.h>
#include <Tab.h>
#include <FindArgument.h>

using std::string;
using namespace flair::core;
using namespace flair::gui;
using namespace flair::sensor;
using namespace flair::filter;
using namespace flair::actuator;

namespace flair {
namespace meta {

SimuX8::SimuX8(string name, uint32_t simu_id,string options,
               filter::UavMultiplex *multiplex)
    : Uav(name, multiplex) {

  if (multiplex == NULL)
    SetMultiplex(new X4X8Multiplex("motors", X4X8Multiplex::X8));

  SetBldc(new SimulatedBldc(GetUavMultiplex(), GetUavMultiplex()->GetLayout(),
                       "motors", GetUavMultiplex()->MotorsCount(), simu_id,0));
  SetUsRangeFinder(new SimulatedUs("us", simu_id,0, 60));
  SetAhrs(new PassthroughAhrs(new SimulatedImu("imu", simu_id,0, 70),"ahrs"));
  Tab *bat_tab = new Tab(getFrameworkManager()->GetTabWidget(), "battery");
  SetBatteryMonitor(new BatteryMonitor(bat_tab->NewRow(), "battery"));
  GetBatteryMonitor()->SetBatteryValue(12);
  
  string useCamV=FindArgument(options,"use_camv=",false);
  if(useCamV!="false") {
    uint16_t camvWidth=320,camvHeight=240;
    ReadCameraResolutionOption(options,"camv",camvWidth,camvHeight);
    Info("using vertical camera resolution: %ix%i\n",camvWidth, camvHeight);
    SetVerticalCamera(new SimulatedCamera("simu_cam_v", camvWidth, camvHeight, 3, simu_id,0, 10));
  }
  
  string useCamH=FindArgument(options,"use_camh=",false);
  if(useCamH!="false") {
    uint16_t camhWidth=320,camhHeight=240;
    ReadCameraResolutionOption(options,"camh",camhWidth,camhHeight);
    Info("using horizontal camera resolution: %ix%i\n",camhWidth, camhHeight);
    SetHorizontalCamera(new SimulatedCamera("simu_cam_h", camhWidth, camhHeight, 3, simu_id,1, 10));
  }
  
  string useGps=FindArgument(options,"use_gps=",false);
  if(useGps=="true") {
    SetGps(new SimulatedGps("gps", (NmeaGps::NMEAFlags_t)(NmeaGps::GGA | NmeaGps::VTG), 0,0, 40));
  } 
  
  string usePressureSensor=FindArgument(options,"use_pressure_sensor=",false);
  if(usePressureSensor=="true") {
    SetPressureSensor(new SimulatedPressureSensor("pressuresensor", 0,0, 10));
  } 
}

SimuX8::~SimuX8() {}

void SimuX8::StartSensors(void) {
  ((SimulatedImu *)(GetAhrs()->GetImu()))->Start();
  ((SimulatedUs *)GetUsRangeFinder())->Start();
  if(GetVerticalCamera())  ((SimulatedCamera *)GetVerticalCamera())->Start();
  if(GetHorizontalCamera())  ((SimulatedCamera *)GetHorizontalCamera())->Start();
  if(GetGps()) ((SimulatedGps *)GetGps())->Start();
  if(GetPressureSensor()) ((SimulatedPressureSensor *)GetPressureSensor())->Start();
}

void SimuX8::ReadCameraResolutionOption(string options,string cameraName,uint16_t &camWidth,uint16_t &camHeight) const {
  string camOpts=FindArgument(options,cameraName +"=",false);
	if(camOpts!="") {
    size_t position=camOpts.find("x");
    if(position!=std::string::npos) {
        camWidth=std::stoi(camOpts.substr(0,position));
        camHeight=std::stoi(camOpts.substr(position+1,std::string::npos));
    } else {
      Warn("bad camera resolution parameter (%s) should be WIDTHxHEIGHT format\n",camOpts.c_str());
    }
  }
}

} // end namespace meta
} // end namespace flair

#endif