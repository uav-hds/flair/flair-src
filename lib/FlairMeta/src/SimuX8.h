// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file SimuX8.h
 * \brief Class defining a simulation x8 uav
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2016/02/08
 * \version 4.0
 */

#ifndef SIMUX8_H
#define SIMUX8_H

#include "Uav.h"

namespace flair {
namespace meta {
/*! \class SimuX8
*
* \brief Class defining a simulation x8 uav
*/
class SimuX8 : public Uav {
  public:
    // simu_id: 0 if simulating only one UAV
    //>0 otherwise
    SimuX8(std::string name, uint32_t simu_id = 0,std::string options="",
           filter::UavMultiplex *multiplex = NULL);
    ~SimuX8();
    void StartSensors(void) override;
    std::string GetType(void) const override{return "simu_x8";}
  
  private:
    void ReadCameraResolutionOption(std::string options,std::string cameraName,uint16_t &camWidth,uint16_t &camHeight) const;
 
};
} // end namespace meta
} // end namespace flair
#endif // SIMUX8_H
