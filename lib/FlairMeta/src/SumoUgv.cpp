// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2020/12/16
//  filename:   SumoUgv.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    Class defining a parrot sumo ugv
//
//
/*********************************************************************/
#ifdef CORE2_64

#include "SumoUgv.h"
#include <SumoUgvControls.h>
#include <FrameworkManager.h>


using std::string;
using namespace flair::core;
using namespace flair::actuator;

namespace flair {
namespace meta {

SumoUgv::SumoUgv(string name): Ugv(name) {


  SetUgvControls(new SumoUgvControls("controls"));
  getFrameworkManager()->AddDeviceToLog(GetUgvControls());
  
}

SumoUgv::~SumoUgv() {}

void SumoUgv::StartSensors(void) {
 
}



} // end namespace meta
} // end namespace flair

#endif