// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file SumoUgv.h
 * \brief Class defining a parrot sumo igv
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2020/12/16
 * \version 4.0
 */

#ifndef SUMOUGV_H
#define SUMOUGV_H

#include "Ugv.h"

namespace flair {
namespace meta {
  
/*! \class SumoUgv
*
* \brief Class defining a parrot sumo ugv
*/
class SumoUgv : public Ugv {
  public:
    SumoUgv(std::string name);
    ~SumoUgv();
    void StartSensors(void) override;
    std::string GetType(void) const override{return "sumo";}
    std::string GetDefaultVrpnAddress(void) const override{return "172.26.213.1:3883";}

  private:
   
};
} // end namespace meta
} // end namespace flair
#endif // SUMOUGV_H
