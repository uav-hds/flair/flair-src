// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2014/06/10
//  filename:   Uav.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    Base class to construct sensors/actuators depending on uav type
//
//
/*********************************************************************/

#include "Uav.h"
#include <FrameworkManager.h>
#include <Tab.h>
#include <GridLayout.h>
#include <DataPlot1D.h>
#include <Ahrs.h>
#include <Imu.h>
#include <UavMultiplex.h>
#include <UsRangeFinder.h>
#include <NmeaGps.h>
#include <PressureSensor.h>
#include <Bldc.h>
#include <Matrix.h>
#include "MetaUsRangeFinder.h"


using std::string;
using namespace flair::core;
using namespace flair::gui;
using namespace flair::filter;
using namespace flair::sensor;
using namespace flair::actuator;

namespace {
  flair::meta::Uav *uavSingleton = NULL;
}

namespace flair {
namespace meta {

Uav *GetUav(void) { return uavSingleton; }
	
Uav::Uav(string name, UavMultiplex *multiplex)
    : Object(getFrameworkManager(), name) {
	if (uavSingleton != NULL) {
    Err("Uav must be instanced only one time\n");
    return;
  }

  uavSingleton = this;
  verticalCamera = NULL;
  horizontalCamera = NULL;
  gps = NULL;
  pressureSensor = NULL;
  this->multiplex = multiplex;
}

Uav::~Uav() {}

void Uav::SetUsRangeFinder(const UsRangeFinder *inUs) {
  us = (UsRangeFinder *)inUs;
  meta_us = new MetaUsRangeFinder(us);
  getFrameworkManager()->AddDeviceToLog(us);
}

void Uav::SetAhrs(const Ahrs *inAhrs) {
  ahrs = (Ahrs *)inAhrs;
  imu = (Imu *)ahrs->GetImu();
  //only add imu; as ahrs is son of imu, it will be logged together
  getFrameworkManager()->AddDeviceToLog(imu);
}

void Uav::SetBldc(const Bldc *inBldc) { 
  bldc = (Bldc *)inBldc;
}

void Uav::SetBatteryMonitor(const BatteryMonitor *inBattery) {
  battery = (BatteryMonitor *)inBattery;
}

void Uav::SetMultiplex(const UavMultiplex *inMultiplex) {
  multiplex = (UavMultiplex *)inMultiplex;
  getFrameworkManager()->AddDeviceToLog(multiplex);
}

void Uav::SetVerticalCamera(const Camera *inVerticalCamera) {
  verticalCamera = (Camera *)inVerticalCamera;
}

void Uav::SetHorizontalCamera(const Camera *inHorizontalCamera) {
  horizontalCamera = (Camera *)inHorizontalCamera;
}

void Uav::SetPressureSensor(const PressureSensor *inPressureSensor) {
  pressureSensor=(PressureSensor *)inPressureSensor;
  getFrameworkManager()->AddDeviceToLog(pressureSensor);
}

void Uav::SetGps(const NmeaGps *inGps) {
  gps=(NmeaGps *)inGps;
  getFrameworkManager()->AddDeviceToLog(gps);
}

void Uav::UseDefaultPlot(void) {
  multiplex->UseDefaultPlot();

  if (bldc->HasSpeedMeasurement()) {
    Tab *plot_tab = new Tab(multiplex->GetTabWidget(), "Speeds");
    DataPlot1D *plots[4];
    plots[0] = new DataPlot1D(plot_tab->NewRow(), "front left", 0, 7000);
    plots[1] =
        new DataPlot1D(plot_tab->LastRowLastCol(), "front right", 0, 7000);
    plots[2] = new DataPlot1D(plot_tab->NewRow(), "rear left", 0, 7000);
    plots[3] =
        new DataPlot1D(plot_tab->LastRowLastCol(), "rear right", 0, 7000);

    if (bldc->MotorsCount() == 8) {
      for (int i = 0; i < 4; i++)
        plots[i]->AddCurve(
            bldc->Output()->Element(multiplex->MultiplexValue(i), 0),
            DataPlot::Red, "top");
      for (int i = 0; i < 4; i++)
        plots[i]->AddCurve(
            bldc->Output()->Element(multiplex->MultiplexValue(i + 4), 0),
            DataPlot::Blue, "bottom");
    } else {
      for (int i = 0; i < 4; i++)
        plots[i]->AddCurve(
            bldc->Output()->Element(multiplex->MultiplexValue(i), 0));
    }
  }

  if (bldc->HasCurrentMeasurement()) {
    Tab *plot_tab = new Tab(multiplex->GetTabWidget(), "Currents");
    DataPlot1D *plots[4];
    plots[0] = new DataPlot1D(plot_tab->NewRow(), "front left", 0, 10);
    plots[1] = new DataPlot1D(plot_tab->LastRowLastCol(), "front right", 0, 10);
    plots[2] = new DataPlot1D(plot_tab->NewRow(), "rear left", 0, 10);
    plots[3] = new DataPlot1D(plot_tab->LastRowLastCol(), "rear right", 0, 10);

    if (bldc->MotorsCount() == 8) {
      for (int i = 0; i < 4; i++)
        plots[i]->AddCurve(
            bldc->Output()->Element(multiplex->MultiplexValue(i), 1),
            DataPlot::Red, "top");
      for (int i = 0; i < 4; i++)
        plots[i]->AddCurve(
            bldc->Output()->Element(multiplex->MultiplexValue(i + 4), 1),
            DataPlot::Blue, "bottom");
    } else {
      for (int i = 0; i < 4; i++)
        plots[i]->AddCurve(
            bldc->Output()->Element(multiplex->MultiplexValue(i), 1));
    }
  }

  meta_us->UseDefaultPlot();
  ahrs->UseDefaultPlot();
  if(gps!=NULL) gps->UseDefaultPlot();
  if(pressureSensor!=NULL) pressureSensor->UseDefaultPlot();
}

UavMultiplex *Uav::GetUavMultiplex(void) const { return multiplex; }

Bldc *Uav::GetBldc(void) const { return bldc; }

Ahrs *Uav::GetAhrs(void) const { return ahrs; }

Imu *Uav::GetImu(void) const { return imu; }

MetaUsRangeFinder *Uav::GetMetaUsRangeFinder(void) const { return meta_us; }

UsRangeFinder *Uav::GetUsRangeFinder(void) const { return us; }

BatteryMonitor *Uav::GetBatteryMonitor(void) const { return battery; }

Camera *Uav::GetVerticalCamera(void) const { return verticalCamera; }

Camera *Uav::GetHorizontalCamera(void) const { return horizontalCamera; }

NmeaGps *Uav::GetGps(void) const { return gps; }

PressureSensor *Uav::GetPressureSensor(void) const { return pressureSensor; }

} // end namespace meta
} // end namespace flair
