// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file Uav.h
 * \brief Base class to construct sensors/actuators depending on uav type
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2014/01/22
 * \version 3.4
 */

#ifndef UAV_H
#define UAV_H

#include <Object.h>
#include <VrpnClient.h>

namespace flair {
	namespace filter {
		class Ahrs;
		class UavMultiplex;
	}
	namespace actuator {
		class Bldc;
	}
	namespace sensor {
		class UsRangeFinder;
		class BatteryMonitor;
		class Imu;
		class Camera;
		class NmeaGps;
		class PressureSensor;
	}
}

namespace flair {
namespace meta {
class MetaUsRangeFinder;

/*! \class Uav
*
* \brief Base class to construct sensors/actuators depending on uav type.
* The Object is created with 
*  the FrameworkManager as parent. FrameworkManager must be created before.
* Only one instance of this class is allowed by program.
*/
class Uav : public core::Object {
public:

  Uav(std::string name,
      filter::UavMultiplex *multiplex = NULL);
  ~Uav();

  virtual void StartSensors(void)=0;
  void UseDefaultPlot(void);
  actuator::Bldc *GetBldc(void) const;
  filter::UavMultiplex *GetUavMultiplex(void) const;
  sensor::Imu *GetImu(void) const;
  filter::Ahrs *GetAhrs(void) const;
  MetaUsRangeFinder *GetMetaUsRangeFinder(void) const;
  sensor::UsRangeFinder *GetUsRangeFinder(void) const;
  sensor::BatteryMonitor *GetBatteryMonitor(void) const;
  sensor::Camera *GetVerticalCamera(void) const;
  sensor::Camera *GetHorizontalCamera(void) const;
  sensor::NmeaGps *GetGps(void) const;
  sensor::PressureSensor *GetPressureSensor(void) const;
	virtual std::string GetDefaultVrpnAddress(void) const{return "127.0.0.1:3883";}
	virtual sensor::VrpnClient::ConnectionType_t GetDefaultVrpnConnectionType(void) const{return sensor::VrpnClient::ConnectionType_t::Vrpn;}
  virtual bool isReadyToFly(void) const { return true;}
  virtual std::string GetType(void) const=0;

protected:
  void SetBldc(const actuator::Bldc *bldc);
  void SetMultiplex(const filter::UavMultiplex *multiplex);
  void SetAhrs(const filter::Ahrs *ahrs);//also sets imu (retrieved from the ahrs)
  void SetUsRangeFinder(const sensor::UsRangeFinder *us);
  void SetBatteryMonitor(const sensor::BatteryMonitor *battery);
  void SetVerticalCamera(const sensor::Camera *verticalCamera);
  void SetHorizontalCamera(const sensor::Camera *verticalCamera);
  void SetGps(const sensor::NmeaGps *gps);
  void SetPressureSensor(const sensor::PressureSensor *pressureSensor);

private:
  sensor::Imu *imu;
  sensor::NmeaGps *gps;
  filter::Ahrs *ahrs;
  actuator::Bldc *bldc;
  filter::UavMultiplex *multiplex;
  sensor::UsRangeFinder *us;
  MetaUsRangeFinder *meta_us;
  sensor::BatteryMonitor *battery;
  sensor::Camera *verticalCamera,*horizontalCamera;
  sensor::PressureSensor *pressureSensor;
};

/*!
* \brief get Uav
*
* \return the Uav
*/
Uav *GetUav(void);

} // end namespace meta
} // end namespace flair
#endif // UAV_H
