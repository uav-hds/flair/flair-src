// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2016/02/05
//  filename:   UavFactory.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:   construct a Uav based on the type name
//
//
/*********************************************************************/

#include "UavFactory.h"
#include "FrameworkManager.h"
#include "SimuX4.h"
#include "SimuX8.h"
#include "HdsX8.h"
#include "XAir.h"
#include "compile_info.h"

using namespace std;
using namespace flair::core;
using namespace flair::filter;
using namespace flair::meta;

namespace { // anonymous
     vector<flair::meta::Uav* (*)(string,string,string,UavMultiplex*)> *vectoroffunctions=NULL;
}


static void constructor() __attribute__((constructor));

void constructor() {
  compile_info("FlairMeta");
}


Uav *CreateUav(string name, string type,string options,
               UavMultiplex *multiplex) {

  Uav *uav;

  if(vectoroffunctions!=NULL) {
    for(int i=0;i<vectoroffunctions->size();i++) {
      uav=vectoroffunctions->at(i)(name,type,options,multiplex);
      if(uav!=NULL) {
        free(vectoroffunctions);
        vectoroffunctions=NULL;
        return uav;
      }
    }
  }

#ifdef ARMV7A
  if (type == "hds_x4") {
    getFrameworkManager()->Err("UAV type %s not yet implemented\n", type.c_str());
    return NULL;
  } else if (type == "hds_x8") {
    return new HdsX8(name,options, multiplex);
  } else if (type == "xair") {
    return new XAir(name,options, multiplex);
  } else if (type == "hds_xufo") {
    getFrameworkManager()->Err("UAV type %s not yet implemented\n", type.c_str());
    return NULL;
  } else {
    getFrameworkManager()->Err("UAV type %s unknown\n", type.c_str());
    return NULL;
  }
#endif
#ifdef CORE2_64
  if (type.compare(0, 7, "x4_simu") == 0) {
    int simu_id = 0;
    if (type.size() > 7) {
      simu_id = atoi(type.substr(7, type.size() - 7).c_str());
    }
    return new SimuX4(name, simu_id,options, multiplex);
  } else if (type.compare(0, 7, "x8_simu") == 0) {
    int simu_id = 0;
    if (type.size() > 7) {
      simu_id = atoi(type.substr(7, type.size() - 7).c_str());
    }
    return new SimuX8(name, simu_id,options, multiplex);
  } else {
    getFrameworkManager()->Err("UAV type %s unknown\n", type.c_str());
    return NULL;
  }
#endif
}

void RegisterUavCreator(flair::meta::Uav*(*func)(string,string,string,UavMultiplex*)) {
  if(vectoroffunctions==NULL) vectoroffunctions=(vector<flair::meta::Uav* (*)(string,string,string,UavMultiplex*)>*)malloc(sizeof(vector<flair::meta::Uav* (*)(string,string,string,UavMultiplex*)>));

  if (vectoroffunctions == NULL) {
    getFrameworkManager()->Err("buffer malloc %i bytes error\n",sizeof(vector<flair::meta::Uav* (*)(string,string,string,UavMultiplex*)>));
  }
  
  vectoroffunctions->push_back(func);
}
