// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2020/12/16
//  filename:   Ugv.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    Base class to construct sensors/actuators depending on Ugv type
//
//
/*********************************************************************/

#include "Ugv.h"
#include <FrameworkManager.h>
#include <UgvControls.h>


using std::string;
using namespace flair::core;
using namespace flair::actuator;

namespace {
  flair::meta::Ugv *UgvSingleton = NULL;
}

namespace flair {
namespace meta {

Ugv *GetUgv(void) { return UgvSingleton; }
	
Ugv::Ugv(string name)
    : Object(getFrameworkManager(), name) {
	if (UgvSingleton != NULL) {
    Err("Ugv must be instanced only one time\n");
    return;
  }

  UgvSingleton = this;
  controls=NULL;
  
}

Ugv::~Ugv() {}


void Ugv::SetUgvControls(const UgvControls *inControls) { 
  controls = (UgvControls *)inControls;
}


void Ugv::UseDefaultPlot(void) {
  if(controls) controls->UseDefaultPlot();
}


UgvControls *Ugv::GetUgvControls(void) const { return controls; }

} // end namespace meta
} // end namespace flair
