// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file Ugv.h
 * \brief Base class to construct sensors/actuators depending on Ugv type
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2020/12/16
 * \version 3.4
 */

#ifndef UGV_H
#define UGV_H

#include <Object.h>

namespace flair {
	namespace actuator {
		class UgvControls;
	}
}

namespace flair {
namespace meta {

/*! \class Ugv
*
* \brief Base class to construct sensors/actuators depending on Ugv type.
* The Object is created with 
*  the FrameworkManager as parent. FrameworkManager must be created before.
* Only one instance of this class is allowed by program.
*/
class Ugv : public core::Object {
public:

  Ugv(std::string name);
  ~Ugv();

  virtual void StartSensors(void)=0;
  void UseDefaultPlot(void);
  actuator::UgvControls *GetUgvControls(void) const;
	virtual std::string GetDefaultVrpnAddress(void) const{return "127.0.0.1:3883";}
  virtual bool isReadyToGo(void) const { return true;}
  virtual std::string GetType(void) const=0;

protected:
  void SetUgvControls(const actuator::UgvControls *controls);
  

private:
  actuator::UgvControls *controls;
};

/*!
* \brief get Ugv
*
* \return the Ugv
*/
Ugv *GetUgv(void);

} // end namespace meta
} // end namespace flair
#endif // UGV_H
