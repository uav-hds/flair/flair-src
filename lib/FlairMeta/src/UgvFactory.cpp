// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2020/12/16
//  filename:   UgvFactory.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:   construct a Ugv based on the type name
//
//
/*********************************************************************/

#include "UgvFactory.h"
#include "FrameworkManager.h"
#include "SimuUgv.h"
#include "SumoUgv.h"


using namespace std;
using namespace flair::core;
using namespace flair::meta;

namespace { // anonymous
     vector<flair::meta::Ugv* (*)(string,string,string)> *vectoroffunctions=NULL;
}


Ugv *CreateUgv(string name, string type,string options) {

  Ugv *ugv;

  if(vectoroffunctions!=NULL) {
    for(int i=0;i<vectoroffunctions->size();i++) {
      ugv=vectoroffunctions->at(i)(name,type,options);
      if(ugv!=NULL) {
        free(vectoroffunctions);
        vectoroffunctions=NULL;
        return ugv;
      }
    }
  }

#ifdef CORE2_64
  if (type.compare(0, 8, "ugv_simu") == 0) {
    int simu_id = 0;
    if (type.size() > 8) {
      simu_id = atoi(type.substr(8, type.size() - 8).c_str());
    }
    return new SimuUgv(name, simu_id);
  } else if (type.compare(0, 4, "sumo") == 0) {
    return new SumoUgv(name);
  } else {
    getFrameworkManager()->Err("UGV type %s unknown\n", type.c_str());
    return NULL;
  }
#endif
}

void RegisterUgvCreator(flair::meta::Ugv*(*func)(string,string,string)) {
  if(vectoroffunctions==NULL) vectoroffunctions=(vector<flair::meta::Ugv* (*)(string,string,string)>*)malloc(sizeof(vector<flair::meta::Ugv* (*)(string,string,string)>));

  if(vectoroffunctions == NULL) {
    getFrameworkManager()->Err("buffer malloc %i bytes error\n",sizeof(vector<flair::meta::Ugv* (*)(string,string,string)>));
  }
  vectoroffunctions->push_back(func);
}
