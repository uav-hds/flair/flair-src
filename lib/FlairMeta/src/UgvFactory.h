// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2020/12/16
//  filename:   UgvFactory.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:   construct a Ugv based on the type name
//
//
/*********************************************************************/

#ifndef UGVFACTORY
#define UGVFACTORY

#include <Ugv.h>

flair::meta::Ugv *CreateUgv(std::string name, std::string type,std::string options="");

void RegisterUgvCreator(flair::meta::Ugv*(*func)(std::string,std::string,std::string));
#endif // UGVFACTORY
