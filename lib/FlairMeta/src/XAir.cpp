// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2015/02/08
//  filename:   XAir.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    Class defining a Xair uav
//
//
/*********************************************************************/
#ifdef ARMV7A

#include "XAir.h"
#include <FrameworkManager.h>
#include <RTDM_I2cPort.h>
#include <RTDM_SerialPort.h>
#include <Srf08.h>
#include <PassthroughAhrs.h>
#include <Gx3_25_imu.h>
#include <AfroBldc.h>
#include <X4X8Multiplex.h>
#include <Ps3Eye.h>
#include <BatteryMonitor.h>
#include <Tab.h>

using std::string;
using namespace flair::core;
using namespace flair::gui;
using namespace flair::sensor;
using namespace flair::filter;
using namespace flair::actuator;

namespace flair {
namespace meta {

XAir::XAir(string name, string options,
           filter::UavMultiplex *multiplex)
    : Uav(name, multiplex) {
  RTDM_I2cPort *i2cport = new RTDM_I2cPort(getFrameworkManager(), "rtdm_i2c", "rti2c3");
  RTDM_SerialPort *imu_port = new RTDM_SerialPort(getFrameworkManager(), "imu_port", "rtser1");

  if (multiplex == NULL)
    SetMultiplex(new X4X8Multiplex("motors", X4X8Multiplex::X8));

  SetBldc(new AfroBldc(GetUavMultiplex(), GetUavMultiplex()->GetLayout(),
                       "motors", GetUavMultiplex()->MotorsCount(), i2cport));
  SetUsRangeFinder(new Srf08("SRF08", i2cport, 0x70, 60));
  SetAhrs(new PassthroughAhrs(new Gx3_25_imu("imu",imu_port,Gx3_25_imu::AccelerationAngularRateAndOrientationMatrix, 70),"ahrs"));
  
  Tab *bat_tab = new Tab(getFrameworkManager()->GetTabWidget(), "battery");
  SetBatteryMonitor(new BatteryMonitor(bat_tab->NewRow(), "battery"));
  GetBatteryMonitor()->SetBatteryValue(12);
  
  if(Image::IsUsingDefaultAllocAndFree()) {
    SetVerticalCamera(new Ps3Eye("camv", 0, false,50));//no cmem and no dsp
  } else {
    SetVerticalCamera(new Ps3Eye("camv", 0, true,50));//cmem and dsp
  }
}

XAir::~XAir() {}

void XAir::StartSensors(void) {
  ((Gx3_25_imu *)(GetAhrs()->GetImu()))->Start();
  ((Srf08 *)GetUsRangeFinder())->Start();
  ((Ps3Eye *)GetVerticalCamera())->Start();
}

bool XAir::isReadyToFly(void) const {
  return GetAhrs()->GetImu()->IsReady();
}

} // end namespace meta
} // end namespace flair

#endif
