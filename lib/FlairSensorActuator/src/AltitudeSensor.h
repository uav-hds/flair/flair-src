// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file AltitudeSensor.h
 * \brief Abstract class for altitude sensor
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2021/07/12
 * \version 3.4
 */

#ifndef ALTITUDESENSOR_H
#define ALTITUDESENSOR_H

#include <Object.h>


namespace flair {
namespace sensor {
/*! \class AltitudeSensor
*
* \brief Abstract class for altitude sensor
*
* sensors like us or vrpn should instantiate a class
* of this type to provide altitude values to
* the state machine
* returned vales are in uav coordinates
*
*/
class AltitudeSensor : public core::Object {
public:
  AltitudeSensor(core::Object *parent,std::string name): core::Object(parent,name) {};
  virtual ~AltitudeSensor() {};
  virtual float z(void) const=0;//in uav frame
  virtual float Vz(void) const=0;//in uav frame
  
private:
  
};
} // end namespace sensor
} // end namespace flair
#endif // ALTITUDESENSOR_H
