// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2014/03/06
//  filename:   Camera.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    Virtual class for Camera
//
//
/*********************************************************************/

#include "Camera.h"
#include <FrameworkManager.h>
#include <Tab.h>
#include <TabWidget.h>
#include <GroupBox.h>
#include <GridLayout.h>
#include <DataPlot1D.h>
#include <Picture.h>
#include <fstream>

using std::string;
using namespace flair::core;
using namespace flair::gui;

namespace flair {
namespace sensor {

Camera::Camera(string name, uint16_t width,
               uint16_t height, Image::Type::Format format)
    : IODevice(getFrameworkManager(), name) {
  plot_tab = NULL;
	logFormat=LogFormat_t::None;

  // do not allocate imagedata, allocation is done by the camera
  output = new Image((IODevice *)this, width, height, format, "out", false);
	
  // station sol
  main_tab = new Tab(getFrameworkManager()->GetTabWidget(), name);
  tab = new TabWidget(main_tab->NewRow(), name);
  sensor_tab = new Tab(tab, "Setup");
  setup_groupbox = new GroupBox(sensor_tab->NewRow(), name);
  setup_layout = new GridLayout(sensor_tab->NewRow(), "setup");
  
}

Camera::~Camera() {
  delete main_tab;
}

void Camera::SetLogFormat(LogFormat_t logFormat) {
	this->logFormat=logFormat;
	switch(logFormat) {
		case LogFormat_t::RawPicture:
			AddDataToLog(output);
			break;
		case LogFormat_t::JpgPicture:
      //AddDataToLog(output);//not called because jpg not appended to dbt
      Warn("logging Image to jpeg\n");
      Warn("jpeg are not included in classical dbt file, as dbt does not handle variable length\n");
      break;
		default:
			Warn("LogFormat unknown\n");
	}
}

DataType const &Camera::GetOutputDataType() const {
  return output->GetDataType();
}

GroupBox *Camera::GetGroupBox(void) const { return setup_groupbox; }

GridLayout *Camera::GetLayout(void) const { return setup_layout; }

void Camera::UseDefaultPlot(const core::Image *image) {
  plot_tab = new Tab(tab, "Picture");
  Picture *plot = new Picture(plot_tab->NewRow(), ObjectName(), image);
}

Tab *Camera::GetPlotTab(void) const { return plot_tab; }

uint16_t Camera::Width(void) const { return output->GetDataType().GetWidth(); }

uint16_t Camera::Height(void) const {
  return output->GetDataType().GetHeight();
}

core::Image *Camera::Output(void) { return output; }

void Camera::ProcessUpdate(core::io_data* data) {
  static bool warnedOnce=false;
  
	if(getFrameworkManager()->IsLogging() && getFrameworkManager()->IsDeviceLogged(this)) {
    //only handle jpg case; raw case is automatically appended to logs
		if(logFormat==LogFormat_t::JpgPicture) {
       Warn("logging to jpeg in not supported\n");
			data->GetMutex();
      const Image* image = dynamic_cast<const Image*>(data);
      if (!image) {
          Warn("casting %s to Image failed\n",data->ObjectName().c_str());
          return;
      }
     /*
			string filename=getFrameworkManager()->GetLogPath()+"/"+ObjectName()+"_"+std::to_string(data->DataTime())+".jpg";
			switch(((Image*)data)->GetDataType().GetFormat()) {
				case Image::Type::Format::Gray:
					saveToJpeg(image,filename,PictureFormat_t::Gray,PictureFormat_t::Gray);
					break;
				case Image::Type::Format::BGR:
					saveToJpeg(image,filename,PictureFormat_t::RGB,PictureFormat_t::RGB);
					break;
				case Image::Type::Format::UYVY:
					saveToJpeg(image,filename,PictureFormat_t::YUV_422ile,PictureFormat_t::YUV_422p);
					break;
				default:
					Warn("cannot log to this format\n");
					break;
			}*/
			data->ReleaseMutex();
		} if(logFormat==LogFormat_t::None) {
      if(!warnedOnce) Warn("not logging camera, you must define a log format using Camera::SetLogFormat\n");
      warnedOnce=true;
    }
	}
	IODevice::ProcessUpdate(data);
}

void Camera::SavePictureToFile(string filename) const {
	if(filename=="") filename="./"+ObjectName()+"_"+std::to_string(GetTime())+".jpg";
  string::size_type idx = filename.rfind('.');
Printf("todo SavePictureToFile without opencv\n");/*
	if(idx != string::npos) {
		Printf("saving %s\n", filename.c_str());
		string extension = filename.substr(idx+1);
		
		output->GetMutex();
		if(extension=="jpg") {
			if(output->GetDataType().GetFormat()==Image::Type::Format::Gray) saveToJpeg(output->img,filename,PictureFormat_t::Gray,PictureFormat_t::Gray);
			if(output->GetDataType().GetFormat()==Image::Type::Format::BGR) saveToJpeg(output->img,filename,PictureFormat_t::RGB,PictureFormat_t::RGB);
			if(output->GetDataType().GetFormat()==Image::Type::Format::UYVY) saveToJpeg(output->img,filename,PictureFormat_t::YUV_422ile,PictureFormat_t::YUV_422p);
		} else {
			cvSaveImage(filename.c_str(),output->img);
		}
		output->ReleaseMutex();
    Printf("saving %s ok\n", filename.c_str());
			
	} else {
		Warn("saving %s no file extension!\n", filename.c_str());
	}*/
}

void Camera::SaveRawPictureToFile(string filename) const {
  Printf("saving %s, size %i\n", filename.c_str(), output->GetDataType().GetSize());
  std::ofstream pFile;
  pFile.open(filename);
  output->GetMutex();
  pFile.write(output->buffer, output->GetDataType().GetSize());
  output->ReleaseMutex();

  pFile.close();
}

} // end namespace sensor
} // end namespace flair
