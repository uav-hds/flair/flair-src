// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file Camera.h
 * \brief Base class for Camera
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2014/03/06
 * \version 4.0
 */

#ifndef CAMERA_H
#define CAMERA_H

#include <IODevice.h>
#include <Image.h>

namespace flair {
	namespace gui {
		class GroupBox;
		class Tab;
		class TabWidget;
		class Picture;
		class GridLayout;
	}
}

namespace flair {
namespace sensor {
/*! \class Camera
*
* \brief Base class for Camera
*
* Use this class to define a custom Camera.
*
*/
class Camera : public core::IODevice {
public:
  /*!
  * \brief Constructor
  *
  * Construct a Camera.
  * It will be child of the FrameworkManager.
	* 
  * \param name name
  * \param width width
  * \param height height
  * \param format image format
  */
  Camera(std::string name, uint16_t width,
         uint16_t height, core::Image::Type::Format format);

  /*!
  * \brief Destructor
  *
  */
  ~Camera();

  /*!
  * \brief Use default plot
  *
  * \param image image to display
  */
  void UseDefaultPlot(const core::Image *image);

  /*!
  * \brief get Layout
  *
  * \return a Layout available
  */
  gui::GridLayout *GetLayout(void) const;

  /*!
  * \brief plot tab
  *
  * \return plot tab
  */
  gui::Tab *GetPlotTab(void) const;

  /*!
  * \brief Save raw picture to file
  *
  * \param filename filename
  */
  void SaveRawPictureToFile(std::string filename) const;
	
	/*!
  * \brief Save picture to file
  *
  * \param filename filename; if ommitted, current time is used and file is saved in current directory with jpg compression
  */
  void SavePictureToFile(std::string filename="") const;

  /*!
  * \brief Width
  *
  * \return width
  */
  uint16_t Width(void) const;

  /*!
  * \brief Height
  *
  * \return height
  */
  uint16_t Height(void) const;

  /*!
 * \brief Output matrix
 *
 * Output matrix is of the same size as declared in constructor. \n
 *
 * \return the output matrix
 */
  core::Image *Output(void);

  core::DataType const &GetOutputDataType() const;
	
	/*!
	\enum LogFormat
	\brief log formats
	*/
	enum class LogFormat_t {
		None, /*!< by default, no logging */
		RawPicture, /*!< raw format */
		JpgPicture, /*!< jpg format */
	};
	
	/*!
	* \brief Set log format
	*
	* Set the log format. \n
	* Logging of the camera will be disabled if this method is not called. \n
	* Calling this method enables logging of camera.
	*
	* \param LogFormat log format
	*/
  void SetLogFormat(LogFormat_t logFormat);
  void ProcessUpdate(core::io_data* data);
protected:
  /*!
  * \brief get GroupBox
  *
  * \return a GroupBox available
  */
  gui::GroupBox *GetGroupBox(void) const;

  core::Image *output;

private:
  gui::Tab *main_tab, *sensor_tab, *plot_tab;
  gui::TabWidget *tab;
  gui::GroupBox *setup_groupbox;
  gui::GridLayout *setup_layout;
	LogFormat_t logFormat;
};
} // end namespace sensor
} // end namespace flair
#endif // CAMERA_H
