// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file Imu.h
 * \brief Base class for Imu
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2014/01/16
 * \version 4.0
 */

#ifndef IMU_H
#define IMU_H

#include <IODevice.h>
#include <Vector3D.h>

namespace flair {
  namespace core {
    class ImuData;
    class OneAxisRotation;
  }
  namespace gui {
    class Tab;
    class TabWidget;
    class GroupBox;
    class Layout;
    class DataPlot1D;
  }
}

class Ahrs_impl;

namespace flair {
namespace sensor {
/*! \class Imu
*
* \brief Base class for Imu
*
* Use this class to define a custom Imu.
*
*/
class Imu : public core::IODevice {
  friend class ::Ahrs_impl;

public:
  /*!
  * \brief Constructor
  *
  * Construct an Imu.
	* It will be child of the FrameworkManager.
  *
  * \param name name
  * \param needRotation true will enable post rotation in GCS. Post rotation must be applied manually in reimplemented code
  */
  Imu(std::string name,bool needRotation=true);

  /*!
  * \brief Destructor
  *
  */
  ~Imu();
  
  /*!
  * \brief Get IMU datas
  *
  * \return ImuData
  */
  const core::ImuData *GetDatas(void) const;


  /*!
  * \brief Setup Layout
  *
  * \return setup Layout
  */
  gui::Layout *GetLayout(void) const;

  /*!
  * \brief Lock user interface
  *
  */
  void LockUserInterface(void) const;

  /*!
  * \brief Unlock user interface
  *
  */
  void UnlockUserInterface(void) const;

  /*!
  * \brief Use default plot
  *
  */
  void UseDefaultPlot(void);

  /*!
  * \brief Plot tab
  *
  * \return plot Tab
  */
  gui::Tab *GetPlotTab(void) const;

  /*!
  * \brief Get OneAxisRotation
  *
  * \return fixed rotation of the imu
  */
  core::OneAxisRotation *GetOneAxisRotation(void) const;


protected:
  /*!
  * \brief Setup GroupBox
  *
  * \return setup GroupBox
  */
  gui::GroupBox *GetGroupBox(void) const;

  /*!
  * \brief ApplyRotation
  *
  * The reimplemented class must call this function to rotate IMU datas, before filling the ImuData. \n
  * It handles the data rotation if it was defined.
  *
  * \param vector vector to apply rotation to
  */
  void ApplyRotation(core::Vector3Df& vector);
  
  /*!
  * \brief ApplyRotation
  *
  * The reimplemented class must call this function to rotate IMU datas, before filling the ImuData. \n
  * It handles the data rotation if it was defined.
  *
  * \param quaternion quaternion to apply rotation to
  */
  void ApplyRotation(core::Quaternion& quaternion);

  /*!
  * \brief Get imu datas
  *
  * Can be used by dervied class to fill core::ImuData
  * 
  * \param imuData imu datas
  */
  void GetDatas(core::ImuData **imuData) const;
  
  /*!
  * \brief Remove Offsets
  *
  * Remove the accelerometers and gyrometers offsets. During first 10s, returns 0 (gathering data to get offset)
  * Then, returns the vectors without offset
  *
  * \param acc accelerometer vector to remove offset
  * \param gyr gyrometer vector to remove offset
  */
  void RemoveOffsets(core::Vector3Df& acc,core::Vector3Df& gyr);

private:
  gui::Tab *mainTab, *sensorTab, *plotTab;
  gui::TabWidget *tab;
  gui::GroupBox *setupGroupbox;
  core::OneAxisRotation *rotation;
  core::ImuData *imuData;

  gui::DataPlot1D *axPlot, *ayPlot, *azPlot;
  gui::DataPlot1D *gxPlot, *gyPlot, *gzPlot;
  gui::DataPlot1D *mxPlot, *myPlot, *mzPlot;
  
  //for calibration:
  core::Time startCalcOffset;
  bool calibrationDone;
  core::Vector3Df accOffset,gyrOffset,accMax,accMin;
  uint16_t cptOffset;
  
};
} // end namespace sensor
} // end namespace flair

#endif // IMU_H
