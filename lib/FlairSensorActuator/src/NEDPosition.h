// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file NEDPosition.h
 * \brief Class for a NED position sensor
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS
 * 7253
 * \date 2021/05/20
 * \version 4.0
 */

#ifndef NEDPOSITION_H
#define NEDPOSITION_H

#include <IODevice.h>
#include <Vector3D.h>

namespace flair {
	namespace core {
		class Matrix;
	}
	namespace gui {
		class TabWidget;
		class Tab;
		class DataPlot1D;
	}
}

class NEDPosition_impl;

namespace flair {
namespace sensor {

/*! \class NEDPosition
*
* \brief abstract class for a NED position sensor.
*/
class NEDPosition : public core::IODevice {
  

public:
  /*!
  * \brief Constructor
  *
  * Construct a NEDPosition.
  *
  * \param parent
  * \param name
  * \param tab Tab for the user interface
  */
  NEDPosition(const Object *parent,std::string name,const flair::gui::TabWidget *tab);

  /*!
  * \brief Destructor
  *
  */
  ~NEDPosition(void);

  /*!
  * \brief Plot tab
  *
  * \return plot Tab
  */
  gui::Tab *GetPlotTab(void) const;

  /*!
  * \brief Get Position
  *
  * \param point output datas
  */
  void GetPosition(core::Vector3Df &point) const;
	
	/*!
  * \brief Get Position
  *
  * \param point output datas
  */
  core::Vector3Df GetPosition(void) const;
  
  /*!
  * \brief Get Velocity
  *
  * \param point output datas
  */
  //void GetVelocity(core::Vector3Df &point) const;

  /*!
  * \brief Output matrix
  *
  * Matrix is of type float and as follows: \n
  * (0,0) x \n
  * (1,0) y \n
  * (2,0) z \n
  *
  * \return Output matrix
  */
  core::Matrix *Output(void) const;

  /*!
  * \brief x plot
  *
  * \return x plot
  */
  gui::DataPlot1D *xPlot(void) const;

  /*!
  * \brief y plot
  *
  * \return y plot
  */
  gui::DataPlot1D *yPlot(void) const;

  /*!
  * \brief z plot
  *
  * \return z plot
  */
  gui::DataPlot1D *zPlot(void) const;
  
  /*!
  * \brief vx plot
  *
  * \return vx plot
  */
  //gui::DataPlot1D *vxPlot(void) const;

  /*!
  * \brief vy plot
  *
  * \return vy plot
  */
  //gui::DataPlot1D *vyPlot(void) const;

  /*!
  * \brief vz plot
  *
  * \return vz plot
  */
  //gui::DataPlot1D *vzPlot(void) const;

protected:
  /*!
  * \brief Set datas
  *
  * \param position position
  * \param time time
  */
  void SetDatas(const core::Vector3Df &position,const core::Time time);
  
 
private:
  class NEDPosition_impl *pimpl_;
};
} // end namespace sensor
} // end namespace flair
#endif // NEDPOSITION_H
