// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file NeoM8N.h
 * \brief Class for NeoM8N gps receiver
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2017/08/30
 * \version 4.0
 */

#ifndef NEOM8N_H
#define NEOM8N_H

#include <Thread.h>
#include <NmeaGps.h>

namespace flair {
namespace core {
class SerialPort;
}
}

namespace flair {
namespace sensor {
/*! \class NeoM8N
*
* \brief Class for mb800 gps receiver
*/
class NeoM8N : public core::Thread, public NmeaGps {
public:
  /*!
  * \brief Constructor
  *
  * Construct a NeoM8N.
  * It will be child of the FrameworkManager.
	* 
  * \param name name
  * \param serialport serialport
  * \param NMEAFlags NMEA sentances to enable
  * \param priority priority of the Thread
  */
  NeoM8N(std::string name,
        core::SerialPort *serialport,NmeaGps::NMEAFlags_t NMEAFlags,uint8_t priority);

  /*!
  * \brief Destructor
  *
  */
  ~NeoM8N();

private:
  /*!
  * \brief Update using provided datas
  *
  * Reimplemented from IODevice.
  *
  * \param data data from the parent to process
  */
  void UpdateFrom(const core::io_data *data) override{};

  /*!
  * \brief Run function
  *
  * Reimplemented from Thread.
  *
  */
  void Run(void) override;

  void Sync(void);
  void SendUbx(char* buf,size_t length);//message without header/footer
  core::SerialPort *serialport;
};
} // end namespace sensor
} // end namespace flair
#endif // NEOM8N_H
