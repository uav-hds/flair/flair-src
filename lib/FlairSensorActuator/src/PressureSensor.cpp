// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2018/02/08
//  filename:   PressureSensor.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    Base class for PressureSensor
//
//
/*********************************************************************/

#include "PressureSensor.h"
#include <FrameworkManager.h>
#include <Tab.h>
#include <TabWidget.h>
#include <GroupBox.h>
#include <Layout.h>
#include <DataPlot1D.h>
#include <Matrix.h>

using std::string;
using namespace flair::core;
using namespace flair::gui;

namespace flair {
namespace sensor {

PressureSensor::PressureSensor(string name)
    : IODevice(getFrameworkManager(), name) {
  plot_tab = NULL;

  MatrixDescriptor *desc = new MatrixDescriptor(1, 1);
  desc->SetElementName(0, 0, name);
  output = new Matrix(this, desc, floatType);
  delete desc;
  AddDataToLog(output);

  main_tab = new Tab(getFrameworkManager()->GetTabWidget(), name);
  tab = new TabWidget(main_tab->NewRow(), name);
  sensor_tab = new Tab(tab, "Setup");
  setup_groupbox = new GroupBox(sensor_tab->NewRow(), name);
}

PressureSensor::~PressureSensor() {
  delete main_tab;
}

GroupBox *PressureSensor::GetGroupBox(void) const { return setup_groupbox; }

Layout *PressureSensor::GetLayout(void) const { return sensor_tab; }

DataPlot1D *PressureSensor::GetPlot(void) const { return plot; }

Tab *PressureSensor::GetPlotTab(void) const { return plot_tab; }

void PressureSensor::UseDefaultPlot(void) {
  plot_tab = new Tab(tab, "Mesures");
  plot = new DataPlot1D(plot_tab->NewRow(), ObjectName(), 101000, 101500);
  plot->AddCurve(output->Element(0));
}

void PressureSensor::LockUserInterface(void) const {
  setup_groupbox->setEnabled(false);
}

void PressureSensor::UnlockUserInterface(void) const {
  setup_groupbox->setEnabled(true);
}

float PressureSensor::Value(void) const { return output->Value(0, 0); }

} // end namespace sensor
} // end namespace flair
