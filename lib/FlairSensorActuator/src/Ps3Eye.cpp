// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2012/01/19
//  filename:   Ps3Eye.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    objet integrant la camera Ps3Eye
//
//
/*********************************************************************/
#ifdef ARMV7A

#include "Ps3Eye.h"

using std::string;
using namespace flair::core;

namespace flair {
namespace sensor {

Ps3Eye::Ps3Eye(string name, int camera_index,bool useMemoryUsrPtr,
               uint8_t priority)
    : V4LCamera( name, camera_index, 320, 240,
                Image::Type::Format::YUYV, useMemoryUsrPtr,priority) {
  SetIsReady(true);                  
}

Ps3Eye::~Ps3Eye() {}

} // end namespace sensor
} // end namespace flair

#endif