// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file Ps3Eye.h
 * \brief Class for Ps3Eye camera
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2012/01/19
 * \version 4.0
 */

#ifndef PS3EYE_H
#define PS3EYE_H

#include "V4LCamera.h"

namespace flair {
namespace sensor {
/*! \class Ps3Eye
*
* \brief Class for Ps3Eye camera
*/
class Ps3Eye : public V4LCamera {

public:
  /*!
  * \brief Constructor
  *
  * Construct a Ps3Eye.
	* It will be child of the FrameworkManager.
  *
  * \param name name
  * \param camera_index index of the camera, ie /dev/videox
  * \param useMemoryUsrPtr true to use memory user pointer (ex with cmem on omap3730), false to use mmap
  * \param priority priority of the Thread
  */
  Ps3Eye(std::string name,
         int camera_index, bool useMemoryUsrPtr,uint8_t priority);

  /*!
  * \brief Destructor
  *
  */
  ~Ps3Eye();

private:
};
} // end namespace sensor
} // end namespace flair

#endif // PS3EYE_H
