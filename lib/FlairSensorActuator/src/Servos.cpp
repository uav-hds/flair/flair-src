// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2019/11/28
//  filename:   Servos.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    Virtual class for servos
//
//
/*********************************************************************/

#include "Servos.h"
#include "Servos_impl.h"
#include <DoubleSpinBox.h>

using std::string;
using namespace flair::core;
using namespace flair::gui;

namespace flair {
namespace actuator {

Servos::Servos(const IODevice *parent, Layout *layout, string name,
           uint8_t motors_count)
    : IODevice(parent, name) {
  pimpl_ = new Servos_impl(this, layout, name, motors_count);

}

Servos::~Servos() { delete pimpl_; }

void Servos::UpdateFrom(const io_data *data) { pimpl_->UpdateFrom(data); }

void Servos::LockUserInterface(void) const { pimpl_->LockUserInterface(); }

void Servos::UnlockUserInterface(void) const { pimpl_->UnlockUserInterface(); }

Layout *Servos::GetLayout(void) const { return (Layout *)pimpl_->layout; }

void Servos::UseDefaultPlot(TabWidget *tabwidget) {
  pimpl_->UseDefaultPlot(tabwidget);
}

uint8_t Servos::ServosCount(void) const { return pimpl_->servos_count; }

bool Servos::AreEnabled(void) const { return pimpl_->are_enabled; }

void Servos::SetEnabled(bool status) {
  if (pimpl_->are_enabled != status) {
    pimpl_->are_enabled = status;
    if (pimpl_->are_enabled) {
      LockUserInterface();
    } else {
      UnlockUserInterface();
    }
  }
}

} // end namespace sensor
} // end namespace flair
