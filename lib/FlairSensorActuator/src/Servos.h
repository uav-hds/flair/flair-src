// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file Servos.h
 * \brief Base class for servos
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2019/11/28
 * \version 4.0
 */

#ifndef SERVOS_H
#define SERVOS_H

#include <IODevice.h>

namespace flair {
namespace gui {
class Layout;
class TabWidget;
}
}

class Servos_impl;

namespace flair {
namespace actuator {
/*! \class Servos
*
* \brief Base class for brushless motors drivers
*/
class Servos : public core::IODevice {
  friend class ::Servos_impl;

public:
  /*!
  * \brief Constructor
  *
  * Construct a Servos.
  *
  * \param parent parent
  * \param layout layout
  * \param name name
  * \param servos_count number of servos
  */
  Servos(const core::IODevice *parent, gui::Layout *layout, std::string name,
       uint8_t servos_count);

  /*!
  * \brief Destructor
  *
  */
  ~Servos();

  /*!
  * \brief Lock user interface
  *
  */
  void LockUserInterface(void) const;

  /*!
  * \brief Unlock user interface
  *
  */
  void UnlockUserInterface(void) const;

  /*!
  * \brief Use default plot
  *
  * \param tabwidget TabWidget to draw plots
  */
  void UseDefaultPlot(gui::TabWidget *tabwidget);


  /*!
  * \brief Servos count
  *
  * \return number of servos
  */
  uint8_t ServosCount(void) const;

  /*!
  * \brief Enable servos
  *
  * \param true to enable all servos
  */
  void SetEnabled(bool status);

  /*!
  * \brief Are servos enabled?
  *
  * \return true if servos are enabled
  */
  bool AreEnabled(void) const;

  /*!
  * \brief Layout
  *
  * This the same Layout as passed to the constructor
  *
  * \return a Layout
  */
  gui::Layout *GetLayout(void) const;

 
protected:
  

private:
  /*!
  * \brief Update using provided datas
  *
  * Reimplemented from IODevice.
  *
  * \param data data from the parent to process
  */
  void UpdateFrom(const core::io_data *data) override;

  /*!
  * \brief Set servos position
  *
  * values size must be the same as ServosCount()
  *
  * \param position set servos position in radians
  */
  virtual void SetServos(float *position) = 0;

  class Servos_impl *pimpl_;
};
} // end namespace actuator
} // end namespace flair
#endif // SERVOS_H
