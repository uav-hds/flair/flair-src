// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2019/11/28
//  filename:   Servos_impl.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    Virtual class for servos
//
//
/*********************************************************************/
#include "Servos_impl.h"
#include "Servos.h"
#include <GridLayout.h>
#include <GroupBox.h>
#include <DoubleSpinBox.h>
#include <Matrix.h>
#include <Label.h>
#include <PushButton.h>
#include <DataPlot1D.h>
#include <TabWidget.h>
#include <Tab.h>
#include <sstream>

using std::string;
using std::ostringstream;
using namespace flair::core;
using namespace flair::gui;
using namespace flair::actuator;

Servos_impl::Servos_impl(Servos *self, Layout *layout, string name,
                     uint8_t servos_count) {
  this->self = self;
  this->servos_count = servos_count;
  this->layout = layout;
  are_enabled = false;
  tested_servo = -1;

  values = (float *)malloc(servos_count * sizeof(float));
  if(values == NULL) {
    self->Err("buffer malloc %i bytes error\n",servos_count*sizeof(float));
  }
  
  // station sol
  GroupBox *groupbox = new GroupBox(layout->NewRow(), "Servos");
  min_value = new DoubleSpinBox(groupbox->NewRow(), "min value:", -3.15, 3.15, .1, 2);
  max_value =
      new DoubleSpinBox(groupbox->LastRowLastCol(), "max value:", -3.15, 3.15, .1, 2);
  test_value =
      new DoubleSpinBox(groupbox->LastRowLastCol(), "test value:", -3.15, 3.15, 0.1);

  int index = 0;
  button_test = (PushButton **)malloc(servos_count * sizeof(PushButton *));
  if(button_test == NULL) {
    self->Err("buffer malloc %i bytes error\n",servos_count*sizeof(PushButton *));
  }
  for (int i = 0; i < servos_count / 2; i++) {
    for (int j = 0; j < 2; j++) {
      ostringstream test_name;
      test_name << "test servo " << index;
      button_test[index] =
          new PushButton(groupbox->At(2 + i, j), test_name.str());
      index++;
    }
  }
}

Servos_impl::~Servos_impl() {
  if (values != NULL)
    free(values);
  if (button_test != NULL)
    free(button_test);
}

void Servos_impl::UseDefaultPlot(TabWidget *tab) {
 
}

void Servos_impl::UpdateFrom(const io_data *data) {
  Matrix *input = (Matrix *)data;
  bool is_motor_running = false;

  if (values == NULL)
    return; // nothing to do in simulator

  if (input->Rows() != servos_count) {
    self->Err("nb servos mismatch\n");
    return;
  }

  input->GetMutex();
  for (int i = 0; i < servos_count; i++) {
    if (are_enabled) {
      // Printf("%i %f %f\n",i,input->ValueNoMutex(i,0),power[i]);
      values[i] =Sat(input->ValueNoMutex(i, 0));
      // Printf("%i %f\n",i,values[i]);
    } else {
      values[i] = 0;
    }
    if(values[i] != 0) is_motor_running = true;
  }
  input->ReleaseMutex();
  

  for (int i = 0; i < servos_count; i++) {
    if (button_test[i]->Clicked() == true) {
      if (!are_enabled) {
        tested_servo = i;
        test_start_time = GetTime();
        LockUserInterface();
      } else {
        self->Warn("testing servo is not possible when enabled\n");
      }
    }
  }
  if (tested_servo != -1) {
    for (int i = 0; i < servos_count; i++) {
      values[i] = 0;
    }
    values[tested_servo] = test_value->Value();

    if (GetTime() > (test_start_time + 2 * 1000000000)) {
      tested_servo = -1;
      UnlockUserInterface();
    }
  }

  self->SetServos(values);
}

float Servos_impl::Sat(float value) {
  float result = value;

  if (result < min_value->Value()) {
    result = min_value->Value();
  }
  if (result > max_value->Value()) {
    result = max_value->Value();
  }

  return result;
}

void Servos_impl::LockUserInterface(void) const { layout->setEnabled(false); }

void Servos_impl::UnlockUserInterface(void) const { layout->setEnabled(true); }
