// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2014/02/07
//  filename:   SimulatedBldc.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    Class for a simulation bldc
//
//
/*********************************************************************/
#ifdef CORE2_64

#include "SimulatedBldc.h"
#include <FrameworkManager.h>
#include <GridLayout.h>
#include <DoubleSpinBox.h>
#include <GroupBox.h>
#include <SharedMem.h>
#include <Matrix.h>
#include <sstream>
#include <string.h>

using std::string;
using std::ostringstream;
using namespace flair::core;
using namespace flair::gui;

namespace flair {
namespace actuator {

SimulatedBldc::SimulatedBldc(const IODevice *parent, Layout *layout, string name,
                   uint8_t motors_count, uint32_t modelId,uint32_t deviceId)
    : Bldc(parent, layout, name, motors_count) {
  shmem =
      new SharedMem(this, ShMemName(modelId, deviceId), motors_count * sizeof(float)+sizeof(Time));

  GroupBox *groupbox = new GroupBox(layout->NewRow(), "simubldc");
  k = new DoubleSpinBox(groupbox->NewRow(), "k driver:", 0, 10000, 1);
  
  buf=(char*)malloc(motors_count * sizeof(float)+sizeof(Time));
  if(buf==NULL) {
    Err("buffer malloc %i bytes error\n",motors_count * sizeof(float)+sizeof(Time));
    return;
  }
  SetIsReady(true);
}

SimulatedBldc::~SimulatedBldc() {
  if(buf!=NULL) free(buf);
}

string SimulatedBldc::ShMemName(uint32_t modelId,uint32_t deviceId) {
  ostringstream dev_name;
  dev_name << "simu" <<  modelId << "_bldc_" << deviceId;
  return dev_name.str().c_str();
}

void SimulatedBldc::SetMotors(float *value) {
  float *values=(float*)buf;
  for (int i = 0; i < MotorsCount(); i++) values[i] = k->Value() * value[i];
  Time time=GetTime();
  memcpy(buf+MotorsCount() * sizeof(float),&time,sizeof(Time));

  shmem->Write(buf, MotorsCount() * sizeof(float)+sizeof(Time));

  // on prend une fois pour toute le mutex et on fait des accès directs
  output->GetMutex();
  for (int i = 0; i < MotorsCount(); i++) {
    output->SetValueNoMutex(i, 0, values[i]);
  }
  output->ReleaseMutex();
}

} // end namespace sensor
} // end namespace flair

#endif