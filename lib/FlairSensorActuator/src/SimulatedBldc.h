// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file SimulatedBldc.h
 * \brief Class for a simulation bldc
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2014/02/07
 * \version 4.0
 */

#ifndef SIMULATEDBLDC_H
#define SIMULATEDBLDC_H

#include <Bldc.h>

namespace flair {
namespace core {
class SharedMem;
class IODevice;
}
namespace gui {
class DoubleSpinBox;
class Layout;
}
}

namespace flair {
namespace actuator {
/*! \class SimulatedBldc
*
* \brief Class for a simulation bldc
*
*/
class SimulatedBldc : public Bldc {
public:
  /*!
  * \brief Constructor
  *
  * Construct a SimulatedBldc
  *
  * \param parent parent
  * \param layout layout
  * \param name name
  * \param motors_count number of motors
  * \param modelId Model id
  * \param deviceId Bldc id of the Model
  */
  SimulatedBldc(const core::IODevice *parent, gui::Layout *layout, std::string name,
           uint8_t motors_count, uint32_t modelId,uint32_t deviceId);

  /*!
  * \brief Destructor
  *
  */
  ~SimulatedBldc();

  /*!
  * \brief Has speed measurement
  *
  * Reimplemented from Bldc. \n
  *
  * \return true if it has speed measurement
  */
  bool HasSpeedMeasurement(void) const  override{ return false; };

  /*!
  * \brief Has current measurement
  *
  * Reimplemented from Bldc. \n
  *
  * \return true if it has current measurement
  */
  bool HasCurrentMeasurement(void) const  override{ return false; };

private:
  /*!
  * \brief Set motors values
  *
  * Reimplemented from Bldc. \n
  * Values size must be the same as MotorsCount()
  *
  * \param values motor values
  */
  void SetMotors(float *value) override;
  
  std::string ShMemName(uint32_t modelId,uint32_t deviceId);
  core::SharedMem *shmem;
  gui::DoubleSpinBox *k;
  char *buf;

};
} // end namespace actuator
} // end namespace flair
#endif // SIMULATEDBLDC_H
