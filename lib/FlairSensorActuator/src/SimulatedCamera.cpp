// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2014/03/06
//  filename:   SimulatedCamera.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    Class for a simulation camera
//
//
/*********************************************************************/
#ifdef CORE2_64

#include "SimulatedCamera.h"
#include <FrameworkManager.h>
#include <GroupBox.h>
#include <SharedMem.h>
#include <sstream>
#include <string.h>

using std::string;
using std::ostringstream;
using namespace flair::core;
using namespace flair::gui;

namespace flair {
namespace sensor {

SimulatedCamera::SimulatedCamera(string name,
                       uint16_t width, uint16_t height, uint8_t channels,
                       uint32_t modelId,uint32_t deviceId, uint8_t priority)
    : Thread(getFrameworkManager(), name, priority),
      Camera(name, width, height, Image::Type::Format::BGR) {

  buf_size = width * height * channels+sizeof(Time);

  shmemReadBuf[0]=(char*)malloc(buf_size);
  if (shmemReadBuf[0] == NULL) {
    Thread::Err("buffer malloc %i bytes error (stack size %i)\n",buf_size,GetStackSize());
  }
  shmemReadBuf[1]=(char*)malloc(buf_size);
  if (shmemReadBuf[1] == NULL) {
    Thread::Err("buffer malloc %i bytes error (stack size %i)\n",buf_size,GetStackSize());
  }
  unusedBuffer=0;
  output->buffer = shmemReadBuf[1];

  shmem = new SharedMem((Thread *)this,ShMemName(modelId, deviceId), buf_size, SharedMem::Type::producerConsumer);
  
  SetIsReady(true);
}


SimulatedCamera::~SimulatedCamera() {
  SafeStop();
  Join();
  free(shmemReadBuf[0]);
  free(shmemReadBuf[1]);
}

string SimulatedCamera::ShMemName(uint32_t modelId,uint32_t deviceId) {
  ostringstream dev_name;
  dev_name << "simu" <<  modelId << "_cam_" << deviceId;
  return dev_name.str().c_str();
}

void SimulatedCamera::Run(void) {
  Time time;

  shmem->ReaderReady();

  while (!ToBeStopped()) {
    //blocking read
    if (shmem->Read(shmemReadBuf[unusedBuffer], buf_size, 1000000000)) { // we copy in the unused buffer
      memcpy(&time, shmemReadBuf[unusedBuffer] + buf_size - sizeof(Time), sizeof(Time));
      output->GetMutex();
      output->buffer=shmemReadBuf[unusedBuffer];
      output->SetDataTime(time);
      output->ReleaseMutex(); //unused buffer becomes new used buffer
      
      unusedBuffer=(unusedBuffer+1)%2;

      ProcessUpdate(output);
    }
  }
}

} // end namespace sensor
} // end namespace flair

#endif
