// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file SimulatedCamera.h
 * \brief Class for a simulation camera
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2014/03/06
 * \version 4.0
 */

#ifndef SIMULATEDCAMERA_H
#define SIMULATEDCAMERA_H

#include <Camera.h>
#include <Thread.h>

namespace flair {
  namespace core {
    class SharedMem;
  }
}

namespace flair {
namespace sensor {
/*! \class SimulatedCamera
*
* \brief Class for a simulation camera
*/
class SimulatedCamera : public core::Thread, public Camera {
public:
  /*!
  * \brief Constructor
  *
  * Construct a SimulatedCamera.
	* It will be child of the FrameworkManager.
  *
  * \param name name
  * \param width width
  * \param height height
  * \param channels number of channels
  * \param modelId Model id
  * \param deviceId Camera id of the Model
  * \param priority priority of the Thread
  */
  SimulatedCamera(std::string name,
             uint16_t width, uint16_t height, uint8_t channels, uint32_t modelId,uint32_t deviceId,
             uint8_t priority);


  /*!
  * \brief Destructor
  *
  */
  ~SimulatedCamera();

protected:
  /*!
  * \brief SharedMem to access datas
  *
  */
  core::SharedMem *shmem;

private:
  /*!
  * \brief Run function
  *
  * Reimplemented from Thread.
  *
  */
  void Run(void) override;

  /*!
  * \brief Update using provided datas
  *
  * Reimplemented from IODevice.
  *
  * \param data data from the parent to process
  */
  void UpdateFrom(const core::io_data *data) override{};
  
  std::string ShMemName(uint32_t modelId,uint32_t deviceId);

  size_t buf_size;
  char* shmemReadBuf[2];
  unsigned char unusedBuffer;
};
} // end namespace sensor
} // end namespace flair
#endif // SIMULATEDCAMERA_H
