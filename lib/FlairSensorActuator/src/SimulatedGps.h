// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file SimulatedGps.h
 * \brief Class for a simulation GPS
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2013/08/23
 * \version 4.0
 */

#ifndef SimulatedGPS_H
#define SimulatedGPS_H

#include <Thread.h>
#include <NmeaGps.h>

namespace flair {
  namespace core {
    class SharedMem;
  }
  namespace gui {
    class SpinBox;
    class DoubleSpinBox;
  }
}

namespace flair {
namespace sensor {
/*! \class SimulatedGps
*
* \brief Class for a simulation GPS
*/
class SimulatedGps : public core::Thread, public NmeaGps {
public:
  /*!
  * \brief Constructor
  *
  * Construct a simulation GPS
  * It will be child of the FrameworkManager.
	* 
  * \param name name
  * \param NMEAFlags NMEA sentances to enable
  * \param modelId Model id
  * \param deviceId Gps id of the Model
  * \param priority priority of the Thread
  */
  SimulatedGps(std::string name,
          NmeaGps::NMEAFlags_t NMEAFlags, uint32_t modelId,uint32_t deviceId,uint8_t priority);

  /*!
  * \brief Destructor
  *
  */
  ~SimulatedGps();

private:
  /*!
  * \brief Update using provided datas
  *
  * Reimplemented from IODevice.
  *
  * \param data data from the parent to process
  */
  void UpdateFrom(const core::io_data *data) override{};

  /*!
  * \brief Run function
  *
  * Reimplemented from Thread.
  *
  */
  void Run(void) override;

  typedef struct {
    float e;
    float n;
    float u;
    float ve;
    float vn;
  } gps_states_t;
  
  std::string ShMemName(uint32_t modelId,uint32_t deviceId);

  core::SharedMem *shmem;
  gui::SpinBox *dataRate,*fixQuality,*numberOfSatellites;
  gui::DoubleSpinBox *latitudeRef,*longitudeRef,*altitudeRef,*magneticDeclination;
};
} // end namespace sensor
} // end namespace flair
#endif // SIMULATEDGPS_H
