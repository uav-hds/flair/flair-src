// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2014/02/07
//  filename:   SimulatedImu.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    Class for a simulation imu
//
//
/*********************************************************************/
#ifdef CORE2_64

#include "SimulatedImu.h"
#include <FrameworkManager.h>
#include <ImuData.h>
#include <SpinBox.h>
#include <GroupBox.h>
#include <Matrix.h>
#include <SharedMem.h>
#include <AhrsData.h>
#include <sstream>

using std::string;
using std::ostringstream;
using namespace flair::core;
using namespace flair::gui;

namespace flair {
namespace sensor {

SimulatedImu::SimulatedImu(string name, uint32_t modelId,uint32_t deviceId,
                 uint8_t priority)
    : Imu(name,false), Thread(getFrameworkManager(), name, priority) {
  dataRate =
      new SpinBox(GetGroupBox()->NewRow(), "data rate", " Hz", 1, 500, 1, 200);
  ahrsData = new AhrsData((Imu *)this);
  
  shmem = new SharedMem((Thread *)this, ShMemName(modelId, deviceId),
                        sizeof(imu_states_t));
  SetIsReady(true);
}

SimulatedImu::~SimulatedImu() {
  SafeStop();
  Join();
}

string SimulatedImu::ShMemName(uint32_t modelId,uint32_t deviceId) {
  ostringstream dev_name;
  dev_name << "simu" <<  modelId << "_imu_" << deviceId;
  return dev_name.str().c_str();
}

void SimulatedImu::Run(void) {
  imu_states_t state;
  ImuData *imuData;
  GetDatas(&imuData);

  SetPeriodUS((uint32_t)(1000000. / dataRate->Value()));

  while (!ToBeStopped()) {
    WaitPeriod();

    if (dataRate->ValueChanged() == true) {
      SetPeriodUS((uint32_t)(1000000. / dataRate->Value()));
    }
    shmem->Read((char *)&state, sizeof(imu_states_t));
    Quaternion quaternion(state.q0, state.q1, state.q2, state.q3);
    Vector3Df angRate(state.wx, state.wy, state.wz);
    Vector3Df rawAcc(state.ax, state.ay, state.az);
    Vector3Df rawMag(state.mx, state.my, state.mz);
    Vector3Df rawGyr(state.wx, state.wy, state.wz);
    //we do not need rotation in simulation
    /*
    ApplyRotation(angRate);
    ApplyRotation(quaternion);
    ApplyRotation(rawAcc);
    ApplyRotation(rawMag);
    ApplyRotation(rawGyr);*/
    ahrsData->SetQuaternionAndAngularRates(quaternion,angRate);
    imuData->SetRawAccMagAndGyr(rawAcc,rawMag,rawGyr);
    imuData->SetDataTime(GetTime());
    ahrsData->SetDataTime(GetTime());
    ProcessUpdate(ahrsData);
  }
}

} // end namespace sensor
} // end namespace flair

#endif