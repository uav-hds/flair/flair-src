// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file SimulatedImu.h
 * \brief Class for a simulation Imu
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2014/02/07
 * \version 4.0
 */

#ifndef SIMULATEDIMU_H
#define SIMULATEDIMU_H

#include <Imu.h>
#include <Thread.h>

namespace flair {
  namespace core {
    class SharedMem;
    class AhrsData;
  }
  namespace gui {
    class SpinBox;
  }
}

namespace flair {
namespace sensor {
/*! \class SimulatedImu
*
* \brief Class for a simulation Imu
*/
class SimulatedImu : public Imu, public core::Thread {
public:
  /*!
  * \brief Constructor
  *
  * Construct a SimulatedImu.
	* It will be child of the FrameworkManager.
  *
  * \param name name
  * \param modelId Model id
  * \param deviceId Imu id of the Model
  * \param priority priority of the Thread
  */
  SimulatedImu(std::string name,
          uint32_t modelId,uint32_t deviceId, uint8_t priority);

  /*!
  * \brief Destructor
  *
  */
  ~SimulatedImu();

private:
  /*!
  * \brief Run function
  *
  * Reimplemented from Thread.
  *
  */
  void Run(void) override;

  /*!
  * \brief Update using provided datas
  *
  * Reimplemented from IODevice.
  *
  * \param data data from the parent to process
  */
  void UpdateFrom(const core::io_data *data) override{};

  typedef struct {
    float q0;
    float q1;
    float q2;
    float q3;
    float wx;
    float wy;
    float wz;
    float ax;
    float ay;
    float az;
    float mx;
    float my;
    float mz;
  } imu_states_t;
  
  std::string ShMemName(uint32_t modelId,uint32_t deviceId);

  gui::SpinBox *dataRate;
  core::SharedMem *shmem;
  core::AhrsData *ahrsData;
};
} // end namespace sensor
} // end namespace flair
#endif // SIMULATEDIMU_H
