// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2014/02/07
//  filename:   SimulatedUs.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    Class for a simulation us
//
//
/*********************************************************************/
#ifdef CORE2_64

#include "SimulatedLaser.h"
#include <FrameworkManager.h>
#include <SpinBox.h>
#include <GroupBox.h>
#include <Matrix.h>
#include <SharedMem.h>
#include <sstream>

using std::string;
using std::ostringstream;
using namespace flair::core;
using namespace flair::gui;

namespace flair {
namespace sensor {

SimulatedLaser::SimulatedLaser(string name,
                     uint32_t modelId,uint32_t deviceId, uint8_t priority)
    : Thread(getFrameworkManager(), name, priority), LaserRangeFinder(name) {
  data_rate =
      new SpinBox(GetGroupBox()->NewRow(), "data rate", " Hz", 1, 500, 1, 50);

  shmem = new SharedMem((Thread *)this, ShMemName(modelId, deviceId),
                        360 * sizeof(float));
  SetIsReady(true);
}

SimulatedLaser::~SimulatedLaser() {
  SafeStop();
  Join();
}

string SimulatedLaser::ShMemName(uint32_t modelId,uint32_t deviceId) {
  ostringstream dev_name;
  dev_name << "simu" <<  modelId << "_laser_" << deviceId;
  return dev_name.str().c_str();
}

void SimulatedLaser::Run(void) {
  float z[360];

  SetPeriodUS((uint32_t)(1000000. / data_rate->Value()));

  while (!ToBeStopped()) {
    WaitPeriod();

    shmem->Read((char *)z, 360 * sizeof(float));

    if (data_rate->ValueChanged() == true) {
      SetPeriodUS((uint32_t)(1000000. / data_rate->Value()));
    }
    for (int i = 0; i < 360; i++) {
      output->SetValue(i, 0, z[i]); //*******
    }
    output->SetDataTime(GetTime());
    ProcessUpdate(output);
  }
}

} // end namespace sensor
} // end namespace flair

#endif