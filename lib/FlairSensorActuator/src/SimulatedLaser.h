// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file SimulatedUs.h
 * \brief Class for a simulation UsRangeFinder
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2014/02/07
 * \version 4.0
 */

#ifndef SIMULATEDLASER_H
#define SIMULATEDLASER_H

#include <LaserRangeFinder.h>
#include <Thread.h>

namespace flair {
namespace core {
class SharedMem;
}
namespace gui {
class SpinBox;
}
}

namespace flair {
namespace sensor {
/*! \class SimulatedUs
*
* \brief Class for a simulation UsRangeFinder
*/
class SimulatedLaser : public core::Thread, public LaserRangeFinder {
public:
  /*!
  * \brief Constructor
  *
  * Construct a SimulatedUs.
  *
  * \param name name
  * \param modelId Model id
  * \param deviceId LaserRangeFinder id of the Model
  * \param priority priority of the Thread
  */
  SimulatedLaser(std::string name,
            uint32_t modelId,uint32_t deviceId, uint8_t priority);

  /*!
  * \brief Destructor
  *
  */
  ~SimulatedLaser();

protected:
  /*!
  * \brief SharedMem to access datas
  *
  */
  core::SharedMem *shmem;

private:
  /*!
  * \brief Update using provided datas
  *
  * Reimplemented from IODevice.
  *
  * \param data data from the parent to process
  */
  void UpdateFrom(const core::io_data *data) override{};

  /*!
  * \brief Run function
  *
  * Reimplemented from Thread.
  *
  */
  void Run(void) override;
  
  std::string ShMemName(uint32_t modelId,uint32_t deviceId);
  gui::SpinBox *data_rate;
};
} // end namespace sensor
} // end namespace flair
#endif // SIMULATEDLASER_H
