// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file SimulatedPressureSensor.h
 * \brief Class for a simulation PressureSensor
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2018/05/24
 * \version 4.0
 */

#ifndef SIMULATEDPRESSURESENSOR_H
#define SIMULATEDPRESSURESENSOR_H

#include <PressureSensor.h>
#include <Thread.h>

namespace flair {
namespace core {
class SharedMem;
}
namespace gui {
class SpinBox;
}
}

namespace flair {
namespace sensor {
/*! \class SimulatedPressureSensor
*
* \brief Class for a simulation PressureSensor
*/
class SimulatedPressureSensor : public core::Thread, public PressureSensor {
public:
  /*!
  * \brief Constructor
  *
  * Construct a SimulatedPressureSensor.
	* It will be child of the FrameworkManager.
  *
  * \param name name
  * \param modelId Model id
  * \param deviceId PressureSensor id of the Model
  * \param priority priority of the Thread
  */
  SimulatedPressureSensor(std::string name,
         uint32_t modelId,uint32_t deviceId, uint8_t priority);

  /*!
  * \brief Destructor
  *
  */
  ~SimulatedPressureSensor();

protected:
  /*!
  * \brief SharedMem to access datas
  *
  */
  core::SharedMem *shmem;

private:
  /*!
  * \brief Update using provided datas
  *
  * Reimplemented from IODevice.
  *
  * \param data data from the parent to process
  */
  void UpdateFrom(const core::io_data *data) override{};

  /*!
  * \brief Run function
  *
  * Reimplemented from Thread.
  *
  */
  void Run(void) override;
  
  std::string ShMemName(uint32_t modelId,uint32_t deviceId);
  gui::SpinBox *data_rate;
};
} // end namespace sensor
} // end namespace flair
#endif // SIMULATEDPRESSURESENSOR_H
