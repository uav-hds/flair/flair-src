// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2019/11/28
//  filename:   SimulatedServos.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    Class for a simulation servos
//
//
/*********************************************************************/
#ifdef CORE2_64

#include "SimulatedServos.h"
#include <FrameworkManager.h>
#include <SharedMem.h>
#include <Matrix.h>
#include <sstream>
#include <string.h>

using std::string;
using std::ostringstream;
using namespace flair::core;
using namespace flair::gui;

namespace flair {
namespace actuator {

SimulatedServos::SimulatedServos(const IODevice *parent, Layout *layout, string name,
                   uint8_t servos_count, uint32_t modelId,uint32_t deviceId)
    : Servos(parent, layout, name, servos_count) {
  shmem =
      new SharedMem(this, ShMemName(modelId, deviceId), servos_count * sizeof(float)+sizeof(Time));

  
  buf=(char*)malloc(servos_count * sizeof(float)+sizeof(Time));
  if(buf==NULL) {
    Err("buffer malloc %i bytes error\n",servos_count * sizeof(float)+sizeof(Time));
    return;
  }
  SetIsReady(true);
}

SimulatedServos::~SimulatedServos() {
  if(buf!=NULL) free(buf);
}

string SimulatedServos::ShMemName(uint32_t modelId,uint32_t deviceId) {
  ostringstream dev_name;
  dev_name << "simu" <<  modelId << "_servos_" << deviceId;
  return dev_name.str().c_str();
}

void SimulatedServos::SetServos(float *position) {
  float *values=(float*)buf;
  for (int i = 0; i < ServosCount(); i++) values[i] = position[i];
  Time time=GetTime();
  memcpy(buf+ServosCount() * sizeof(float),&time,sizeof(Time));

  shmem->Write(buf, ServosCount() * sizeof(float)+sizeof(Time));

}

} // end namespace sensor
} // end namespace flair

#endif