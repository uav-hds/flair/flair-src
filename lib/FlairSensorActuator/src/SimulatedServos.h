// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file SimulatedServos.h
 * \brief Class for a simulation servos
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2019/11/28
 * \version 4.0
 */

#ifndef SIMULATEDSERVOS_H
#define SIMULATEDSERVOS_H

#include <Servos.h>

namespace flair {
namespace core {
class SharedMem;
class IODevice;
}
}

namespace flair {
namespace actuator {
/*! \class SimulatedServos
*
* \brief Class for a simulation bldc
*
*/
class SimulatedServos : public Servos {
public:
  /*!
  * \brief Constructor
  *
  * Construct a SimulatedServos
  *
  * \param parent parent
  * \param layout layout
  * \param name name
  * \param servos_count number of servos
  * \param modelId Model id
  * \param deviceId Bldc id of the Model
  */
  SimulatedServos(const core::IODevice *parent, gui::Layout *layout, std::string name,
           uint8_t servos_count, uint32_t modelId,uint32_t deviceId);

  /*!
  * \brief Destructor
  *
  */
  ~SimulatedServos();

private:
  /*!
  * \brief Set servos positions
  *
  * Reimplemented from Servos. \n
  * Values size must be the same as ServosCount()
  *
  * \param positions positions values in radians
  */
  void SetServos(float *positions);
  
  std::string ShMemName(uint32_t modelId,uint32_t deviceId);
  core::SharedMem *shmem;
  char *buf;

};
} // end namespace actuator
} // end namespace flair
#endif // SIMULATEDSERVOS_H
