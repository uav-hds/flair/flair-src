// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2020/&2/09
//  filename:   SimulatedUgvControls.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    Class for a simulation ugv controls
//
//
/*********************************************************************/
#ifdef CORE2_64

#include "SimulatedUgvControls.h"
#include <FrameworkManager.h>
#include <Matrix.h>
#include <SharedMem.h>
#include <sstream>
#include <string.h>

using std::string;
using std::ostringstream;
using namespace flair::core;

namespace flair {
namespace actuator {

SimulatedUgvControls::SimulatedUgvControls(string name, uint32_t modelId,uint32_t deviceId)
    : UgvControls(name) {
  
  shmem = new SharedMem(this, ShMemName(modelId, deviceId),2 * sizeof(float)+sizeof(Time));
  
  buf=(char*)malloc(2 * sizeof(float)+sizeof(Time));
  if(buf==NULL) {
    Err("buffer malloc %i bytes error\n",2 * sizeof(float)+sizeof(Time));
    return;
  }
  
  SetIsReady(true);
}

SimulatedUgvControls::~SimulatedUgvControls() {
  if(buf!=NULL) free(buf);
}

string SimulatedUgvControls::ShMemName(uint32_t modelId,uint32_t deviceId) {
  ostringstream dev_name;
  dev_name << "simu" <<  modelId << "_ugvcontrols_" << deviceId;
  return dev_name.str().c_str();
}

void SimulatedUgvControls::SetControls(float speed,float turn) {
  float *values=(float*)buf;
  values[0]=speed;
  values[1]=turn;
  Time time=GetTime();
  memcpy(buf+2 * sizeof(float),&time,sizeof(Time));

  shmem->Write(buf, 2 * sizeof(float)+sizeof(Time));

  // on prend une fois pour toute le mutex et on fait des accès directs
  output->GetMutex();
  for (int i = 0; i < 2; i++) {
    output->SetValueNoMutex(i, 0, values[i]);
  }
  output->ReleaseMutex();
  
  output->SetDataTime(GetTime());
  ProcessUpdate(output);
}


} // end namespace sensor
} // end namespace flair

#endif