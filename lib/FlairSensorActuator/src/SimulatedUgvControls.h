// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file SimulatedUgvControls.h
 * \brief Class for a simulation ugv controls
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2020/12/09
 * \version 4.0
 */

#ifndef SIMULATEDUGVCONTROLS_H
#define SIMULATEDUGVCONTROLS_H

#include <UgvControls.h>

namespace flair {
  namespace core {
    class SharedMem;
  }
}

namespace flair {
namespace actuator {
/*! \class SimulatedUgvControls
*
* \brief Class for a simulation ugv controls
*/
class SimulatedUgvControls : public UgvControls {
public:
  /*!
  * \brief Constructor
  *
  * Construct a SimulatedUgvControls.
  *
  * \param name name
  * \param modelId Model id
  * \param deviceId Imu id of the Model
  */
  SimulatedUgvControls(std::string name,
          uint32_t modelId,uint32_t deviceId);

  /*!
  * \brief Destructor
  *
  */
  ~SimulatedUgvControls();

  /*!
  * \brief Set controls values
  *
  * Reimplemented from UgvControls. \n
  *
  * \param speed speed value
  * \param turn turn value
  */
  void SetControls(float speed,float turn) override;
private:
 
  
  char *buf;
  std::string ShMemName(uint32_t modelId,uint32_t deviceId);
  core::SharedMem *shmem;
};
} // end namespace actuator
} // end namespace flair
#endif // SIMULATEDUGVCONTROLS_H
