// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file SumoUgvControls.h
 * \brief Class for a parrot sumo ugv controls
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2020/12/09
 * \version 4.0
 */

#ifndef SUMOUGVCONTROLS_H
#define SUMOUGVCONTROLS_H

#include <UgvControls.h>

namespace sumo {
    class Control;
}

namespace flair {
namespace actuator {
/*! \class SumoUgvControls
*
* \brief Class for a parrot sumo ugv controls
*/
class SumoUgvControls : public UgvControls {
public:
  /*!
  * \brief Constructor
  *
  * Construct a SumoUgvControls.
  *
  * \param name name
  */
  SumoUgvControls(std::string name);

  /*!
  * \brief Destructor
  *
  */
  ~SumoUgvControls();

  /*!
  * \brief Set controls values
  *
  * Reimplemented from UgvControls. \n
  *
  * \param speed speed value
  * \param turn turn value
  */
  void SetControls(float speed,float turn) override;
private:
 sumo::Control *sumoControl;
  
};
} // end namespace actuator
} // end namespace flair
#endif // SUMOUGVCONTROLS_H
