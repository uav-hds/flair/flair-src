// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2020/12/09
//  filename:   UgvControls.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    Virtual class for igv controls
//
//
/*********************************************************************/

#include "UgvControls.h"
#include <Matrix.h>
#include <FrameworkManager.h>
#include <Tab.h>
#include <DataPlot1D.h>

using namespace flair::core;
using namespace flair::gui;

using std::string;

namespace flair {
namespace actuator {

UgvControls::UgvControls(string name)
    : IODevice(getFrameworkManager(), name) {
  mainTab = new Tab(getFrameworkManager()->GetTabWidget(), name);
  
  MatrixDescriptor *desc = new MatrixDescriptor(2, 1);
  desc->SetElementName(0, 0, "speed");
  desc->SetElementName(1, 0, "turn");

  output = new Matrix(this, desc, floatType);
  delete desc;
  AddDataToLog(output);
}

UgvControls::~UgvControls() {
}

void UgvControls::UseDefaultPlot(void) {
  DataPlot1D *speedPlot = new DataPlot1D(mainTab->NewRow(), "speed", -1, 1);
  speedPlot->AddCurve(output->Element(0));
  DataPlot1D *turnPlot = new DataPlot1D(mainTab->LastRowLastCol(), "turn", -1, 1);
  turnPlot->AddCurve(output->Element(1));
}

Matrix *UgvControls::Output(void) const {
 return output;
}


} // end namespace actuator
} // end namespace flair
