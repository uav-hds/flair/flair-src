// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file UgvControls.h
 * \brief Base class for ugv controls
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2020/12/09
 * \version 4.0
 */

#ifndef UGVCONTROLS_H
#define UGVCONTROLS_H

#include <IODevice.h>

namespace flair {
namespace core {
  class Matrix;
}
namespace gui {
  class Tab;
}
}


namespace flair {
namespace actuator {
/*! \class UgvControls
*
* \brief Base class for ugv controls
*/
class UgvControls : public core::IODevice {
  
public:
  /*!
  * \brief Constructor
  *
  * Construct a UgvControls. It will be child of the FrameworkManager.
  *
  * \param name name
  */
  UgvControls(std::string name);

  /*!
  * \brief Destructor
  *
  */
  ~UgvControls();

  /*!
  * \brief Use default plot
  *
  */
  void UseDefaultPlot(void);

  /*!
  * \brief Output from motors
  *
  * First column is real speed if available, secund column is current if
  *available
  *
  */
  core::Matrix *Output(void) const;
  
  /*!
  * \brief Set controls values
  *
  * \param speed speed value
  * \param turn turn value
  */
  virtual void SetControls(float speed,float turn)=0;
  
protected:
  core::Matrix *output;

private:
  /*!
  * \brief Update using provided datas
  *
  * Reimplemented from IODevice.
  *
  * \param data data from the parent to process
  */
  void UpdateFrom(const core::io_data *data) override{};

  gui::Tab *mainTab;
};
} // end namespace actuator
} // end namespace flair
#endif // UGVCONTROLS_H
