// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2014/07/17
//  filename:   V4LCamera.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    base class for V4l camera
//
//
/*********************************************************************/

#include "V4LCamera.h"
#include "V4LCamera_impl.h"
#include <Image.h>
#include <FrameworkManager.h>

using std::string;
using namespace flair::core;

namespace flair {
namespace sensor {
  
V4LCamera::V4LCamera(string name,uint8_t camera_index, uint16_t width, uint16_t height,
                     Image::Type::Format format, bool useMemoryUsrPtr,uint8_t priority)
    : Thread(getFrameworkManager(), name, priority),
      Camera(name, width, height, format) {
  
  pimpl_=new V4LCamera_impl(this,name,camera_index,width,height,format,useMemoryUsrPtr);
}

V4LCamera::~V4LCamera() {
  SafeStop();
  Join();
  delete pimpl_;
}

void V4LCamera::Run(void) {
  pimpl_->Run();
}

bool V4LCamera::HasProblems(void) {
  return pimpl_->hasProblems;
}

void V4LCamera::SetAutoGain(bool value) {
  pimpl_->SetAutoGain(value);
}

void V4LCamera::SetAutoExposure(bool value) {
  Thread::Warn("not implemented\n");
}

void V4LCamera::SetGain(float value) {
  pimpl_->SetGain(value);
}

void V4LCamera::SetExposure(float value) {
  pimpl_->SetExposure(value);
}

void V4LCamera::SetBrightness(float value) {
  pimpl_->SetBrightness(value);
}

void V4LCamera::SetSaturation(float value) {
  pimpl_->SetSaturation(value);
}

void V4LCamera::SetHue(float value) {
  pimpl_->SetHue(value);
}

void V4LCamera::SetContrast(float value) {
  pimpl_->SetContrast(value);
}

} // end namespace sensor
} // end namespace flair
