// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file VrpnClient.h
 * \brief Class to connect to a Vrpn server
 * \author César Richard, Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS
 * 7253
 * \date 2013/04/03
 * \version 4.0
 */

#ifndef VRPNCLIENT_H
#define VRPNCLIENT_H

#include <Thread.h>

namespace flair {
	namespace core {
		class SerialPort;
	}
	namespace gui {
		class TabWidget;
		class Layout;
	}
}

class VrpnClient_impl;
class VrpnObject_impl;

namespace flair {
namespace sensor {
/*! \class VrpnClient
*
* \brief Class to connect to a Vrpn server. The Thread is created with 
*  the FrameworkManager as parent. FrameworkManager must be created before.
* Only one instance of this class is allowed by program.
*/
class VrpnClient : public core::Thread {
  friend class ::VrpnObject_impl;

public:
  typedef enum { Vrpn, VrpnLite, Xbee } ConnectionType_t;

  /*!
  * \brief Constructor
  *
  * Construct a VrpnClient. Connection is done by IP to a vrpn server or vrpnlite server (see tools/VrpnLite in flair-src)
  *
  * \param name name
  * \param address server address
  * \param priority priority of the Thread
  * \param connection connection type: Vrpn or VrpnLite
  */
  VrpnClient(std::string name,
             std::string address,  uint8_t priority,ConnectionType_t connectionType=Vrpn);
             
  /*!
  * \brief Constructor
  *
  * Construct a VrpnClient. Connection is done by XBee modem.
  *
  * \param name name
  * \param serialport SerialPort for XBee modem
  * \param us_period Xbee RX timeout in us
  * \param priority priority of the Thread
  */
  VrpnClient(std::string name,
             core::SerialPort *serialport, uint16_t us_period,
             uint8_t priority);

  /*!
  * \brief Destructor
  *
  */
  ~VrpnClient();

  /*!
  * \brief Setup Layout
  *
  * \return a Layout available
  */
  gui::Layout *GetLayout(void) const;

  /*!
  * \brief Setup Tab
  *
  * \return a Tab available
  */
  gui::TabWidget *GetTabWidget(void) const;
  
  ConnectionType_t ConnectionType(void) const;

private:
  /*!
  * \brief Run function
  *
  * Reimplemented from Thread.
  *
  */
  void Run(void) override;

  class VrpnClient_impl *pimpl_;
};

/*!
* \brief get VrpnClient
*
* \return the VrpnClient
*/
VrpnClient *GetVrpnClient(void);

} // end namespace sensor
} // end namespace flair
#endif // VRPNCLIENT_H
