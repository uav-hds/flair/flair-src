// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2013/04/03
//  filename:   VrpnObject.cpp
//
//  author:     César Richard, Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    Class for VRPN object
//
//
/*********************************************************************/

#include "VrpnObject.h"
#include "VrpnObject_impl.h"
#include <Matrix.h>

using std::string;
using namespace flair::core;
using namespace flair::gui;

namespace flair {
namespace sensor {

VrpnObject::VrpnObject(string name,
                       const TabWidget *tab,VrpnClient *client)
    : NEDPosition(client, name,tab) {
  pimpl_ = new VrpnObject_impl(this, name, -1,client);
  AddDataToLog(pimpl_->output);
  
  SetIsReady(true);
}

VrpnObject::VrpnObject(string name, uint8_t id,
                       const TabWidget *tab,VrpnClient *client)
    : NEDPosition(client, name,tab) {
  Warn("Creation of object %s with id %i\n", name.c_str(), id);
  pimpl_ = new VrpnObject_impl(this, name, id,client);
  AddDataToLog(pimpl_->output);
  
  SetIsReady(true);
}

VrpnObject::~VrpnObject(void) { 
  delete pimpl_;
}

Matrix *VrpnObject::Output(void) const { return pimpl_->output; }

Matrix *VrpnObject::State(void) const { return pimpl_->state; }

Time VrpnObject::GetLastPacketTime(void) const {
  return pimpl_->output->DataTime();
}

bool VrpnObject::IsTracked(unsigned int timeout_ms) const {
  return pimpl_->IsTracked(timeout_ms);
}

void VrpnObject::GetQuaternion(Quaternion &quaternion) const {
  pimpl_->GetQuaternion(quaternion);
}

} // end namespace sensor
} // end namespace flair
