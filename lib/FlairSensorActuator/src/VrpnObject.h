// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file VrpnObject.h
 * \brief Class for VRPN object
 * \author César Richard, Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS
 * 7253
 * \date 2013/04/03
 * \version 4.0
 */

#ifndef VRPNOBJECT_H
#define VRPNOBJECT_H

#include <NEDPosition.h>
#include <VrpnClient.h>
#include <Vector3D.h>

namespace flair {
	namespace core {
		class Matrix;
		class Quaternion;
	}
	namespace gui {
		class Tab;
	}
}

class VrpnObject_impl;

namespace flair {
namespace sensor {
class VrpnClient;

/*! \class VrpnObject
*
* \brief Class for VRPN object. The IODevice is created with 
*  the VrpnClient as parent. VrpnClient must be created before.
*/
class VrpnObject : public NEDPosition {
  friend class ::VrpnObject_impl;

public:
  /*!
  * \brief Constructor
  *
  * Construct a VrpnObject. Connection is done by IP with vrpn or vrpnlite (see tools/VrpnLite in flair-src)
  *
  * \param name VRPN object name, should be the same as defined in the server
  * \param tab Tab for the user interface
  * \param client VrpnClient of the connection, if unspecified, use the default one
  */
  VrpnObject(std::string name,
             const gui::TabWidget *tab,sensor::VrpnClient *client=GetVrpnClient());

  /*!
  * \brief Constructor
  *
  * Construct a VrpnObject. Connection is done by xbee 
  *
  * \param name name
  * \param id VRPN object id, should be the same as defined in the xbee bridge
  * \param tab Tab for the user interface
  * \param client VrpnClient of the connection, if unspecified, use the default one
  */
  VrpnObject(std::string name, uint8_t id,
             const gui::TabWidget *tab,sensor::VrpnClient *client=GetVrpnClient());

  /*!
  * \brief Destructor
  *
  */
  ~VrpnObject(void);

  /*!
  * \brief Get Last Packet Time
  *
  * \return Time of last received packet
  */
  core::Time GetLastPacketTime(void) const;

  /*!
  * \brief Is object tracked?
  *
  * \param timeout_ms timeout
  * \return true if object is tracked during timeout_ms time
  */
  bool IsTracked(unsigned int timeout_ms) const;

  /*!
  * \brief Get Quaternion
  *
  * \param quaternion output datas
  */
  void GetQuaternion(core::Quaternion &quaternion) const;
 

  /*!
  * \brief Output matrix
  *
  * Matrix is of type float and as follows: \n
  * (0,0) q0 \n
  * (1,0) q1 \n
  * (2,0) q2 \n
	* (3,0) q3 \n
  *
  * call NEDPosition::Output() to get position
  *
  * \return Output matrix
  */
  core::Matrix *Output(void) const;

  /*!
  * \brief State matrix
  *
	* Used for plotting on ground station. \n
  * Matrix is of type float and as follows: \n
  * (0,0) roll (deg) \n
  * (1,0) pitch (deg) \n
  * (2,0) yaw (deg) \n
  *
  * \return State matrix
  */
  core::Matrix *State(void) const;

private:
  /*!
  * \brief Update using provided datas
  *
  * Reimplemented from IODevice.
  *
  * \param data data from the parent to process
  */
  void UpdateFrom(const core::io_data *data) override{};

  class VrpnObject_impl *pimpl_;
};
} // end namespace sensor
} // end namespace flair
#endif // VRPNOBJECT_H
