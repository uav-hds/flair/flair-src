// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2019/11/28
//  filename:   Servos_impl.h
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    Virtual class for servos
//
//
/*********************************************************************/

#ifndef SERVOS_IMPL_H
#define SERVOS_IMPL_H

#include <IODevice.h>

namespace flair {
namespace gui {
class DoubleSpinBox;
class Layout;
class Label;
class DataPlot1D;
class TabWidget;
class PushButton;
}
namespace actuator {
class Servos;
}
}

class Servos_impl {
public:
  Servos_impl(flair::actuator::Servos *self, flair::gui::Layout *layout,
            std::string name, uint8_t servos_count);
  ~Servos_impl();
  void UpdateFrom(const flair::core::io_data *data);
  void LockUserInterface(void) const;
  void UnlockUserInterface(void) const;
  bool are_enabled;
  void UseDefaultPlot(flair::gui::TabWidget *tab);
  uint8_t servos_count;
  flair::gui::Layout *layout;

private:
  float *values;
  float Sat(float value);
  flair::actuator::Servos *self;
  flair::gui::DoubleSpinBox *min_value, *max_value, *test_value;
  flair::gui::DataPlot1D *plots;
  flair::gui::PushButton **button_test;
  flair::core::Time test_start_time;
  int tested_servo; //=-1 if no servo is tested
};

#endif // SERVOS_IMPL_H
