// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file V4LCamera.h
 * \brief Base class for V4l camera
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2014/07/17
 * \version 4.0
 */

#ifndef V4LCAMERA_IMPL_H
#define V4LCAMERA_IMPL_H

#include "V4LCamera.h"
#include <linux/videodev2.h>

#define DEFAULT_V4L_BUFFERS 4

namespace flair {
  namespace core {
    class Image;
  }
  namespace gui {
    class DoubleSpinBox;
    class CheckBox;
    class Label;
  }
}


/*! \class V4LCamera_impl
*
* \brief Base class for V4l camera
*/
class V4LCamera_impl {
public:
  /*!
  * \brief Constructor
  *
  * Construct a Camera.
	* It will be child of the FrameworkManager.
  *
  * \param name name
  * \param camera_index camera index
  * \param width width
  * \param height height
  * \param format image format
  * \param useMemoryUsrPtr true to use memory user pointer (ex with cmem on omap3730), false to use mmap
  */
  V4LCamera_impl(flair::sensor::V4LCamera *self,std::string name,
            uint8_t camera_index, uint16_t width, uint16_t height,
            flair::core::Image::Type::Format format,bool useMemoryUsrPtr);

  /*!
  * \brief Destructor
  *
  */
  ~V4LCamera_impl();
  
  /*!
  * \brief Run function
  *
  */
  void Run(void);
  
  bool hasProblems;
  
  /*!
  * \brief Set Gain
  *
  * \param value value between 0 and 1
  */
  void SetGain(float value);

  /*!
  * \brief Set Auto Gain
  *
  * \param value value
  */
  void SetAutoGain(bool value);

  /*!
  * \brief Set Exposure
  *
  * \param value value between 0 and 1
  */
  void SetExposure(float value);

  /*!
  * \brief Set Auto Exposure
  *
  * \param value value
  */
  void SetAutoExposure(bool value);

  /*!
  * \brief Set Brightness
  *
  * \param value value between 0 and 1
  */
  void SetBrightness(float value);

  /*!
  * \brief Set Saturation
  *
  * \param value value between 0 and 1
  */
  void SetSaturation(float value);

  /*!
  * \brief Set Hue
  *
  * \param value value between 0 and 1
  */
  void SetHue(float value);

  /*!
  * \brief Set Contrast
  *
  * \param value value between 0 and 1
  */
  void SetContrast(float value);
  
private:
 
  flair::sensor::V4LCamera *self;
  flair::gui::Tab *sensor_tab;
  flair::gui::DoubleSpinBox *bright, *exposure, *gain, *contrast, *hue, *sharpness,*sat;
  flair::gui::CheckBox *autogain, *awb, *autoexposure;
  flair::gui::Label *fps;
  
  int device;
  
  void* buffers[DEFAULT_V4L_BUFFERS];
  int nbBuffers;
  char *imageData;
  bool useMemoryUsrPtr;
  
  int Init(int width, int height,unsigned long colorspace);
  int xioctl( int fd, int request, void *arg);
  void SetProperty(int property,float value);
  float GetProperty(int property);
  int GrabFrame(void);
  int AllocV4LBuffers(void);
  int AllocUserBuffers(void);
  int QueueBuffer(int index);
  int QueueBuffer(struct v4l2_buffer *buf);
  struct v4l2_buffer dQueuedBuffer;
};

#endif // V4LCAMERA_IMPL_H
