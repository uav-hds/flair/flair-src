// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2013/04/03
//  filename:   VrpnClient_impl.h
//
//  author:     César Richard, Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    objet se connectant au serveur vrpn
//
//
/*********************************************************************/

#ifndef VRPNCLIENT_IMPL_H
#define VRPNCLIENT_IMPL_H

#include <string>
#include <vector>
#include "Vector3D.h"

namespace flair {
namespace core {
class OneAxisRotation;
class Quaternion;
class Mutex;
class SerialPort;
class UdpSocket;
}
namespace gui {
class TabWidget;
class Tab;
class Layout;
}
namespace sensor {
class VrpnClient;
class VrpnObject;
}
}

class VrpnObject_impl;
class vrpn_Connection;

class VrpnClient_impl {
public:
  VrpnClient_impl(flair::sensor::VrpnClient *self, std::string name,
                  std::string address,flair::sensor::VrpnClient::ConnectionType_t connectionType);
  VrpnClient_impl(flair::sensor::VrpnClient *self, std::string name,
                  flair::core::SerialPort *serialport, uint16_t us_period);
  
  ~VrpnClient_impl();
  void AddTrackable(VrpnObject_impl *obj);    // normal
  void AddTrackable(VrpnObject_impl *obj, uint8_t id);  // xbee
  void RemoveTrackable(VrpnObject_impl *obj);           // normal+xbee
  void ComputeRotations(flair::core::Vector3Df &point);
  void ComputeRotations(flair::core::Quaternion &quat);
  void Run(void);
  flair::gui::Tab *setup_tab;
  flair::gui::TabWidget *tab;
  vrpn_Connection *connection;
  flair::sensor::VrpnClient::ConnectionType_t connectionType;

private:
  void CommonConstructor(std::string name);
  float ConvertPosition(int16_t value) const;
  float ConvertQuaternion(int16_t value) const;
  flair::sensor::VrpnClient *self;
  flair::core::Mutex *mutex;
  uint16_t us_period;
  std::vector<VrpnObject_impl *> trackables;
  typedef struct xbeeObject_t {
    VrpnObject_impl *vrpnobject;
    uint8_t id;
  } xbeeObject_t;

  std::vector<xbeeObject_t> xbeeObjects;
  flair::gui::Tab *main_tab;
  flair::core::OneAxisRotation *rotation_1, *rotation_2;
  flair::core::SerialPort *serialport;
	bool isConnected;//only for ip connection, not for xbee
  std::string address;
  flair::core::UdpSocket* dataSocket;
  
};

#endif // VRPNCLIENT_IMPL_H
