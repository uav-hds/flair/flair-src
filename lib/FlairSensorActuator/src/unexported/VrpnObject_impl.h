// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2013/04/03
//  filename:   VrpnObject_impl.h
//
//  author:     César Richard, Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    objet vrpn
//
//
/*********************************************************************/

#ifndef VRPNOBJECT_IMPL_H
#define VRPNOBJECT_IMPL_H

#include <IODevice.h>
#include <vrpn_Tracker.h>
#include "Quaternion.h"

namespace flair {
	namespace core {
		class Matrix;
	}
	namespace sensor {
		class VrpnClient;
		class VrpnObject;
	}
}

class VrpnObject_impl {
  friend class VrpnClient_impl;

public:
  VrpnObject_impl(flair::sensor::VrpnObject *self,
                  std::string name,
                  int id,flair::sensor::VrpnClient *client);
  ~VrpnObject_impl(void);

  void GetQuaternion(flair::core::Quaternion &quaternion);
  bool IsTracked(unsigned int timeout_ms);

  flair::core::Matrix *output, *state;

  static void VRPN_CALLBACK handle_pos(void *userdata, const vrpn_TRACKERCB t);

private:
  flair::sensor::VrpnObject *self;
  const flair::sensor::VrpnClient *parent;
  vrpn_Tracker_Remote *tracker;
  void Update(void);
  flair::core::Time previousTime;
  std::vector<flair::core::io_data*> outputs;
};

#endif // VRPNOBJECT_IMPL_H
