PROJECT(FlairSimulator_gl)

ADD_DEFINITIONS(-D_GNU_SOURCE -D_REENTRANT -DGL)

FLAIR_LIB(${PROJECT_NAME} "${FLAIRSIMU_SRC}")
