// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2020/05/26
//  filename:   Ball.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    classe chargeant une balle
//
/*********************************************************************/
#ifdef GL

#include "Ball.h"
#include "Gui.h"
#include "Euler.h"
#include <ISceneManager.h>
#include <IMeshSceneNode.h>
#include <ITexture.h>
#include <Tab.h>
#include <DoubleSpinBox.h>
#include <Vector3DSpinBox.h>
#include <PushButton.h>
#include "FollowMeCamera.h"

#define G (float)9.81 // gravity ( N/(m/s²) )

using namespace irr;
using namespace irr::core;
using namespace irr::scene;
using namespace irr::video;
using namespace flair::core;
using namespace flair::gui;

namespace flair {
namespace simulator {

Ball::Ball(std::string name,uint32_t modelId) : Model(name,modelId) {
  
    node = getGui()->getSceneManager()->addSphereSceneNode(100,16,getSceneNode());

    ITexture *texture = getGui()->getTexture("ball.jpg");
    node->setMaterialTexture(0, texture);
    node->setMaterialFlag(video::EMF_LIGHTING, false);
    
    getFollowMeCamera()->setPositionOffset(vector3df(0, 0, 0));
    getFollowMeCamera()->setTargetOffset(vector3df(0, 0, 0));

    setTriangleSelector(getGui()->getSceneManager()->createTriangleSelector(node->getMesh(),node));

    Tab *setup_tab = GetParamsTab();
    radius = new DoubleSpinBox(setup_tab->NewRow(), "radius :", "m",0, 1, 0.1,2,0.05);
    Km = new DoubleSpinBox(setup_tab->NewRow(), "Km :",0, 1, 0.1,2,0.05);
    
    Tab *throw_tab = new Tab(GetTabWidget(), "throw");
    velInit = new Vector3DSpinBox(throw_tab->NewRow(), "initial velocity :", -15, 15, 0.1);
    throwButton = new PushButton(throw_tab->NewRow(), "throw");
    resetButton = new PushButton(throw_tab->NewRow(), "reset position");
                          
    node->setScale(vector3df(radius->Value(), radius->Value(), radius->Value()));
    
    throwing=false;
  
    SetIsReady(true);
}

Ball::~Ball() {}

void Ball::CalcModel(void) {
    if(throwButton->Clicked()) {
        throwing=true;
        state[-1].Vel = velInit->Value();
        state[-2].Vel = velInit->Value();
    }
    if(resetButton->Clicked()) {
        throwing=false;
    }
    
    if(throwing) {
        state[0].Pos = state[-1].Pos + dT() * state[-1].Vel + Vector3D<double>(0,0,Mass()*G*dT()*dT()/2);
        state[0].Vel = state[-1].Vel + dT() * (state[-1].Vel * velInit->Value().GetNorm()+Vector3D<double>(-Km->Value(),-Km->Value(),-Km->Value()+Mass()*G) );
    } else {
        state[0].Pos=InitialPosition();
    }
  //Printf("pos %f %f %f\n",state[0].Pos.x,state[0].Pos.y,state[0].Pos.z);
  //Printf("vel %f %f %f\n",state[0].Vel.x,state[0].Vel.y,state[0].Vel.z);
  //From Julio:
  /*
    x(k+1)  = x(k) + h*(vx(:,k));
    y(k+1)  = y(k) + h*(vy(:,k));
    z(k+1)  = z(k) + h*(vz(:,k)) - (g*h^2/2) ;
    
    vx(k+1) = vx(k) + h*(vx(k)*v*-Km);
    vy(k+1) = vy(k) + h*(vy(k)*v*-Km);
    vz(k+1) = vz(k) + h*(vz(k)*v*-Km-g);
*/

}

void Ball::AnimateModel(void){ 
// adapt  size
    if (radius->ValueChanged() == true) {
        node->setScale(vector3df(radius->Value(), radius->Value(), radius->Value()));
    }
}

size_t Ball::dbtSize(void) const { return 6 * sizeof(float); }

void Ball::WritedbtBuf(char *dbtbuf) { /*
                                         float *buf=(float*)dbtbuf;
                                         vector3df vect=node->getPosition();
                                         memcpy(buf,&vect.X,sizeof(float));
                                         buf++;
                                         memcpy(buf,&vect.Y,sizeof(float));
                                         buf++;
                                         memcpy(buf,&vect.Z,sizeof(float));
                                         buf++;
                                         vect=node->getRotation();
                                         memcpy(buf,&vect.X,sizeof(float));
                                         buf++;
                                         memcpy(buf,&vect.Y,sizeof(float));
                                         buf++;
                                         memcpy(buf,&vect.Z,sizeof(float));
                                         buf++;*/
}

void Ball::ReaddbtBuf(char *dbtbuf) { /*
                                        float *buf=(float*)dbtbuf;
                                        vector3df vect;
                                        memcpy(&vect.X,buf,sizeof(float));
                                        buf++;
                                        memcpy(&vect.Y,buf,sizeof(float));
                                        buf++;
                                        memcpy(&vect.Z,buf,sizeof(float));
                                        buf++;
                                        node->setPosition(vect);
                                        memcpy(&vect.X,buf,sizeof(float));
                                        buf++;
                                        memcpy(&vect.Y,buf,sizeof(float));
                                        buf++;
                                        memcpy(&vect.Z,buf,sizeof(float));
                                        buf++;
                                        node->setRotation(vect);
                                        node->setAnimationSpeed(2.f);*/
}

} // end namespace simulator
} // end namespace flair
#endif // GL
