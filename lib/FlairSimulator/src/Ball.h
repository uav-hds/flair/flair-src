// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2020/05/26
//  filename:   Ball.h
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    classe chargeant une balle
//
/*********************************************************************/

#ifndef BALL_H
#define BALL_H

#include <Model.h>

namespace irr {
namespace scene {
class IMeshSceneNode;
}
}

namespace flair {
namespace gui {
class DoubleSpinBox;
class Vector3DSpinBox;
class PushButton;
}
}

namespace flair {
namespace simulator {

class Ball : private Model {
public:
  Ball(std::string name,uint32_t modelId);
  ~Ball();

private:
  size_t dbtSize(void) const override;
  void WritedbtBuf(char *dbtbuf) override;
  void ReaddbtBuf(char *dbtbuf) override;
  void CalcModel(void) override;
  void AnimateModel(void) override;

  irr::scene::IMeshSceneNode *node;
  gui::DoubleSpinBox *radius,*Km;
  gui::Vector3DSpinBox *velInit;
  gui::PushButton *throwButton,*resetButton;
  bool throwing;
};
} // end namespace simulator
} // end namespace flair
#endif // BALL_H
