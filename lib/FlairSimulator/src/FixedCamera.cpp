// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2016/09/01
//  filename:   FixedCamera.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    class for a fixed camera in the gui
//
/*********************************************************************/
#ifdef GL

#include "FixedCamera.h"
#include "Simulator.h"
#include "Model.h"
#include "Model_impl.h"
#include "Gui.h"
#include <ICursorControl.h>
#include <ICameraSceneNode.h>
#include <IrrlichtDevice.h>
#include <ISceneManager.h>
#include <Euler.h>


using namespace irr;
using namespace gui;
using namespace core;
using namespace scene;

namespace flair {
namespace simulator {

FixedCamera::FixedCamera(std::string name,core::Vector3Df position,core::Vector3Df lookat,float inRotateSpeed,float inZoomSpeed):VisualizationCamera(name)  {
  Rotating = false;
  rotateSpeed=inRotateSpeed;
  zoomSpeed=inZoomSpeed;

  camera->bindTargetAndRotation(true);
  camera->setPosition(vector3df(ToIrrlichtCoordinates(position)));
  camera->setTarget(vector3df(ToIrrlichtCoordinates(lookat)));

  fov=camera->getFOV();
}

FixedCamera::~FixedCamera() {}

void FixedCamera::animateNode(ISceneNode *node, u32 timeMs) {
  ICameraSceneNode *camera = static_cast<ICameraSceneNode *>(node);
  
  if (LMouseKey == true) {
    if (!Rotating) {
      RotateStart = MousePos;
      Rotating = true;
      target = (camera->getTarget() - camera->getAbsolutePosition());
    } else {
      float nRotY = (RotateStart.Y - MousePos.Y) * rotateSpeed;
      float nRotZ = -(RotateStart.X - MousePos.X) * rotateSpeed;

      //normal between target and up vector
      cameraAxeY=target.crossProduct(vector3df(0,0,1));
      cameraAxeY.normalize();

      //rotation around z axis
      irr::core::quaternion q1(target.X,target.Y,target.Z,0);
      irr::core::quaternion q2;
      q2.fromAngleAxis(nRotZ,vector3df(0,0,1));
      irr::core::quaternion q3=q2*q1;
      q2.makeInverse();
      q3=q3*q2;

      //rotation around cameraAxeY
      q1.set(q3.X,q3.Y,q3.Z,0);
      q2.fromAngleAxis(nRotY,cameraAxeY);
      q3=q2*q1;
      q2.makeInverse();
      q3=q3*q2;

      //check angle
      vector3df newTarget(q3.X,q3.Y,q3.Z);
      float angle=acos(newTarget.dotProduct(vector3df(0,0,1))/newTarget.getLength());
      vector3df cross = newTarget.crossProduct(vector3df(0,0,1));
      if (cross.dotProduct(cameraAxeY) > 0) {
        newTarget += camera->getAbsolutePosition();
        camera->setTarget(newTarget);
      }
    }
  } else if (Rotating) {
    Rotating = false;
  }

  //handle zoom
  float newFov=fov+currentZoom*zoomSpeed;
  if(newFov>fov) {
    newFov=fov;
    currentZoom=0;
  }
  if(newFov<0) {
    newFov=zoomSpeed;
    currentZoom=1-fov/zoomSpeed;
  }
  camera->setFOV(newFov);
}


} // end namespace simulator
} // end namespace flair

#endif // GL
