// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2016/09/01
//  filename:   FixedCamera.h
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    cclass for a fixed camera in the gui
//
/*********************************************************************/

#ifndef FIXEDCAMERA_H
#define FIXEDCAMERA_H

#include "VisualizationCamera.h"
#include <Vector3D.h>

namespace flair {
namespace simulator {

class FixedCamera : public VisualizationCamera {
public:
  FixedCamera(std::string name,core::Vector3Df position,core::Vector3Df lookat=core::Vector3Df(0,0,0),float rotateSpeed = -3.0f, float zoomSpeed = .05f);
  ~FixedCamera();

  void animateNode(irr::scene::ISceneNode *node, irr::u32 timeMs) override;

private:
  irr::core::position2df RotateStart;
  bool Rotating;
  float rotateSpeed;
  float zoomSpeed;
  float fov;
  irr::core::vector3df target,cameraAxeY;
};

} // end namespace simulator
} // end namespace flair

#endif // FIXEDCAMERA_H
