// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2013/03/25
//  filename:   GenericObject.cpp
//
//  author:     Cesar Richard
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    classe definissant un modele a simuler
//
/*********************************************************************/

#ifdef GL
#include "GenericObject.h"
#include "Simulator.h"
#include "Simulator_impl.h"
#include "Gui.h"

#include "Quaternion.h"
#include "Euler.h"
#include <ISceneManager.h>
#include <IMeshSceneNode.h>

using namespace irr;
using namespace video;
using namespace scene;
using namespace core;

using namespace flair::core;
using namespace flair::simulator;

namespace flair {
namespace simulator {

GenericObject::GenericObject(IMesh *mesh) {
	meshSceneNode=getGui()->getSceneManager()->addMeshSceneNode(mesh);
	
  selector = getGui()->getSceneManager()->createTriangleSelector(mesh, meshSceneNode);
  meshSceneNode->setTriangleSelector(selector);
	
  meshSceneNode->setMaterialFlag(EMF_LIGHTING, false);

  getSimulator()->pimpl_->objects.push_back(this);
}

GenericObject::~GenericObject() {}

ITriangleSelector *GenericObject::TriangleSelector(void) { return selector; }

void GenericObject::setVisible(bool isVisible) {
  meshSceneNode->setVisible(isVisible);
}
void GenericObject::setScale(float value) {
  meshSceneNode->setScale(vector3df(value, value, value));
}

void GenericObject::setScale(const vector3df& scale) { 
	meshSceneNode->setScale(scale);
}

void GenericObject::setPosition(const vector3df& pos) {
  meshSceneNode->setPosition(ToIrrlichtCoordinates(pos));
}

void GenericObject::setRotation(const vector3df& rotation) {
  Euler eulerA(rotation.X, rotation.Y, rotation.Z);
  Euler eulerB;
  Quaternion quatA, quatB;
  eulerA.ToQuaternion(quatA);
  quatB = ToIrrlichtOrientation(quatA);
  quatB.ToEuler(eulerB);

  meshSceneNode->setRotation(Euler::ToDegree(1) *
                          vector3df(eulerB.roll, eulerB.pitch, eulerB.yaw));
}

} // end namespace simulator
} // end namespace flair
#endif
