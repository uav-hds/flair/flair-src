// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2013/03/25
//  filename:   Model.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    classe definissant un modele a simuler
//
/*********************************************************************/

#include "Model.h"
#include "Model_impl.h"
#include "Simulator.h"
#include <DoubleSpinBox.h>
#include <Vector3DSpinBox.h>

#ifdef GL
#include "FollowMeCamera.h"
#include <ICameraSceneNode.h>
using namespace irr;
using namespace irr::scene;
using namespace irr::core;
#endif

using namespace flair::core;
using namespace flair::gui;

namespace flair {
namespace simulator {

Model::Model(std::string name,uint32_t modelId)
    : IODevice(getSimulator(), name) {
  pimpl_ = new Model_impl(this, name,modelId);
}

Model::~Model() { delete pimpl_; }
#ifdef GL
ISceneNode *Model::getSceneNode() const { return pimpl_; }
/*
only used by dbt, to rewrite using conversion functions (irrlicht <-> simulator)
void Model::setPosition(Vector3D pos)
{
    //note that irrlicht frame is left handed
    pimpl_->setPosition(ToIrrlichtScale(1)*vector3df(pos.y,pos.z,-pos.x));
}
*/
void Model::setScale(float value) {
  pimpl_->setScale(vector3df(value, value, value));
}

aabbox3d<f32> *Model::Box() const { return &(pimpl_->box); }

FollowMeCamera *Model::getFollowMeCamera(void) const { return pimpl_->camera; }

void Model::setTriangleSelector(ITriangleSelector *selector) {

  pimpl_->selector = selector;
  pimpl_->setTriangleSelector(selector);
}

void Model::setCameraFarValue(float zf) { pimpl_->camera->getCameraSceneNode()->setFarValue(zf); }
#endif

void Model::AddPerturbation(Perturbation* perturbation) {
  pimpl_->perturbation=perturbation;
}

Perturbation* Model::GetPerturbation(void) {
  return pimpl_->perturbation;
}

TabWidget *Model::GetTabWidget(void) const { return pimpl_->tabwidget; }

Tab *Model::GetParamsTab(void) const { return pimpl_->params_tab; }

double Model::dT(void) const { return pimpl_->dT->Value(); }

int Model::GetId(void) const {
  return pimpl_->modelId;
}

core::Vector3D<double> Model::GetPosition(void) {
  return state[0].Pos;
}

core::Vector3D<double> Model::GetVelocity(void) {
  return state[0].Vel;
}

core::Quaternion Model::GetQuaternion(void) {
  return state[0].Quat;
}

core::Vector3Df Model::InitialPosition(void) const{
    return pimpl_->pos_init->Value();
}

float Model::Mass(void) const {
  return pimpl_->Mass();
}

} // end namespace simulator
} // end namespace flair
