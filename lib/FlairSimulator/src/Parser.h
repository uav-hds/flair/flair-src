// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2013/08/02
//  filename:   Parser.h
//
//  author:     César Richard
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    classe chargeant un XML decrivant une map
//
/*********************************************************************/

#ifndef PARSER_H
#define PARSER_H

#include <Gui.h>
#include <Vector3D.h>
#include <SColor.h>

//todo make a pimpl to remove these forward declaration
typedef struct _xmlNode xmlNode;
typedef unsigned char xmlChar;
typedef struct _xmlDoc xmlDoc;

namespace flair {
namespace simulator {
class Parser : public Gui {

  /*can create:
  - cylinders: in y axis
  - fixed cameras

  */
public:
  Parser(int app_width, int app_height, int scene_width,
         int scene_height, std::string media_path, std::string xmlFile);
  ~Parser();

private:
  xmlDoc *doc;
  std::string media_path;
  void processElements(xmlNode *a_node);
  void processObjects(xmlNode *a_node);
  void processParams(xmlNode *a_node);
	irr::video::SColor getScolor(xmlNode *mesh_node);
  irr::core::vector3df getMeshVect(xmlNode *mesh_node, xmlChar *param);
  irr::core::vector3df getSceneVect(xmlNode *mesh_node, xmlChar *param,
                                    bool isScale = false);
  core::Vector3Df getMeshVector3D(xmlNode *mesh_node, xmlChar *param);
};
}
}
#endif // PARSER_H
