// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2022/05/16
//  filename:   Perturbation.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    classe definig a perturbation applied to a model
//
/*********************************************************************/

#include "Perturbation.h"
#include "Simulator.h"

using namespace flair::core;

namespace flair {
namespace simulator {

Perturbation::Perturbation(std::string name) : Object(getSimulator(), name) {

}

Perturbation::~Perturbation() {
}


} // end namespace simulator
} // end namespace flair
