// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2022/05/16
//  filename:   Perturbation.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    classe definig a perturbation applied to a model
//
/*********************************************************************/
#ifndef PERTURBATION_H
#define PERTURBATION_H

#include <Object.h>
#include <Vector3D.h>

//force perturbation applied to center of gravity of the model
//each model needs to implement the perturbation in its calc model
//only 1 perturbation/model for now
//TODO: improve!

namespace flair {
namespace simulator {

class Perturbation : public core::Object {

public:
  Perturbation(std::string name);
  ~Perturbation();
  
  virtual core::Vector3D<double> GetValue(void) const=0;
  

private:
 
};
} // end namespace simulator
} // end namespace flair
#endif // PERTURBATION_H
