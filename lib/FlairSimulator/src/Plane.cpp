// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2021/12/22
//  filename:   Plane.cpp
//
//  author:     Armando Alatorre Sevilla, Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    classe definissant un avion
//
/*********************************************************************/

#include "Plane.h"
#include <SimuBldc.h>
#include <SimuServos.h>
#include <TabWidget.h>
#include <Tab.h>
#include <DoubleSpinBox.h>
#include <SpinBox.h>
#include <GroupBox.h>
#include <math.h>
#ifdef GL
#include <ISceneManager.h>
#include <IMeshManipulator.h>
#include "Blade.h"
#include "MeshSceneNode.h"
#include "Gui.h"
#include <Mutex.h>
#endif

#define K_MOT 0.4f    // blade animation
#define G (float)9.81 // gravity ( N/(m/s²) )

#ifdef GL
using namespace irr::video;
using namespace irr::scene;
using namespace irr::core;
#endif
using namespace flair::core;
using namespace flair::gui;
using namespace flair::actuator;

namespace flair {
namespace simulator {

Plane::Plane(std::string name, uint32_t modelId)
    : Model(name,modelId) {
  Tab *setup_tab = GetParamsTab();
  
  motorTimeout = new SpinBox(setup_tab->NewRow(), "motor timeout:","ms", 0, 1000, 100,100);
  rho = new DoubleSpinBox(setup_tab->NewRow(), "air density:",0, 2, 0.1);
  S = new DoubleSpinBox(setup_tab->NewRow(), "surface area:","m²",0, 2, 0.1);
  c = new DoubleSpinBox(setup_tab->NewRow(), "mean chord:",0, 2, 0.1);
  b = new DoubleSpinBox(setup_tab->NewRow(), "wingspan:",0, 2, 0.1);
  Cy0= new DoubleSpinBox(setup_tab->NewRow(), "Cy0:",0, 2, 0.1);
  Cybeta= new DoubleSpinBox(setup_tab->NewRow(), "Cybeta:",0, 2, 0.1);
  Cyp= new DoubleSpinBox(setup_tab->NewRow(), "Cyp:",0, 2, 0.1);
  Cyr= new DoubleSpinBox(setup_tab->NewRow(), "Cyr:",0, 2, 0.1);
  Cydeltaa= new DoubleSpinBox(setup_tab->NewRow(), "Cydeltaa:",0, 2, 0.1);
  Cydeltar= new DoubleSpinBox(setup_tab->NewRow(), "Cydeltar:",0, 2, 0.1);
  Sh= new DoubleSpinBox(setup_tab->NewRow(), "Sh:",0, 2, 0.1);
  Ch= new DoubleSpinBox(setup_tab->NewRow(), "Ch:",0, 2, 0.1);
  kr= new DoubleSpinBox(setup_tab->NewRow(), "kr:",0, 2, 0.1);
  
  
  motor = new SimuBldc(this, name, 1, modelId,0);
  servos = new SimuServos(this, name, 2, modelId,0);
  
  SetIsReady(true);
}

Plane::~Plane() {
  // les objets irrlicht seront automatiquement detruits (moteurs, helices,
  // pales) par parenté
}

#ifdef GL

void Plane::Draw(void) {
  // create unite (1m=100cm) UAV; scale will be adapted according to arm_length
  // parameter
  // note that the frame used is irrlicht one:
  // left handed, North East Up
  const IGeometryCreator *geo;
  geo = getGui()->getSceneManager()->getGeometryCreator();

  // cylinders are aligned with y axis
  IMesh *motor_mesh = geo->createCylinderMesh(7.5, 5, 16);
  ITexture *texture = getGui()->getTexture("metal047.jpg");
  MeshSceneNode *f_motor = new MeshSceneNode(this, motor_mesh, vector3df(150, 0, 20),
                               vector3df(0, 0, -90), texture);
  f_blade = new Blade(this, vector3df(155, 0,20), vector3df(90, 0, 90));
  
  IMesh *body_mesh = geo->createCylinderMesh(20, 150, 32,SColor(0, 255, 255, 255));
  MeshSceneNode *body = new MeshSceneNode(this, body_mesh, vector3df(0, 0, 20),
                             vector3df(0, 0, -90));
  
  IMesh *wing= geo->createCubeMesh (vector3df(35, 100, 2));
  IMesh *aileron= geo->createCubeMesh (vector3df(10, 90, 2));
  IMesh *axe = geo->createCylinderMesh(.5, 100, 16);//axe pour definir l'axe de rotation en bout d'aileron (sinon l'axe est au milieu d'aileron)
  
  //origine de l'aile en son centre
  MeshSceneNode *l_wing = new MeshSceneNode(this, wing, vector3df(75, -20-50, 20),vector3df(0, 0, 0));
  MeshSceneNode *r_wing = new MeshSceneNode(this, wing, vector3df(75, 20+50, 20),vector3df(0, 0, 0));
                      
       
  l_axe_aileron = new MeshSceneNode(this, axe, vector3df(75-35/2, -20-100, 20),vector3df(0, 0, 0));
  MeshSceneNode *l_aileron = new MeshSceneNode(l_axe_aileron, aileron, vector3df(-5, 50, 0),vector3df(0, 0, 0),texture);
 
                        
  r_axe_aileron = new MeshSceneNode(this, axe, vector3df(75-35/2, 20, 20),vector3df(0, 0, 0));
  MeshSceneNode *r_aileron = new MeshSceneNode(r_axe_aileron, aileron, vector3df(-5, 50,0),vector3df(0, 0, 0),texture);
                               
  actuators_mutex = new Mutex(this);
  motor_speed = 0;
  for (int i = 0; i < 2; i++) servos_pos[i] = 0;
  ExtraDraw();
}

void Plane::AnimateModel(void) {
  actuators_mutex->GetMutex();
  f_blade->SetRotationSpeed(K_MOT *vector3df( 0,motor_speed,0));
  l_axe_aileron->setRotation(vector3df(0,servos_pos[1]*180./3.14,0));
  r_axe_aileron->setRotation(vector3df(0,servos_pos[0]*180./3.14,0));

  actuators_mutex->ReleaseMutex();

  // adapt UAV size
  /*
  if (arm_length->ValueChanged() == true) {
    setScale(arm_length->Value());
  }*/
}

size_t Plane::dbtSize(void) const {
  return 6 * sizeof(float) + 1 * sizeof(float); // 6ddl+1moteur
}

void Plane::WritedbtBuf(char *dbtbuf) {
}

void Plane::ReaddbtBuf(char *dbtbuf) {
}
#endif // GL

// states are computed on fixed frame NED
// x north
// y east
// z down
void Plane::CalcModel(void) {
  Time motorTime,servoTime;
  
#ifdef GL
  actuators_mutex->GetMutex();
#endif // GL
  motor->GetSpeeds(&motor_speed,&motorTime);
  if((GetTime()-motorTime)/1000000>motorTimeout->Value()) {
      if(motor_speed!=0) {
        motor_speed=0;
      }
  }
  servos->GetPositions(servos_pos,&servoTime);
  
#ifdef GL
  actuators_mutex->ReleaseMutex();
#endif // GL
  float deltaa,deltae,deltar,deltat;
  
  //reste a calculer:
  //vb eq1 en fonction de dP?
  //input: deltaa,deltae,deltar,deltat
  
  Vector3D<double> fb,fa,fp,fg;
  Vector3D<double> Vb;
  
  float Au,Av,Aw,Va,alpha,beta;
  float Cx,Cxq,Cxdeltae,Cz,Czq,Czdeltae;
  Va=Vb.GetNorm();
  alpha=atan(Vb.z/Vb.x);
  beta=asin(Vb.y/Va);
  
  Cx=-Cd->Value()*cos(alpha)+Cl->Value()*sin(alpha);
  Cxq=-Cdq->Value()*cos(alpha)+Clq->Value()*sin(alpha);
  Cxdeltae=-Cddeltae->Value()*cos(alpha)+Cldeltae->Value()*sin(alpha);
  Cz=-Cd->Value()*sin(alpha)-Cl->Value()*cos(alpha);
  Czq=-Cdq->Value()*sin(alpha)-Clq->Value()*cos(alpha);
  Czdeltae=-Cddeltae->Value()*sin(alpha)-Cldeltae->Value()*cos(alpha);
  
  Au=Cx+Cxq*c->Value()*state[0].W.y/(2*Va)+Cxdeltae*deltae;
  Av=Cy0->Value()+Cybeta->Value()*beta+Cyp->Value()*b->Value()*state[0].W.x/(2*Va)+
      Cyr->Value()*b->Value()*state[0].W.z/(2*Va)+Cydeltaa->Value()*deltaa+Cydeltar->Value()*deltar;
  Aw=Cz*Czq*c->Value()*state[0].W.y/(2*Va)+Czdeltae*deltae;
  
  fa=rho->Value()*Va*Va*S->Value()/(2.*Mass())*Vector3D<double>(Au,Av,Aw);
  fp=rho->Value()*Sh->Value()*Ch->Value()/(2*Mass())*Vector3D<double>((kr->Value()*deltat*kr->Value()*deltat-Va*Va),0,0);
  //fg=state[-1].Quat*Quaternion(0,0,0,Mass()*G)*state[-1].Quat.GetConjugate();
  fb=fa+fp+fg;
  
  // acceleration in body frame
  // dVb/dt=-S(Wb)Vb+1/m*fb
  // Vb velocity in body frame
  // S() anty symetric matrice
  // fb=fa+fp+fg
  
  Vector3D<double> dVb=-Vector3D<double>(-state[0].W.z*Vb.y+state[0].W.y*Vb.z,
                                          state[0].W.z*Vb.x-state[0].W.x*Vb.z,
                                         -state[0].W.y*Vb.x+state[0].W.x*Vb.y)+1./Mass()*fb;
  
  
  // compute quaternion from W
  // Quaternion derivative: dQ = 0.5*(Q*Qw)
  Quaternion dQ = state[-1].Quat.GetDerivative(state[0].W);

  // Quaternion integration
  state[0].Quat = state[-1].Quat + dQ * dT();
  state[0].Quat.Normalize();

 

#ifndef GL
  if (state[0].Pos.z < 0)
    state[0].Pos.z = 0;
#endif
}

} // end namespace simulator
} // end namespace flair
