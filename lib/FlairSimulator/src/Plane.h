// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2021/12/22
//  filename:   Plane.h
//
//  author:     Armando Alatorre Sevilla, Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    classe definissant un avion
//
/*********************************************************************/

#ifndef PLANE_H
#define PLANE_H

#include <Model.h>

namespace flair {
namespace core {
class Mutex;
}
namespace gui {
class SpinBox;
class DoubleSpinBox;
}
namespace actuator {
class SimuBldc;
class SimuServos;
}
}

#ifdef GL
namespace irr {
namespace scene {
class IMesh;
}
}
#endif

namespace flair {
namespace simulator {
class Blade;
class MeshSceneNode;

class Plane : public Model {
public:
  Plane(std::string name, uint32_t modelId);
  ~Plane();
#ifdef GL
  virtual void Draw(void) override;
  virtual void ExtraDraw(void) override{}; 
#endif

private:
  void CalcModel(void) override;
#ifdef GL
  void AnimateModel(void) override;
  size_t dbtSize(void) const override;
  void WritedbtBuf(char *dbtbuf) override;
  void ReaddbtBuf(char *dbtbuf) override;
  core::Mutex *actuators_mutex;
  Blade *f_blade;
  MeshSceneNode *l_axe_aileron,*r_axe_aileron;
#endif

  actuator::SimuBldc *motor;
  actuator::SimuServos *servos;
  float motor_speed,servos_pos[2];//servo pos in radians
  gui::SpinBox *motorTimeout;
  gui::DoubleSpinBox *rho,*S,*c,*b;
  gui::DoubleSpinBox *Cd,*Cl,*Cdq,*Clq,*Cddeltae,*Cldeltae,*Cy0,*Cybeta,*Cyp,*Cyr,*Cydeltaa,*Cydeltar,*Sh,*Ch,*kr;
};
} // end namespace simulator
} // end namespace flair
#endif // PLANE_H
