// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2014/02/07
//  filename:   SimuBldc.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    Class for a simulation bldc
//
//
/*********************************************************************/
#include "SimuBldc.h"
#include <SharedMem.h>
#include <sstream>
#include <string.h>

using std::string;
using std::ostringstream;
using namespace flair::core;

namespace flair {
namespace actuator {


SimuBldc::SimuBldc(const Object *parent, string name, uint8_t motors_count,
                   uint32_t modelId,uint32_t deviceId)
    : IODevice(parent, name) {
  shmem =
      new SharedMem(this, ShMemName(modelId, deviceId), motors_count * sizeof(float)+sizeof(Time));
      
  buf=(char*)malloc(motors_count * sizeof(float)+sizeof(Time));
  if(buf==NULL) {
    Err("buffer malloc %i bytes error\n",motors_count * sizeof(float)+sizeof(Time));
    return;
  } 
  
  // reset values
  float *values=(float*)buf;
  for (int i = 0; i < motors_count; i++) values[i] = 0;
  Time time=GetTime();
  memcpy(buf+motors_count * sizeof(float),&time,sizeof(Time));

  shmem->Write(buf, motors_count * sizeof(float)+sizeof(Time));
  
  SetIsReady(true);
  this->motors_count=motors_count;
}

SimuBldc::~SimuBldc() {
  if(buf!=NULL) free(buf);
}

string SimuBldc::ShMemName(uint32_t modelId,uint32_t deviceId) {
  ostringstream dev_name;
  dev_name << "simu" <<  modelId << "_bldc_" << deviceId;
  return dev_name.str().c_str();
}


void SimuBldc::GetSpeeds(float *value,Time* time) const {
  float *values=(float*)buf;
  shmem->Read(buf, motors_count * sizeof(float)+sizeof(Time));
  memcpy(time,buf+motors_count * sizeof(float),sizeof(Time));

  for (int i = 0; i < motors_count; i++) {
    value[i] = values[i];
  }
}

} // end namespace actuator
} // end namespace flair
