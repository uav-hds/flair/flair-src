// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file SimuBldc.h
 * \brief Class for a simulation bldc
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2014/02/07
 * \version 4.0
 */

#ifndef SIMUBLDC_H
#define SIMUBLDC_H

#include <IODevice.h>

namespace flair {
namespace core {
class SharedMem;
}
}

namespace flair {
namespace actuator {
/*! \class SimuBldc
*
* \brief Class for a simulation bldc
*
*/
class SimuBldc : public core::IODevice {
public:
  /*!
  * \brief Constructor
  *
  * Construct a SimuBldc
  *
  * \param parent parent
  * \param name name
  * \param motors_count number of motors
  * \param modelId Model id
  * \param deviceId Bldc id of the Model
  */
  SimuBldc(const core::Object *parent, std::string name, uint8_t motors_count,
           uint32_t modelId,uint32_t deviceId);

  /*!
  * \brief Destructor
  *
  */
  ~SimuBldc();

  /*!
  * \brief Get motors speeds.
  *
  *
  * \param value array to store motors speeds
  * \param time time when shared memory was written
  */
  void GetSpeeds(float *value,core::Time* time) const;


private:
  /*!
  * \brief Update using provided datas
  *
  * Reimplemented from IODevice.
  *
  * \param data data from the parent to process
  */
  void UpdateFrom(const core::io_data *data) override{};
  std::string ShMemName(uint32_t modelId,uint32_t deviceId);
  core::SharedMem *shmem;
  char *buf;
  uint8_t motors_count;

};
} // end namespace actuator
} // end namespace flair
#endif // SIMUBLDC_H
