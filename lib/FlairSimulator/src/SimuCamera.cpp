// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2014/03/06
//  filename:   SimuCamera.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    Class for a simulation camera
//
//
/*********************************************************************/

#include "SimuCamera.h"
#include <SharedMem.h>
#include <sstream>

using std::string;
using std::ostringstream;
using namespace flair::core;

namespace flair {
namespace sensor {


SimuCamera::SimuCamera(const IODevice *parent, string name, uint16_t width,
                       uint16_t height, uint8_t channels, uint32_t modelId,uint32_t deviceId)
    : IODevice(parent,name) {

  size_t buf_size = width * height * channels+sizeof(Time);
  
  shmem = new SharedMem(this,ShMemName(modelId, deviceId),
                        buf_size, SharedMem::Type::producerConsumer);
  
  SetIsReady(true);
}

SimuCamera::~SimuCamera() {
  SetIsReady(false);
  delete shmem;
}

string SimuCamera::ShMemName(uint32_t modelId,uint32_t deviceId) {
  ostringstream dev_name;
  dev_name << "simu" <<  modelId << "_cam_" << deviceId;
  return dev_name.str().c_str();
}



} // end namespace sensor
} // end namespace flair
