// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file SimuCamera.h
 * \brief Class for a simulation camera
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2014/03/06
 * \version 4.0
 */

#ifndef SIMUCAMERA_H
#define SIMUCAMERA_H

#include <IODevice.h>

namespace flair {
  namespace core {
    class SharedMem;
  }
}

namespace flair {
namespace sensor {
/*! \class SimuCamera
*
* \brief Class for a simulation camera
*/
class SimuCamera : public core::IODevice {
public:

  /*!
  * \brief Constructor
  *
  * Construct a SimuCamera. 
  *
  * \param parent parent
  * \param name name
  * \param width width
  * \param height height
  * \param channels number of channels
  * \param modelId Model id
  * \param deviceId Camera id of the Model
  */
  SimuCamera(const core::IODevice *parent, std::string name, uint16_t width,
             uint16_t height, uint8_t channels, uint32_t modelId,uint32_t deviceId);

  /*!
  * \brief Destructor
  *
  */
  ~SimuCamera();

protected:
  /*!
  * \brief SharedMem to access datas
  *
  */
  core::SharedMem *shmem;

private:
  /*!
  * \brief Update using provided datas
  *
  * Reimplemented from IODevice.
  *
  * \param data data from the parent to process
  */
  void UpdateFrom(const core::io_data *data) override{};
  
  std::string ShMemName(uint32_t modelId,uint32_t deviceId);

};
} // end namespace sensor
} // end namespace flair
#endif // SIMUCAMERA_H
