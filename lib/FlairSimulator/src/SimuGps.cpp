// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2014/05/26
//  filename:   SimuGps.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    Class for a simulation GPS
//
//
/*********************************************************************/

#include "SimuGps.h"
#include <FrameworkManager.h>
#include <SharedMem.h>
#include <Matrix.h>
#include <sstream>

using std::string;
using std::ostringstream;
using namespace flair::core;

namespace flair {
namespace sensor {


SimuGps::SimuGps(const IODevice *parent, string name, uint32_t modelId,uint32_t deviceId)
    : IODevice(parent, name) {

  shmem = new SharedMem(this,ShMemName(modelId, deviceId),
                        sizeof(gps_states_t));
  
  SetIsReady(true);
}

SimuGps::~SimuGps() {
 
}

string SimuGps::ShMemName(uint32_t modelId,uint32_t deviceId) {
  ostringstream dev_name;
  dev_name << "simu" <<  modelId << "_gps_" << deviceId;
  return dev_name.str().c_str();
}

void SimuGps::UpdateFrom(const io_data *data) {
  if (data != NULL) {
    Matrix *input = (Matrix *)data;
    gps_states_t state;

    input->GetMutex();
    //simulator is ned, convert it to enu
    //TODO: put simulator in enu?
    state.e = input->ValueNoMutex(5, 0);//y simulator
    state.n = input->ValueNoMutex(4, 0);//x simulator
    state.u = -input->ValueNoMutex(6, 0);//z simulator
    state.ve = input->ValueNoMutex(11, 0);//vy simulator
    state.vn = input->ValueNoMutex(10, 0);//vx simulator
    input->ReleaseMutex();

    shmem->Write((char *)&state, sizeof(gps_states_t));
  }
}

} // end namespace sensor
} // end namespace flair
