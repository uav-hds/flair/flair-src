// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file SimuGps.h
 * \brief Class for a simulation GPS
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2013/08/23
 * \version 4.0
 */

#ifndef SIMUGPS_H
#define SIMUGPS_H

#include <IODevice.h>

namespace flair {
  namespace core {
    class SharedMem;
  }
}

namespace flair {
namespace sensor {
/*! \class SimuGps
*
* \brief Class for a simulation GPS
*/
class SimuGps : public core::IODevice {
public:
  /*!
  * \brief Constructor
  *
  * Construct a simulation GPS.
  *
  * \param parent parent
  * \param name name
  * \param modelId Model id
  * \param deviceId Gps id of the Model
  */
  SimuGps(const core::IODevice *parent, std::string name, uint32_t modelId,uint32_t deviceId);

  /*!
  * \brief Destructor
  *
  */
  ~SimuGps();

private:
  /*!
  * \brief Update using provided datas
  *
  * Reimplemented from IODevice.
  *
  * \param data data from the parent to process
  */
  void UpdateFrom(const core::io_data *data) override;

  typedef struct {
    float e;
    float n;
    float u;
    float ve;
    float vn;
  } gps_states_t;
  
  std::string ShMemName(uint32_t modelId,uint32_t deviceId);

  core::SharedMem *shmem;
};
} // end namespace sensor
} // end namespace flair
#endif // SIMUGPS_H
