// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2014/02/07
//  filename:   SimuImu.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    Class for a simulation imu
//
//
/*********************************************************************/

#include "SimuImu.h"
#include <Matrix.h>
#include <SharedMem.h>
#include <sstream>

using std::string;
using std::ostringstream;
using namespace flair::core;

namespace flair {
namespace sensor {

SimuImu::SimuImu(const IODevice *parent, string name, uint32_t modelId,uint32_t deviceId)
    : IODevice(parent,name) {
 
  shmem = new SharedMem(this, ShMemName(modelId, deviceId),
                        sizeof(imu_states_t));
  SetIsReady(true);
}

SimuImu::~SimuImu() {
}

string SimuImu::ShMemName(uint32_t modelId,uint32_t deviceId) {
  ostringstream dev_name;
  dev_name << "simu" <<  modelId << "_imu_" << deviceId;
  return dev_name.str().c_str();
}

void SimuImu::UpdateFrom(const io_data *data) {
  if (data != NULL) {
    Matrix *input = (Matrix *)data;
    imu_states_t state;

    input->GetMutex();
    state.q0 = input->ValueNoMutex(0, 0);
    state.q1 = input->ValueNoMutex(1, 0);
    state.q2 = input->ValueNoMutex(2, 0);
    state.q3 = input->ValueNoMutex(3, 0);
    state.wx = input->ValueNoMutex(7, 0);
    state.wy = input->ValueNoMutex(8, 0);
    state.wz = input->ValueNoMutex(9, 0);
    state.ax = input->ValueNoMutex(13, 0);
    state.ay = input->ValueNoMutex(14, 0);
    state.az = input->ValueNoMutex(15, 0);
    state.mx = input->ValueNoMutex(16, 0);
    state.my = input->ValueNoMutex(17, 0);
    state.mz = input->ValueNoMutex(18, 0);
    input->ReleaseMutex();

    shmem->Write((char *)&state, sizeof(imu_states_t));
  }
}


} // end namespace sensor
} // end namespace flair
