// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file SimuImu.h
 * \brief Class for a simulation Imu
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2014/02/07
 * \version 4.0
 */

#ifndef SIMUIMU_H
#define SIMUIMU_H

#include <IODevice.h>

namespace flair {
  namespace core {
    class SharedMem;
  }
}

namespace flair {
namespace sensor {
/*! \class SimuImu
*
* \brief Class for a simulation Imu
*/
class SimuImu : public core::IODevice {
public:
  /*!
  * \brief Constructor
  *
  * Construct a SimuImu.
  *
  * \param parent parent
  * \param name name
  * \param modelId Model id
  * \param deviceId Imu id of the Model
  */
  SimuImu(const core::IODevice *parent, std::string name, uint32_t modelId,uint32_t deviceId);

  /*!
  * \brief Destructor
  *
  */
  ~SimuImu();

private:
  /*!
  * \brief Update using provided datas
  *
  * Reimplemented from IODevice.
  *
  * \param data data from the parent to process
  */
  void UpdateFrom(const core::io_data *data) override;

  typedef struct {
    float q0;
    float q1;
    float q2;
    float q3;
    float wx;
    float wy;
    float wz;
    float ax;
    float ay;
    float az;
    float mx;
    float my;
    float mz;
  } imu_states_t;
  
  std::string ShMemName(uint32_t modelId,uint32_t deviceId);

  core::SharedMem *shmem;
};
} // end namespace sensor
} // end namespace flair
#endif // SIMUIMU_H
