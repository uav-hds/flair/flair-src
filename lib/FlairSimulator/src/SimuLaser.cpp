// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2014/02/07
//  filename:   SimuUs.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    Class for a simulation us
//
//
/*********************************************************************/
#include "SimuLaser.h"
#include <SharedMem.h>
#include <sstream>

using std::string;
using std::ostringstream;
using namespace flair::core;
using namespace flair::gui;

namespace flair {
namespace sensor {


SimuLaser::SimuLaser(const IODevice *parent, string name, uint32_t modelId,uint32_t deviceId)
    : IODevice(parent, name) {

  shmem = new SharedMem((Thread *)this, ShMemName(modelId, deviceId),
                        360 * sizeof(float));
  SetIsReady(true);
}

SimuLaser::~SimuLaser() {
}

string SimuLaser::ShMemName(uint32_t modelId,uint32_t deviceId) {
  ostringstream dev_name;
  dev_name << "simu" <<  modelId << "_laser_" << deviceId;
  return dev_name.str().c_str();
}


} // end namespace sensor
} // end namespace flair
