// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file SimuUs.h
 * \brief Class for a simulation UsRangeFinder
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2014/02/07
 * \version 4.0
 */

#ifndef SIMULASER_H
#define SIMULASER_H

#include <LaserRangeFinder.h>
#include <Thread.h>

namespace flair {
namespace core {
class SharedMem;
}
}

namespace flair {
namespace sensor {
/*! \class SimuUs
*
* \brief Class for a simulation UsRangeFinder
*/
class SimuLaser : public core::IODevice {
public:

  /*!
  * \brief Constructor
  *
  * Construct a SimuUs.
  *
  * \param parent parent
  * \param name name
  * \param modelId Model id
  * \param deviceId LaserRangeFinder id of the Model
  */
  SimuLaser(const core::IODevice *parent, std::string name, uint32_t modelId,uint32_t deviceId);

  /*!
  * \brief Destructor
  *
  */
  ~SimuLaser();

protected:
  /*!
  * \brief SharedMem to access datas
  *
  */
  core::SharedMem *shmem;

private:
  /*!
  * \brief Update using provided datas
  *
  * Reimplemented from IODevice.
  *
  * \param data data from the parent to process
  */
  void UpdateFrom(const core::io_data *data) override{};

  std::string ShMemName(uint32_t modelId,uint32_t deviceId);
};
} // end namespace sensor
} // end namespace flair
#endif // SIMULASER_H
