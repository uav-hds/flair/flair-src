// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2018/05/24
//  filename:   SimuPressureSensor.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    Class for a simulation pressure sensor
//
//
/*********************************************************************/
#include "SimuPressureSensor.h"
#include <SharedMem.h>
#include <Tab.h>
#include <DoubleSpinBox.h>
#include <Matrix.h>
#include "Model.h"
#include <sstream>
#include <math.h>

using std::string;
using std::ostringstream;
using namespace flair::core;
using namespace flair::simulator;
using namespace flair::gui;

namespace flair {
namespace sensor {

SimuPressureSensor::SimuPressureSensor(const Model *parent, string name,uint32_t modelId,uint32_t deviceId)
    : IODevice(parent,name) {
  Tab *setup_tab = new Tab(parent->GetTabWidget(), name);
  seaPressure = new DoubleSpinBox(setup_tab->NewRow(), "pressure at sea level:","Pa", 0, 200000, 1000,0,101325);
  
  shmem = new SharedMem(this, ShMemName(modelId, deviceId), sizeof(float));
  
  SetIsReady(true);
}

SimuPressureSensor::~SimuPressureSensor() {
}

string SimuPressureSensor::ShMemName(uint32_t modelId,uint32_t deviceId) {
  ostringstream dev_name;
  dev_name << "simu" <<  modelId << "_pressure_" << deviceId;
  return dev_name.str().c_str();
}

void SimuPressureSensor::UpdateFrom(const io_data *data) {
  if (data != NULL) {
    Matrix *input = (Matrix *)data;
    float z_feet = -input->Value(6, 0)*3.28084;
    float p = seaPressure->Value() *pow(1 - 6.87535*1e-6*z_feet,5.2561);//from https://www.brisbanehotairballooning.com.au/pressure-and-altitude-conversion/
    shmem->Write((char *)&p, sizeof(float));
  }
}

} // end namespace sensor
} // end namespace flair
