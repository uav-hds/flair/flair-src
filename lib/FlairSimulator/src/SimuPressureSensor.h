// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file SimuPressureSensor.h
 * \brief Class for a simulation PressureSensor
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2018/05/24
 * \version 4.0
 */

#ifndef SIMUPRESSURESENSOR_H
#define SIMUPRESSURESENSOR_H

#include <IODevice.h>

namespace flair {
namespace core {
class SharedMem;
}
namespace gui {
class SpinBox;
class DoubleSpinBox;
}
namespace simulator {
class Model;
}
}

namespace flair {
namespace sensor {
/*! \class SimuPressureSensor
*
* \brief Class for a simulation PressureSensor
*/
class SimuPressureSensor : public core::IODevice {
public:
  /*!
  * \brief Constructor
  *
  * Construct a SimuPressureSensor
  *
  * \param parent parent
  * \param name name
  * \param modelId Model id
  * \param deviceId PressureSensor id of the Model
  */
  SimuPressureSensor(const simulator::Model *parent, std::string name, uint32_t modelId,uint32_t deviceId);

  /*!
  * \brief Destructor
  *
  */
  ~SimuPressureSensor();

protected:
  /*!
  * \brief SharedMem to access datas
  *
  */
  core::SharedMem *shmem;

private:
  /*!
  * \brief Update using provided datas
  *
  * Reimplemented from IODevice.
  *
  * \param data data from the parent to process
  */
  void UpdateFrom(const core::io_data *data) override;
  
  std::string ShMemName(uint32_t modelId,uint32_t deviceId);
  gui::SpinBox *data_rate;
  gui::DoubleSpinBox *seaPressure;
};
} // end namespace sensor
} // end namespace flair
#endif // SIMUPRESSURESENSOR_H
