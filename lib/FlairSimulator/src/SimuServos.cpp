// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2019/11/27
//  filename:   SimuServos.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    Class for a simulation servos
//
//
/*********************************************************************/
#include "SimuServos.h"
#include <SharedMem.h>
#include <sstream>
#include <string.h>

using std::string;
using std::ostringstream;
using namespace flair::core;

namespace flair {
namespace actuator {


SimuServos::SimuServos(const Object *parent, string name, uint8_t servos_count,
                   uint32_t modelId,uint32_t deviceId)
    : IODevice(parent, name) {
  shmem =
      new SharedMem(this, ShMemName(modelId, deviceId), servos_count * sizeof(float)+sizeof(Time));
      
  buf=(char*)malloc(servos_count * sizeof(float)+sizeof(Time));
  if(buf==NULL) {
    Err("buffer malloc %i bytes error\n",servos_count * sizeof(float)+sizeof(Time));
    return;
  } 
  
  // reset values
  float *values=(float*)buf;
  for (int i = 0; i < servos_count; i++) values[i] = 0;
  Time time=GetTime();
  memcpy(buf+servos_count * sizeof(float),&time,sizeof(Time));

  shmem->Write(buf, servos_count * sizeof(float)+sizeof(Time));
  
  SetIsReady(true);
  this->servos_count=servos_count;
}

SimuServos::~SimuServos() {
  if(buf!=NULL) free(buf);
}

string SimuServos::ShMemName(uint32_t modelId,uint32_t deviceId) {
  ostringstream dev_name;
  dev_name << "simu" <<  modelId << "_servos_" << deviceId;
  return dev_name.str().c_str();
}


void SimuServos::GetPositions(float *value,Time* time) const {
  float *values=(float*)buf;
  shmem->Read(buf, servos_count * sizeof(float)+sizeof(Time));
  memcpy(time,buf+servos_count * sizeof(float),sizeof(Time));

  for (int i = 0; i < servos_count; i++) {
    value[i] = values[i];
  }
}

} // end namespace actuator
} // end namespace flair
