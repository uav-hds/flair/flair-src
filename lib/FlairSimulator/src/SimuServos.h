// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file SimuServos.h
 * \brief Class for a simulation servos
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2019/11/27
 * \version 4.0
 */

#ifndef SIMUSERVOS_H
#define SIMUSERVOS_H

#include <IODevice.h>

namespace flair {
namespace core {
class SharedMem;
}
}

namespace flair {
namespace actuator {
/*! \class SimuServos
*
* \brief Class for a simulation bldc
*
*/
class SimuServos : public core::IODevice {
public:
  /*!
  * \brief Constructor
  *
  * Construct a SimuServos
  *
  * \param parent parent
  * \param name name
  * \param servos_count number of servos
  * \param modelId Model id
  * \param deviceId servos id of the Model
  */
  SimuServos(const core::Object *parent, std::string name, uint8_t servos_count,
           uint32_t modelId,uint32_t deviceId);

  /*!
  * \brief Destructor
  *
  */
  ~SimuServos();

  /*!
  * \brief Get servos positions
  *
  *
  * \param value array to store servos positions in radians
  * \param time time when shared memory was written
  */
  void GetPositions(float *value,core::Time* time) const;


private:
  /*!
  * \brief Update using provided datas
  *
  * Reimplemented from IODevice.
  *
  * \param data data from the parent to process
  */
  void UpdateFrom(const core::io_data *data) override{};
  std::string ShMemName(uint32_t modelId,uint32_t deviceId);
  core::SharedMem *shmem;
  char *buf;
  uint8_t servos_count;

};
} // end namespace actuator
} // end namespace flair
#endif // SIMUSERVOS_H
