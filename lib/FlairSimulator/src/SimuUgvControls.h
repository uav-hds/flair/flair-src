// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
/*!
 * \file SimuUgvControls.h
 * \brief Class for a simulation ugv
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2020/12/07
 * \version 4.0
 */

#ifndef SIMUUGVCONTROLS_H
#define SIMUUGVCONTROLS_H

#include <IODevice.h>

namespace flair {
namespace core {
class SharedMem;
}
}

namespace flair {
namespace actuator {
/*! \class SimuUgvControls
*
* \brief Class for a simulation ugv
*
*/
class SimuUgvControls : public core::IODevice {
public:
  /*!
  * \brief Constructor
  *
  * Construct a SimuUgvControls
  *
  * \param parent parent
  * \param name name
  * \param modelId Model id
  * \param deviceId UgvControls id of the Model
  */
  SimuUgvControls(const core::Object *parent, std::string name,
           uint32_t modelId,uint32_t deviceId);

  /*!
  * \brief Destructor
  *
  */
  ~SimuUgvControls();

  /*!
  * \brief Get motors speeds.
  *
  *
  * \param value array to store motors speeds
  * \param time time when shared memory was written
  */
  void GetControls(float *speed,float *turn,core::Time* time) const;


private:
  /*!
  * \brief Update using provided datas
  *
  * Reimplemented from IODevice.
  *
  * \param data data from the parent to process
  */
  void UpdateFrom(const core::io_data *data) override{};
  std::string ShMemName(uint32_t modelId,uint32_t deviceId);
  core::SharedMem *shmem;
  char *buf;
};
} // end namespace actuator
} // end namespace flair
#endif // SIMUUGVCONTROLS_H
