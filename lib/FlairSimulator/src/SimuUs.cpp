// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2014/02/07
//  filename:   SimuUs.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    Class for a simulation us
//
//
/*********************************************************************/
#include "SimuUs.h"
#include <SharedMem.h>
#include <sstream>

using std::string;
using std::ostringstream;
using namespace flair::core;

namespace flair {
namespace sensor {

SimuUs::SimuUs(const IODevice *parent, string name,uint32_t modelId,uint32_t deviceId)
    : IODevice(parent,name) {
  
  shmem = new SharedMem(this, ShMemName(modelId, deviceId), sizeof(float));
  
  SetIsReady(true);
}

SimuUs::~SimuUs() {
}

string SimuUs::ShMemName(uint32_t modelId,uint32_t deviceId) {
  ostringstream dev_name;
  dev_name << "simu" <<  modelId << "_us_" << deviceId;
  return dev_name.str().c_str();
}


} // end namespace sensor
} // end namespace flair
