// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2020/11/20
//  filename:   TwoWheelRobot.h
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    classe definissant un TwoWheelRobot
//
/*********************************************************************/

#ifndef TWOWHEELROBOT_H
#define TWOWHEELROBOT_H

#include <Model.h>

namespace flair {
namespace core {
class Mutex;
}
namespace gui {
class DoubleSpinBox;
class SpinBox;
}
namespace actuator {
class SimuUgvControls;
}
}

#ifdef GL
namespace irr {
namespace scene {
class IMesh;
}
}
#endif

namespace flair {
namespace simulator {
class Blade;

class TwoWheelRobot : public Model {
public:
  TwoWheelRobot(std::string name, uint32_t modelId);
  ~TwoWheelRobot();
#ifdef GL
  virtual void Draw(void) override;
  virtual void ExtraDraw(void) override{}; 
#endif

private:
  void CalcModel(void) override;
#ifdef GL
  void AnimateModel(void) override;
  size_t dbtSize(void) const override;
  void WritedbtBuf(char *dbtbuf) override;
  void ReaddbtBuf(char *dbtbuf) override;
  irr::scene::IMesh *colored_body;
#endif
  gui::SpinBox *bodyColorR,*bodyColorG,*bodyColorB;
  gui::DoubleSpinBox *size,*t_speed,*r_speed;
  actuator::SimuUgvControls *controls;

 
};
} // end namespace simulator
} // end namespace flair
#endif // TWOWHEELROBOT_H
