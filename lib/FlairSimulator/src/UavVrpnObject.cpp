// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2018/12/11
//  filename:   UavVrpnObject.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    uav vrpn object, can display a real vrpn object in a simulated environment
//
/*********************************************************************/
#ifdef GL

#include "UavVrpnObject.h"
#include "UavVrpnObject_impl.h"

using namespace flair::sensor;
  
namespace flair {
namespace simulator {

UavVrpnObject::UavVrpnObject(std::string name,VrpnClient *client) 
  : VrpnObject(name,client->GetTabWidget(),client){
  
  
  pimpl_ = new UavVrpnObject_impl(this, name);
  pimpl_->setPosition(irr::core::vector3df(4,4,100));
}

UavVrpnObject::~UavVrpnObject() {
  delete pimpl_; 
}

} // end namespace simulator
} // end namespace flair

#endif // GL