// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2018/12/11
//  filename:   UavVrpnObject.h
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    uav vrpn object, can display a real vrpn object in a simulated environment
//
/*********************************************************************/

#ifndef UAVVRPNOBJECT_H
#define UAVVRPNOBJECT_H

#ifdef GL

#include <VrpnObject.h>

class UavVrpnObject_impl;

namespace flair {
namespace simulator {

class UavVrpnObject :public sensor::VrpnObject {
public:
  UavVrpnObject(std::string name,sensor::VrpnClient *client=sensor::GetVrpnClient());
  ~UavVrpnObject();

private:
  UavVrpnObject_impl *pimpl_;

};
} // end namespace simulator
} // end namespace flair

#endif // GL

#endif // UAVVRPNOBJECT_H
