// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2016/09/01
//  filename:   VisualizationCamera.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    class for a visualization camera in the gui
//
/*********************************************************************/
#ifdef GL

#include "VisualizationCamera.h"
#include "Simulator.h"
#include "Model.h"
#include "Model_impl.h"
#include "Gui.h"
#include "Gui_impl.h"
#include <Euler.h>
#include <ICursorControl.h>
#include <ICameraSceneNode.h>
#include <IrrlichtDevice.h>
#include <ISceneManager.h>
#include <IGUIEnvironment.h>
#include <IGUIFont.h>

using namespace irr;
using namespace gui;
using namespace core;
using namespace scene;
using namespace video;

namespace flair {
namespace simulator {

class AxisSceneNode: public ISceneNode {
    public:
      AxisSceneNode(ISceneManager *axis_scenemanager):ISceneNode(axis_scenemanager->getRootSceneNode(), axis_scenemanager, -1) {
        //draw ned axis
        IAnimatedMesh* arrowMeshRed = axis_scenemanager->addArrowMesh( "x_axisArrow",video::SColor(255, 255, 0, 0),video::SColor(255, 255, 0, 0));
        nodeX = axis_scenemanager->addMeshSceneNode(arrowMeshRed,this);
        nodeX->setMaterialFlag(video::EMF_LIGHTING, false);
        nodeX->setRotation(vector3df(0,0,-90));//use vrpn yaw rotation from earth
        nodeX->setScale(vector3df(1,3,1));
        
        IAnimatedMesh* arrowMeshGreen = axis_scenemanager->addArrowMesh( "y_axisArrow",video::SColor(255, 0, 255, 0),video::SColor(255, 0, 255, 0));
        nodeY = axis_scenemanager->addMeshSceneNode(arrowMeshGreen,this);
        nodeY->setMaterialFlag(video::EMF_LIGHTING, false);
        nodeY->setScale(vector3df(1,3,1));
        
        IAnimatedMesh* arrowMeshBlue = axis_scenemanager->addArrowMesh( "z_axisArrow",video::SColor(255, 0, 0, 255),video::SColor(255, 0, 0, 255));
        nodeZ = axis_scenemanager->addMeshSceneNode(arrowMeshBlue,this);
        nodeZ->setMaterialFlag(video::EMF_LIGHTING, false);
        nodeZ->setRotation(vector3df(-90,0,0));//irrlicht is left handed, draw a right handed axis
        nodeZ->setScale(vector3df(1,3,1));  
      }
      
      void render(void) {
        IVideoDriver *driver = SceneManager->getVideoDriver();
        driver->setTransform(ETS_WORLD, AbsoluteTransformation);
      }

      void OnRegisterSceneNode(void) {
        if (IsVisible)
          SceneManager->registerNodeForRendering(this);

        ISceneNode::OnRegisterSceneNode();
      }
      
      const aabbox3d<f32> &getBoundingBox(void) const {
        return box;
      }
      
    private:
      aabbox3d<f32> box;
      ISceneNode *nodeX,*nodeY,*nodeZ;
};

VisualizationCamera::VisualizationCamera(std::string inName) {
  name=inName;
  currentZoom = 0;
  LMouseKey = false;

  // camera for visualization
  camera = getGui()->getSceneManager()->addCameraSceneNode();
  camera->setAspectRatio(getGui()->getAspectRatio()); // on force a cause du view port
  camera->setUpVector(vector3df(0, 0, 1));
  camera->addAnimator(this);
  camera->setFarValue(8000);

  getGui()->pimpl_->AddVisualizationCamera(this);
  
  // camera to draw axis, in a new dedicated scene manager
  axis_scenemanager=getGui()->getSceneManager()->createNewSceneManager();
  axis_camera= axis_scenemanager->addCameraSceneNode();
  axis_camera->setUpVector(vector3df(0, 0, 1));
  axis_camera->setFOV(PI / 2.5f);
  axis_scenemanager->setActiveCamera(axis_camera);
  
  vrpnSceneNode=new AxisSceneNode(axis_scenemanager);
  vrpnSceneNode->setRotation(vector3df(0,0,core::Euler::ToDegree(getSimulator()->Yaw())));//use vrpn yaw rotation from earth
  earthSceneNode=new AxisSceneNode(axis_scenemanager);
}

VisualizationCamera::~VisualizationCamera() {}


void VisualizationCamera::renderAxisToTexture(ITexture* texture,IGUIFont *font,AxisType axisType) {
  //put axis at a "normalized" distance
  vector3df direction=camera->getTarget()-camera->getPosition();
  direction.normalize();
  vrpnSceneNode->setPosition(camera->getPosition()+direction*6);
  earthSceneNode->setPosition(camera->getPosition()+direction*6);
  
  stringw axisText;
  switch(axisType) {
    case AxisType::vrpn:
      vrpnSceneNode->setVisible(true);
      earthSceneNode->setVisible(false);
      axisText="VRPN";
      break;
    case AxisType::earth:
      vrpnSceneNode->setVisible(false);
      earthSceneNode->setVisible(true);
      axisText="earth";
      break;
    case AxisType::none:
      vrpnSceneNode->setVisible(false);
      earthSceneNode->setVisible(false);
      break;
  }
  
  axis_camera->setPosition(camera->getPosition());
  axis_camera->setRotation(camera->getRotation());
  axis_camera->setTarget(camera->getTarget());
  axis_camera->setAspectRatio((float)(texture->getSize().Width)/(float)(texture->getSize().Height));  // same as texture ratio

  
  axis_scenemanager->getVideoDriver()->setRenderTarget(texture, true, true, SColor(0,0,0,0));
  axis_scenemanager->drawAll();
  
  if(font && axisType!=AxisType::none) {
    font->draw(axisText,rect<s32>(10,texture->getSize().Height-25,texture->getSize().Width,texture->getSize().Height),SColor(255,255,255,255));
    font->draw("X",rect<s32>(60,texture->getSize().Height-25,texture->getSize().Width,texture->getSize().Height),SColor(255,255,0,0));
    font->draw("Y",rect<s32>(70,texture->getSize().Height-25,texture->getSize().Width,texture->getSize().Height),SColor(255,0,255,0));
    font->draw("Z",rect<s32>(80,texture->getSize().Height-25,texture->getSize().Width,texture->getSize().Height),SColor(255,0,0,255));
    getGui()->getDevice()->getGUIEnvironment()->drawAll();
  }
  
  axis_scenemanager->getVideoDriver()->setRenderTarget(0, true, true, 0);
}

std::string VisualizationCamera::getName(void) {
  return name;
}

ICameraSceneNode *VisualizationCamera::getCameraSceneNode(void) {
  return camera;
}

ISceneNodeAnimator *VisualizationCamera::createClone(ISceneNode *node,
                                               ISceneManager *newManager) {
  return NULL;
}

bool VisualizationCamera::OnEvent(const irr::SEvent& event) {
  if (event.EventType != EET_MOUSE_INPUT_EVENT)
    return false;

  switch (event.MouseInput.Event) {

  case EMIE_MOUSE_WHEEL:
    currentZoom -= event.MouseInput.Wheel;
    break;
  case EMIE_LMOUSE_PRESSED_DOWN:
    LMouseKey = true;
    break;
  case EMIE_LMOUSE_LEFT_UP:
    LMouseKey = false;
    break;
  case EMIE_MOUSE_MOVED:
    MousePos = getGui()->getDevice()->getCursorControl()->getRelativePosition();
    break;
  default:
    return false;
    break;
  }

  return true;
}

} // end namespace simulator
} // end namespace flair

#endif // GL
