// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2012/08/21
//  filename:   X4.h
//
//  author:     Osamah Saif, Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    classe definissant un x4
//
/*********************************************************************/

#ifndef X4_H
#define X4_H

#include <Model.h>

namespace flair {
namespace core {
class Mutex;
}
namespace gui {
class DoubleSpinBox;
class SpinBox;
}
namespace actuator {
class SimuBldc;
}
}

#ifdef GL
namespace irr {
namespace scene {
class IMesh;
}
}
#endif

namespace flair {
namespace simulator {
class Blade;

class X4 : public Model {
public:
  X4(std::string name, uint32_t modelId);
  ~X4();
#ifdef GL
  virtual void Draw(void) override;
  virtual void ExtraDraw(void) override{}; 
#endif
  float Thrust(void) const;

private:
  void CalcModel(void) override;
#ifdef GL
  void AnimateModel(void) override;
  size_t dbtSize(void) const override;
  void WritedbtBuf(char *dbtbuf) override;
  void ReaddbtBuf(char *dbtbuf) override;
  Blade *fl_blade, *fr_blade, *rl_blade, *rr_blade;
  core::Mutex *motor_speed_mutex;
  irr::scene::IMesh *colored_arm;
#endif

  actuator::SimuBldc *motors;
  float motor_speed[4];
  gui::DoubleSpinBox *arm_length, *l_cg;
  gui::DoubleSpinBox *k_mot, *c_mot;
  gui::DoubleSpinBox *f_air_vert, *f_air_lat;
  gui::DoubleSpinBox *j_roll, *j_pitch, *j_yaw;
  gui::SpinBox *motorTimeout;
  gui::SpinBox *armColorR,*armColorG,*armColorB;
};
} // end namespace simulator
} // end namespace flair
#endif // X4_H
