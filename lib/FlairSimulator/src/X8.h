// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2014/04/03
//  filename:   X8.h
//
//  author:     Majd Saied, Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    classe definissant un X8
//
/*********************************************************************/

#ifndef X8_H
#define X8_H

#include <Model.h>

namespace flair {
namespace core {
class Mutex;
}
namespace gui {
class DoubleSpinBox;
class SpinBox;
}
namespace actuator {
class SimuBldc;
}
}

#ifdef GL
namespace irr {
namespace scene {
class IMesh;
}
}
#endif

namespace flair {
namespace simulator {
class Blade;

class X8 : public Model {
public:
  X8(std::string name, uint32_t modelId);
  ~X8();
#ifdef GL
  virtual void Draw(void) override;
  virtual void ExtraDraw(void) override{};
#endif
protected:

private:
  void CalcModel(void) override;
#ifdef GL
  void AnimateModel(void) override;
  size_t dbtSize(void) const override;
  void WritedbtBuf(char *dbtbuf) override;
  void ReaddbtBuf(char *dbtbuf) override;
  core::Mutex *motor_speed_mutex;
  Blade *tfl_blade, *tfr_blade, *trl_blade, *trr_blade;
  Blade *bfl_blade, *bfr_blade, *brl_blade, *brr_blade;
  irr::scene::IMesh *colored_arm;
#endif

  actuator::SimuBldc *motors;
  float motor_speed[8];
  gui::DoubleSpinBox *arm_length, *l_cg;
  gui::DoubleSpinBox *k_mot, *c_mot;
  gui::DoubleSpinBox *f_air_vert, *f_air_lat;
  gui::DoubleSpinBox *j_roll, *j_pitch, *j_yaw;
  gui::DoubleSpinBox *j_r, *sigma, *S;
  gui::SpinBox *motorTimeout;
  gui::SpinBox *armColorR,*armColorG,*armColorB;
};
} // end namespace simulator
} // end namespace flair
#endif // X8_H
