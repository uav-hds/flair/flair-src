// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2013/03/25
//  filename:   GenericObject.h
//
//  author:     Cesar Richard
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    classe definissant un modele a simuler
//
/*********************************************************************/

#ifndef GENERICOBJECT_H
#define GENERICOBJECT_H
#ifdef GL

#include <vector3d.h>

namespace irr {
	namespace scene {
		class IMesh;
		class IMeshSceneNode;
		class ITriangleSelector;
		class ISceneNode;
	}
}

class Simulator_impl;

namespace flair {
namespace simulator {

class GenericObject {
  friend class ::Simulator_impl;

public:
  GenericObject(irr::scene::IMesh *mesh);
  virtual ~GenericObject();

  irr::scene::ITriangleSelector *TriangleSelector(void);
  void setScale(float value);
  void setScale(const irr::core::vector3df& scale);
	void setPosition(const irr::core::vector3df& position);
  void setRotation(const irr::core::vector3df& rotation);
  void setVisible(bool isVisible);//visible by default at construction
 
private:
  irr::scene::ITriangleSelector *selector;
	irr::scene::IMeshSceneNode *meshSceneNode;
}; 
} // end namespace simulator
} // end namespace flair
#endif // GL
#endif // GENERICOBJECT_H
