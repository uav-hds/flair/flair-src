// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2013/03/25
//  filename:   Model_impl.h
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    classe definissant un modele a simuler
//
/*********************************************************************/

#ifndef MODEL_IMPL_H
#define MODEL_IMPL_H

#include <Thread.h>
#include <vrpn_Tracker.h>
#include "Model.h"
#include "Quaternion.h"

namespace flair {
namespace core {
class Matrix;
class Mutex;
class ConditionVariable;
}
namespace gui {
class TabWidget;
class Tab;
class CheckBox;
class DoubleSpinBox;
class SpinBox;
class Vector3DSpinBox;
}
namespace simulator {
class Simulator;
class FollowMeCamera;
class Perturbation;
}
}

#ifdef GL
#include <Vector3D.h>
#include <IMeshSceneNode.h>

namespace irr {
namespace scene {
class ISceneManager;
class ITriangleSelector;
class IMetaTriangleSelector;
class ISceneNodeAnimatorCollisionResponse;
}
}
#endif

#ifdef GL
class Model_impl : public irr::scene::ISceneNode,
                   public flair::core::Thread,
                   private vrpn_Tracker
#else
class Model_impl : public flair::core::Thread,
                   private vrpn_Tracker
#endif
                   {
public:
  Model_impl(flair::simulator::Model *self, std::string name,uint32_t modelId);
  ~Model_impl();

#ifdef GL
  void OnRegisterSceneNode(void);
  void render(void);

  const irr::core::aabbox3d<irr::f32> &getBoundingBox(void) const {
    return box;
  }
  void UpdatePos(void);
  void CheckCollision(void);
  irr::scene::ITriangleSelector *TriangleSelector(void);
  irr::scene::IMetaTriangleSelector *MetaTriangleSelector(void);
  irr::core::aabbox3d<irr::f32> box;
  void SynchronizationPoint();
  flair::simulator::FollowMeCamera *camera;
  irr::scene::ITriangleSelector *selector;
#endif
  void mainloop(void);
  float Mass(void) const;

  flair::gui::TabWidget *tabwidget;
  flair::gui::Tab *params_tab;
  flair::gui::DoubleSpinBox *dT;
  int modelId;
  flair::gui::Vector3DSpinBox *pos_init;
  flair::simulator::Perturbation* perturbation;

private:
  flair::gui::SpinBox *yaw_init;
  flair::gui::CheckBox *enable_opti;
  flair::gui::DoubleSpinBox *m;
  flair::simulator::Model *self;
  flair::core::Matrix *output;
  flair::core::Mutex *states_mutex;

  void Run(void);
  flair::core::Quaternion ComputeInitRotation(flair::core::Quaternion quat_in);
#ifdef GL
  void CollisionHandler(void);

  irr::scene::IMetaTriangleSelector *meta_selector;
  irr::scene::ISceneNodeAnimatorCollisionResponse *anim;

  bool position_init;

  flair::core::ConditionVariable *cond;
  int sync_count;

  flair::core::Mutex *collision_mutex;
  bool collision_occured;
  flair::core::Vector3D<double> collision_point;
#endif
};

#endif // MODEL_IMPL_H
