// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2018/12/11
//  filename:   UavVrpnObject_impl.h
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    uav vrpn object, can display a real vrpn object in a simulated environment
//
/*********************************************************************/

#ifndef UAVVRPNOBJECT_IMPL_H
#define UAVVRPNOBJECT_IMPL_H

#ifdef GL

#include <IODevice.h>
#include <IMeshSceneNode.h>

namespace flair {
  namespace gui {
    class DoubleSpinBox;
  }
}

namespace flair {
  namespace simulator {
    class Blade;
    class UavVrpnObject;
  }
}

class UavVrpnObject_impl : private flair::core::IODevice,
                      public irr::scene::ISceneNode {
public:
  UavVrpnObject_impl(flair::simulator::UavVrpnObject *self,std::string name);
  ~UavVrpnObject_impl();

  void Draw(void);
  
  const irr::core::aabbox3d<irr::f32> &getBoundingBox(void) const {
    return box;
  }
  
 void render(void);

private:
  flair::simulator::Blade *fl_blade, *fr_blade, *rl_blade, *rr_blade;
  flair::gui::DoubleSpinBox *arm_length;
  irr::core::aabbox3d<irr::f32> box;
  void OnRegisterSceneNode(void);
  flair::simulator::UavVrpnObject *self;
  
  /*!
  * \brief Update using provided datas
  *
  * Reimplemented from IODevice.
  *
  * \param data data from the parent to process
  */
  void UpdateFrom(const flair::core::io_data *data);

};


#endif // GL

#endif // UAVVRPNOBJECT_IMPL_H
