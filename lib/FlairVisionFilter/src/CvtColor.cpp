//  created:    2014/07/17
//  filename:   CvtColor.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    Image color conversion
//
//
/*********************************************************************/

#include "CvtColor.h"
#include "VisionFilter.h"
#include <Image.h>
#include <typeinfo>

using std::string;
using namespace flair::core;

class CvtColor_impl {
public:
    CvtColor_impl(flair::filter::CvtColor *self,flair::filter::CvtColor::Conversion_t conversion):output(0) {
        this->conversion=conversion;
        this->self=self;

        switch(conversion) {
        case flair::filter::CvtColor::Conversion_t::ToGray:
            try{
                Image::Type const &imageType=dynamic_cast<Image::Type const &>(((IODevice*)(self->Parent()))->GetOutputDataType());
                output=new Image(self,imageType.GetWidth(),imageType.GetHeight(),Image::Type::Format::Gray,"conversion",true,2);
                //inIplImage = (IplImage*)AllocFunction(sizeof(IplImage));
                //outIplImage = (IplImage*)AllocFunction(sizeof(IplImage));
            } catch(std::bad_cast& bc) {
                self->Err("io type mismatch\n");
            }
            break;
        default:
            self->Err("conversion not supported\n");
        }
    }

    ~CvtColor_impl() {
        //FreeFunction((char*)(inIplImage));
        //FreeFunction((char*)(outIplImage));
    }
    
    void UpdateFrom(const io_data *data){
        Image *img=(Image*)data;

        if(output!=NULL) {
            data->GetMutex();/*
            inIplImage->width=img->GetDataType().GetWidth();
            inIplImage->height=img->GetDataType().GetHeight();
            inIplImage->imageData=img->buffer;
            inIplImage->imageSize=img->GetDataType().GetSize();
            */
            output->GetMutex();/*
            outIplImage->width=output->GetDataType().GetWidth();
            outIplImage->height=output->GetDataType().GetHeight();
            outIplImage->imageData=output->buffer;
            outIplImage->imageSize=output->GetDataType().GetSize();
*/
            switch(conversion) {
            case flair::filter::CvtColor::Conversion_t::ToGray:
                switch(((Image*)data)->GetDataType().GetFormat()) {
                case Image::Type::Format::YUYV:
                    //dspCvtColor(inIplImage,outIplImage,DSP_YUYV2GRAY);
                    break;
                case Image::Type::Format::UYVY:
                    //dspCvtColor(inIplImage,outIplImage,DSP_UYVY2GRAY);
                    break;
                case Image::Type::Format::BGR:
                    //dspCvtColor(inIplImage,outIplImage,DSP_BGR2GRAY);
                    break;
                default:
                    self->Err("input format not supported\n");
                }
                break;
            default:
                self->Err("conversion not supported\n");
            }

            output->ReleaseMutex();
            data->ReleaseMutex();

            output->SetDataTime(data->DataTime());
        }
    };
    
    Image *output;

private:
    flair::filter::CvtColor::Conversion_t conversion;
    //IplImage *inIplImage,*outIplImage;
    flair::filter::CvtColor *self;
};


namespace flair { namespace filter {

CvtColor::CvtColor(const core::IODevice* parent,std::string name,Conversion_t conversion) : IODevice(parent,name) {
    pimpl_=new CvtColor_impl(this,conversion);
}

CvtColor::~CvtColor(void) {
    delete pimpl_;
}

Image* CvtColor::Output(void) {
    return pimpl_->output;

}

DataType const &CvtColor::GetOutputDataType() const {
    if(pimpl_->output!=NULL) {
        return pimpl_->output->GetDataType();
    } else {
        return dummyType;
    }
}

void CvtColor::UpdateFrom(const io_data *data) {
    pimpl_->UpdateFrom(data);
    if(pimpl_->output!=NULL) ProcessUpdate(pimpl_->output);
    
}

} // end namespace filter
} // end namespace flair
