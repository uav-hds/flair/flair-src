/*!
 * \file HoughLines.h
 * \brief HoughLines algorithm
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2015/10/07
 * \version 4.0
 */


#ifndef HOUGHLINES_H
#define HOUGHLINES_H

#include <IODevice.h>
#include <Vector2D.h>

namespace flair {
    namespace core {
        class Matrix;
    }
    namespace gui {
        class LayoutPosition;
    }
}

class HoughLines_impl;

namespace flair { namespace filter {
    /*! \class HoughLines
    *
    * \brief HoughLines algorithm
    *
    * HoughLines is done using the DSP of the DM3730.
    */
    class HoughLines : public core::IODevice {
        public:
            /*!
            * \brief Constructor
            *
            * Construct an OpticalFlow filter at given position. \n
            * After calling this function, position will be deleted as it is no longer usefull. \n
            *
            * \param parent parent
            * \param position position
            * \param name name
            * \param ptRefGlobal reference point used to compute distance to the line
            * \param thetaRefGlobal reference angle used to compute orientation in range [-pi/2,pi/2]. Default value is 0 which defines a vertical line
            */
            HoughLines(const core::IODevice* parent,const gui::LayoutPosition* position,std::string name,const core::Vector2Df *ptRefGlobal=NULL,float thetaRefGlobal=0);

            /*!
            * \brief Destructor
            *
            */
            ~HoughLines();

            bool isLineDetected(void) const;
            float GetOrientation(void) const;
            float GetDistance(void) const;
            core::Matrix *Output(void) const;

        private:
            void UpdateFrom(const core::io_data *data) override;
            class HoughLines_impl *pimpl_;
            
            
    };
} // end namespace filter
} // end namespace flair
#endif // HOUGHLINES_H
