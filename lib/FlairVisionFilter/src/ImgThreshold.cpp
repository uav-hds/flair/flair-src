//  created:    2015/10/07
//  filename:   ImgThreshold.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    ImgThreshold
//
//
/*********************************************************************/

#include "ImgThreshold.h"
#include "VisionFilter.h"
#include <Image.h>
#include <Layout.h>
#include <GroupBox.h>
#include <SpinBox.h>
#include <typeinfo>

using std::string;
using namespace flair::core;
using namespace flair::gui;

class ImgThreshold_impl {
public:
    ImgThreshold_impl(flair::filter::ImgThreshold *self,const LayoutPosition* position,string name):output(0) {
        this->self=self;
        
        GroupBox* reglages_groupbox=new GroupBox(position,name);
        threshold=new SpinBox(reglages_groupbox->NewRow(),"threshold:",0,255,1,127);

        Printf("todo: pouvoir reutiliser la meme image en sortie\n");
        try{
            Image::Type const &imageType=dynamic_cast<Image::Type const &>(((IODevice*)(self->Parent()))->GetOutputDataType());
            if(imageType.GetFormat()==Image::Type::Format::Gray) {
                output=new Image(self,imageType.GetWidth(),imageType.GetHeight(),imageType.GetFormat(),"threshold");
                //inIplImage = (IplImage*)AllocFunction(sizeof(IplImage));
                //outIplImage = (IplImage*)AllocFunction(sizeof(IplImage));
            } else {
                self->Err("input image is not gray\n");
            }
        } catch(std::bad_cast& bc) {
            self->Err("io type mismatch\n");
        }

        
    }

    ~ImgThreshold_impl() {
        //FreeFunction((char*)(inIplImage));
        //FreeFunction((char*)(outIplImage));
    }
    
    void UpdateFrom(const io_data *data){
        Image *img=(Image*)data;
/*
        data->GetMutex();
        inIplImage->width=img->GetDataType().GetWidth();
        inIplImage->height=img->GetDataType().GetHeight();
        inIplImage->imageData=img->buffer;
        inIplImage->imageSize=img->GetDataType().GetSize();
        
        output->GetMutex();
        outIplImage->width=output->GetDataType().GetWidth();
        outIplImage->height=output->GetDataType().GetHeight();
        outIplImage->imageData=output->buffer;
        outIplImage->imageSize=output->GetDataType().GetSize();
                
        dspThreshold(inIplImage,outIplImage, threshold->Value(), 255, CV_THRESH_BINARY);
        output->ReleaseMutex();
        data->ReleaseMutex();
*/
        output->SetDataTime(data->DataTime());
    };
    
    Image *output;

private:
    flair::filter::ImgThreshold *self;
    SpinBox *threshold;
    //IplImage *inIplImage,*outIplImage;
};

namespace flair { namespace filter {

ImgThreshold::ImgThreshold(const IODevice* parent,const LayoutPosition* position,string name) : IODevice(parent,name) {
    pimpl_=new ImgThreshold_impl(this,position,name);
}

ImgThreshold::~ImgThreshold(void) {
    delete pimpl_;
}

Image* ImgThreshold::Output(void) {
    return pimpl_->output;
}

void ImgThreshold::UpdateFrom(const io_data *data) {
    pimpl_->UpdateFrom(data);
    ProcessUpdate(pimpl_->output);
}

DataType const &ImgThreshold::GetOutputDataType() const {
    if(pimpl_->output!=NULL) {
        return pimpl_->output->GetDataType();
    } else {
        return dummyType;
    }
}

} // end namespace filter
} // end namespace flair
