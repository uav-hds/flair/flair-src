/*!
 * \file OpticalFlow.h
 * \brief Lucas and Kanade optical flow algorithm
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2012/04/12
 * \version 4.0
 */


#ifndef OPTICALFLOW_H
#define OPTICALFLOW_H

#include <IODevice.h>

namespace flair {
    namespace gui {
        class LayoutPosition;
    }
    namespace filter {
        class OpticalFlowData;
    }
}

class OpticalFlow_impl;

namespace flair {
namespace filter {
    /*! \class OpticalFlow
    *
    * \brief Lucas and Kanade optical flow algorithm
    *
    * Optical flow is done using the DSP of the DM3730.
    */
    class OpticalFlow : public core::IODevice
    {

        public:
            /*!
            * \brief Constructor
            *
            * Construct an OpticalFlow filter at given position. \n
            * After calling this function, position will be deleted as it is no longer usefull. \n
            *
            * \param parent parent
            * \param position position
            * \param name name
            */
            OpticalFlow(const core::IODevice* parent,const gui::LayoutPosition* position,std::string name);

            /*!
            * \brief Destructor
            *
            */
            ~OpticalFlow();
            
            filter::OpticalFlowData* Output(void);

            core::DataType const &GetOutputDataType() const;

        private:
            void UpdateFrom(const core::io_data *data) override;
            
            class OpticalFlow_impl *pimpl_;
            
    };
} // end namespace filter
} // end namespace flair
#endif // OPTICALFLOW_H
