//  created:    2015/10/07
//  filename:   Sobel.cpp
//
//  author:     Gildas Bayard
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    Sobel
//
//
/*********************************************************************/

#include "Sobel.h"
#include "VisionFilter.h"
#include <Image.h>
#include <Layout.h>
#include <GroupBox.h>
#include <SpinBox.h>
#include <typeinfo>

using std::string;
using namespace flair::core;
using namespace flair::gui;

class Sobel_impl {
public:
    Sobel_impl(flair::filter::Sobel *self,const LayoutPosition* position,string name):output(0) {
        this->self=self;
        
        GroupBox* reglages_groupbox=new GroupBox(position,name);
        dx=new SpinBox(reglages_groupbox->NewRow(),"dx:",0,1,1,1);
        dy=new SpinBox(reglages_groupbox->NewRow(),"dy:",0,1,1,1);

        Printf("TODO: IODevice doit faire un check de GetInputDataType et GetOutputDataType\n");
        //Image devrait accepter un type dans son constructeur pour construire un type identique
        try{
            Image::Type const &imageType=dynamic_cast<Image::Type const &>(((IODevice*)(self->Parent()))->GetOutputDataType());
            if(imageType.GetFormat()==Image::Type::Format::Gray) {
                output=new Image(self,imageType.GetWidth(),imageType.GetHeight(),imageType.GetFormat(),"sobel");
                //inIplImage = (IplImage*)AllocFunction(sizeof(IplImage));
                //outIplImage = (IplImage*)AllocFunction(sizeof(IplImage));
            } else {
                self->Err("input image is not gray\n");
            }

        } catch(std::bad_cast& bc) {
            self->Err("io type mismatch\n");
        }
    }

    ~Sobel_impl() {
        //FreeFunction((char*)(inIplImage));
        //FreeFunction((char*)(outIplImage));
    }
    
    void UpdateFrom(const io_data *data){
        Image *image=(Image*)data;
/*
        data->GetMutex();
        inIplImage->width=image->GetDataType().GetWidth();
        inIplImage->height=image->GetDataType().GetHeight();
        inIplImage->imageData=image->buffer;
        inIplImage->imageSize=image->GetDataType().GetSize();
        
        output->GetMutex();
        outIplImage->width=output->GetDataType().GetWidth();
        outIplImage->height=output->GetDataType().GetHeight();
        outIplImage->imageData=output->buffer;
        outIplImage->imageSize=output->GetDataType().GetSize();
                
        dspSobel(inIplImage,outIplImage,dx->Value(),dy->Value());
        output->ReleaseMutex();
        data->ReleaseMutex();
*/
        output->SetDataTime(data->DataTime());

    };
    
    Image *output;

private:
    flair::filter::Sobel *self;
    SpinBox *dx;
    SpinBox *dy;
    //IplImage *inIplImage,*outIplImage;
};

namespace flair { namespace filter {

Sobel::Sobel(const IODevice* parent,const LayoutPosition* position,string name) : IODevice(parent,name) {
   pimpl_=new Sobel_impl(this,position,name);
}

Sobel::~Sobel(void) {
    delete pimpl_;
}

Image* Sobel::Output(void) {
    return pimpl_->output;
}

void Sobel::UpdateFrom(const io_data *data) {
    pimpl_->UpdateFrom(data);
    ProcessUpdate(pimpl_->output);
}

DataType const &Sobel::GetOutputDataType() const {
    if(pimpl_->output!=NULL) {
        return pimpl_->output->GetDataType();
    } else {
        return dummyType;
    }
}

} // end namespace filter
} // end namespace flair
