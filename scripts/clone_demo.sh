#!/bin/bash

function info () {
	echo -e "\033[32m$1\033[0m"
}

function warn () {
	echo -e "\033[33m$1\033[0m"
}

function err () {
	echo -e "\033[31m$1\033[0m"
	exit 1
}

#arg 1 cmake file
#arg 2 old project name
#arg 3 new project name
function parse_cmakefile () {
    echo
    info "parsing ${1}"
    dir=$(dirname ${1})

    #rename cpp and h files from cmakelist
    cat  ${1} | grep cpp | grep $2 | while read file; do
        #remove \r
        file=$(echo $file | sed 's/\r//')
        new_file=$(echo ${file/$2/$3})

        mv ${dir}/${file} ${dir}/${new_file} 
        echo renamed $file to $new_file

        file=$(echo ${file/cpp/h})
        if  [ -f  ${dir}/${file} ]; then
           new_file=$(echo ${file/$2/$3})
           mv ${dir}/${file} ${dir}/${new_file} 
	       echo renamed $file to $new_file
        fi
        
    done
 
    #replace old project refeerence to new project un cmakelists
    sed -i "s/${2}/${3}/g" ${1}
    echo "changed reference of $2 to $3"
}

#arg 1 cpp file
#arg 2 old project name
#arg 3 new project name
function parse_source_file () {
    echo
    info "parsing ${1}"
    sed -i "s/${2}/${3}/g" ${1}
    echo "changed reference of $2 to $3"
}

if [ "${2}" = "" ]; then
    err "usage clone_demo.sh path_to_source_demo destination_demo_name"
fi

if ! [ -f  ${1}/CMakeLists.txt ]; then
	err "${1} directory does not contain CMakeLists.txt"
fi

if [ -d ${2} ];then
    err "output ${2} directory already exists"
fi

echo cloning ${1} to ${2}
cp -r ${1} ./${2}
cd ./${2}

#get old project name from top cmakelists
old_project=$(cat ./CMakeLists.txt | grep -i project\( |  cut -d "(" -f2 | cut -d ")" -f1)

echo "old project name is" $old_project

#search all CMakeLists.txt, sort by depth and loop
find -name "CMakeLists.txt" -printf '%h\0%d\0%p\n' | sort -t '\0' -n | awk -F '\0' '{print $3}' | while read cmakefile; do
    parse_cmakefile $cmakefile ${old_project} ${2}
done


#find all cpp files
find ${dir} -name "*.cpp" | while read file; do
    parse_source_file $file ${old_project} ${2}
done

#find all h files
find ${dir} -name "*.h" | while read file; do
    parse_source_file $file ${old_project} ${2}
done

#find all sh files
find ${dir} -name "*.sh" | while read file; do
    parse_source_file $file ${old_project} ${2}
done
