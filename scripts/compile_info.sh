#usage compile_info.sh gcc dir file
#gcc: gcc path
#dir: directory to get git info
#file: output file to put info

USER=$(whoami)@$(hostname)
DATE=$(date)
GCC_REV=$($1 -dumpversion)
GIT_PATH=$(readlink -f $2)
GIT_REV=$(git -C $(readlink -f $2) show -s --pretty='format:commit %h, by %an, on %as')

mkdir -p $(dirname $3)

cat > $3 <<EOF
//file generated automatically by compile_info.sh script
//do not edit it manually

#include <string>

#define USER "${USER}"
#define DATE "${DATE}"
#define GCC_REV "${GCC_REV}"
#define GCC_PATH "$1"
#define GIT_PATH "${GIT_PATH}"
#define GIT_REV "${GIT_REV}"

static inline void compile_info(std::string name) { 
	fprintf(stderr,"Using %s library:\n",name.c_str());
    fprintf(stderr,"  -path " GIT_PATH "\n");
	fprintf(stderr,"  -built by " USER " on " DATE "\n");
	fprintf(stderr,"  -with GCC " GCC_REV " from " GCC_PATH "\n");
    fprintf(stderr,"  -" GIT_REV "\n\n");
}
EOF
