#!/bin/bash

function red_echo () {
	echo -e "\033[33m$1\033[0m"
}

red_echo "Calling distribution_specific_hack.sh is no longer necessary; this script now does nothing"
red_echo "If you encounter problems running your program, call the distribution_specific_hack_compat.sh script and warn a Flair developer\n"
