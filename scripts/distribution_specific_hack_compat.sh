#check required version of a lib in host graphic lib (ie i965)
#return 1 if robomap3 version is not sufficient
function check_required_lib_ver () {
    DRIVER_LIB=$1	
    PATTERN=$2
    LIB=$3

    #get the highest required version of $PATTERN
    VER_REQUIRED=$(objdump -T $DRIVER_LIB | grep $PATTERN | sed -e 's/.*'$PATTERN'_\(.*\) .*/\1/' | sort -uV | tail -1)
    #get currently used $LIB
    LIB_PROVIDED=$(ldd $EXEC | grep $LIB | sed -e 's/.*=> \(.*\) (.*/\1/')
    #get the highest version of $PATTERN supported by the $LIB
    VER_PROVIDED=$(objdump -T $LIB_PROVIDED | grep ".*"$PATTERN"_[0-9.]*$" | sed -e 's/.*'$PATTERN'_\(.*\)/\1/' | sort -V | tail -1)
    if [ $(echo -e "$VER_PROVIDED\n$VER_REQUIRED" | sort -V | tail -n1) != $VER_PROVIDED ]; then
        echo "We must use the local system $LIB"
        return 1
    fi
}

function green_echo () {
	echo -e "\033[32m$1\033[0m"
}

function red_echo () {
	echo -e "\033[33m$1\033[0m"
}

red_echo "Calling distribution_specific_hack_compat.sh should not be necessary; try without it or warn a Flair developer"
red_echo "This script will be removed in future releases\n"

if [ -f /etc/lsb-release ]; then
  . /etc/lsb-release
fi

#also needed in mint, but for different reason?
#if not in sudo in mint, simlator does not work cause of:
#libGL: MESA-LOADER: failed to open /usr/lib/x86_64-linux-gnu/dri/iris_dri.so: /usr/lib/x86_64-linux-gnu/dri/iris_dri.so: failed to map segment from shared object

#if [ _$DISTRIB_ID = _Ubuntu ]; then
  #tested on Ubuntu 17.10
  #we must run as root
  if [ $EUID -ne 0 ]; then
    exec sudo -E $0 $*
  fi

  #we must run in root cgroup to escape possible restrictions on scheduling
  if [ -d /sys/fs/cgroup/cpu ]; then
    echo $$ > /sys/fs/cgroup/cpu/tasks
  fi

#fi

#special actions if we use 3D
ldd $EXEC | grep "libGL.so" > /dev/null
if [ $? -eq 0 ]; then
    green_echo "checking for incompatibility when using libGL and trying to solve it..."
    #allow root access to the X server
    xhost si:localuser:root

    #Mesa DRI driver may need a lib more recent than robomap3's
    ldconfig -p | grep mesa > /dev/null
    if [ $? -eq 0 ]; then
        read DRI_CARD_MAJOR DRI_CARD_MINOR < <(stat -c '%t %T' /dev/dri/card0)
        DRIVER=$(cat /sys/dev/char/$((16#$DRI_CARD_MAJOR)):$((16#$DRI_CARD_MINOR))/device/uevent | grep DRIVER | cut -d= -f2)
        echo "Mesa DRI driver is $DRIVER"
        DRIVER_LIB=$(LIBGL_DEBUG=verbose glxinfo 2>&1 | grep "\.so" | tail -n 1 | grep -o '/.*\.so')

        #todo: make a list, find a complete one!
        check_required_lib_ver $DRIVER_LIB GLIBC libm.so.6
        if [ "$?" = "1" ]; then
		   ADDED_LIBS=$(ldconfig -p | grep libm.so.6 | grep libc6,x86-64 | cut -d'>' -f2)
	    fi

        check_required_lib_ver $DRIVER_LIB CXXABI libstdc++.so.6
        if [ "$?" = "1" ]; then
		   ADDED_LIBS="$ADDED_LIBS:$(ldconfig -p | grep libstdc++.so.6 | grep libc6,x86-64 | cut -d'>' -f2)"
	    fi

        #remove whitespace
        #add libs to LD_PRELOAD if version robomap3 version is not sufficient
        export LD_PRELOAD="$(echo -e "${ADDED_LIBS}" | tr -d '[:space:]')"
    fi

    #check if using DRI3 and disable it
    #otherwise you get /usr/lib/x86_64-linux-gnu/libGLX_mesa.so.0: undefined symbol: xcb_dri3_get_supported_modifiers
    cat /var/log/Xorg.0.log | grep DRI3 > /dev/null
    if [ $? -eq 0 ]; then
        export LIBGL_DRI3_DISABLE=1
        echo "we must disable DRI3"
    fi
    green_echo "...done\n"
fi



