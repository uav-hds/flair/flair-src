_runscript()
{
    local DEMO_PATH="$FLAIR_ROOT"/flair-install/bin/demos/core2-64/
    local cmd=$1 cur=$2

    #first arguemnt, list all demos in $DEMO_PATH
    if [[ ${COMP_CWORD} -eq 1 ]] ; then        
        COMPREPLY=( $( cd "$DEMO_PATH" && compgen -d -- "$cur" ) )
    fi

    #second argument, list all launch files
    if [[ ${COMP_CWORD} -eq 2 ]] ; then
        local arr
        arr=( $(find "$DEMO_PATH/${COMP_WORDS[COMP_CWORD-1]}" -type f -name "*.flair"  -printf '%f\n'))
        COMPREPLY=($(compgen -W "${arr[*]}" -- "$cur" ))
    fi
}
complete -F _runscript flairrun

_makescript()
{
  _script_commands=$("$FLAIR_ROOT"/flair-src/bin/flairmake shortlist)

  local cur
  COMPREPLY=()
  cur="${COMP_WORDS[COMP_CWORD]}"
  COMPREPLY=( $(compgen -W "${_script_commands}" -- ${cur}) )
}
complete -o nospace -F _makescript flairmake
