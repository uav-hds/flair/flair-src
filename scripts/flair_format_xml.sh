#! /bin/sh

for i in `find $FLAIR_ROOT -name "*.xml" -type f`; do
    echo "formatting $i"
    xmllint --format $i -o $i
done

