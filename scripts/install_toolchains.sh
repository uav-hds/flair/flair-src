#!/bin/bash

TOOLCHAIN_VER="r1"

function green_echo () {
	echo -e "\033[32m$1\033[0m"
}

function red_echo () {
	echo -e "\033[31m$1\033[0m"
}

function install_toolchain () {
	green_echo "Downloading $1 toolchain version $TOOLCHAIN_VER"
    TOOLCHAIN_FILE=x86_64-meta-toolchain-flair-"$1".sh

    wget https://devel.hds.utc.fr/flair/toolchain/"$TOOLCHAIN_VER"/"$TOOLCHAIN_FILE" -O /tmp/"$TOOLCHAIN_FILE"
    if [ "$?" != 0 ]; then
        red_echo "Download failed"
        return
    fi

    green_echo "Installing $1 toolchain"
    chmod +x /tmp/"$TOOLCHAIN_FILE"
    sudo -E /tmp/"$TOOLCHAIN_FILE"
    rm /tmp/"$TOOLCHAIN_FILE"
}

function program_check () {
	if [ "$(command -v "$1")" = "" ]; then
        red_echo "You must install $1"
    fi
}

function sanity_check () {
	if [ -z $FLAIR_ROOT ]; then
		red_echo "You must set the FLAIR_ROOT environement variable"
		exit 1
	fi
    program_check python
}

sanity_check

TOOLCHAINS="x86_64 armv7a-neon"

#iterate over available toolchains
for arch in ${TOOLCHAINS[@]}; do
    install_toolchain $arch
done
