#!/bin/bash

function red_echo () {
	echo -e "\033[33m$1\033[0m"
}


red_echo "Calling ubuntu_cgroup_hack.sh is no longer necessary; this script now does nothing"
red_echo "If you encounter problems running your program, call the ubuntu_cgroup_hack_compat.sh script and warn a Flair developer"
