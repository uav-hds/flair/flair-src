function red_echo () {
	echo -e "\033[33m$1\033[0m"
}

if [ -f /etc/lsb-release ]; then
  . /etc/lsb-release
fi

if [ _$DISTRIB_ID == _Ubuntu ]; then
  #we must run as root
  if [ $EUID -ne 0 ]; then
    exec sudo -E $0 $*
  fi

  #we must run in root cgroup to escape possible restrictions on scheduling
  if [ -d /sys/fs/cgroup/cpu ]; then
    echo $$ > /sys/fs/cgroup/cpu/tasks
  fi
fi

red_echo "Calling distribution_specific_hack_compat.sh should not be necessary; try without it or warn a Flair developer"
red_echo "This script will be removed in future releases\n"
