#! /bin/bash
ARCH_DIR=core2-64
#on kernel >=4.13, ds3 is seen as a mouse
#search every ds3 connected and disable them
for id in `xinput --list|grep 'Sony PLAYSTATION(R)3 Controller'|perl -ne 'while (m/id=(\d+)/g){print "$1\n";}'`; do
    xinput set-prop $id 'Device Enabled' 0
done

${FLAIR_ROOT}/flair-install/bin/tools/$ARCH_DIR/dualshock3 $*
