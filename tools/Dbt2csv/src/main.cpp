#include <stdio.h>
#include <QApplication>
#include <QCleanlooksStyle>
#include <QLocale>
#include <QTextCursor>
#include <QFileDialog>

#include "compile_info.h"
#include "LogFileUI.h"

int main(int argc, char *argv[]) {
	compile_info("Dbt2Csv");

	qRegisterMetaType<const char *>("const char*");
	qRegisterMetaType<QTextCursor>("QTextCursor");
	QLocale::setDefault(QLocale::C);
	QApplication app(argc, argv);
	app.setStyle(new QCleanlooksStyle);
  

	LogFileUI *files = new LogFileUI("Dbt2Csv");
	files->connect(files, SIGNAL(finished()), &app, SLOT(quit()));

	QString path = QFileDialog::getExistingDirectory(new QWidget(),
												   "Select a directory", 0, 0);
	QDir directory(path);
	QStringList list_files = directory.entryList();

	for (int i = 0; i < list_files.size(); i++) {
		files->addFile(directory.path() + "/" + list_files.at(i));
	}

	files->endOfFiles();

	app.exec();
}
