#! /bin/sh
#ARCH_DIR=$(uname -m)
ARCH_DIR=core2-64

#defaults options values
PORT=9000
NAME="FlairGCS"

usage() {
  USAGE="usage: $0 [-n name] [-p port] [-?]|[-h]"
  echo $USAGE;
  exit 1
}

while getopts h?n:p: OPT; do
  case $OPT in
  n)      NAME=$OPTARG;;
  p)      PORT=$OPTARG;;
  h|\?)     usage;;
  esac
done
shift `expr $OPTIND - 1`

#bail out in case unknown options remain
[ "$1" = "--" ] && usage 

export FONTCONFIG_PATH=${OECORE_HOST_SYSROOT}/etc/fonts/ 
export FONTCONFIG_FILE=${OECORE_HOST_SYSROOT}/etc/fonts/fonts.conf 
$FLAIR_ROOT/flair-install/bin/tools/${ARCH_DIR}/flairgcs -n $NAME -p $PORT
