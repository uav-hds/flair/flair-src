// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
#include "ConnectionLayout.h"
#include "UdtSocket.h"
#include "DataRemote.h"
#include <qendian.h>
#include "communication.h"
#include <QDir>
#include <QMessageBox>

ConnectionLayout::ConnectionLayout(UdtSocket *socket, QString name, QTime startTime)
    : Layout(NULL, name, "root") {
  this->socket = socket;
  this->name = name;
  this->startTime = startTime;
  
  // create temp directory for data log storage
  QDateTime dateTime = QDateTime::currentDateTime();
  logDirNameBase=dateTime.toString("yyyyMMdd_hhmm") + "_" + name;
  logDirName ="/tmp/FlairGCS/" +logDirNameBase;
  if (QDir().exists(logDirName) == true) {
    logDirNameBase=dateTime.toString("yyyyMMdd_hhmm_ss") + "_" + name;
    logDirName ="/tmp/FlairGCS/" +logDirNameBase;
  }
  QDir().mkpath(logDirName);
}

ConnectionLayout::~ConnectionLayout() {
}

QTime ConnectionLayout::GetStartTime(void) {
    return startTime;
}

void ConnectionLayout::udtSocketDestroyed(QObject *obj){
  socket=NULL;
}

void ConnectionLayout::receive(char *buf, int size) {
   //fprintf(stderr,"trame %x\n",buf[0]);
  // for(int i=0; i<size;i++) fprintf(stderr,"%x ",buf[i]);
  // fprintf(stderr,"\n");
  switch ((unsigned char)buf[0]) {
  case XML_HEADER: {
    QString xml;
    QDomDocument doc;
    xml = QString((char *)buf);
    xml.resize(size);

    //fprintf(stderr,"recu %i\n%s\n",size,xml.toLocal8Bit().constData());
    if (!doc.setContent(xml)) {
      fprintf(stderr,"prob setContent fichier\n");
    }

    QDomElement dom=doc.firstChildElement("root").firstChildElement();
    ParseXml(&dom);
    break;
  }
  case DATA_BIG_ENDIAN: {
    uint16_t period;
    memcpy(&period, &buf[1], sizeof(uint16_t));
    period = qFromBigEndian(period);
    drawDatas(&buf[3], size - 3, period, 1,true);
    break;
  }
  case DATA_LITTLE_ENDIAN: {
    uint16_t period;
    memcpy(&period, &buf[1], sizeof(uint16_t));
    drawDatas(&buf[3], size - 3, period,1);
    break;
  }
  case MULTIPLE_DATA_BIG_ENDIAN: {
    uint16_t period;
    uint16_t nb_buffering;
    memcpy(&period, buf+sizeof(char), sizeof(uint16_t));
    period = qFromBigEndian(period);
    memcpy(&nb_buffering,buf+sizeof(char)+sizeof(uint16_t), sizeof(uint16_t));
    nb_buffering = qFromBigEndian(nb_buffering);
    //fprintf(stderr,"recu %i, period %i, nb_buff %i\n",size,period,nb_buffering);
    drawDatas(buf+sizeof(char)+sizeof(uint16_t)+sizeof(uint16_t), size - sizeof(char)+sizeof(uint16_t)+sizeof(uint16_t), period, nb_buffering,true);
    break;
  }
  case MULTIPLE_DATA_LITTLE_ENDIAN: {
    uint16_t period;
    uint16_t nb_buffering;
    memcpy(&period, buf+sizeof(char), sizeof(uint16_t));
    memcpy(&nb_buffering,buf+sizeof(char)+sizeof(uint16_t), sizeof(uint16_t));
    //fprintf(stderr,"recu %i, period %i, nb_buff %i\n",size,period,nb_buffering);
    drawDatas(buf+sizeof(char)+sizeof(uint16_t)+sizeof(uint16_t), size - sizeof(char)+sizeof(uint16_t)+sizeof(uint16_t), period, nb_buffering);
    break;
  }
  case CLOSING_CONNECTION: {
    deleteLater();
    break;
  }
  default:
    fprintf(stderr,"trame non supportée %x\n", buf[0]);
  }
}

void ConnectionLayout::XmlToSend(QDomDocument doc) {
   //fprintf(stderr,"xml to send\n%s\n",doc.toString().toLocal8Bit().constData());
  if(!socket) return;
  
  // xml to send a mettre dans le manager
  if(doc.toString().toLocal8Bit().length()!=0) {
    socket->write(doc.toString().toLocal8Bit().constData(),doc.toString().toLocal8Bit().length());
  }
  /*
  QMetaObject::invokeMethod(
      socket, "write", Qt::BlockingQueuedConnection,
      Q_ARG(const char *, doc.toString().toLocal8Bit().constData()),
      Q_ARG(qint64, doc.toString().toLocal8Bit().length()));*/
}

void ConnectionLayout::LoadXml(QDomDocument *to_parse) {
  QDomElement tmp = to_parse->firstChildElement("root");
  while (tmp.attribute("name") != name && !tmp.isNull())
    tmp = to_parse->nextSiblingElement("root");

  if (!tmp.isNull()) {
    XmlWidget::LoadXml(&tmp);
  } else {
    fprintf(stderr,"%s not found in xml file \n", name.toLocal8Bit().constData());
  }
}

void ConnectionLayout::removeDataRemote(DataRemote *data) {
  dataremotes.removeOne(data);
}

void ConnectionLayout::addDataRemote(DataRemote *data) {
  dataremotes.append(data);
}

QString ConnectionLayout::getName() { return name; }

QString ConnectionLayout::getLogDirName() { return logDirName; }

void ConnectionLayout::drawDatas(char *buf, int buf_size, uint16_t period,uint16_t nb_buffering, bool big_endian) {
    for (int i = 0; i < nb_buffering; i++) {
        for (int j = 0; j < dataremotes.count(); j++) {
            dataremotes.at(j)->BufEvent(&buf, &buf_size, period, nb_buffering,big_endian);
        }
    }
}

QString ConnectionLayout::getDocRootName(char* buf, int size) {
  QString xml;
  QDomDocument doc;
  xml = QString((char *)buf);
  xml.resize(size);

  if (!doc.setContent(xml)) {
    fprintf(stderr,"prob setContent fichier\n");
  }
  
  return doc.firstChildElement("root").attribute("name");
}


void ConnectionLayout::saveGCSLogs(void) {
  QDir srcDir(logDirName);
  if (! srcDir.exists())
      return;
  
  QDir().mkpath(logDirNameBase);
  QStringList filesList = srcDir.entryList(QDir::Files);
   
  for (int i = 0; i < filesList.size(); ++i) {
    if (QFile::exists(logDirNameBase + QDir::separator() + filesList.at(i))) {
        QFile::remove(logDirNameBase + QDir::separator() + filesList.at(i));
    }
    QFile::copy(logDirName + QDir::separator() + filesList.at(i), logDirNameBase + QDir::separator() + filesList.at(i));
  }
  
  QMessageBox msgBox;
  msgBox.setText(QString("Log saved to %1/%2").arg(QDir::currentPath()).arg(logDirNameBase));
  msgBox.exec();
}