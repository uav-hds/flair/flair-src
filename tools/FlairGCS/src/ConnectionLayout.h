// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
#ifndef CONNECTIONLAYOUT_H
#define CONNECTIONLAYOUT_H

#include "Layout.h"
#include <QTime>

class UdtSocket;
class DataRemote;

class ConnectionLayout : public Layout {
  Q_OBJECT

public:
  ConnectionLayout(UdtSocket *socket, QString name, QTime startTime);
  ~ConnectionLayout();
  void XmlToSend(QDomDocument doc);
  void addDataRemote(DataRemote *data);
  void removeDataRemote(DataRemote *data);
  void LoadXml(QDomDocument *to_parse);
  QString getName();
  QString getLogDirName();
  static QString getDocRootName(char* buf, int size);
  QTime GetStartTime(void);
  void saveGCSLogs(void);
  
private:
  void drawDatas(char *buf, int buf_size, uint16_t period,uint16_t nb_buffering,
                 bool big_endian = false);
  QString name;
  UdtSocket *socket;
  QList<DataRemote *> dataremotes;
  QTime startTime;
  QString logDirName,logDirNameBase;
  
private slots:
  void receive(char *buf, int size);
  void udtSocketDestroyed(QObject *obj);

signals:
  void UDTStats(QString stats,QString stylesheet,bool loosingPackets);  
};

#endif // CONNECTIONLAYOUT_H
