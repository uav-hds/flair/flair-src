// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
#include "DataPlot1D.h"
#include "ConnectionLayout.h"
#include "Layout.h"
#include <QMouseEvent>
#include <QMenu>
#include <QGridLayout>
#include <QTime>
#include <qendian.h>

DataPlot1D::DataPlot1D(Layout *parent, int row, int col, QString title,
                       float ymin, float ymax, bool enabled, uint16_t period, uint16_t nb_buffering)
    : ScopeFixedStep(title, ymin, ymax, period / 1000.),
      DataRemote(title, "DataPlot1D", parent, enabled, period, nb_buffering) {

  setEnabled(enabled);
  ResetElapsedTime((float)connectionLayout()->GetStartTime().msecsTo(QTime::currentTime())/1000.);
  parent->addWidget(this, row, col);
  visible_widget = this;
  
  
  QDomNode node = XmlDoc().firstChild();
  while (node.firstChild().isNull() == false) {
    node = node.firstChild();
     if(node.toElement().tagName()!="TabWidget") {
       if(logFileName!="") logFileName+="_";
       logFileName+=node.toElement().attribute("name");
     }
  }
 
  logDescFileName=logFileName+".txt";
  logFileName+=".csv";
  
  logDescFileName.replace(QString("/"), QString("_"));
  logFileName.replace(QString("/"), QString("_"));
 
  logFile = new QFile(connectionLayout()->getLogDirName() + "/" +logFileName);
  if (!logFile->open(QIODevice::Append | QIODevice::Text)) {
    printf("unable to open file %s\n",QString(connectionLayout()->getLogDirName() + "/" +logFileName).toLocal8Bit().constData());
    logFile=NULL;
  }
  logDescFile = new QFile(connectionLayout()->getLogDirName() + "/" +logDescFileName);
  if (!logDescFile->open(QIODevice::Append | QIODevice::Text)) {
    printf("unable to open file %s\n",QString(connectionLayout()->getLogDirName() + "/" +logDescFileName).toLocal8Bit().constData());
    logDescFile=NULL;
  }
  logDescFile->write("time (s)");
  logDescFile->flush();
}

DataPlot1D::~DataPlot1D() {
  visible_widget = NULL; // because otherwise xmlwidget will delete it
  if(logFile!=NULL) delete logFile;
  if(logDescFile!=NULL) delete logDescFile;
}

void DataPlot1D::XmlEvent(QDomElement *dom) {
  bool previouslyEnabled=IsEnabled();
  
  if (dom->attribute("curve") != "") {
    QString type = dom->attribute("type");
    int r = dom->attribute("r").toInt();
    int g = dom->attribute("g").toInt();
    int b = dom->attribute("b").toInt();
    QString name = dom->attribute("curve");
    addCurve(QPen(QColor(r, g, b, 255)), name);
    datas_type.append(type);
    if (type == "float") {
      receivesize += sizeof(float);
    } else if (type == "int8_t") {
      receivesize += sizeof(int8_t);
    } else if (type == "int16_t") {
      receivesize += sizeof(int16_t);
    } else {
      fprintf(stderr,"MyDataPlot1D::addCurve unknown type %s\n",
             type.toLocal8Bit().constData());
    }
    logDescFile->write(QString(",%1").arg(name).toLocal8Bit().constData());
    logDescFile->flush();
  } else {
    XmlSetup(dom);
  }
  
    if(previouslyEnabled!=IsEnabled() && IsEnabled()) 
        ResetElapsedTime((float)connectionLayout()->GetStartTime().msecsTo(QTime::currentTime())/1000.);
}

bool DataPlot1D::eventFilter(QObject *o, QEvent *e) {
  if (o == canvas()) {
    switch (e->type()) {
    case QEvent::Resize: {
      // resolution bug taille widgets:
      setMaximumHeight(parentWidget()->height() /
                       ((QGridLayout *)(parentWidget()->layout()))->rowCount());
      break;
    }
    case QEvent::MouseButtonPress: {
      return mouseEvent((QMouseEvent *)e);
    }

    default:
      break;
    }
  }
  return Scope::eventFilter(o, e);
}

void DataPlot1D::BufEvent(char **buf, int *buf_size, uint16_t period,uint16_t nb_buffering,bool big_endian) {
  setEnabled(IsEnabled());
  if (IsEnabled() == false || RefreshRate_ms() != period || NbBuffering()!=nb_buffering) return;
  double *datas = (double *)malloc(datas_type.count() * sizeof(double));

  logFile->write(QString("%1").arg(elapsed_time_s).toLocal8Bit().constData());
  
  for (int i = 0; i < datas_type.count(); i++) {
    if (datas_type.at(i) == "float") {
      uint32_t data_raw;
      float *data = (float *)&data_raw;
      memcpy((void *)&data_raw, *buf, sizeof(uint32_t));
      *buf += sizeof(uint32_t);
      if (big_endian == true) data_raw = qFromBigEndian(data_raw);
      datas[i] = *data;
    } else if (datas_type.at(i) == "int8_t") {
      int8_t data;
      memcpy((void *)&data, *buf, sizeof(data));
      *buf += sizeof(data);
      datas[i] = data;
    } else if (datas_type.at(i) == "int16_t") {
      int16_t data;
      memcpy((void *)&data, *buf, sizeof(data));
      *buf += sizeof(data);
      if (big_endian == true) data = qFromBigEndian(data);
      datas[i] = data;
    } else {
      fprintf(stderr,"DataPlot1D::BufEvent unknown type %s\n",datas_type.at(i).toLocal8Bit().constData());
    }
    
    logFile->write(QString(",%1").arg(datas[i]).toLocal8Bit().constData());
    
  }

  logFile->write("\n");
  logFile->flush();

  step_s = period / 1000.;
  plot(datas);
  free(datas);
}

// context menu
bool DataPlot1D::mouseEvent(QMouseEvent *event) {
  if (event->button() == Qt::RightButton) {
    QMenu *menu = new QMenu("nom", (QwtPlot *)this);
    QAction *resetX, *resetY, *clear,*action;

    resetX = menu->addAction("reset time view");
    resetY = menu->addAction("reset y view");
    menu->addSeparator();

    appendmenu(menu);
    
    menu->addSeparator();
    clear = menu->addAction("clear graph");
    
    action = execmenu((QwtPlot *)this, menu, event->globalPos());
    delete menu;

    if (action == resetX)
      resetXView();
    if (action == resetY)
      resetYView();
      
    if (action == clear)
      clearCurves();
        
    return true;
  }
  return false;
}
