// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
#ifndef DATAPLOT1D_H_INCLUDED
#define DATAPLOT1D_H_INCLUDED

#include <ScopeFixedStep.h>
#include "DataRemote.h"

class Layout;
class QMouseEvent;
class QFile;

class DataPlot1D : public ScopeFixedStep, public DataRemote {
public:
  DataPlot1D(Layout *parent, int row, int col, QString title, float ymin,
             float ymax, bool enabled, uint16_t period, uint16_t nb_buffering=1);
  ~DataPlot1D();

private:
  void XmlEvent(QDomElement *dom);
  bool eventFilter(QObject *, QEvent *);
  void BufEvent(char **buf, int *buf_size, uint16_t period,uint16_t nb_buffering,bool big_endian);
  bool mouseEvent(QMouseEvent *event);
  QList<QString> datas_type;
  QString logFileName,logDescFileName;
  QFile *logFile,*logDescFile;
};

#endif // DATAPLOT1D_H_INCLUDED
