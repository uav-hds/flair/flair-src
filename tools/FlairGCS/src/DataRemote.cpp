// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
#include "DataRemote.h"
#include <QMenu>
#include <QInputDialog>
#include <QtXml>
#include "ConnectionLayout.h"

DataRemote::DataRemote(QString name, QString type, XmlWidget *parent,
                       bool enabled, uint16_t period, uint16_t nb_buffering)
    : XmlWidget(name, type, parent) {
  auto_refresh = enabled;
  is_logging = true;
  receivesize = 0;
  refresh_rate = (double)period / 1000.;
   
   //flair programs without nb buffering have nb_buffering=0, set it to 1 for compatibility
    if(nb_buffering==0) {
        this->nb_buffering=1;
        nbBufferingCompatible=false;
    } else {
        this->nb_buffering=nb_buffering;
        nbBufferingCompatible=true;
    }
 
  
  connectionLayout()->addDataRemote(this);

  // pour ne pas faire de doublons qd on change la periode
  SetIsExpandable(true);
}

DataRemote::~DataRemote() { connectionLayout()->removeDataRemote(this); }

void DataRemote::appendmenu(QMenu *menu) {
  menu->addSeparator();

  SetAutoRefresh = menu->addAction("auto refresh");
  SetAutoRefresh->setCheckable(true);
  SetAutoRefresh->setChecked(auto_refresh);

  setRefreshRate = menu->addAction(QString("set refresh rate (%1ms)")
                          .arg((uint16_t)(qRound(refresh_rate * 1000))));
  setRefreshRate->setEnabled(auto_refresh);
  
  //flair programs without nb buffering have nb_buffering=0
  if(nbBufferingCompatible) {
      menu->addSeparator();
      setNbBuffering = menu->addAction(QString("set nb buffering (%1)").arg(nb_buffering));
  }
  
/*  menu->addSeparator();

  log = menu->addAction("log");
  log->setCheckable(true);
  log->setChecked(is_logging);
  */
}

QAction *DataRemote::execmenu(QWidget *parent, QMenu *menu, QPoint point) {
  QAction *action;

  action = menu->exec(point);
    if(action==NULL) return action;//setNbBuffering can also be null if in compatibility mode
    
  if (action == SetAutoRefresh) {
    SendPeriod(RefreshRate_ms(), SetAutoRefresh->isChecked());
  }

  if (action == setRefreshRate) {
    bool ok;
  
    uint16_t time = QInputDialog::getInt(
        parent, "Set refresh rate ", "Value (ms):",
        (uint16_t)(qRound(refresh_rate * 1000)), 1, 65535, 10, &ok);
    if (ok  && time != qRound(refresh_rate * 1000)) {
      // refresh_rate=time/1000.;
      SendPeriod(time, SetAutoRefresh->isChecked());
    }
  }
  
  if (action == setNbBuffering) {
    bool ok;
  
    uint16_t nb_buffering = QInputDialog::getInt(
        parent, "Set nb buffering ", "Value :",
        this->nb_buffering, 1, 65535, 10, &ok);
    if (ok  && nb_buffering !=this->nb_buffering) {
      SendNbBuffering(nb_buffering);
    }
  }
  
  return action;
}

uint16_t DataRemote::RefreshRate_ms(void) {
  return (uint16_t)(qRound(refresh_rate * 1000.));
}

uint16_t DataRemote::NbBuffering(void) {
   return nb_buffering;
}

bool DataRemote::IsEnabled(void) { return auto_refresh; }

void DataRemote::SendPeriod(uint16_t period, bool auto_refresh) {
  RemoveAllAttributes();

  SetAttribute("period", period);
  SetAttribute("enabled", auto_refresh);
  connectionLayout()->XmlToSend(XmlDoc());
  RemoveAttribute("period");
  RemoveAttribute("enabled");
}

void DataRemote::SendNbBuffering(uint16_t nb_buffering) {
    RemoveAllAttributes();

    SetAttribute("nb_buf", nb_buffering);
    connectionLayout()->XmlToSend(XmlDoc());
    RemoveAttribute("nb_buf");
}

void DataRemote::XmlSetup(QDomElement *dom) {
  refresh_rate = dom->attribute("period").toUShort() / 1000.;
  if(nbBufferingCompatible) nb_buffering = dom->attribute("nb_buf").toUShort();
  
  if (dom->attribute("enabled") == "1")
    auto_refresh = true;
  else
    auto_refresh = false;
}
