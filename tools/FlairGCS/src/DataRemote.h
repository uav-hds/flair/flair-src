// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
#ifndef DATAREMOTE_H
#define DATAREMOTE_H

#include <stdint.h>
#include "XmlWidget.h"
#include <QPoint>

class QMenu;
class QAction;

class DataRemote : public XmlWidget {
public:
  DataRemote(QString name, QString type, XmlWidget *parent, bool enabled,
             uint16_t period, uint16_t nb_buffering=1);
  ~DataRemote();
  virtual void BufEvent(char **buf, int *buf_size, uint16_t period,uint16_t nb_buffering,bool big_endian) = 0;

protected:
  int receivesize;
  double refresh_rate; // en s
  
  void XmlSetup(QDomElement *dom);
  uint16_t RefreshRate_ms(void);
  uint16_t NbBuffering(void);
  bool IsEnabled(void);
  void appendmenu(QMenu *menu);
  QAction *execmenu(QWidget *parent, QMenu *menu, QPoint point);

private:
  void SendPeriod(uint16_t period, bool auto_refresh);
  void SendNbBuffering(uint16_t nb_buffering);
  bool is_logging;
  bool auto_refresh;
  uint16_t nb_buffering;
  QAction *SetAutoRefresh, *setRefreshRate,*setNbBuffering;
  bool nbBufferingCompatible;
  //QAction **log;//todo implement this feature (log directly in gcs, in case of uav program crash)
};

#endif // DATAREMOTE_H
