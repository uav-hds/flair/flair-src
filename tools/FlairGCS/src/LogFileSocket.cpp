// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
#include "LogFileSocket.h"
#include "LogFileUI.h"
#include "communication.h"

#include <stdio.h>
#include <QtEndian>
#include <QDateTime>
#include <QDir>

using namespace std;

LogFileSocket::LogFileSocket(LogFileUI* logFileUI,UDTSOCKET socket): QObject() {
  this->socket=socket;
  this->logFileUI=logFileUI;
  
  bool blocking = true;
  if (UDT::setsockopt(socket, 0, UDT_SNDSYN, &blocking, sizeof(bool))!= 0) {
  fprintf(stderr,"UDT::setsockopt error (UDT_SNDSYN) %s\n",UDT::getlasterror().getErrorMessage());
  }
  
  if (UDT::setsockopt(socket, 0, UDT_RCVSYN, &blocking, sizeof(bool)) != 0) {
  fprintf(stderr,"UDT::setsockopt error (UDT_RCVSYN) %s\n",UDT::getlasterror().getErrorMessage());
  }
  
  linger _linger;
  _linger.l_onoff=1;
  _linger.l_linger=180;
  
  if (UDT::setsockopt(socket, 0, UDT_LINGER, &_linger, sizeof(struct linger)) != 0)
  fprintf(stderr,"UDT::setsockopt error (UDT_LINGER) %s\n",UDT::getlasterror().getErrorMessage());
 
}

LogFileSocket::~LogFileSocket() { 
}

void LogFileSocket::receive(void) {
  char *recvBuf;
  int bytesRead;
  bool flagNewSeq = true;
  QString folderName;
//ffprintf(stderr,stderr,"LogFileSocket thread %x\n",thread());
  while(1) {
    // receive file info
    recvBuf = (char *)malloc(1024);
    bytesRead = UDT::recvmsg(socket, recvBuf, 1024);
    if (bytesRead <= 0) {
      free(recvBuf);
      break;
    }

    int size;
    memcpy(&size, &recvBuf[1], sizeof(int));
    if (recvBuf[0] == FILE_INFO_BIG_ENDIAN)
      size = qFromBigEndian(size);

    // fprintf(stderr,"LogFileSocket recu %i %x\n",bytesRead,recvBuf[0]);
    if ((recvBuf[0]==FILE_INFO_LITTLE_ENDIAN || recvBuf[0]==FILE_INFO_BIG_ENDIAN) && size>0) {
      if (flagNewSeq == true) {
        // create directory for storage
        QDateTime dateTime = QDateTime::currentDateTime();
        folderName = dateTime.toString("yyyyMMdd_hhmm") + "_" + logFileUI->getName();;
        if (QDir().exists(folderName) == true) {
          folderName = dateTime.toString("yyyyMMdd_hhmm_ss") + "_" + logFileUI->getName();;
        }
        QDir().mkdir(folderName);

        flagNewSeq = false;
        logFileUI->log("Creating directory " + folderName);
      }

      QString fileName=QString::fromAscii((const char *)&recvBuf[5], bytesRead - 5);
      QString filePath=folderName+"/"+fileName;
      logFileUI->log(QString("receiving %1 (%2 bytes)").arg(fileName).arg(size));
      QFile fichier(filePath);

      if (!fichier.open(QIODevice::WriteOnly)) {
        logFileUI->log("      could not write to file!");
      } else {
        // receive file by chunks
        recvBuf = (char *)realloc((void *)recvBuf, size);
        int totalBytesRead=0;
        while(1) {
          bytesRead = UDT::recvmsg(socket, recvBuf+totalBytesRead, size);
          totalBytesRead+=bytesRead;
          if(totalBytesRead==size) break;
        }
        logFileUI->log("      ok");

        QDataStream stream(&fichier);
        stream.writeRawData(recvBuf, size);
        fichier.close();

        logFileUI->addFile(filePath);
      }

      free(recvBuf);
    } else if (recvBuf[0] == END_SENDING_FILES) {
      //end ack
      UDT::sendmsg(socket,&recvBuf[0],1);
      logFileUI->endOfFiles();
      UDT::close(socket);
      fprintf(stderr,"disconnected from log files\n");
      break;
    }
  }
  
  emit finished();
}
