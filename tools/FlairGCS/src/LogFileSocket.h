// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
#ifndef LOGFILESOCKET_H
#define LOGFILESOCKET_H

#include <QObject>
#include <udt.h>


class LogFileUI;

class LogFileSocket : public QObject {
  Q_OBJECT

public:
  LogFileSocket(LogFileUI* logFileUI,UDTSOCKET socket);
  ~LogFileSocket();
 

private:
  UDTSOCKET socket;
  LogFileUI* logFileUI;
  
public slots:
  void receive(void);
  
signals:
  void finished(void);  
};

#endif // LOGFILESOCKET_H
