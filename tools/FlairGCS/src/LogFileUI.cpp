// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
#include "LogFileUI.h"

#include <stdio.h>
#include <cstdint>
#include <io_hdfile.h>
#include <QDir>
#include <QTextStream>
#include <QComboBox>
#include <QPushButton>
#include <QTextEdit>
#include <QDialog>
#include <QStringList>
#include <QFormLayout>

using namespace std;

LogFileUI::LogFileUI(QString name): QObject() {
  this->name=name;
    
  dialog = new QDialog();

  dialog->setWindowTitle("log files");
  QGridLayout *mainLayout = new QGridLayout(dialog);
  okButton = new QPushButton("Ok", dialog);
  logText = new QTextEdit(dialog);
  logText->setReadOnly(true);
  inputText = new QTextEdit(dialog);
  inputText->setText("add your log comment here");
  inputCleared = false;
  okButton->setEnabled(false);

  mainLayout->addWidget(logText, 0, 0);
  mainLayout->addWidget(inputText, 1, 0);

  QWidget *widget = new QWidget(dialog);
  QFormLayout *formLayout = new QFormLayout(widget);
  csvCombo = new QComboBox(widget);
  formLayout->addRow(tr("save all log with following base time"), csvCombo);
  csvCombo->addItem("(no base time)");
  mainLayout->addWidget(widget, 2, 0);
  mainLayout->addWidget(okButton, 3, 0);

  //connect for multithreaded stuffs
  connect(okButton, SIGNAL(clicked()), this, SLOT(save()),Qt::QueuedConnection);
  connect(inputText, SIGNAL(cursorPositionChanged()), this,SLOT(clearInputText()),Qt::DirectConnection);
  connect(this, SIGNAL(appendToLog(QString)), logText, SLOT(append(QString)));

  fileNames = new QStringList();
  
  dialog->show();
}

LogFileUI::~LogFileUI() { 
  delete dialog;
}

QString LogFileUI::getName(void) const {
  return name;
}

void LogFileUI::log(QString text) {
  appendToLog(text);
}

void LogFileUI::addFile(QString filePath) {
  // framework sends dbt file then txt file
  // when we receive txt, we have both files
  // and we can convert it to .csv
  if (filePath.endsWith(".dbt") == true) {
    QString name =filePath.section('/', -1); // remove path for displaying on combobox
    csvCombo->addItem(name.replace(QString(".dbt"), QString(".csv")));
    fileNames->append(filePath.replace(QString(".dbt"), QString(".csv")));
  }

  if (filePath.endsWith(".txt") == true) {
	filePath.replace(QString(".txt"), QString(".dbt"));
	if (!QFile::exists(filePath)) {
		fprintf(stderr,"%s has no corresponding dbt file\n",filePath.replace(QString(".dbt"), QString(".txt")).toLocal8Bit().constData());
	} else {
		dbt2csv(filePath);
	}
  }
}

void LogFileUI::endOfFiles(void) {
  okButton->setEnabled(true);

  qint64 maxFileSize = 0;
  for (int i = 0; i < fileNames->count(); i++) {
    QFileInfo info(fileNames->at(i));
    if (info.size() > maxFileSize) {
      maxFileSize = info.size();
      csvCombo->setCurrentIndex(i+1); // first item of combobox is already taken
    }
  }
}

void LogFileUI::dbt2csv(QString filePath) {
  hdfile_t *dbtFile = NULL;
  char *data;
  QStringList dataType;

  QString filename =filePath.section('/', -1); // remove path for displaying on logs
  appendToLog(QString("converting %1 to csv").arg(filename));

  // open csv file
  QString csvFilename = filePath;
  csvFilename.replace(QString(".dbt"), QString(".csv"));
  QFile csvFile(csvFilename);

  if (!csvFile.open(QIODevice::WriteOnly | QIODevice::Text)) {
    appendToLog("      error opening csv file!");
    return;
  }
  QTextStream out(&csvFile);

  // open txt file
  QString txtFilename = filePath;
  txtFilename.replace(QString(".dbt"), QString(".txt"));
  QFile txtFile(txtFilename);

  if (!txtFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
    appendToLog("      error opening txt file!");
    return;
  }

  // read txt file
  QTextStream txtIn(&txtFile);
  txtIn.readLine(); // time us
  txtIn.readLine(); // time ns
  while (1) {
    if (txtIn.atEnd() == true)
      break;
    QString txt_line = txtIn.readLine();
    dataType.append(txt_line.section("(",-1)); // on part de la fin pour trouver la premiere parenthese ouvrante
    // fprintf(stderr,"type %s\n",txt_line.section("(",-1).toLocal8Bit().constData());
  }
  txtFile.close();

  dbtFile = open_hdfile(filePath.toLocal8Bit().data(), READ_MODE);

  if (!dbtFile) {
    appendToLog("      error opening dbt file!");
    return;
  }
  data = (char *)malloc(dbtFile->h.DataSize);
  if (data == NULL) {
    appendToLog("      error malloc!");
    return;
  }

  bool dataWritten = false;
  while (1) {
    road_time_t time;
    road_timerange_t tr = 0;
    int offset = 0;
    QTextStream csv_line;

    if (read_hdfile(dbtFile, (void *)data, &time, &tr) == 0) {
      break;
    }
    dataWritten = true;

    out << time << "," << tr;
    for (int i = 0; i < dataType.size(); i++) {
      if (dataType.at(i) == "double)") {
        double *value = (double *)(data + offset);
        offset += sizeof(double);
        out << "," << *value;
      } else if (dataType.at(i) == "float)") {
        float *value = (float *)(data + offset);
        offset += sizeof(float);
        out << "," << *value;
      } else if (dataType.at(i) == "int8_t)") {
        int8_t *value = (int8_t *)(data + offset);
        offset += sizeof(int8_t);
        out << "," << *value;
      } else if (dataType.at(i) == "uint8_t)") {
        uint8_t *value = (uint8_t *)(data + offset);
        offset += sizeof(uint8_t);
        out << "," << *value;
      } else if (dataType.at(i) == "int16_t)") {
        int16_t *value = (int16_t *)(data + offset);
        offset += sizeof(int16_t);
        out << "," << *value;
      } else if (dataType.at(i) == "uint16_t)") {
        uint16_t *value = (uint16_t *)(data + offset);
        offset += sizeof(uint16_t);
        out << "," << *value;
      } else {
        appendToLog(QString("      unhandled type: %1").arg(dataType.at(i)));
      }
    }

    out << "\n";
  }

  if (!dataWritten) {
    // empty file!
    out << "0,0"; // timr
    for (int i = 0; i < dataType.size(); i++) {
      out << ",0";
    }
    out << "\n";
  }

  csvFile.close();
  close_hdfile(dbtFile);
  if (data != NULL)
    free(data);

  appendToLog("      ok");
}

void LogFileUI::clearInputText(void) {
  if (inputCleared == false) {
    inputCleared = true;
    inputText->clear();
  }
}

void LogFileUI::save(void) {
  saveComment();
  if (csvCombo->currentIndex() != 0) {
    saveCsv();
    saveTxt();
  }

  emit finished();
}

void LogFileUI::saveComment(void) {
  QString folderName = fileNames->at(0).section('/', 0, -2);

  QString filename = folderName + "/comment.txt";
  QFile file(filename);
  if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
    fprintf(stderr,"LogFileUI::saveComment: erreur ouverture fichier %s\n",filename.toLocal8Bit().constData());
  
  QTextStream out(&file);

  out << inputText->toPlainText();
  file.close();
}

void LogFileUI::saveCsv(void) {
  // global csv file
  QString folderName = fileNames->at(0).section('/', 0, -2);
  QString filename = folderName + "/all_logs.csv";
  QFile globalFile(filename);
  if (!globalFile.open(QIODevice::WriteOnly | QIODevice::Text))
    fprintf(stderr,"LogFileUI::saveCsv: erreur ouverture fichier %s\n",filename.toLocal8Bit().constData());
	
  QTextStream out(&globalFile);

  // reference csv file
  filename = fileNames->at(csvCombo->currentIndex() - 1);
  QFile refFile(filename);
  // fprintf(stderr,"LogFileUI::saveCsv: ref %s\n",filename.toLocal8Bit().constData());
  if (!refFile.open(QIODevice::ReadOnly | QIODevice::Text))
    fprintf(stderr,"LogFileUI::saveCsv: erreur ouverture ficher %s\n",filename.toLocal8Bit().constData());

  // other csv files
  int j = 0;
  QFile mFile[fileNames->count() - 1];
  QTextStream mIn[fileNames->count() - 1];
  for (int i = 0; i < fileNames->count(); i++) {
    if (i == csvCombo->currentIndex() - 1)
      continue;
	  
    filename = fileNames->at(i);
    mFile[j].setFileName(filename);
    if (!mFile[j].open(QIODevice::ReadOnly | QIODevice::Text))
      fprintf(stderr,"LogFileUI::saveCsv: erreur ouverture ficher %s\n",filename.toLocal8Bit().constData());
	  
    mIn[j].setDevice(&mFile[j]);
    j++;
  }

  // init
  QTextStream refIn(&refFile);
  QString mLine[fileNames->count() - 1];
  QString mLinePrev[fileNames->count() - 1];
  for (int i = 0; i < fileNames->count() - 1; i++) {
    mLine[i] = mIn[i].readLine();
    mLinePrev[i] = mLine[i];
  }

  // organize csv files in one file
  while (1) {
    if (refIn.atEnd() == true)
      break;
    QString refLine = refIn.readLine();

    qint64 refUS = refLine.section(',', 0, 0).toLongLong();
    int refNS = refLine.section(',', 1, 1).toInt();
    // fprintf(stderr,"ref %lld %i\n",refUS,refNS);

    for (int i = 0; i < fileNames->count() - 1; i++) {
      qint64 csvUS = mLine[i].section(',', 0, 0).toLongLong();
      int csvNS = mLine[i].section(',', 1, 1).toInt();
      // fprintf(stderr,"m %lld %i\n",csvUS,csvNS);

      while (isGreater(refUS, csvUS, refNS, csvNS) == true) {
        mLinePrev[i] = mLine[i];
        if (mIn[i].atEnd() == true)
          break;
        mLine[i] = mIn[i].readLine();
        csvUS = mLine[i].section(',', 0, 0).toLongLong();
        csvNS = mLine[i].section(',', 1, 1).toInt();
        // fprintf(stderr,"m %lld %i\n",csvUS,csvNS);
      }
      csvUS = mLinePrev[i].section(',', 0, 0).toLongLong();
      csvNS = mLinePrev[i].section(',', 1, 1).toInt();
      // fprintf(stderr,"m ok %lld %i\n",csvUS,csvNS);

      refLine += "," + mLinePrev[i].section(',', 2);
    }

    out << refLine << "\n";
  }

  globalFile.close();
  refFile.close();
  for (int i = 0; i < fileNames->count() - 1; i++)
    mFile[i].close();
}

void LogFileUI::saveTxt(void) {
  // global txt file
  QString folderName = fileNames->at(0).section('/', 0, -2);
  QString fileName = folderName + "/all_logs.txt";
  QFile globalFile(fileName);
  if (!globalFile.open(QIODevice::WriteOnly | QIODevice::Text))
    fprintf(stderr,"LogFileUI::saveTxt: erreur ouverture ficher %s\n",fileName.toLocal8Bit().constData());
	
  QTextStream out(&globalFile);

  // reference txt file
  fileName = fileNames->at(csvCombo->currentIndex() - 1);
  fileName.replace(QString(".csv"), QString(".txt"));
  QFile refFile(fileName);
  if (!refFile.open(QIODevice::ReadOnly | QIODevice::Text))
    fprintf(stderr,"LogFileUI::saveTxt: erreur ouverture ficher %s\n",fileName.toLocal8Bit().constData());

  QTextStream refIn(&refFile);
  QString currentLine = refIn.readLine();
  int nbLines = 1;
  while (currentLine != NULL) {
    out << currentLine << "\n";
    ;
    currentLine = refIn.readLine();
    nbLines++;
  }

  // other txt files
  for (int i = 0; i < fileNames->count(); i++) {
    if (i == csvCombo->currentIndex() - 1)
      continue;
    fileName = fileNames->at(i);
    fileName.replace(QString(".csv"), QString(".txt"));
    QFile txtFile(fileName);
    if (!txtFile.open(QIODevice::ReadOnly | QIODevice::Text))
      fprintf(stderr,"LogFileUI::saveTxt: erreur ouverture ficher %s\n",fileName.toLocal8Bit().constData());
	  
    QTextStream txtIn(&txtFile);
    txtIn.readLine(); // time us
    txtIn.readLine(); // time ns
    currentLine = txtIn.readLine();
    while (currentLine != NULL) {
      out << nbLines << ":" << currentLine.section(':', 1) << "\n";
      currentLine = txtIn.readLine();
      nbLines++;
    }
    txtFile.close();
  }
  globalFile.close();
  refFile.close();
}

bool LogFileUI::isGreater(qint64 refUS, qint64 csvUS, int refNS, int csvNS) {
  if (refUS == csvUS) {
    if (refNS > csvNS) {
      return true;
    } else {
      return false;
    }
  }
  if (refUS > csvUS) {
    return true;
  } else {
    return false;
  }
}
