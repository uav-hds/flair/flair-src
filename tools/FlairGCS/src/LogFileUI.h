// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
#ifndef LOGFILEUI_H
#define LOGFILEUI_H

#include <QObject>

class QStringList;
class QDialog;
class QTextEdit;
class QPushButton;
class QComboBox;
class QCloseEvent;

class LogFileUI : public QObject {
  Q_OBJECT

public:
  LogFileUI(QString name);
  ~LogFileUI();
  void log(QString text);
  void addFile(QString filePath);
  void endOfFiles(void);
  QString getName(void) const;

private:
  QDialog *dialog;
  QStringList *fileNames;
  QTextEdit *logText, *inputText;
  QPushButton *okButton;
  QComboBox *csvCombo;
  void saveComment(void);
  void saveCsv(void);
  void saveTxt(void);
  void dbt2csv(QString file_path);
  bool isGreater(qint64 refUS, qint64 csvUS, int refNS, int csvNS);
  bool inputCleared;
  QString name;

private slots:
  void save(void);
  void clearInputText(void);

signals:
  void finished(void);
  void appendToLog(QString);
};

#endif // LOGFILEUI_H
