// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
#include "Manager.h"
#include "UdtSocket.h"
#include "ConnectionLayout.h"
#include "communication.h"
#include "LogFileUI.h"
#include "LogFileSocket.h"
#include <QDate>
#include <QFileDialog>
#include <QMessageBox>
#include <QPushButton>
#include <QTabBar>
#include <QTimer>
#include <QThread>
#include <QTextStream>
#include <QVBoxLayout>
#include <QModelIndex>
#include <QStatusBar>
#include <qendian.h>
#include <stdio.h>
#include <arpa/inet.h>

using namespace std;

Manager::Manager(QString name, int port) : QWidget() {
  qRegisterMetaType<QModelIndex>("QModelIndex"); // pour le file ui??
  this->name = name;
//fprintf(stderr,stderr,"Manager %x\n",thread());
  setWindowTitle(name);

  // manager layout
  managerLayout = new QVBoxLayout;
  setLayout(managerLayout);

  // tab bar for multiple connections
  tabBar = new QTabBar();
  //tabBar->setTabsClosable(true);
  managerLayout->addWidget(tabBar);
  connect(tabBar, SIGNAL(currentChanged(int)), this,
          SLOT(tabBarCurrentChanged(int)));
  currentTab = 0;

  // layout boutons
  button_layout = new QGridLayout();
  managerLayout->addLayout(button_layout);

  // boutons du button_Layout
  send_button = new QPushButton("apply all");
  reset_button = new QPushButton("reset all");
  load_button = new QPushButton("load all locally");
  save_button = new QPushButton("save all locally");
  saveGCSLogs_button= new QPushButton("save GCS data logs");
  button_layout->addWidget(send_button, 0, 0);
  button_layout->addWidget(reset_button, 0, 1);
  button_layout->addWidget(load_button, 0, 2);
  button_layout->addWidget(save_button, 0, 3);
  button_layout->addWidget(saveGCSLogs_button,1,0);

  connect(send_button, SIGNAL(clicked(bool)), this, SLOT(send()));
  connect(reset_button, SIGNAL(clicked(bool)), this, SLOT(reset()));
  connect(load_button, SIGNAL(clicked(bool)), this, SLOT(load()));
  connect(save_button, SIGNAL(clicked(bool)), this, SLOT(save()));
  connect(saveGCSLogs_button, SIGNAL(clicked(bool)), this, SLOT(saveGCSLogs()));

  status=new QStatusBar();
  status->setSizeGripEnabled(false);
  managerLayout->addWidget(status);

  UDT::startup();
  serv = UDT::socket(AF_INET, SOCK_DGRAM, 0);

  // for non blocking accept
  bool blocking = false;
  UDT::setsockopt(serv, 0, UDT_RCVSYN, &blocking, sizeof(bool));
//UDT::setsockopt(serv, 0, UDT_CC, new CCCFactory<CUDPBlast>, sizeof(CCCFactory<CUDPBlast>));
  sockaddr_in my_addr;
  my_addr.sin_family = AF_INET;
  my_addr.sin_port = htons(port);
  my_addr.sin_addr.s_addr = INADDR_ANY;
  memset(&(my_addr.sin_zero), '\0', 8);

  if (UDT::ERROR == UDT::bind(serv, (sockaddr *)&my_addr, sizeof(my_addr))) {
    fprintf(stderr,"bind error, %s\n", UDT::getlasterror().getErrorMessage());
  }

  if (UDT::ERROR == UDT::listen(serv, 1)) {
    fprintf(stderr,"listen error, %s\n", UDT::getlasterror().getErrorMessage());
  }

  
  icon_green = QIcon(":green.png");
  icon_red = QIcon(":red.png");
  icon_orange = QIcon(":orange.png");
  
  QTimer *timer = new QTimer(this);
  connect(timer, SIGNAL(timeout()), this, SLOT(acceptConnections()));
  timer->start(500);
  
  startTime=QTime::currentTime();
}

Manager::~Manager() {
  for (int i = 0; i < connections.count(); i++) {
    if(connections.at(i).socket!=NULL) {
      connections.at(i).socket->kill();
      connections.at(i).socket->thread()->wait();
    }
  }

  // delete main_layout;
  UDT::cleanup();
}

void Manager::acceptConnections(void) {
  UDTSOCKET socket;

  sockaddr_in their_addr;
  int namelen = sizeof(their_addr);

  if (UDT::INVALID_SOCK ==(socket = UDT::accept(serv, (sockaddr *)&their_addr, &namelen))) {
    if (UDT::getlasterror().getErrorCode() != 6002)
      fprintf(stderr,"accept error: %s, code %i\n", UDT::getlasterror().getErrorMessage(),UDT::getlasterror().getErrorCode());
    return;
  } else {
    QString ipPort=QString("%1:%2").arg(inet_ntoa(their_addr.sin_addr)).arg(their_addr.sin_port);
    fprintf(stderr,"connected to %s\n",ipPort.toLocal8Bit().constData());
    
    QThread *thread = new QThread(this);
    UdtSocket *udtSocket = new UdtSocket(socket,ipPort);
    udtSocket->moveToThread(thread);
    
    connect(udtSocket, SIGNAL(newLogFile(UDTSOCKET)), this, SLOT(newLogFile(UDTSOCKET)));
    connect(udtSocket, SIGNAL(newConnectionLayout(QString)), this, SLOT(newConnectionLayout(QString)),Qt::BlockingQueuedConnection);
    connect(thread, SIGNAL(started()), udtSocket, SLOT(receiveData()));

    thread->start();  
  }
}

void Manager::newConnectionLayout(QString name) {
  UdtSocket* udtSocket=(UdtSocket *)sender();
  
  ConnectionLayout *newLayout = new ConnectionLayout(udtSocket, name, startTime);
  connect(udtSocket, SIGNAL(UDTStats(QString,QString,bool)), newLayout, SIGNAL(UDTStats(QString,QString,bool)));//connection in 2 steps to get udtsocket as sender
  connect(newLayout, SIGNAL(UDTStats(QString,QString,bool)), this, SLOT(printUDTStats(QString,QString,bool)));
  connect(udtSocket, SIGNAL(dataReady(char *, int)), newLayout,SLOT(receive(char *, int)),Qt::BlockingQueuedConnection);
  connect(newLayout, SIGNAL(destroyed(QObject *)), this, SLOT(layoutDestroyed(QObject *)));
  connect(udtSocket, SIGNAL(destroyed(QObject *)), this, SLOT(udtSocketDestroyed(QObject *)));
  connect(udtSocket, SIGNAL(destroyed(QObject *)), newLayout, SLOT(udtSocketDestroyed(QObject *)));
  
  // widget
  QWidget *newWidget = new QWidget();
  newWidget->setLayout(newLayout->getQGridLayout());
  managerLayout->insertWidget(1, newWidget);
  newWidget->hide();
  
  connections_t connection;
  connection.layout=newLayout;
  connection.widget=newWidget;
  connection.socket=udtSocket;
  connections.append(connection);
  
  //tab: avoid having only 1 tab (0, 2 or more)
  if (connections.count() == 1) { // first connection
    newWidget->show();
    hiddenTabName = name;
  }
  if (connections.count() == 2) {
    if(connections[0].socket!=NULL) {
      tabBar->addTab(icon_green,hiddenTabName);
    } else {
      tabBar->addTab(icon_red,hiddenTabName);
    }
    currentTab = 0;
  }
  if (connections.count() > 1) {
    tabBar->addTab(icon_green,name);
  }
}

void Manager::udtSocketDestroyed(QObject *obj) {
  //mark socket as not valid, it can be a connection lost
  //in this case we keep the layout for reading, and mark it as problematic (socket=NULL)
  for(int i=0;i<connections.count();i++) {
    if(connections.at(i).socket==(UdtSocket *)obj) {
      connections[i].socket=NULL;
      tabBar->setTabIcon(i,icon_red);
      break;
    }
  }
}

void Manager::layoutDestroyed(QObject *obj) {
  //remove the connection, it comes from a proper close
  int index=-1;
  for(int i=0;i<connections.count();i++) {
    if(connections.at(i).layout==(ConnectionLayout *)obj) {
      delete connections.at(i).widget;
      //connections[i].widget=NULL;
      //connections[i].layout=NULL;
      connections.removeAt(i);
      index=i;
      break;
    }
  }
  
  if(index==-1) {
    fprintf(stderr,"layoutDestroyed: error, layout not found!\n");
    return;
  }
  
  //tab: avoid having only 1 tab (only 0, 2 or more)
  if (tabBar->count() > 1) {
    tabBar->removeTab(index);
  }

  if (connections.count() == 1) {
    hiddenTabName = tabBar->tabText(0);
    tabBar->removeTab(0);
  }

  if (connections.count() == 0) {
    status->showMessage("");
  }
}

void Manager::newLogFile(UDTSOCKET socket) {
  QThread *thread = new QThread(this);
  LogFileUI* logFileUI = new LogFileUI(name);
  LogFileSocket* logFileSocket = new LogFileSocket(logFileUI,socket);
  logFileSocket->moveToThread(thread);
  connect(thread, SIGNAL(started()), logFileSocket, SLOT(receive()));
  connect(logFileUI, SIGNAL(finished()), this, SLOT(deleteLogFileUI()));
  connect(logFileSocket, SIGNAL(finished()), this, SLOT(deleteLogFileSocket()));
  thread->start();
}

void Manager::deleteLogFileSocket(void) {
  sender()->thread()->quit();
  delete sender();
}

void Manager::deleteLogFileUI(void) {
  delete sender();
}

void Manager::printUDTStats(QString stats,QString stylesheet,bool loosingPackets) {
  int index = -1;
  for(int i=0;i<connections.count();i++) {
    if(connections.at(i).layout==(ConnectionLayout *)sender()) {
      index=i;
      break;
    }
  }
  
  if(index==-1) return;
  
  if(!loosingPackets) {
    tabBar->setTabIcon(index,icon_green);
  } else {
    tabBar->setTabIcon(index,icon_orange);
  }
  
  if (tabBar->count() == 0) {
    status->setStyleSheet(stylesheet);
    status->showMessage(stats);
  } else if (index==tabBar->currentIndex()) {
    status->setStyleSheet(stylesheet);
    status->showMessage(QString("%1: %2").arg(tabBar->tabText(index)).arg(stats));
  }
}

void Manager::tabBarCurrentChanged(int index) {
  
  if (index >= 0) {
    //if we are coming from layout destroyed
    if(currentTab<connections.count()) connections.at(currentTab).widget->hide();
    connections.at(index).widget->show();
    currentTab = index;
  } else {
    currentTab = 0;
    connections.at(0).widget->show();
  }
  QString msg="not connected";
  if (tabBar->count() == 0) {
    if(connections.at(0).socket!=NULL) msg=connections.at(0).socket->getUDTStats();
    saveGCSLogs_button->setText("save GCS data logs");
    
  } else {
    if(connections.at(index).socket!=NULL) msg=QString("%1: %2").arg(tabBar->tabText(index)).arg(connections.at(index).socket->getUDTStats());
    saveGCSLogs_button->setText(QString("save GCS data logs (%1)").arg(tabBar->tabText(index)));
  }
  status->showMessage(msg);
}

void Manager::load(void) {
  QString dir_name =
      QFileDialog::getExistingDirectory(this, "Select a directory", 0, 0);

  if (dir_name != "") {
    for (int i = 0; i < connections.count(); i++) {
      QFile *file;
      file = new QFile(dir_name + "/" +
                       connections.at(i).layout->getName() + ".xml");
      if (!file->open(QIODevice::ReadOnly | QIODevice::Text)) {
        QMessageBox::warning(this, "Warning",
                             "Enable to load " +
                                 connections.at(i).layout->getName() +
                                 ".xml");
        continue;
      }

      QDomDocument doc;
      QString errorMsg;
      int errorLine;
      int errorColumn;
      if (!doc.setContent(file, &errorMsg, &errorLine, &errorColumn)) {
        QMessageBox::critical(
            this, "Error",
            "unable to read " + connections.at(i).layout->getName() +
                ".xml" + " (" + errorMsg + " at " + QString::number(errorLine) +
                "," + QString::number(errorColumn) + ")");
      } else {
        connections.at(i).layout->LoadXml(&doc);
      }
      delete file;
    }
  }
}

void Manager::save(void) {
  bool isUptodate = true;

  for (int i = 0; i < connections.count(); i++) {
    if (!connections.at(i).layout->IsUptodate()) {
      isUptodate = false;
      break;
    }
  }

  if (!isUptodate) {
    QMessageBox msgBox;
    msgBox.setText("There are pending modifications");
    msgBox.setInformativeText("Apply and save?");
    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Yes);
    int ret = msgBox.exec();

    switch (ret) {
    case QMessageBox::Yes:
      send();
      break;
    case QMessageBox::Cancel:
      return;
      break;
    default:
      // should never be reached
      break;
    }
  }

  // create dirctory for storage
  QDateTime dateTime = QDateTime::currentDateTime();
  QString dir_name =
      "configs_" + dateTime.toString("yyyyMMdd_hhmm") + "_" + name;
  if (QDir().exists(dir_name) == true) {
    dir_name = "configs_" + dateTime.toString("yyyyMMdd_hhmm_ss") + "_" + name;
  }
  QDir().mkdir(dir_name);

  for (int i = 0; i < connections.count(); i++) {
    QDomDocument *xml = new QDomDocument("remote_ui_xml");

    connections.at(i).layout->GetFullXml((QDomElement *)xml);

    QFile fichier(dir_name + "/" + connections.at(i).layout->getName() +
                  ".xml");
    QString write_doc = (xml->ownerDocument()).toString();

    if (!fichier.open(QIODevice::WriteOnly)) {
      fichier.close();
      QMessageBox::critical(this, "Error", "Enable to write XML");
      continue;
    }
    QTextStream stream(&fichier);
    stream << write_doc;
    fichier.close();

    delete xml;
  }

  QMessageBox::information(this, "save all", "saved to ./" + dir_name);
}

void Manager::send(void) {
  for (int i = 0; i < connections.count(); i++) {
    if(connections.at(i).socket!=NULL) {
      QDomDocument doc("remote_ui_xml");
      connections.at(i).layout->GetUpdateXml((QDomElement *)&doc);
      // fprintf(stderr,"merge\n%s\n",doc.toString().toLocal8Bit().constData());

      connections.at(i).layout->XmlToSend(doc);
    }
  }
}

void Manager::reset() {
  for (int i = 0; i < connections.count(); i++)
    connections.at(i).layout->ResetAllChilds();
}

void Manager::saveGCSLogs() {
  if(connections.count()==0) return;
  if (tabBar->count() == 0) {
    connections.at(0).layout->saveGCSLogs();
  } else {
    connections.at(tabBar->currentIndex()).layout->saveGCSLogs();
  }
}