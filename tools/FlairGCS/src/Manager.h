// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
#ifndef MANAGER_H
#define MANAGER_H

#include <QWidget>
#include <QIcon>
#include <QTime>
#include <udt.h>
#include <qdom.h>

class UdtSocket;
class ConnectionLayout;
class QVBoxLayout;
class QTabBar;
class QPushButton;
class QGridLayout;
class QStatusBar;

class Manager : public QWidget {
  Q_OBJECT

public:
  Manager(QString name, int port);
  ~Manager();

private:
  UDTSOCKET serv;
  QVBoxLayout *managerLayout;
  typedef struct {
    ConnectionLayout *layout;
    QWidget *widget;
    UdtSocket *socket;
  } connections_t;
  QList<connections_t> connections;
  //QList<ConnectionLayout *> connectionsLayout;
  //QList<UdtSocket *> udtSockets;
  QTabBar *tabBar;
  QString name, hiddenTabName;
  int currentTab;

  QPushButton *send_button;
  QPushButton *reset_button;
  QPushButton *load_button;
  QPushButton *save_button;
  QPushButton *saveGCSLogs_button;
  QGridLayout *button_layout;
  QStatusBar *status;
  QIcon icon_red,icon_green,icon_orange;
  QTime startTime;

private slots:
  void acceptConnections(void);
  void load(void);
  void send(void);
  void save(void);
  void reset(void);
  void saveGCSLogs(void);
  void tabBarCurrentChanged(int index);
  void printUDTStats(QString stats,QString stylesheet,bool loosingPackets);
  void newLogFile(UDTSOCKET socket);
  void deleteLogFileUI(void);
  void deleteLogFileSocket(void);
  void newConnectionLayout(QString name);
  void udtSocketDestroyed(QObject *obj);
  void layoutDestroyed(QObject *obj);

protected:
};

#endif // MANAGER_H
