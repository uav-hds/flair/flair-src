// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
#include "Tab.h"
#include "TabWidget.h"
#include <QGridLayout>
#include <QScrollArea>

Tab::Tab(TabWidget *parent, QString name, int position)
    : Layout(new QWidget(), parent, name, "Tab") {
  parent_tab = parent;
  QWidget *onglet = getQGridLayout()->parentWidget();
  onglet->setObjectName(name);
  
  scroll = new QScrollArea;
  scroll->setWidget(onglet);
  scroll->setWidgetResizable(true);
  scroll->setFrameShape(QFrame::NoFrame);
        
  if (position == -1) {
    parent->tab->addTab(scroll, name);
  } else {
    parent->tab->insertTab(position, scroll, name);
  }
}

Tab::~Tab() { 
    parent_tab->tab->removeTab(parent_tab->tab->indexOf(scroll));
}
