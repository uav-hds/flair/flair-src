// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
#ifndef UDTSOCKET_H
#define UDTSOCKET_H

#include <udt.h>
#include <QObject>
#include <QTimer>

class file_ui;

class UdtSocket : public QObject {
  Q_OBJECT

public:
  UdtSocket(UDTSOCKET socket,QString ipPort);
  ~UdtSocket();
  void setName(QString name);
  QString getUDTStats();

private:
  UDTSOCKET socket;
  bool stop,destroySocket;
  QTimer *heartbeat_timer,*udtstats_timer;
  QString name,ipPort;
  static int uncompressBuffer(char *in, ssize_t in_size, char *out,ssize_t *out_size);
  enum SocketType { unknown, gui, log };
  SocketType socketType;
  unsigned int total_received;
  QString stats;
  unsigned int pktSndLossTotal;
  unsigned int lastpktRecvTotal;
 

signals:
  void dataReady(char *, int size);
  void newLogFile(UDTSOCKET socket);
  void newConnectionLayout(QString name);
  void UDTStats(QString stats,QString stylesheet,bool loosingPackets);

public slots:
  void receiveData(void);
  void kill(void);
  qint64 write(const char *buf, qint64 size,int ttl=-1,bool inOrder=true);
  
private slots:
  void heartbeat(void);
  void getUTDStats(void);
};

#endif // UDTSOCKET_H
