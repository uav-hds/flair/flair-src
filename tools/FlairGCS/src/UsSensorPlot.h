// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
#ifndef USSENSORPLOT_H_INCLUDED
#define USSENSORPLOT_H_INCLUDED

#include "DataRemote.h"
#include <stdint.h>
#include <QPen>

class Layout;
class QwtPlot;
class QwtPlotCurve;
class QwtPlotItem;
class QMouseEvent;
class QwtPlotMarker;
class QScrollBar;

class UsSensorPlot : public DataRemote {
  Q_OBJECT

public:
  UsSensorPlot(Layout *parent, int row, int col, QString plot_name,
             QString y_name, float ymin,float ymax, uint32_t nbSamples,QString datasType,bool enabled, uint16_t period);
  ~UsSensorPlot();

protected:
  void mousePressEvent(QMouseEvent *event);

private:
  QwtPlot *plot;
  void XmlEvent(QDomElement *dom);
  void alignScales(void);
  float xmin_orig, xmax_orig, ymin_orig, ymax_orig;
  QScrollBar* scrollbar;

  QwtPlotCurve *datas;
  QwtPlotMarker *firstPeakStart,*firstPeakEnd,*secondPeakStart,*secondPeakEnd;
  QString datasType;
  double *dx;
  
  struct measure_t {
    double *dy;
    uint16_t firstPeakStartIndex;
    uint16_t firstPeakEndIndex;
    uint16_t secondPeakStartIndex;
    uint16_t secondPeakEndIndex;
  };
  QList<measure_t> measures;
        
  
  uint32_t nbSamples;
  bool scrolling;

  bool eventFilter(QObject *, QEvent *);
  void setYAxisScale(float ymin, float ymax);
  void setXAxisScale(float xmin, float xmax);
  void BufEvent(char **buf, int *buf_size, uint16_t period,uint16_t nb_buffering,bool big_endian);
  void plotMeasure(int index);
  
 private slots:
    void scrollBarMoved(int value);
};

#endif // USSENSORPLOT_H_INCLUDED
