// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
#ifndef VECTOR2DSPINBOX_H
#define VECTOR2DSPINBOX_H

#include "XmlWidget.h"
#include <QDoubleSpinBox>

class Layout;
class QGroupBox;
class QGridLayout;

class Vector2DSpinBox : public XmlWidget {
  Q_OBJECT

public:
  // handle value as string, becouse double value are not exact
  Vector2DSpinBox(Layout *parent, int row, int col, QString name,
                  QString value[2], float min, float max, float step,
                  uint16_t decimals);
  ~Vector2DSpinBox();

private:
  QGridLayout *qgridlayout;
  QGroupBox *box;
  QDoubleSpinBox doublespinbox[2];
  QString doublespinbox_value[2];
  void AddElement(QString name, int index);
  void SetUptodate(void);
  void SetValues(QString value[2]);
  void Reset(void);
  void LoadEvent(QDomElement *dom);
  void ui_to_var(void);
  void ui_to_xml(void);
  bool eventFilter(QObject *o, QEvent *e);
  void adjust_decimals(QString value[2]);
  bool IsUptodate(void);

private slots:
  void valuechanged(const QString &value);
};

#endif // VECTOR2DSPINBOX_H
