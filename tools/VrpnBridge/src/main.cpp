//  created:    2018/03/13
//  filename:   main.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    vrpn bridge, to forward it to bth for exemple
//
//
/*********************************************************************/
#include <vrpn_Connection.h>
#include <vrpn_Forwarder.h>

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>

vrpn_Connection * server_connection;
vrpn_Connection * client_connection;
vrpn_StreamForwarder * forwarder;

void sighandler (int signal) {
  delete forwarder;
  delete server_connection;
  delete client_connection;
  exit(0);
}

void install_handler (void) {
  signal( SIGINT, sighandler );
  signal( SIGKILL, sighandler );
  signal( SIGTERM, sighandler );
  signal( SIGPIPE, sighandler );
}

int main (int argc, char ** argv) {

  int retval;

  //server_connection = vrpn_get_connection_by_name ("172.26.213.1:3883");
  server_connection = vrpn_get_connection_by_name ("127.0.0.1:3883");
  client_connection = vrpn_create_server_connection(3884);

  forwarder = new vrpn_StreamForwarder(server_connection, "target", client_connection, "target");

  retval = forwarder->forward("vrpn_Tracker Pos_Quat", "vrpn_Tracker Pos_Quat");
  if (retval)
    fprintf(stderr, "forwarder->forward (vrpn_Tracker Pos_Quat) failed.\n");
    
  retval = forwarder->forward("vrpn_Base ping_message", "vrpn_Base ping_message");
   if (retval)
    fprintf(stderr, "forwarder->forward (vrpn_Base ping_message) failed.\n");

  install_handler();

  while (1) {
    usleep(5000);
    server_connection->mainloop();
    client_connection->mainloop();
  }
}
