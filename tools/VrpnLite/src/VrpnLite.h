//  created:    2019/03/12
//  filename:   VrpnLite.h
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    vrpnlite, to forward it to bth for exemple
//              usefull to reduce vrpn frame size
//
/*********************************************************************/

#ifndef VRPNLITE_H
#define VRPNLITE_H

#include <string>
#include <Thread.h>

namespace flair {
  namespace core {
    class UdpSocket;
  }
  namespace gui {
    class PushButton;
  }
	namespace sensor {
		class VrpnClient;
		class VrpnObject;
	}
}

class VrpnLite: public flair::core::Thread {
  public:
      VrpnLite(int vrpnLitePort,std::string vrpnServerAddress);
      ~VrpnLite();

private:
      typedef struct {
          std::vector<flair::sensor::VrpnObject*> vrpnobjects;
          flair::core::Time lastAck;
          uint16_t srcId;
        } connection_t;
      flair::gui::PushButton* killButton;
      flair::sensor::VrpnClient* vrpnclient;
      std::vector<connection_t> connections;
      flair::core::UdpSocket* dataSocket;
      void Run(void);
      void SendObjects(void) ;
      int16_t ConvertPosition(float value) const;
      int16_t ConvertQuaternion(float value) const;
      connection_t* ConnectionOfSrcId(uint16_t srcId);  
};

#endif // VRPNLITE_H
