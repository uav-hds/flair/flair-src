//  created:    2019/03/12
//  filename:   main.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    vrpnlite, to forward it to bth for exemple
//              usefull to reduce vrpn frame size
//
/*********************************************************************/

#include "FrameworkManager.h"
#include "VrpnLite.h"
#include <stdio.h>
#include <tclap/CmdLine.h>

using namespace TCLAP;
using namespace std;
using namespace flair::core;

string xml_file;
string vrpnServerAddress;
int gcsPort,vrpnLitePort;
bool headless;

void parseOptions(int argc, char **argv);

int main (int argc, char ** argv) {
  parseOptions(argc,argv);
  
  FrameworkManager *manager;
  manager = new FrameworkManager("vrpnforwarder");
  if(!headless) manager->SetupConnection("127.0.0.1", gcsPort);
  manager->SetupUserInterface(xml_file);
  
  VrpnLite* vrpnlite=new VrpnLite(vrpnLitePort,vrpnServerAddress);
  
  vrpnlite->Start();
  vrpnlite->Join();

  delete manager;
}

void parseOptions(int argc, char **argv) {
  try {
    CmdLine cmd("Command description message", ' ', "0.1",false);

    ValueArg<int> vrpnLitePortArg("v", "vport","vrpn lite port", true,3884, "int");
    cmd.add(vrpnLitePortArg);
    
    ValueArg<string> serveraddressArg("s", "saddress","server address", true,"127.0.0.1:3883", "string");
    cmd.add(serveraddressArg);

    ValueArg<int> gcsPortArg("p", "port","local port used to connect to the ground station",false, 9000, "int");
    cmd.add(gcsPortArg);

    ValueArg<string> xmlArg("x", "xml", "xml file", true, "./settings.xml","string");
    cmd.add(xmlArg);
    
    ValueArg<bool> headlessArg("h", "headless", "headless mode", false, false,"bool");
    cmd.add(headlessArg);

    cmd.parse(argc, argv);

    vrpnLitePort = vrpnLitePortArg.getValue();
    vrpnServerAddress = serveraddressArg.getValue();
    gcsPort = gcsPortArg.getValue();
    xml_file = xmlArg.getValue();
    headless = headlessArg.getValue();

  } catch (ArgException &e) { // catch any exceptions
    cerr << "error: " << e.error() << " for arg " << e.argId() << endl;
  }
}
